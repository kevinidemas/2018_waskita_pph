function showSetting() {
    window.location = '../setting/view';
}

function showCalendar() {
    window.location = '../pph-calendar/index';
}

function createWP() {
    window.location = '../pph-berkas/pilih-bulan';
}

function generateBp() {
    window.location = '../pph-bukti-potong/generate-bp';
}

function removeBp() {
    var r = confirm("Anda yakin ingin menghapus seluruh Nomor Bukti Potong?");
    if (r == true) {
        window.location = '../pph-bukti-potong/remove-bp';
    }
}

function removeData() {
    var strvalue = "";
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue != "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });

    if (strvalue.length != 0) {
        var r = confirm("Anda yakin ingin menghapus data?");
        if (r == true) {
            document.getElementById('remove-data').click();
        }
    } else {
        alert('Silahkan pilih Data terlebih dahulu!');
    }
}

function settingUser() {
    var x = document.getElementById('bootstrap-duallistbox-selected-list_VatoutCustomizeHeader[list_header][]');
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue !== "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });
    window.location = 'index?sel-rows=' + btoa(strvalue);
}

function cetakBerkasWp() {
    var strvalue = "";
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue !== "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });
    window.location = 'index?sel-rows=' + btoa(strvalue);
}

function cetakBerkas() {
    var strvalue = "";
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue !== "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });
    window.location = 'index?sel-rows=' + btoa(strvalue);
}

function cetakBerkas4() {
    var strvalue = "";
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue !== "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });
    window.location = 'index-pasal-4?sel-rows=' + btoa(strvalue);
}

function cetakBerkas23() {
    var strvalue = "";
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue !== "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });
    window.location = 'index-pasal-23?sel-rows=' + btoa(strvalue);
}

function cetakBerkas21() {
    var strvalue = "";
    $('input[name="selection[]"]:checked').each(function () {
        if (strvalue !== "")
            strvalue = strvalue + "," + this.value;
        else
            strvalue = this.value;
    });
    window.location = 'index?sel-rows=' + btoa(strvalue);
}

function backToMainIndex() {
    window.location = '../index.php';
}

$(function () {
    $('#modalButton').click(function () {
        $('#modal').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
    });
});

$(document).ready(function () {
    $('#nomorBP').click(function () {
        $(this).hide();
    });
});

$(function () {
    if ($('div').is('.setting-form')) {
        $('.checkbox-pasal').change(function () {
            if (this.checked) {
                var pub = $("#wp-npwp").val();
                if (pub == 0) {
                    alert('Masukkan terlebih dahulu nomor NPWP Cabang');
                    $(".checkbox-pasal").prop("checked", false);
                }
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-form')) {
        $(document).ready(function () {
            $('input#pph-bruto').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-jasa-form')) {
        $(document).ready(function () {
            $('input#pph-bruto').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.master-pegawai-form')) {
        $(document).ready(function () {
            $('input#pegawai-gajiKotor').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.master-pegawai-form')) {
        $(document).ready(function () {
            $('input#pegawai-pph21').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.master-penghasilan-form')) {
        $(document).ready(function () {
            $('input#penghasilan-bruto').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.master-penghasilan-form')) {
        $(document).ready(function () {
            $('input#penghasilan-pph21').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-sewa-form')) {
        $(document).ready(function () {
            $('input#pph-bruto-4-sewa').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-perencana-form')) {
        $(document).ready(function () {
            $('input#pph-bruto-4-perencana').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-23-form')) {
        $(document).ready(function () {
            $('input#pph-bruto-23').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-iujk-form')) {
        $(document).ready(function () {
            $('input#keuangan').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bujpk-form')) {
        $(document).ready(function () {
            $('input#keuangan').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-form-update')) {
        $(document).ready(function () {
            $('input#pph-bruto').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-jasa-form-update')) {
        $(document).ready(function () {
            $('input#pph-bruto').keyup(function (event) {
                if (event.which >= 37 && event.which <= 40)
                    return;

                $(this).val(function (index, value) {
                    return value
                            .replace(/\D/g, "")
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                            ;
                });
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-iujk-form')) {
        $(document).ready(function () {
            $("#iujk-publish").change(function () {
                var pub = $("#iujk-publish").val();
                var day = pub.substr(0, 2);
                var month = pub.substr(3, 2);
                var year = parseInt(pub.substr(6)) + 1;
                var exp = day + "-" + month + "-" + year;
                document.getElementById('iujk-expire').value = exp;
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bujpk-form')) {
        $(document).ready(function () {
            $("#bujpk-publish").change(function () {
                var pub = $("#bujpk-publish").val();
                var day = pub.substr(0, 2);
                var month = pub.substr(3, 2);
                var year = parseInt(pub.substr(6)) + 1;
                var exp = day + "-" + month + "-" + year;
                document.getElementById('bujpk-expire').value = exp;
            });
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-form')) {
        $(document).ready(function () {
            document.getElementById("pph-bruto").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#pph-bruto").val();
                bruto = bruto.split('.').join("");
                var checkbox = document.getElementById('npwp');
                var result = '';
                if (checkbox.checked) {
                    result = (bruto * 3) / 100;
                } else {
                    result = (bruto * 1.5) / 100;
                }
                result = Math.floor(result);
                result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
                document.getElementById('pph-potong').value = result;
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-23-form')) {
        $(document).ready(function () {
            document.getElementById("pph-bruto-23").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#pph-bruto-23").val();
                bruto = bruto.split('.').join("");
                var checkbox = document.getElementById('npwp-23');
                var result = '';
                if (checkbox.checked) {
                    result = (bruto * 4) / 100;
                } else {
                    result = (bruto * 2) / 100;
                }
                result = Math.floor(result);
                result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
                document.getElementById('pph-potong-23').value = result;
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-sewa-form')) {
        $(document).ready(function () {
            document.getElementById("pph-bruto-4-sewa").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#pph-bruto-4-sewa").val();
                bruto = bruto.split('.').join("");
                result = (bruto * 10) / 100;
                result = Math.floor(result);
                result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
                document.getElementById('pph-potong-4-sewa').value = result;
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-form-update')) {
        $(document).ready(function () {
            document.getElementById("pph-bruto").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#pph-bruto").val();
                bruto = bruto.split('.').join("");
                var checkbox = document.getElementById('npwp');
                var result = '';
                if (checkbox.checked) {
                    result = (bruto * 3) / 100;
                    result = Math.floor(result);
                } else {
                    result = (bruto * 1.5) / 100;
                    result = Math.floor(result);
                }
                result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
                document.getElementById('pph-potong').value = result;
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-index')) {
        $(document).ready(function () {
            history.pushState(null, "", location.href.split("?")[0]);
        });
    }
});

$(function () {
    if ($('div').is('.master-penghasilan-index')) {
        $(document).ready(function () {
            history.pushState(null, "", location.href.split("?")[0]);
        });
    }
});

$(function () {
    if ($('div').is('.setting-form')) {
        $(document).ready(function () {
            var status22 = $('#status-npwp-22').val();
            var status23jasa = $('#status-npwp-23-jasa').val();
            var status23sewa = $('#status-npwp-23-sewa').val();
            var status4jasa = $('#status-npwp-4-jasa').val();
            var status4sewa = $('#status-npwp-4-sewa').val();
            if (status22 == 1) {
                $("#pasal-22").prop("checked", true);
            } else {
                $("#pasal-22").prop("checked", false);
            }
            if (status23jasa == 1) {
                $("#pasal-23-jasa").prop("checked", true);
            } else {
                $("#pasal-23-jasa").prop("checked", false);
            }
            if (status23sewa == 1) {
                $("#pasal-23-sewa").prop("checked", true);
            } else {
                $("#pasal-23-sewa").prop("checked", false);
            }
            if (status4jasa == 1) {
                $("#pasal-4-jasa").prop("checked", true);
            } else {
                $("#pasal-4-jasa").prop("checked", false);
            }
            if (status4sewa == 1) {
                $("#pasal-4-sewa").prop("checked", true);
            } else {
                $("#pasal-4-sewa").prop("checked", false);
            }
        });
    }
});


$(function () {
    if ($('div').is('.pph-bukti-potong-index')) {
        window.setInterval(function () {
            var status = $('#act-status').val();
            if (status == '') {
                $('#removeBtn').prop('disabled', false);
                $("[id=edit-bp]").fadeIn('fast');
                $("#create-new-bp").show();
            }
        }, 500);
    }
});

$(function () {
    if ($('div').is('.master-penghasilan-index')) {
        window.setInterval(function () {
            var status = $('#act-status').val();
            if (status == '') {
                $('#removeBtn').prop('disabled', false);
                $("[id=edit-bp]").fadeIn('fast');
            }
        }, 500);
    }
});

//$(function () {
//    if ($('div').is('.pph-bukti-potong-index')) {
//        $('.form-control').keydown(function (e) {
//            if (e.keyCode === 13) {
//                var status = $('#act-status').val();
//                if (status === '') {
//                    $('#removeBtn').prop('disabled', false);
//                    $("[id=edit-bp]").show();
//                }
//            }
//        });
//    }
//});

$(function () {
    if ($('div').is('.pph-bukti-potong-form-update')) {
        $(document).ready(function () {
            var wp = $('#pph-wp').val();
            if (wp.length === 0) {
                $("#npwp").prop("checked", true);
                $("#Show").show();
                $('div#wajib-pajak').hide();
                $("#date-picker").css({"margin-bottom": '20px', "float": 'right'});
                $("#form-satu").css({"margin-top": '-90px'});
            } else {
                $("#npwp").prop("checked", false);
                $("#Show").hide();
                $('div#wajib-pajak').show();
                $("#date-picker").css({"margin-bottom": '0px'});
                $("#form-satu").css({"margin-top": '0px'});
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-jasa-form-update')) {
        $(document).ready(function () {
            var wp = $('#pph-wp').val();
            if (wp.length === 0) {
                $("#npwp").prop("checked", true);
                $("#Show").show();
                $('div#wajib-pajak').hide();
                $("#date-picker").css({"margin-bottom": '20px', "float": 'right'});
                $("#form-satu").css({"margin-top": '-90px'});
            } else {
                $("#npwp").prop("checked", false);
                $("#Show").hide();
                $('div#wajib-pajak').show();
                $("#date-picker").css({"margin-bottom": '0px'});
                $("#form-satu").css({"margin-top": '0px'});
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-sewa-form')) {
        $('#btn-create-bp').prop('disabled', true);
        $("#pph-wp").change(function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-23-form')) {
        $('#btn-create-bp').prop('disabled', true);
        $("#pph-wp-23").change(function () {
            var wp = $('#pph-wp-23').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-form')) {
        $('#btn-create-bp').prop('disabled', true);
        $("#pph-wp").change(function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

//$(function () {
//    if ($('div').is('.pph-bukti-potong-4-jasa-form')) {
//        $('#btn-create-bp').prop('disabled', true);
//        $("#pph-wp").change(function () {
//            var wp = $('#pph-wp').val();
////            document.getElementById('wpIdButton').value = wp;
////            $('#wpIdButton').trigger('click');
//            if (wp.length > 0) {
//                $('#btn-create-bp').prop('disabled', false);
//                var as = $('.select2-selection__rendered').attr('title');
//                console.log(as);
//            } else {
//                $('#btn-create-bp').prop('disabled', true);
//            }
//        });
//    }
//});

//$(function () {
//    if ($('div').is('.pph-bukti-potong-4-jasa-form')) {
//        $(document).ready(function () {
//            var wpParam = (location.search.split(name + '=')[1] || '').split('&')[0];
//            $("#wpIdButton").on("click", function () {
//                if (wpParam > 0) {
//                    var wp = $('.select2-selection__rendered').attr('title');
//                    //                alert(a);
//                    $('.select2-selection__placeholder').html('asa');
////                document.getElementsByClassName('select2-selection__placeholder').value = 'a';
//                }
//                ;
//            });
//        });
//    }
//    ;
//});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-jasa-form')) {
        $(document).ready(function () {
            var wpParam = (location.search.split(name + '=')[1] || '').split('&')[0];
            history.pushState(null, "", location.href.split("?")[0]);
            if (wpParam.match(/[a-z]/i)) {
                var a = wpParam.replace(/[+]/g, " ");
                $('.select2-selection__placeholder').html(a);
                $('#btn-create-bp').prop('disabled', false);
            }
        });
    }
    ;
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-perencana-form')) {
        $(document).ready(function () {
            var wpParam = (location.search.split(name + '=')[1] || '').split('&')[0];
            history.pushState(null, "", location.href.split("?")[0]);
            if (wpParam.match(/[a-z]/i)) {
                var a = wpParam.replace(/[+]/g, " ");
                $('.select2-selection__placeholder').html(a);
                $('#btn-create-bp').prop('disabled', false);
            }
        });
    }
    ;
});

//$(function () {
//    if ($('div').is('.pph-bukti-potong-form')) {
//        $("#kev").change(function () {
////            $('#wpIdButton').trigger('click');
//              document.getElementById("wpIdButton").click();
//        });
//    }
//});

//$(function () {
/*  $(document).ready(function () {
 if ($('div').is('.pph-iujk-index')) {
 $("tr[data-key").each(function () {
 var status = $(this).find("td[data-col-seq=8]").html();
 var id = $(this).data("key");
 if (status != '-') {
 $("tr[data-key="+id+"]").css({"background-color": '#ebcccc', "color": '#ff0000', "font-weight": 'bold'});
 }
 });
 }
 });*/
//});

$(function () {
    if ($('div').is('.pph-bukti-potong-form-update')) {
        $("#pph-wp").change(function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
                var wp = $('.select2-selection__rendered').attr('title');
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-jasa-form-update')) {
        $("#pph-wp").change(function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
                var wp = $('.select2-selection__rendered').attr('title');
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-form')) {
        $(window).on('load', function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-4-jasa-form')) {
        $(window).on('load', function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-create')) {
        $(window).on('load', function () {
            var wp = $('#pph-wp').val();
            if (wp.length > 0) {
                $('#btn-create-bp').prop('disabled', false);
            } else {
                $('#btn-create-bp').prop('disabled', true);
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-wajib-pajak-create')) {
        var jwp = $('#pphwajibpajak-jeniswp').val();
        if (jwp == 2) {
            $("#modalButton").show();
            $("#wp-iujk").show();
            $("#wp-bujpk").show();
        } else {
            $("#modalButton").hide();
            $("#wp-iujk").hide();
            $("#wp-bujpk").hide();
        }
    }
});

$(function () {
    if ($('div').is('.pph-wajib-pajak-create')) {
        let searchParams = new URLSearchParams(window.location.search);
        if (searchParams.has('id')) {
            let param = searchParams.get('id');
            if (param != null) {
                $("#wp-status").hide();
            }
        }
        $("#modalButton").hide();
        $('#pphwajibpajak-jeniswp').change(function () {
            var jwp = $('#pphwajibpajak-jeniswp').val();
            if (jwp == 2) {
                $("#modalButton").show();
                $("#wp-iujk").show();
                $("#wp-status").hide();
            } else {
                $("#modalButton").hide();
                $("#wp-iujk").hide();
                $("#wp-status").show();
            }
        });
    }
});

$(function () {
    if ($('div').is('.pph-bukti-potong-index')) {
        $("#remove-data").hide();
        $("#create-new-bp").hide();
        var dataStatus = $('#data-count-status').val();
        if (dataStatus != '') {
            $('#btn-print-file').prop('disabled', false);
        } else {
            $('#btn-print-file').prop('disabled', true);
        }
        var status = $('#act-status').val();
        if (status == '') {
            $('#removeBtn').prop('disabled', false);
            $("[id=edit-bp]").show();
            $("#create-new-bp").show();
        }
        var choosen = $('#choosen-id').val();
        if (choosen.length > 0) {
            $('#modalButton').trigger('click');
        }
    }
});

$(function () {
    if ($('div').is('.master-penghasilan-index')) {
        $("#remove-data").hide();
        $("#create-new-bp").hide();
        var dataStatus = $('#data-count-status').val();
        if (dataStatus != '') {
            $('#btn-print-file').prop('disabled', false);
        } else {
            $('#btn-print-file').prop('disabled', true);
        }
        var status = $('#act-status').val();
        if (status == '') {
            $('#removeBtn').prop('disabled', false);
            $("[id=edit-bp]").show();
            $("#create-new-bp").show();
        }
        var choosen = $('#choosen-id').val();
        if (choosen.length > 0) {
            $('#modalButton').trigger('click');
        }
    }
});

$('#npwp').change(function () {
    if (this.checked) {
        $("#Show").toggle(this.checked);
        $('div#wajib-pajak').hide();
        $('#btn-create-bp').prop('disabled', false);
        $("#date-picker").css({"margin-bottom": '20px', "float": 'right'});
        $("#form-satu").css({"margin-top": '-90px'});
        var bruto = $("#pph-bruto").val();
        bruto = bruto.split('.').join("");
        result = (bruto * 3) / 100;
        result = Math.floor(result);
        result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
        result = result.replace(/\,/g, '.');
        document.getElementById('pph-potong').value = result;
    } else {
        if ($('.select2-selection__rendered').length) {
            var wp = $('.select2-selection__rendered').attr('title');
            if (wp === null || typeof (wp) === 'undefined') {
                $('#btn-create-bp').prop('disabled', true);
            } else {
                $('#btn-create-bp').prop('disabled', false);
            }
        }
        $("#Show").hide();
        $('div#wajib-pajak').show();
        $("#date-picker").css({"margin-bottom": '0px'});
        $("#form-satu").css({"margin-top": '0px'});
        var bruto = $("#pph-bruto").val();
        bruto = bruto.split('.').join("");
        result = (bruto * 1.5) / 100;
        result = Math.floor(result);
        result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
        result = result.replace(/\,/g, '.');
        document.getElementById('pph-potong').value = result;
    }
});

$('#npwp-23').change(function () {
    if (this.checked) {
        $("#Show").toggle(this.checked);
        $('div#wajib-pajak').hide();
        $('#btn-create-bp').prop('disabled', false);
        $("#date-picker").css({"margin-bottom": '20px', "float": 'right'});
        $("#form-satu").css({"margin-top": '-90px'});
        var bruto = $("#pph-bruto-23").val();
        bruto = bruto.split('.').join("");
        result = (bruto * 4) / 100;
        result = Math.floor(result);
        result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
        result = result.replace(/\,/g, '.');
        document.getElementById('pph-potong-23').value = result;
    } else {
        if ($('.select2-selection__rendered').length) {
            var wp = $('.select2-selection__rendered').attr('title');
            if (wp === null || typeof (wp) === 'undefined') {
                $('#btn-create-bp').prop('disabled', true);
            } else {
                $('#btn-create-bp').prop('disabled', false);
            }
        }
        $("#Show").hide();
        $('div#wajib-pajak').show();
        $("#date-picker").css({"margin-bottom": '0px'});
        $("#form-satu").css({"margin-top": '0px'});
        var bruto = $("#pph-bruto-23").val();
        bruto = bruto.split('.').join("");
        result = (bruto * 2) / 100;
        result = Math.floor(result);
        result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
        result = result.replace(/\,/g, '.');
        document.getElementById('pph-potong-23').value = result;
    }
});

$('#iujk-modal').change(function () {
    if (this.checked) {
        $("#create-iujk").toggle(this.checked);
    } else {
        $("#create-iujk").hide();
    }
});

$(function () {
    if ($('div').is('.pph-vendor-form')) {
        $("#create-iujk").hide();
    }
});

function formatAngka(angka) {
    if (typeof (angka) != "string")
        angka = angka.toString();
    var reg = new RegExp("([0-9]+)([0-9]{3})");
    while (reg.test(angka))
        angka = angka.replace(reg, "$1.$2");
    return angka;
}


/*fungsi mengambil PPN*/
function tambahBrutoPph() {
    var totalBruto = 0;
    var totalPph = 0;
    $('input[name="selection[]"]:checked').each(function () {
        if (this.checked) {
            var bruto = $(this).parent().parent().find("td[data-col-seq=7]").html();
            var pph = $(this).parent().parent().find("td[data-col-seq=8]").html();
            var parseKomaBruto = parseFloat(bruto.replace(/,/g, ""));
            var parseKomaPph = parseFloat(pph.replace(/,/g, ""));
            totalBruto = totalBruto + parseKomaBruto;
            totalPph = totalPph + parseKomaPph;
        }
    });
    totalBruto = formatAngka(totalBruto);
    totalPph = formatAngka(totalPph);
    $("#totalBrutoPph").html("Jumlah Bruto yang dipilih Rp. " + totalBruto + " & Jumlah PPh Dipotong Rp. " + totalPph);
}

$(window).scroll(scroolDivBrutoPph);

scroolDivBrutoPph();

function scroolDivBrutoPph() {
    var $cache = $("#totalBrutoPph");
    if ($(window).scrollTop() > 100) {
        $cache.css({
            "right": "5%",
            "position": "fixed",
            "z-index": "1000",
            "margin-top": "-50px",
        });
    } else {
        $cache.removeAttr("style");
        $cache.css({
            "margin-right": "5%",
            "background-color": "#ffb",
        });
    }
}
$(document).ajaxSuccess(function () {
    var totalBruto = 0;
    var totalPph = 0;

    $("#totalBrutoPph").html("Jumlah Bruto yang dipilih Rp. " + totalBruto + " & Jumlah PPh Dipotong Rp. " + totalPph);

    $("input[name='selection[]']").change(function () {
        tambahBrutoPph();
    })

    $("input[name='selection_all']").change(function () {
        tambahBrutoPph();
    })
});

$("input[name='selection[]']").change(function () {
    tambahBrutoPph();
})

$("input[name='selection_all']").change(function () {
    tambahBrutoPph();
})

$(function () {
    if ($('div').is('.master-penghasilan-form')) {
        var pegawai = $("#pilih-pegawai").val();
        if (pegawai == 0) {
            $('#btn-penghasilan-submit').prop('disabled', true);
            alert('Tidak ada Pegawai terdaftar');
        }
    }
});

//$("td").click(function (e) {
//    var id = $(this).closest("tr").data("key");
//    if (e.target == this)
//        var checkBoxEl = $(this).parent().find("input");
//    var ischecked = checkBoxEl.is(":checked");
//
//    if (ischecked) {
//        $(this).parent().find("input").prop("checked", false);
//        tambahPPN();
//    } else {
//        $(this).parent().find("input").prop("checked", true);
//        tambahPPN();
//    }
//});

//$("td").click(function (e) {
//    var id = $(this).closest("tr").data("key");
//    if (e.target == this)
////    var checkBoxEl = $(this).parent().find("input").val();
//    var checkBoxEl = $(this).parent().find("input");
////    alert(checkBoxEl);
//console.dir(checkBoxEl);
////    alert(Object.prototype.toString.call([checkBoxEl]));
//    var ischecked = checkBoxEl.is(":checked");
//
//    if (ischecked) {
//        $(this).parent().find("input").prop("checked", false);
//        tambahPPN();
//    } else {
//        $(this).parent().find("input").prop("checked", true);
//        tambahPPN();
//    }
//});
//
//function clickMeCheckbox(element, id) {
//    var checkBoxEl = $(element).find("input");
//    var ischecked = checkBoxEl.is(":checked");
//    if (ischecked) {
//        alert('1');
//        $(element).find("input").prop("checked", false);
//        tambahPPN();
//    } else {
//        $(element).find("input").prop("checked", true);
//        tambahPPN();
//    }
//}


//var parentEl = $(this).parent().parent().find("td[data-col-seq=6]").find("input").val();
//var bruto = $(this).parent().parent().find("td[data-col-seq=7]").html();
//
//var checkBoxEl = $(this).parent().html();
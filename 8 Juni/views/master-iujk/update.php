<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterIujk */

$this->title = 'Update Master Iujk: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Master Iujks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iujkId, 'url' => ['view', 'id' => $model->iujkId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-iujk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

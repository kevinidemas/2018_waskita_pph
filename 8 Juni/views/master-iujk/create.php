<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterIujk */

$this->title = 'Create Master Iujk';
$this->params['breadcrumbs'][] = ['label' => 'Master Iujks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-iujk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Options */

$this->title = 'Ubah Options';
//$this->params['breadcrumbs'][] = ['label' => 'Options', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="options-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

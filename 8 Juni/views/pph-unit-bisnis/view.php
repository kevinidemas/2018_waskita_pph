<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphUnitBisnis */

$this->title = $model->unitBisnisId;
$this->params['breadcrumbs'][] = ['label' => 'Pph Unit Bisnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-unit-bisnis-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->unitBisnisId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->unitBisnisId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'unitBisnisId',
            'nama',
            'userId',
            'parent',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphUnitBisnisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Unit Bisnis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-unit-bisnis-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Unit Bisnis', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'unitBisnisId',
            'nama',
            'userId',
            'parent',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

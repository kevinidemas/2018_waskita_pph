<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphUnitBisnis */

$this->title = 'Update Pph Unit Bisnis: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Unit Bisnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->unitBisnisId, 'url' => ['view', 'id' => $model->unitBisnisId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-unit-bisnis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphWajibPajak */

$this->title = 'Ubah Wajib Pajak: '.$model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pph Wajib Pajaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wajibPajakId, 'url' => ['view', 'id' => $model->wajibPajakId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-wajib-pajak-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
//        'id' => $id
    ]) ?>

</div>

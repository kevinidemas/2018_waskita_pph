<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use app\models\PphIujk;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$dataIujk = PphIujk::find()->where(['created_by' => Yii::$app->user->id, 'wajibPajakId' => null])->asArray()->all();
$arrayIujkId = ArrayHelper::getColumn($dataIujk, 'iujkId');

if ($arrayIujkId != null) {
    for ($n = 0; $n < count($arrayIujkId); $n++) {
        $mn = $arrayIujkId[$n];
        if (isset($arrayIujkId[$n])) {
            $modelIujk = PphIujk::find()->where(['iujkId' => $arrayIujkId[$n]])->one();
            $iujkOptions[$mn] = $modelIujk->nomorIujk;
        }
    }
} else {
    $iujkOptions[] = 'KOSONG';
}


?>

<div class="pph-vendor-form">
    <?php
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'iujkId' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $iujkOptions,
                        'options' => [
                            'id' => 'wp-iujk',
                        ],
                    ],
                ]
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
                            'class' => 'btn btn-default',
                        ]) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
                            'class' => 'btn btn-primary',
                        ]) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusNpwp */

$this->title = 'Update Pph Status Npwp: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Npwps', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->npwpId, 'url' => ['view', 'id' => $model->npwpId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-status-npwp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

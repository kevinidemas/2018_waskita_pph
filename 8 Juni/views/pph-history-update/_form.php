<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PphHistoryUpdate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-history-update-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomorPembukuan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal')->textInput() ?>

    <?= $form->field($model, 'jumlahBruto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlahPphDiPotong')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'potonganPegawai')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'potonganMaterial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nilaiTagihan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dpp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pasalId')->textInput() ?>

    <?= $form->field($model, 'wajibPajakId')->textInput() ?>

    <?= $form->field($model, 'wajibPajakNonId')->textInput() ?>

    <?= $form->field($model, 'statusWpId')->textInput() ?>

    <?= $form->field($model, 'statusNpwpId')->textInput() ?>

    <?= $form->field($model, 'statusKerjaId')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'parentId')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lokasi_tanah')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tarif')->textInput() ?>

    <?= $form->field($model, 'userIndukId')->textInput() ?>

    <?= $form->field($model, 'pembetulanId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

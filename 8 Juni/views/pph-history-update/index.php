<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\User;
use app\models\PphPasal;
use app\models\PphWajibPajak;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphHistoryUpdateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Update History';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-history-update-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'nomorPembukuan',
                'value' => function ($data) {
                    $noPembukuan = \Yii::t('app', $data->nomorPembukuan);
                    return $noPembukuan;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],                   
            [
                'attribute' => 'tanggal',
                'value' => function ($data) {
                    $tanggal = \Yii::t('app', $data->tanggal);
                    return $tanggal;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],           
            [
                'attribute' => 'jumlahBruto',
                'value' => function ($data) {
                    $jumBruto = \Yii::t('app', $data->jumlahBruto);
                    return $jumBruto;
                },
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],           
            [
                'attribute' => 'jumlahPphDiPotong',
                'value' => function ($data) {
                    $jumPot = \Yii::t('app', $data->jumlahBruto);
                    return $jumPot;
                },
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],           
//            [
//                'attribute' => 'pasalId',
//                'value' => function ($data) {
//                    $modelPasal = PphPasal::find()->where(['id' => $data->pasalId])->one();
//                    $nama = $modelPasal->nama;
//                    return ucfirst($nama);
//                },
//                'hAlign' => 'center',
//                'options' => [
////                    'width' => '10px',
//                ],
//                'mergeHeader' => true
//            ],           
//            [
//                'attribute' => 'wajibPajakId',
//                'value' => function ($data) {
//                    $modelWp = PphWajibPajak::find()->where(['id' => $data->wajibPajakId])->one();
//                    $nama = $modelWp->nama;
//                    return ucfirst($nama);
//                },
//                'hAlign' => 'center',
//                'options' => [
////                    'width' => '10px',
//                ],
//                'mergeHeader' => true
//            ],           
            [
                'attribute' => 'pembetulanCount',
                'header' => 'Pembetulan',
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],          

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

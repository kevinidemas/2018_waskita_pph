<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PphHistoryUpdateSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-history-update-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'historyUpId') ?>

    <?= $form->field($model, 'nomorPembukuan') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'jumlahBruto') ?>

    <?= $form->field($model, 'jumlahPphDiPotong') ?>

    <?php // echo $form->field($model, 'potonganPegawai') ?>

    <?php // echo $form->field($model, 'potonganMaterial') ?>

    <?php // echo $form->field($model, 'nilaiTagihan') ?>

    <?php // echo $form->field($model, 'dpp') ?>

    <?php // echo $form->field($model, 'pasalId') ?>

    <?php // echo $form->field($model, 'wajibPajakId') ?>

    <?php // echo $form->field($model, 'wajibPajakNonId') ?>

    <?php // echo $form->field($model, 'statusWpId') ?>

    <?php // echo $form->field($model, 'statusNpwpId') ?>

    <?php // echo $form->field($model, 'statusKerjaId') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'parentId') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'nama') ?>

    <?php // echo $form->field($model, 'alamat') ?>

    <?php // echo $form->field($model, 'lokasi_tanah') ?>

    <?php // echo $form->field($model, 'tarif') ?>

    <?php // echo $form->field($model, 'userIndukId') ?>

    <?php // echo $form->field($model, 'pembetulanId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

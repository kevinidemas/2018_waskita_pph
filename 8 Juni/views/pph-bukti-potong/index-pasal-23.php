<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\alert\Alert;
use kartik\grid\GridView;
use app\models\PphTarif;
use app\models\User;
use app\models\PphStatusNpwp;
use app\models\PphWajibPajak;
use app\models\PphBuktiPotong;
use app\models\PphIujk;
use app\models\PphPasal;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\db\ActiveRecord;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphBuktiPotongSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bukti Potong Pasal 23';
//$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
if (isset($session['notification-success-download'])) {
    $msgSuccess = $session['notification-success-download'];
    unset($session['notification-success-download']);
    echo Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'title' => 'Berhasil!',
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => $msgSuccess,
        'showSeparator' => true,
        'delay' => 3500
    ]);
} else if (isset($session['delete-bp-failed'])) {
    $msgDeleteFailed = $session['delete-bp-failed'];
    unset($session['delete-bp-failed']);
    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'title' => 'Gagal!',
        'icon' => 'glyphicon glyphicon-remove-sign',
        'body' => $msgDeleteFailed,
        'showSeparator' => true,
        'delay' => 5500
    ]);
} else if (isset($session['update-bp-failed'])) {
    $msgUpdateFailed = $session['update-bp-failed'];
    unset($session['update-bp-failed']);
    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'title' => 'Gagal!',
        'icon' => 'glyphicon glyphicon-remove-sign',
        'body' => $msgUpdateFailed,
        'showSeparator' => true,
        'delay' => 5500
    ]);
} else if (isset($session['notification-failed-generate'])) {
    $$msgGenerateFailed = $session['notification-failed-generate'];
    unset($session['notification-failed-generate']);
    echo Alert::widget([
        'type' => Alert::TYPE_INFO,
        'title' => 'Pemberitahuan!',
        'icon' => 'glyphicon glyphicon-info-sign',
        'body' => $msgGenerateFailed,
        'showSeparator' => true,
        'delay' => 3500
    ]);
}

if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}

if (Yii::$app->session->hasFlash('info')) {
    $msgInfo = Yii::$app->session->getFlash('error');
    $alertInfo = '<div class="alert alert-danger">';
    if (!is_array($msgInfo)) {
        $alertInfo .= $msgInfo;
        $alertInfo .= '</div>';
    } else {
        foreach ($msgInfo as $value) {
            $alertInfo .= $value . '<br/>';
        }

        $alertInfo .= '</div>';
    }
}
?>

<?php
if ($session['expiredDate'] == 0 && $arrayCrNoBp == true) {
    ?>
    <style type="text/css">
        #act-button{
            display:none;
        };
    </style>
    <?php
}
?>

<?php
if ($arrayRmNoBp == true) {
    ?>
    <style type="text/css">
        #act-button-rm{
            display:none;
        };
    </style>
    <?php
}
?>

<?php
if ($session['expiredDate'] == 0) {
    ?>
    <style type="text/css">
        #act-button-rm{
            display:none;
        };
    </style>
    <style type="text/css">
        #act-button{
            display:none;
        };
    </style>
<?php }
?>
<?php
if (Yii::$app->user->id != 1) {
    ?>
    <style type="text/css">
        #act-button{
            display:none;
        };
    </style>
    <?php
}
?>
<div class="col-md-12">
    <p id="totalBrutoPph" style="background-color: #ffb;", class="pull-right">Kalkulasi Bruto yang dipilih Rp. 0 & Jumlah PPh Dipotong Rp. 0</p>
</div>
<div class = "pph-bukti-potong-index">
    <h1><?= Html::encode($this->title)
?></h1>
    <?php
    $fieldValue = '';
    $actStatus = null;
    if ($session['expiredDate'] == 1) {
        $actStatus = 'not-active';
    }
    $arrayRows = [];
    if (isset($_GET['sel-rows'])) {
        if (!$_GET['sel-rows'] == null) {
            $session = Yii::$app->session;
            $rows = base64_decode($_GET['sel-rows']);
            $arrayRows = explode(",", $rows);
            $jsonArray = json_encode($rows);
            $session['sel-rows'] = $jsonArray;
        }
        $fieldValue = 'ada';
    }

    if (isset($session['notification-success-download'])) {
        unset($session['notification-success-download']);
        $fieldValue = null;
    }

    if (isset($session['dataCount-pasal-23'])) {
        unset($session['dataCount-pasal-23']);
        $dataCountStatus = 'Ada';
    } else {
        $dataCountStatus = null;
    }
    ?>
    <div style="visibility: hidden;">
        <input type="hidden" id="choosen-id" name="choosen-id" value=<?php echo $fieldValue ?>> 
        <input type="hidden" id="act-status" name="act-status" value=<?php echo $actStatus ?>>
        <input type="hidden" id="data-count-status" name="data-count-status" value=<?php echo $dataCountStatus ?>>
    </div>
    <p style="margin-bottom: -38px">
        <!-- Html::a('<i class="glyphicon glyphicon-plus"></i> Bukti Potong Baru', ['create'], ['class' => 'btn btn-success', 'id' => 'create-new-bp']) -->
        <button type="button" onclick="generateBp()" class="btn btn-generate" id="act-button"><i class="glyphicon glyphicon-cog"></i> Generate No BP</button>
        <button type="button" onclick="removeBp()" class="btn btn-remove" id="act-button-rm"><i class="glyphicon glyphicon-trash"></i> Hapus No BP</button>
        <button type="button" onclick="cetakBerkas23()" class="btn btn-choosen" id="btn-print-file"><i class="glyphicon glyphicon-print"></i> Cetak Berkas</button>
        <?= Html::button('<i class="glyphicon glyphicon-print"></i> Cetak Berkas', ['value' => Url::to('../pph-berkas/pilih-bulan'), 'class' => 'btn btn-pilih-berkas', 'id' => 'modalButton']) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Pilih Masa Pajak dan Jenis Berkas</h4>',
        'id' => 'modal',
        'size' => 'modal-lg'
    ]);

    echo "<div id='modalContent'></div>";

    Modal::end();
    ?>

    <?= Html::beginForm(['pph-bukti-potong/remove-data'], 'post'); ?>
    <?php $removeBtn = '<button type="button" disabled onclick="removeData()" id="removeBtn" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>'; ?>
    <div style="visibility: hidden;">
        <button type="submit" class="btn btn-danger" id="remove-data"><i class="glyphicon glyphicon-trash"></i></button>
    </div>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) use ($arrayRows) {
                    if (isset($_GET['sel-rows'])) {
                        $bool = in_array($model->buktiPotongId, $arrayRows);
                        return ['checked' => $bool];
                    }
                }
            ],
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'nomorPembukuan',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'wajibPajakId',
                'label' => 'NPWP',
                'value' => function ($model) {
                    if ($model->wajibPajakId != 0) {
                        $npwp = $model->getNpwp($model->wajibPajakId);
                        $result = substr($npwp, 0, 2) . '.' . substr($npwp, 2, 3) . '.' . substr($npwp, 5, 3) . '.' . substr($npwp, 8, 1) . '-' . substr($npwp, 9, 3) . '.' . substr($npwp, 12, 3);
                        return $result;
                    } else {
                        return $npwp = '00.000.000.0-000.000';
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '90px',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'wajibPajakId',
                'label' => 'Nama',
                'format' => 'raw',
//                'value' => 'wajibPajakNama',
                'value' => function ($model) {
                    if ($model->wajibPajakId != 0) {
                        return $model->getNama($model->wajibPajakId);
                    } elseif ($model->nama != null) {
                        return $model->nama;
                    } else {
                        return 'Kosong';
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '220px',
//                'mergeHeader' => true
            ],
            [
                'attribute' => 'nomorBuktiPotong',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
//                'mergeHeader' => true
            ],
            [
                'attribute' => 'tanggal',
                'format' => 'raw',
                'value' => function ($data) {
                    $tanggal = date('d ', strtotime($data->tanggal)) . \Yii::t('app', date('n', strtotime($data->tanggal))) . date(' Y', strtotime($data->tanggal));
                    return $tanggal;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '85px',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'jumlahBruto',
                'format' => ['decimal', 0],
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'jumlahPphDiPotong',
                'label' => 'Jumlah Potong',
                'format' => ['decimal', 0],
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true
            ],
//            [
//                'attribute' => 'pasalId',
//                'label' => 'Pasal',
//                'value' => function ($data) {
//                    $modelPasal = PphPasal::find()->where(['pasalId' => $data->pasalId])->one();
//                    $pasal = $modelPasal->nama;
//                    return $pasal;
//                },
//                'vAlign' => 'middle',
//                'hAlign' => 'center',
//                'mergeHeader' => true
//            ],
            [
                'attribute' => 'jenisBuktiPotong',
                'label' => 'Jenis',
                'value' => function ($data) {
                    if ($data->statusWpId == 3) {
                        return 'Jasa';
                    } else if ($data->statusWpId == 2) {
                        return 'Sewa';
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'Tarif',
                'label' => 'Tarif',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelWaPa = PphWajibPajak::find()->where(['wajibPajakId' => $data->wajibPajakId])->one();
                    $wpId = $data->wajibPajakId;
                    if ($wpId != null) {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 10])->one();
                        $tarif = $modelTarif->tarif . '%';
                        return $tarif;
                    } else {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 11])->one();
                        $tarif = $modelTarif->tarif . '%';
                        return $tarif;
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'created_by',
//                'label' => 'Created By',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = User::find()->where(['id' => $data->created_by])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '80px',
//                'mergeHeader' => true
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '10', 'align' => 'center'],
                'template' => '{edit}',
                'buttons' => [
                    'edit' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-edit-npwp" id="edit-bp" style="display: none"><i class="glyphicon glyphicon-edit"></i></button>', $url, [
                                    'title' => Yii::t('app', 'Edit'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                                    'id' => 'editBtn'
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    $url = Url::toRoute(['update-pasal-23', 'id' => $model->buktiPotongId]);
                    return $url;
                }
            ],
        ],
        'toolbar' => [
            ['content' =>
                $removeBtn . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index-pasal-23'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
            ],
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
            'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR BUKTI POTONG</b>',
//            'before' => $tombol,
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]
    ]);
    ?>
    <?= Html::endForm(); ?> 
</div>

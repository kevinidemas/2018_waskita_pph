<?php

use yii\helpers\Html;
use kartik\growl\Growl;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$this->title = 'Bukti Potong Pasal 21 - Honorer ';
//$this->params['breadcrumbs'][] = ['label' => 'Bukti Potong', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;

$session = Yii::$app->session;
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');    
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}

?>


<div class="pph-bukti-potong-create" id="form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php

if (isset($session['iujkExpiredArr'])) {
    $errIujkJson = $session['iujkExpiredArr'];
    $errIujk = json_decode($errIujkJson);    
    unset($session['iujkExpiredArr']);
        for($en = 0; $en < count($errIujk); $en++){
            Growl::widget([
                'type' => Growl::TYPE_INFO,
                'title' => 'Peringatan IUJK!',
                'icon' => 'glyphicon glyphicon-info-sign',
                'body' => $errIujk[$en],
                'showSeparator' => true,
                'delay' => 1000,
                'pluginOptions' => [
                    //'showProgressbar' => true,
                    'placement' => [
                        'from' => 'top',
                        'align' => 'right',
                    ]
                ]
            ]);
        }
    }

if (isset($session['bujpkExpiredArr'])) {
    $errBujpkJson = $session['bujpkExpiredArr'];
    $errBujpk = json_decode($errBujpkJson);
    unset($session['bujpkExpiredArr']);
        for($em = 0; $em < count($errBujpk); $em++){
            Growl::widget([
                'type' => Growl::TYPE_INFO,
                'title' => 'Peringatan BUJPK!',
                'icon' => 'glyphicon glyphicon-info-sign',
                'body' => $errBujpk[$em],
                'showSeparator' => true,
                'delay' => 1000,
                'pluginOptions' => [
                    //'showProgressbar' => true,
                    'placement' => [
                        'from' => 'top',
                        'align' => 'right',
                    ]
                ]
            ]);
        }
    }

    echo $this->render('_form-pasal-21-honorer', [
        'model' => $model,
        'modelWp' => $modelWp,
    ])
    ?>

</div>

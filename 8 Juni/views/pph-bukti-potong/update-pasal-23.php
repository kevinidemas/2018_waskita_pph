<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$this->title = 'Ubah Bukti Potong: ' . $model->nomorBuktiPotong;
$this->params['breadcrumbs'][] = ['label' => 'Pph Bukti Potongs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->buktiPotongId, 'url' => ['view', 'id' => $model->buktiPotongId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-bukti-potong-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?=
    $this->render('_formUpdate-pasal-23', [
        'model' => $model,
        'modelWp' => $modelWp,
    ])
    ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphJenisBisnis */

$this->title = 'Update Pph Jenis Bisnis: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Bisnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenisBisnisId, 'url' => ['view', 'id' => $model->jenisBisnisId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-jenis-bisnis-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

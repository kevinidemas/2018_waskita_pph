<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphNomorBuktiPotongSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Nomor Bukti Potongs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-nomor-bukti-potong-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Nomor Bukti Potong', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nomorBuktiPotongId',
            'tahun',
            'bulan',
            'lastNomorBP',
            'prevLastNomorBP',
            // 'userId',
            // 'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphTarif */

$this->title = 'Update Pph Tarif: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Tarifs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tarifId, 'url' => ['view', 'id' => $model->tarifId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-tarif-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

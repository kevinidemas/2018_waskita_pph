<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphJenisWajibPajak */

$this->title = 'Update Pph Jenis Wajib Pajak: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Wajib Pajaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenisWajibPajakId, 'url' => ['view', 'id' => $model->jenisWajibPajakId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-jenis-wajib-pajak-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

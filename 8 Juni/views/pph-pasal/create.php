<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphPasal */

$this->title = 'Create Pph Pasal';
$this->params['breadcrumbs'][] = ['label' => 'Pph Pasals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-pasal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

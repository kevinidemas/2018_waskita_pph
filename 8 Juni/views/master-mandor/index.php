<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterMandorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Mandors';

if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    if (!is_array($msgErrors)) {
        $alertBootstrap = '<div class="alert-single alert-danger">';
    } else {
        $alertBootstrap = '<div class="alert alert-danger">';
    }
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
        echo $alertBootstrap;
}

$session = Yii::$app->session;
$fieldValue = '';
$arrayRows = [];
if (isset($_GET['sel-rows'])) {
    if (!$_GET['sel-rows'] == null) {
        $session = Yii::$app->session;
        $rows = base64_decode($_GET['sel-rows']);
        $arrayRows = explode(",", $rows);
        $jsonArray = json_encode($rows);
        $session['sel-rows'] = $jsonArray;
    }
    $fieldValue = 'ada';
}

$jenisWp = 2;
$session['jenis-wajib-pajak'] = $jenisWp;
?>
<div class="master-mandor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Mandor Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <div style="visibility: hidden;">
        <input type="hidden" id="choosen-id" name="choosen-id" value=<?php echo $fieldValue ?>> 
    </div>
    <p style="margin-bottom: -38px">
        <button type="button" onclick="cetakBerkasMandor()" class="btn btn-choosen" id="btn-print-file"><i class=" glyphicon glyphicon-cloud-download"></i> Download</button>
        <?= Html::button('<i class="glyphicon glyphicon-print"></i> Download', ['value' => Url::to('../pph-berkas/pilih-data'), 'class' => 'btn btn-pilih-berkas', 'id' => 'modalButton']) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Pilih Masa Pajak dan Jenis Berkas</h4>',
        'id' => 'modal',
        'size' => 'modal-lg'
    ]);

    echo "<div id='modalContent'></div>";

    Modal::end();
    ?>

    <?= 
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) use ($arrayRows) {
                    if (isset($_GET['sel-rows'])) {
                        $bool = in_array($model->mandorId, $arrayRows);
                        return ['checked' => $bool];
                    }
                }
                    ],
                    ['class' => 'kartik\grid\SerialColumn',
                        'header' => 'No',
                    ],
                    [
                        'attribute' => 'nama',
                        'options' => [
                            'width' => 275
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'npwp',
                        'options' => [
                            'width' => 100
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'alamat',
                        'options' => [
                            'width' => 367
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'posisi',
                        'options' => [
                            'width' => 200
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'created_by',
                        'header' => 'Created By',
                        'value' => function ($data) {
                            $modelUser = User::find()->where(['id' => $data->created_by])->one();
                            $nama = $modelUser->username;
                            return ucfirst($nama);
                        },
                                'options' => [
                                    'width' => 80
                                ],
                                'vAlign' => 'middle',
                                'hAlign' => 'center',
                    ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

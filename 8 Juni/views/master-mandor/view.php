<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterMandor */

$this->title = $model->mandorId;
$this->params['breadcrumbs'][] = ['label' => 'Master Mandors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-mandor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->mandorId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->mandorId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'mandorId',
            'nama',
            'posisi',
            'nik',
            'npwp',
            'alamat',
            'jenisKelamin',
            'statusKerjaId',
            'hp',
            'email',
            'buktiNpwp',
            'buktiNik',
            'created_at',
            'created_by',
            'updated_at',
            'updated_by',
        ],
    ]) ?>

</div>

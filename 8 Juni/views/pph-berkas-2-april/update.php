<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphBerkas */

$this->title = 'Update Pph Berkas: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Berkas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->berkasId, 'url' => ['view', 'id' => $model->berkasId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-berkas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

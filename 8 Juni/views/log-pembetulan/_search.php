<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\LogPembetulanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-pembetulan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'pembetulanId') ?>

    <?= $form->field($model, 'masa') ?>

    <?= $form->field($model, 'tahun') ?>

    <?= $form->field($model, 'pasalId') ?>

    <?= $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'prevId') ?>

    <?php // echo $form->field($model, 'currId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

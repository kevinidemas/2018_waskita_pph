<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LogPembetulan */

$this->title = 'Create Log Pembetulan';
$this->params['breadcrumbs'][] = ['label' => 'Log Pembetulans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-pembetulan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

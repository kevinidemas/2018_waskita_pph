<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LogPembetulan */

$this->title = 'Update Log Pembetulan: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Log Pembetulans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pembetulanId, 'url' => ['view', 'id' => $model->pembetulanId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="log-pembetulan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

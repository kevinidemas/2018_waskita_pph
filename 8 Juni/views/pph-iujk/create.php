<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphIujk */

$this->title = 'Tambah IUJK Baru';
//$this->params['breadcrumbs'][] = ['label' => 'Pph Iujks', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}
?>
<div class="pph-iujk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

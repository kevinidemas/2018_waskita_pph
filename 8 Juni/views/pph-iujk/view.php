<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\PphWajibPajak;

/* @var $this yii\web\View */
/* @var $model app\models\PphIujk */

$this->title = 'IUJK - ' . $model->nomorIujk;
//$this->params['breadcrumbs'][] = ['label' => 'IUJK'];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-iujk-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->iujkId], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->iujkId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
                [
                'attribute' => 'wajibPajakId',
                'value' => function ($model) {
                    if ($model->wajibPajakId != null) {
                        $modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $model->wajibPajakId])->one();
                        $nama = $modelWp->nama;
                        return $nama;
                    } else {
                        return $npwp = 'Tanpa NPWP';
                    }
                },
            ],
            'nomorIujk',
            'penanggungJawab',
                [
                'attribute' => 'kemampuanKeuangan',
                'value' => $model->kemampuanKeuangan,
                'format' => ['decimal', 0]
            ],
                [
                'attribute' => 'is_approve',
                'label' => 'Status Verifikasi',
                'value' => function ($model) {
                    if ($model->is_approve == 0) {
                        return 'Ditolak';
                    } else if ($model->is_approve == 1) {
                        return 'Diterima';
                    } else {
                        return 'Belum Diproses oleh Admin';
                    }
                },
            ],
                [
                'attribute' => 'is_expired',
                'label' => 'Status Masa Berlaku',
                'value' => function ($model) {
                    if ($model->is_expired == 0) {
                        return 'Masih Berlaku';
                    } else {
                        return 'Masa Berlaku telah berakhir';
                    }
                },
            ],
                [
                'attribute' => 'published_at',
                'format' => ['date', 'php:d - m - Y']
            ],
                [
                'attribute' => 'expired_at',
                'format' => ['date', 'php:d - m - Y']
            ],
                [
                'attribute' => 'created_at',
                'label' => 'Dibuat Pada',
                'format' => ['date', 'php:d - m - Y']
            ],
                [
                'attribute' => 'created_by',
                'label' => 'Dibuat Oleh',
                'value' => function ($model) {
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
            ],
                [
                'attribute' => 'updated_at',
                'label' => 'Diubah Pada',
            ],
                [
                'attribute' => 'updated_by',
                'label' => 'Diubah Oleh',
                'value' => function ($model) {
                    if ($model->updated_by != null) {
                        $modelUser = User::find()->where(['id' => $model->updated_by])->one();
                        $nama = $modelUser->username;
                        return ucfirst($nama);
                    }
                },
            ],
            'bukti',
        ],
    ])
    ?>

</div>
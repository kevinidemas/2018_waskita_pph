<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use kartik\file\FileInput;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PphIujk */
/* @var $form yii\widgets\ActiveForm */

$session = Yii::$app->session;
$dataEncode = '';
$arrayData = [];
if (isset($session['dataGagalIujk'])) {
    $dataEncode = $session['dataGagalIujk'];
    unset($session['dataGagalIujk']);
    $dataEncode = json_decode($dataEncode);
    $arrayData = explode(",", $dataEncode);
    $nomorIujk = $arrayData[0];
    $kemampuanKeuangan = $arrayData[1];
    $penanggungJawab = $arrayData[2];
    $berlakuSejak = $arrayData[3];
    $berakhirPada = "$arrayData[4]";
} else {    
    $nomorIujk = '';
    $kemampuanKeuangan = '';
    $penanggungJawab = '';
    $berlakuSejak = 'Y-m-d';
    $berakhirPada = date('d-m-Y', strtotime('+1 year'));
}

// echo '<pre>';
//                print_r($model);
//                die();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#iujk-npwp").mask("99.999.999.9-999.999");
    });
</script>

<div class="pph-iujk-form">
    <?php
    $form = ActiveForm::begin(['enableAjaxValidation' => false, 'type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 4]]);
    ?>
    <?php
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nomorIujk' => ['type' => Form::INPUT_TEXT,
                        'options' => [
//                            'value' => $nomorIujk,
                            'placeholder' => 'Masukkan Nomor IUJK tanpa mengandung tanda baca'
                        ]
                    ],
                ],
            ],
            [
                'attributes' => [
                    'kemampuanKeuangan' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
//                            'value' => $kemampuanKeuangan,
                            'placeholder' => 'Masukkan Nilai Kemampuan Keuangan Perusahaan',
                            'id' => 'keuangan'
                        ]
                    ],
                ]
            ],
            [
                'attributes' => [
                    'penanggungJawab' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
//                            'value' => $penanggungJawab,
                            'placeholder' => 'Masukkan Nama Penanggung Jawab Perusahaan',
                        ],
                    ],
                ]
            ],
        ],
    ]);
    ?>

    <div class="row" id="iujk-ex">
        <div class="col-sm-4" id="iujk-published">
            <?php
            echo '<label class="control-label">Berlaku Sejak</label>';
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'published_at',
                'options' => [
                    'value' => date('d-m-Y', strtotime(date("$berlakuSejak"))),
                    'id' => 'iujk-publish'
                ],
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd-mm-yyyy',
                ]
            ]);
            ?>
        </div>
        <div class="col-sm-4" id="iujk-expired">
            <?php
            //$ed = date('Y-m-d');
            echo '<label class="control-label">Berakhir Pada</label>';
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'expired_at',
                'options' => [
                    'value' => $berakhirPada,
                    'id' => 'iujk-expire'
                ],
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'dd-mm-yyyy',
                    'startDate' => "0d"
                ]
            ]);
            ?>
        </div>
    </div>
    <?=
    $form->field($model, 'bukti')->widget(FileInput::classname(), [
        'options' => [
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'doc', 'docx', 'pdf', 'txt', 'pptx', 'ppt', 'xlsx', 'xls'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ]);
    ?>

    <div class="form-group">
        <?php echo '<div class="text-right" style="margin-right: 18px">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

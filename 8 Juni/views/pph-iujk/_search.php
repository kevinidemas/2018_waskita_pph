<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PphIujkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-iujk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iujkId') ?>

    <?= $form->field($model, 'nomorIujk') ?>

    <?= $form->field($model, 'namaPerusahaan') ?>

    <?= $form->field($model, 'alamatPerusahaan') ?>

    <?= $form->field($model, 'telepon') ?>

    <?php // echo $form->field($model, 'penanggungJawab') ?>

    <?php // echo $form->field($model, 'kemampuanKeuangan') ?>

    <?php // echo $form->field($model, 'npwp') ?>

    <?php // echo $form->field($model, 'expired_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'bukti') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

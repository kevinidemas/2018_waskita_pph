<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\PphWajibPajak;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphIujkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}

$wpId = $_GET['id'];
$modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $wpId])->one();
$namaWp = $modelWp->nama;
$this->title = 'Daftar IUJK - ' . $namaWp;
//$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('modalJs');
$this->registerCssFile('modalCss');
?>

<!--<link rel="stylesheet" href="@webroot/css/style/magnific-popup.css">-->
<!--<script src="@webroot/js/jquery.magnific-popup.js"></script>-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php
$session = Yii::$app->session;
$session['iujk-wpId'] = $_GET['id'];
Modal::begin([
    'header' => '<h4>Pilih IUJK Baru untuk Wajib Pajak Terpilih</h4>',
    'id' => 'modal',
    'size' => 'modal-lg'
]);

echo "<div id='modalContent'></div>";

Modal::end();
?>
<div class="pph-iujk-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah IUJK Baru', ['create-by-wp', 'id' => $_GET['id']], ['class' => 'btn btn-create-mini']) ?>
        <!-- Html::button('<i class="glyphicon glyphicon-hand-up"></i> Pilih IUJK', ['value' => Url::to('../pph-wajib-pajak/select-iujk'), 'class' => 'btn btn-create-mini', 'id' => 'modalButton']) -->
    </p>
    <?php
    if (Yii::$app->user->id == 1) {

        echo GridView::widget([
            'filterModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'font-size:12px;'],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => '-pjax',
                    'enableReplaceState' => false,
                    'enablePushState' => false,
                ],
            ],
            'striped' => true,
            'hover' => true,
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'tableOptions' => ['class' => 'table table-hover'],
            'resizableColumns' => true,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'options' => [
                        'width' => '10px',
                    ],
                ],
                    [
                    'attribute' => 'nomorIujk',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '220px',
                    ],
                ],
                    [
                    'attribute' => 'published_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->published_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'expired_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->expired_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'penanggungJawab',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '240px',
                    ],
                ],
                    [
                    'attribute' => 'kemampuanKeuangan',
                    'format' => 'Decimal',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '140px',
                    ],
                        'mergeHeader' => true
                ],
                    [
                    'attribute' => 'created_at',
                    'label' => 'Dibuat Pada',
                    'value' => function ($data) {
                        return $created_at = date('d ', strtotime($data->created_at)) . \Yii::t('app', date('n', strtotime($data->created_at))) . date(' Y', strtotime($data->created_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '110px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'is_active',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->is_active == 0) {
                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else {
                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'is_approve',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->is_approve == 0) {
                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else if ($data->is_approve == 1) {
                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        } else {
                            return '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'header' => 'Ubah Status Aktif',
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '15', 'align' => 'center'],
                    'template' => '{active}',
                    'buttons' => [
                        'active' => function ($url, $model) {
                            return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
                                        'title' => Yii::t('app', 'Active'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        $url = Url::toRoute(['pph-iujk/active-by-wp', 'id' => $model->iujkId]);
                        return $url;
                    },
                ],
                    ['class' => 'yii\grid\ActionColumn'],
            ],
            'toolbar' => [
                    ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index-iujk-wp', 'id' => $modelWp->wajibPajakId], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
                'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR IUJK</b>',
            ],
            'persistResize' => false,
            'toggleDataOptions' => ['minCount' => 10]
        ]);
    } else {
        echo GridView::widget([
            'filterModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'font-size:12px;'],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => '-pjax',
                    'enableReplaceState' => false,
                    'enablePushState' => false,
                ],
            ],
            'striped' => true,
            'hover' => true,
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'tableOptions' => ['class' => 'table table-hover'],
            'resizableColumns' => true,
            'columns' => [
                    ['class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'options' => [
                        'width' => '10px',
                    ],
                ],
                    [
                    'attribute' => 'nomorIujk',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '230px',
                    ],
                ],
                    [
                    'attribute' => 'published_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->published_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'expired_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->expired_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'penanggungJawab',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '260px',
                    ],
                ],
                    [
                    'attribute' => 'kemampuanKeuangan',
                    'format' => 'Decimal',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '160px',
                    ],
                        'mergeHeader' => true
                ],
                    [
                    'attribute' => 'created_at',
                    'label' => 'Dibuat Pada',
                    'value' => function ($data) {
                        return $created_at = date('d ', strtotime($data->created_at)) . \Yii::t('app', date('n', strtotime($data->created_at))) . date(' Y', strtotime($data->created_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '110px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'is_active',
                        'label' => 'Status Aktif',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->is_active == 0) {
                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else {
                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    [
                    'attribute' => 'is_approve',
                        'label' => 'Status Verifikasi',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->is_approve == 0) {
                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else if ($data->is_approve == 1) {
                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        } else {
                            return '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                    ['class' => 'yii\grid\ActionColumn'],
            ],
            'toolbar' => [
                    ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index-iujk-wp', 'id' => $modelWp->wajibPajakId], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
                'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR IUJK</b>',
            ],
            'persistResize' => false,
            'toggleDataOptions' => ['minCount' => 10]
        ]);
    }
    ?>

</div>

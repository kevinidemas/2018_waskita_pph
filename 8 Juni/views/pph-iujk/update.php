<?php

use yii\helpers\Html;
use app\models\PphWajibPajak;

/* @var $this yii\web\View */
/* @var $model app\models\PphIujk */

$modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $model->wajibPajakId])->one();
$wp = $modelWp->nama;

$this->title = 'Update IUJK : ' . $wp . ' (' . $model->nomorIujk.')';
$this->params['breadcrumbs'][] = ['label' => 'Pph Iujks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iujkId, 'url' => ['view', 'id' => $model->iujkId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-iujk-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>

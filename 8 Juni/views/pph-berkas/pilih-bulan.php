<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\db\ActiveRecord;

/* @var $this yii\web\View */
/* @var $model app\models\Donatur */
?>

<h4 align="center">Masa Pajak Terpilih</h4>

<?php $form = ActiveForm::begin(['layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
            'error' => '',
            'hint' => true,
        ],
    ],
    ]); ?>

<?php 
$session = Yii::$app->session;
$masa = $session['masaPajak'];
$pasal = $session['pasal-terpilih'];
?>

<h5 align="center"><?php echo $masa ?></h5>

<h4 align="center">Pilih Jenis Berkas</h4>
<?php 
if($pasal == 1){
    echo $form->field($model, 'jenisBerkas[]')->dropDownList([
    '1' => 'CSV',
    '2' => 'Excel - Akumulasi',
    '3' => 'Excel - Individu',
    ]); 
} elseif($pasal == 5) {
    echo $form->field($model, 'jenisBerkas[]')->dropDownList([
    // '1' => 'CSV (BETA)',
    '2' => 'Excel',
    '3' => 'PDF',
    '4' => 'SPT INDUK',
    ]); 
} else {
    echo $form->field($model, 'jenisBerkas[]')->dropDownList([
    '1' => 'CSV',
    '2' => 'Excel',
    '3' => 'PDF',
    '4' => 'SPT INDUK',
    ]); 
}
?>
<!--<input type="text" id="choosen-id" value="">--> 
<div class="form-group">
    <div class="col-sm-offset-5">
<?= Html::submitButton('Pilih', ['class'=> 'btn btn-primary']) ?>
<?php
echo "&nbsp";
echo "&nbsp"; 
echo Html::a('Keluar', ['index'],[
	'class'=>'btn btn-danger',
	'onclick' =>'$("#modal").modal("hide");
	return false;'
	]);
?>
    </div>
</div>
<?php ActiveForm::end();?>
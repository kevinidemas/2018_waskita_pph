<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-penghasilan-form">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php
    echo FileInput::widget([
        'name' => 'master_penghasilan',
        'options' => [
            'accept' => 'doc/*', 'file/*',
            'multiple' => true,
            'enableLabel' => true,
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['xls', 'xlsx'],
            'uploadUrl' => Url::to(['master-penghasilan/create-upload']),
            'maxFileSize' => 5120,
            'showCaption' => false,
            'maxFileCount' => 1,
            'browseLabel' => 'Pilih berkas Excel (maks 5MB)',
//            'showUpload' => true,
            'removeClass' => 'btn btn-danger',
            'dropZoneTitle' => 'Tarik dan Lepaskan Berkas Transaksi Faktur Manual Anda disini...'
        ],
    ]);
    ?>
    <?php ActiveForm::end(); ?>
</div>
<?php
$this->registerJS("
    $('#w1').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        $(location).attr('href', response.url);
    }
);", View::POS_END);
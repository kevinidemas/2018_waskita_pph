<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */

$this->title = 'Tambah(Upload) Penghasilan Pegawai';
//$this->params['breadcrumbs'][] = ['label' => 'Master Penghasilans', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-penghasilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-upload', [
        'model' => $model,
    ]) ?>

</div>

<div class="col-md-12" style="background-color:#FFE4C4; border: 2px solid #FFE4C4; border-radius: 3px; margin: 10px 0px 10px 0px">
        <b>Jenis Penghasilan :</b>
        <p>&emsp;&#x261B; 1. Gaji</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 2. Lembur</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 3. Natura</p>
		<p style="margin-top: -14px">&emsp;&#x261B; 4. Honorarium</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 5. THR</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 6. Jasa Produksi</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 7. SPJ</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 8. Cuti Besar</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 9. Cuti Tahunan</p>
        <p style="margin-top: -14px">&emsp;&#x261B; 10. BPJS Kesehatan </p>
        <p style="margin-top: -14px">&emsp;&#x261B; 11. JKK </p>
        <p style="margin-top: -14px">&emsp;&#x261B; 12. JKM </p>
        <p style="margin-top: -14px">&emsp;&#x261B; 13. Penghasilan Lainnya</p>
        <p style="font-weight: bold">&emsp;Jika jenis penghasilan kosong, akan dianggap sebagai TIDAK DIKETAHUI</p>
    </div>
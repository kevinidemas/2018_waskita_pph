<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PphWpInduk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-wp-induk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'wajibPajakId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

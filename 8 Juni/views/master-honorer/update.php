<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterHonorer */

$this->title = 'Update Master Honorer: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Master Honorers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->honorerId, 'url' => ['view', 'id' => $model->honorerId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-honorer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

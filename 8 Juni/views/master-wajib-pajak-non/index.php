<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterWajibPajakNonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Wajib Pajak Nons';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-wajib-pajak-non-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master Wajib Pajak Non', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'wajibPajakNonId',
            'nama',
            'alamat',
            'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

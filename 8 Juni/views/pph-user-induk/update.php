<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphUserInduk */

$this->title = 'Update Pph User Induk: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph User Induks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->userIndukId, 'url' => ['view', 'id' => $model->userIndukId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-user-induk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

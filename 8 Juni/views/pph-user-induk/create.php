<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphUserInduk */

$this->title = 'Create Pph User Induk';
$this->params['breadcrumbs'][] = ['label' => 'Pph User Induks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-user-induk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

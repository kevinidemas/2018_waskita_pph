<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphVendorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Vendors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-vendor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Vendor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'wajibPajakId',
            'nama',
            'npwp',
            'alamat',
            'statusNpwpId',
            // 'iujkId',
            // 'statusVendorId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

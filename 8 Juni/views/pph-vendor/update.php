<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */

$this->title = 'Update Pph Vendor: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wajibPajakId, 'url' => ['view', 'id' => $model->wajibPajakId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphStatusKerja */

$this->title = 'Create Pph Status Kerja';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-status-kerja-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

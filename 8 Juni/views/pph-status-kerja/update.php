<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusKerja */

$this->title = 'Update Pph Status Kerja: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Kerjas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->statusKerjaId, 'url' => ['view', 'id' => $model->statusKerjaId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-status-kerja-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

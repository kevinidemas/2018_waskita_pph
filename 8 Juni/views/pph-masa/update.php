<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphMasa */

$this->title = 'Update Pph Masa: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Masas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->masaId, 'url' => ['view', 'id' => $model->masaId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-masa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

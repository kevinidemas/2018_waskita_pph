<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\db\ActiveRecord;
use kartik\select2\Select2;
use app\models\PphMasa;
/* @var $this yii\web\View */
/* @var $model app\models\Donatur */
?>

<h4 align="center">Pilih Masa Pajak</h4>

<?php
$form = ActiveForm::begin(['layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'error' => '',
                    'hint' => true,
                ],
            ],
        ]);
?>

<?php
setlocale(LC_TIME, "IND");
$currDate = strftime("%B %Y");
echo $form->field($model, 'nama[]')->widget(Select2::classname(), [
    'data' => PphMasa::getOptions(),
    'options' => [
        'placeholder' => 'Pilih Masa Pajak . . .',
        'value' => $currDate
    ],
]);
?>

<div class="form-group">
    <div class="col-sm-offset-5">
        <?= Html::submitButton('Pilih', ['class' => 'btn btn-primary']) ?>

        <?php
        echo "&nbsp";
        echo "&nbsp";
        echo Html::a('Keluar', ['index'], [
            'class' => 'btn btn-danger',
            'onclick' => '$("#modal").modal("hide");
	return false;'
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
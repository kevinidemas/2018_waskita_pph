<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphMasa */

$this->title = 'Create Pph Masa';
$this->params['breadcrumbs'][] = ['label' => 'Pph Masas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-masa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

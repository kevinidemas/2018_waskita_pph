<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PphCalendar */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$session = Yii::$app->session;
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');    
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}

$connection = \Yii::$app->db;
$userId = Yii::$app->user->id;
$sql = $connection->createCommand("SELECT COUNT(calendarId) FROM pph_calendar WHERE created_by='$userId'");
$exec = $sql->queryColumn();

$button = '';
if ($exec[0] != 0) {
    echo $button = '<p><button type="button" onclick="showCalendar()" class="btn btn-add"><i class="glyphicon glyphicon-eye-open"></i> LIHAT KALENDER</button></p>';
    echo '<br>';
}
?>

<div class="pph-calendar-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    if (isset($_SESSION['updateCalendar'])) {
        unset($_SESSION['updateCalendar']);
        $startDate = date('d-m-Y', strtotime($model->startDate));
            $endDate = date('d-m-Y', strtotime($model->endDate));
    } else {
        $startDate = date('d-m-Y');
        $datetime = new DateTime('tomorrow');
        $endDate = $datetime->format('d-m-Y');
    }


    echo '<label class="control-label">Rentang Kalender Aktif</label>';
    echo DatePicker::widget([
        'name' => 'startDate',
        'value' => $startDate,
        'type' => DatePicker::TYPE_RANGE,
        'name2' => 'endDate',
        'value2' => $endDate,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
            'todayHighlight' => true,
        ]
    ]);
    ?>

    <?= $form->field($model, 'calendarNote')->textInput(['maxlength' => true, 'readOnly' => true, 'placeholder' => 'Masa akan mengikuti Kalender secara otomatis']) ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

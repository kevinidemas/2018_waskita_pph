<?php

use yii\helpers\Html;
use kartik\alert\Alert;


/* @var $this yii\web\View */
/* @var $model app\models\PphCalendar */

$this->title = 'Calendar';
$this->params['breadcrumbs'][] = ['label' => 'Calendar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-calendar-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

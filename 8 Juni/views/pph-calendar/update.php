<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphCalendar */

$this->title = 'Update Calendar '.$model->calendarNote;
$this->params['breadcrumbs'][] = ['label' => 'Pph Calendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->calendarId, 'url' => ['view', 'id' => $model->calendarId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-calendar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

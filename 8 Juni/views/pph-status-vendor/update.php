<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusVendor */

$this->title = 'Update Pph Status Vendor: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->statusVendorId, 'url' => ['view', 'id' => $model->statusVendorId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-status-vendor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

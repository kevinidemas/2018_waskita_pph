<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphStatusVendor */

$this->title = 'Create Pph Status Vendor';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-status-vendor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

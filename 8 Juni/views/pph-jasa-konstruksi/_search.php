<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PphJasaKonstruksiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-jasa-konstruksi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'jasaKonstruksiId') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'tarifId') ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'parentId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

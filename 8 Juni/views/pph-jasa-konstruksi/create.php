<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphJasaKonstruksi */

$this->title = 'Create Pph Jasa Konstruksi';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jasa Konstruksis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jasa-konstruksi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

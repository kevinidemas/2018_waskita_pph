<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphJasaKonstruksi */

$this->title = 'Update Pph Jasa Konstruksi: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jasa Konstruksis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jasaKonstruksiId, 'url' => ['view', 'id' => $model->jasaKonstruksiId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-jasa-konstruksi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphBujpk;

/**
 * PphBujpkSearch represents the model behind the search form of `app\models\PphBujpk`.
 */
class PphBujpkSearch extends PphBujpk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bujpkId', 'wajibPajakId', 'created_by', 'updated_by', 'is_active', 'is_approve', 'is_expired', 'tingkatKeuangan', 'is_expired_warning'], 'integer'],
            [['nomorBujpk', 'penanggungJawab', 'published_at', 'expired_at', 'created_at', 'updated_at', 'bukti', 'keterangan'], 'safe'],
            [['kemampuanKeuangan'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphBujpk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'bujpkId' => $this->bujpkId,
            'wajibPajakId' => $this->wajibPajakId,
            'kemampuanKeuangan' => $this->kemampuanKeuangan,
            'published_at' => $this->published_at,
            'expired_at' => $this->expired_at,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'is_active' => $this->is_active,
            'is_approve' => $this->is_approve,
            'is_expired' => $this->is_expired,
            'tingkatKeuangan' => $this->tingkatKeuangan,
            'is_expired_warning' => $this->is_expired_warning,
        ]);

        $query->andFilterWhere(['like', 'nomorBujpk', $this->nomorBujpk])
            ->andFilterWhere(['like', 'penanggungJawab', $this->penanggungJawab])
            ->andFilterWhere(['like', 'bukti', $this->bukti])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}

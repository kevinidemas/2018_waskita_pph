<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Setting;

/**
 * SettingSearch represents the model behind the search form of `app\models\Setting`.
 */
class SettingSearch extends Setting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['settingId', 'userId', 'pasalId', 'digitNomorBP', 'userIndukId'], 'integer'],
            [['npwpPemungutPajak', 'namaPemungutPajak', 'kota', 'nama', 'jabatan', 'prefixNomorBP', 'startNomorBP', 'lastNomorBP', 'prevLastNomorBP'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Setting::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'settingId' => $this->settingId,
            'userId' => $this->userId,
            'pasalId' => $this->pasalId,
            'digitNomorBP' => $this->digitNomorBP,
            'userIndukId' => $this->userIndukId,
        ]);

        $query->andFilterWhere(['like', 'npwpPemungutPajak', $this->npwpPemungutPajak])
            ->andFilterWhere(['like', 'namaPemungutPajak', $this->namaPemungutPajak])
            ->andFilterWhere(['like', 'kota', $this->kota])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jabatan', $this->jabatan])
            ->andFilterWhere(['like', 'prefixNomorBP', $this->prefixNomorBP])
            ->andFilterWhere(['like', 'startNomorBP', $this->startNomorBP]);
//            ->andFilterWhere(['like', 'lastNomorBP', $this->lastNomorBP])
//            ->andFilterWhere(['like', 'prevLastNomorBP', $this->prevLastNomorBP]);

        return $dataProvider;
    }
}

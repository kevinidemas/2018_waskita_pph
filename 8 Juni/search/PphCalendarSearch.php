<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphCalendar;

/**
 * PphCalendarSearch represents the model behind the search form of `app\models\PphCalendar`.
 */
class PphCalendarSearch extends PphCalendar
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['calendarId', 'created_by', 'status'], 'integer'],
            [['startDate', 'endDate', 'calendarNote', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphCalendar::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'calendarId' => $this->calendarId,
            'startDate' => $this->startDate,
            'endDate' => $this->endDate,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'calendarNote', $this->calendarNote]);

        return $dataProvider;
    }
}

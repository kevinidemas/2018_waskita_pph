<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterPegawai;

/**
 * MasterPegawaiSearch represents the model behind the search form of `app\models\MasterPegawai`.
 */
class MasterPegawaiSearch extends MasterPegawai
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pegawaiId', 'jenisKelamin', 'statusPerkawinan'], 'integer'],
            [['nip', 'npwp', 'alamat', 'nik'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterPegawai::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pegawaiId' => $this->pegawaiId,
            'jenisKelamin' => $this->jenisKelamin,
            'statusPerkawinan' => $this->statusPerkawinan,
        ]);

        $query->andFilterWhere(['like', 'nip', $this->nip])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'nik', $this->nik]);

        return $dataProvider;
    }
}

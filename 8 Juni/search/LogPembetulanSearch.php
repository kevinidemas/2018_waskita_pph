<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LogPembetulan;

/**
 * LogPembetulanSearch represents the model behind the search form of `app\models\LogPembetulan`.
 */
class LogPembetulanSearch extends LogPembetulan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pembetulanId', 'pasalId', 'userId', 'prevId', 'currId'], 'integer'],
            [['masa', 'tahun'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogPembetulan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pembetulanId' => $this->pembetulanId,
            'pasalId' => $this->pasalId,
            'userId' => $this->userId,
            'prevId' => $this->prevId,
            'currId' => $this->currId,
        ]);

        $query->andFilterWhere(['like', 'masa', $this->masa])
            ->andFilterWhere(['like', 'tahun', $this->tahun]);

        return $dataProvider;
    }
}

<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterHonorer;

/**
 * MasterHonorerSearch represents the model behind the search form of `app\models\MasterHonorer`.
 */
class MasterHonorerSearch extends MasterHonorer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['honorerId', 'jenisKelamin', 'statusKerjaId', 'userId', 'created_by', 'updated_by', 'hp', 'approvalStatus'], 'integer'],
            [['nama', 'posisi', 'nik', 'npwp', 'alamat', 'created_at', 'updated_at', 'email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterHonorer::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'honorerId' => $this->honorerId,
            'jenisKelamin' => $this->jenisKelamin,
            'statusKerjaId' => $this->statusKerjaId,
            'userId' => $this->userId,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'hp' => $this->hp,
            'approvalStatus' => $this->approvalStatus,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'posisi', $this->posisi])
            ->andFilterWhere(['like', 'nik', $this->nik])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}

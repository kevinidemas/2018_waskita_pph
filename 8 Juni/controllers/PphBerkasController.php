<?php

namespace app\controllers;

use Yii;
use yii\web\Session;
use app\models\PphBerkas;
use app\models\PphBuktiPotong;
use app\models\PphJenisPenghasilan;
use app\models\PphWajibPajak;
use app\models\MasterMandor;
use app\models\Setting;
use app\models\MasterPenghasilan;
use app\models\MasterPegawai;
use app\models\MasterWajibPajakNon;
use app\models\PphStatusVendor;
use app\models\PphJenisKelamin;
use app\models\PphIujk;
use app\models\PphBujpk;
use app\models\PphStatusNpwp;
use app\models\PphTarif;
use app\models\User;
use app\search\PphBerkasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\LogPembetulan;
use DateTime;
use \Mpdf\Mpdf;
use kartik\mpdf\Pdf;

/**
 * PphBerkasController implements the CRUD actions for PphBerkas model.
 */
class PphBerkasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all PphBerkas models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PphBerkasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PphBerkas model.
     * @param integer $id
     * @return mixed
     */
    public function actionPilihBulan() {
        $model = new PphBerkas();
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('pilih-bulan', ['model' => $model]);
            } else {
                $masa = $session['masaId'];
                $jenisArray = $model->jenisBerkas;
                $jenisBerkas = $jenisArray[0];
                $pasal = $session['pasal-terpilih'];

                if ($jenisBerkas == 1) {
                    if ($pasal == 2) {
                        return $this->redirect(['cetak-csv', 'idBulan' => $masa]);
                    } else if ($pasal == 3) {
                        return $this->redirect(['cetak-csv-pasal-23', 'idBulan' => $masa]);
                    } else if ($pasal == 4) {
                        return $this->redirect(['cetak-csv-pasal-4', 'idBulan' => $masa]);
                    } else if ($pasal == 1) {
                        return $this->redirect(['cetak-csv-pasal-21']);
                    } else if ($pasal == 5) {
                        return $this->redirect('../pph-bukti-potong/index-pasal-21');
//                        return $this->redirect(['cetak-csv-pasal-21-mandor']);
                    }
                } elseif ($jenisBerkas == 2) {
                    if ($pasal == 2) {
                        return $this->redirect(['cetak-excel', 'idBulan' => $masa]);
                    } else if ($pasal == 3) {
                        return $this->redirect(['cetak-excel-pasal-23', 'idBulan' => $masa]);
                    } else if ($pasal == 4) {
                        return $this->redirect(['cetak-excel-pasal-4', 'idBulan' => $masa]);
                    } else if ($pasal == 1) {
                        return $this->redirect(['cetak-excel-pasal-21-akumulasi']);
                    } else if ($pasal == 5) {
                        return $this->redirect(['cetak-excel-pasal-21-ptt', 'idBulan' => $masa]);
                    }
                } elseif($jenisBerkas == 3) {
                    if ($pasal == 2) {
                        return $this->redirect(['cetak-pdf', 'idBulan' => $masa]);
                    } else if ($pasal == 3) {
                        return $this->redirect(['cetak-pdf-pasal-23', 'idBulan' => $masa]);
                    } else if ($pasal == 4) {
                        return $this->redirect(['cetak-pdf-pasal-4', 'idBulan' => $masa]);
                    } else if ($pasal == 1) {
                        return $this->redirect(['cetak-excel-pasal-21-individu', 'idBulan' => $masa]);
                    } else if ($pasal == 5) {
                        return $this->redirect(['cetak-pdf-pasal-21-ptt', 'idBulan' => $masa]);
                    }
                } else {
                    if ($pasal == 2) {
                        return $this->redirect(['spt-induk-22-pdf', 'idBulan' => $masa]);
                    } else if ($pasal == 3) {
                        return $this->redirect(['spt-induk-23-pdf', 'idBulan' => $masa]);
                    } else if ($pasal == 4) {
                        return $this->redirect(['spt-induk-4-pdf', 'idBulan' => $masa]);
                    } else if ($pasal == 5) {
                        return $this->redirect(['spt-induk-21-pdf', 'idBulan' => $masa]);
                    }
                }
            }
        }
    }
    
    public function actionPilihData() {
        $model = new PphBerkas();
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('pilih-data', ['model' => $model]);
            } else {
                $masa = $session['masaId'];
                $jenisArray = $model->jenisBerkas;
                $jenisBerkas = $jenisArray[0];
                $pasal = $session['pasal-terpilih'];
                $jenisWp = $session['jenis-wajib-pajak'];
                if ($jenisBerkas == 1) {
                    if ($jenisWp == 1) {
                        return $this->redirect(['cetak-npwp-wp']);
                    } else {
                        return $this->redirect(['cetak-npwp-mandor']);
                    }
                } elseif ($jenisBerkas == 2) {
                    if ($jenisWp == 1) {
                        return $this->redirect(['cetak-sppkp-wp']);
                    } else {
                        return $this->redirect(['cetak-nik-mandor']);
                    }
                } elseif ($jenisBerkas == 3) {
                    if ($jenisWp == 1) {
                        return $this->redirect(['cetak-iujk-wp']);
                    }
                } elseif ($jenisBerkas == 4) {
                    if ($jenisWp == 1) {
                        return $this->redirect(['cetak-bujpk-wp']);
                    }
                } else {
                    if ($jenisWp == 1) {
                        return $this->redirect(['cetak-excel-wp']);
                    } else {
                        return $this->redirect(['cetak-excel-mandor']);
                    }
                }
            }
        }
    }

    public function actionCetakExcelPasal4($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 2;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        $arrayId = [];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }

        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $tempDate = DateTime::createFromFormat('Y-m-d', $downloadedTime)->format('d/m/Y');
        $bulan = \Yii::t('app', $idBulan);
        $filename = 'Bukti Potong PPh Pasal 4 Ayat (2) ' . $bulan . ' ' . $tahun . '.xls';
//        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index-pasal-4");
        echo "Kode Form Bukti Potong / Kode Form Input PPh Yang Dibayar Sendiri\t";
        echo "Masa Pajak\t";
        echo "Tahun Pajak\t";
        echo "Pembetulan\t";
        echo "NPWP WP\t";
        echo "Nama WP yang Dipotong\t";
        echo "Alamat WP yang Dipotong\t";
        echo "Nomor Bukti Potong / NTPN\t";
        echo "Tanggal Bukti Potong/Tanggal SSP\t";
        echo "Jenis Hadiah Undian 1 / Lokasi Tanah dan atau Bangunan / Nama Obligasi\t";
        echo "Kode Option Tempat Penyimpanan 1 (Khusus F113310)\t";
        echo "Jumlah Nilai Bruto 1 / Jumlah Nilai Nominal Obligasi Yg Diperdagangkan Di Bursa Efek / Jumlah Penghasilan Pada Form Input Yang Dibayar Sendiri\t";
        echo "Tarif 1 / Tingkat Bunga per Tahun\t";
        echo "PPh Yang Dipotong  1 /Jumlah PPh Pada Form Input Yang Dibayar Sendiri\t";
        echo "Jenis Hadiah Undian 2 / Nomor Seri Obligasi \t";
        echo "Kode Option Tempat Penyimpanan 2\t";
        echo "Jumlah Nilai Bruto 2 / Jumlah Harga Perolehan Bersih (tanpa Bunga) Pada Obligasi Yg Diperdagangkan Di Bursa Efek\t";
        echo "Tarif 2\t";
        echo "PPh Yang Dipotong 2\t";
        echo "Jenis Hadiah Undian 3\t";
        echo "Kode Option Tempat Penyimpanan 3\t";
        echo "Jumlah Nilai Bruto 3 / Jumlah Harga Penjualan Bersih (tanpa Bunga) Pada Obligasi Yg Diperdagangkan Di Bursa Efek\t";
        echo "Tarif 3\t";
        echo "PPh Yang Dipotong 3\t";
        echo "Jenis Hadiah Undian 4\t";
        echo "Kode Option Tempat Penyimpanan 4 / Kode Option Perencanaan (1) atau Pengawasan (2) atau selainnya (0) untuk BP Jasa Konstruksi poin 4\t";
        echo "Jumlah Nilai Bruto 4 / Jumlah Diskonto Pada Obligasi Yg Diperdagangkan Di Bursa Efek\t";
        echo "Tarif 4\t";
        echo "PPh Yang Dipotong 4\t";
        echo "Jenis Hadiah Undian 5\t";
        echo "Kode Option Tempat Penyimpanan 5 / Kode Option Perencanaan (1) atau Pengawasan (2) atau selainnya (0) untuk BP Jasa Konstruksi poin 5\t";
        echo "Jumlah Nilai Bruto 5 / Jumlah Bunga Pada Obligasi Yg Diperdagangkan Di Bursa Efek\t";
        echo "Tarif 5\t";
        echo "PPh Yang Dipotong 5\t";
        echo "Jenis Hadiah Undian 6\t";
        echo "Jumlah Nilai Bruto 6 / Jumlah Total Bunga atau Diskonto Obligasi Yang Diperdagangkan\t";
        echo "Tarif 6 / Tarif PPh Final Pada Obligasi Yang Diperdagangkan Di Bursa Efek\t";
        echo "PPh Yang Dipotong 6\t";
        echo "Jumlah Nilai Bruto 7\t";
        echo "Tarif 7\t";
        echo "PPh Yang Dipotong 7\t";
        echo "Jenis Penghasilan 8\t";
        echo "Jumlah Nilai Bruto 8\t";
        echo "Tarif 8\t";
        echo "PPh Yang Dipotong 8\t";
        echo "Jumlah PPh Yang Dipotong\t";
        echo "Tanggal Jatuh Tempo Obligasi\t";
        echo "Tanggal Perolehan Obligasi\t";
        echo "Tanggal Penjualan Obligasi\t";
        echo "Holding Periode Obligasi (Hari)\t";
        echo "Time Periode Obligasi (Hari)\t";
        echo "Jenis Bukti Potong\t";
        echo "Dibuat Oleh" . "\t";
        echo "Nomor Pembukuan";
        echo "\r\n";
        $tarif = '';

        for ($node = 0; $node < count($arrayId); $node++) {            
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            $statusWpId = $modelData->statusWpId;
            $wpId = $modelData->wajibPajakId;
            if($modelVendor == null){
                $npwpWp = '000000000000000';
            } else {
                $npwpWp = $modelVendor->npwp;
            }
            if ($statusWpId == 3) {
                $kodeForm = 'F113316';
                $lokasi = '';
                $iujkId = $modelVendor->iujkId;
                if ($iujkId != NULL) {
                    $modelIujk = PphIujk::find()->where(['iujkId' => $iujkId])->one();
                    $is_approve = $modelIujk->is_approve;
                    if ($is_approve == 1) {
                        $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                        if ($tingkatKeuanganWp == 1) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 6])->one();
                            $tarif = $modelTarif->tarif;
                            $jumlahBruto1 = '0';
                            $tarif1 = $tarif;
                            $jumlahPphDipotong1 = '0';
                            $jumlahBruto2 = '0';
                            $tarif2 = '4';
                            $jumlahPphDipotong2 = '0';
                            $jumlahBruto3 = $modelData->jumlahBruto;
                            $tarif3 = '3';
                            $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                            $jumlahPphDipotong = $jumlahPphDipotong3;
                        } else if ($tingkatKeuanganWp == 2) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                            $tarif = $modelTarif->tarif;
                            $jumlahBruto1 = '0';
                            $tarif1 = '2';
                            $jumlahPphDipotong1 = '0';
                            $jumlahBruto2 = '0';
                            $tarif2 = $tarif;
                            $jumlahPphDipotong2 = '0';
                            $jumlahBruto3 = $modelData->jumlahBruto;
                            $tarif3 = '3';
                            $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                            $jumlahPphDipotong = $jumlahPphDipotong3;
                        } else if ($tingkatKeuanganWp == 3) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                            $tarif = $modelTarif->tarif;
                            $jumlahBruto1 = '0';
                            $tarif1 = '2';
                            $jumlahPphDipotong1 = '0';
                            $jumlahBruto2 = '0';
                            $tarif2 = $tarif;
                            $jumlahPphDipotong2 = '0';
                            $jumlahBruto3 = $modelData->jumlahBruto;
                            $tarif3 = '3';
                            $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                            $jumlahPphDipotong = $jumlahPphDipotong3;
                        }
                    } else {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                        $tarif = $modelTarif->tarif;
                        $jumlahBruto1 = '0';
                        $tarif1 = '2';
                        $jumlahPphDipotong1 = '0';
                        $jumlahBruto2 = '0';
                        $tarif2 = '4';
                        $jumlahPphDipotong2 = '0';
                        $jumlahBruto3 = $modelData->jumlahBruto;
                        $tarif3 = $tarif;
                        $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                        $jumlahPphDipotong = $jumlahPphDipotong3;
                    }
                } else {                    
                    $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                    $tarif = $modelTarif->tarif;
                    $jumlahBruto1 = '0';
                    $tarif1 = '2';
                    $jumlahPphDipotong1 = '0';
                    $jumlahBruto2 = '0';
                    $tarif2 = '4';
                    $jumlahPphDipotong2 = '0';
                    $jumlahBruto3 = $modelData->jumlahBruto;
                    $tarif3 = $tarif;
                    $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                    $jumlahPphDipotong = $jumlahPphDipotong3;
                }
            } else {
                $kodeForm = 'F113312';
                $modelTarif = PphTarif::find()->where(['tarifId' => 3])->one();
                $tarif = $modelTarif->tarif;
                $jumlahBruto1 = $modelData->jumlahBruto;
                $tarif1 = $tarif;
                $jumlahPphDipotong1 = $modelData->jumlahPphDiPotong;
                $jumlahBruto2 = '0';
                $tarif2 = '4';
                $jumlahPphDipotong2 = '0';
                $jumlahBruto3 = '0';
                $tarif3 = '3';
                $jumlahPphDipotong3 = '0';
                $jumlahPphDipotong = $jumlahPphDipotong1;
                $lokasi = $modelData->lokasi_tanah;
            }

            $remove_character = array('\n', '\r\n', '\r');
            if ($modelData->wajibPajakId != null) {
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
            } else {
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
            }

            $noBp = $modelData->nomorBuktiPotong;
            $tanggalPungut = $modelData->tanggal;
            $JHU1 = $lokasi;           
            $KOTP1 = '0';            
            $tarif1 = $tarif1;
            $pphDipotong1 = $jumlahPphDipotong1;
            $JHU2 = '';
            $KOTP2 = '0';
            $jumlahBruto2 = $jumlahBruto2;
            $tarif2 = $tarif2;
            $pphDipotong2 = $jumlahPphDipotong2;
            $JHU3 = '';
            $KOTP3 = '0';
            $jumlahBruto3 = $jumlahBruto3;
            $tarif3 = $tarif3;
            $pphDipotong3 = $jumlahPphDipotong3;
            $JHU4 = '';
            $KOTP4 = '0';
            $jumlahBruto4 = '0';
            $tarif4 = '4';
            $pphDipotong4 = '0';
            $JHU5 = '';
            $KOTP5 = '0';
            $jumlahBruto5 = '0';
            $tarif5 = '6';
            $pphDipotong5 = '0';
            $JHU6 = '';
            $jumlahBruto6 = '0';
            $tarif6 = '0';
            $pphDipotong6 = '0';
            $jumlahBruto7 = '0';
            $tarif7 = '0';
            $pphDipotong7 = '0';
            $jenisPenghasilan8 = '';
            $jumlahBruto8 = '0';
            $tarif8 = '0';
            $pphDipotong8 = '0';
            $jumlahPphDipotong = $jumlahPphDipotong;
            $TJTO = '';
            $TPO = '';
            $TPenO = '';
            $HPO = '';
            $TimePO = '0';
            if ($modelData->statusWpId == 3) {
                $jenisBp = 'Jasa';
            } else {
                $jenisBp = 'Sewa';
            }
            $createdById = $modelData->created_by;
            $modelUser = User::find()->where(['id' => $createdById])->one();
            $dibuatOleh = ucfirst($modelUser->username);

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalPungut)->format('d/m/Y');
            $nomorPembukuan = $modelData->nomorPembukuan;
            echo $kodeForm . "\t";
            echo $masaPajak . "\t";
            echo $tahunPajak . "\t";
            echo $pembetulan . "\t";
            echo $npwpWp . "\t";
            echo $namaWp . "\t";
            echo $alamatWp . "\t";
            echo $noBp . "\t";
            echo $tanggal . "\t";
            echo $JHU1 . "\t";
            echo $KOTP1 . "\t";
            echo $jumlahBruto1 . "\t";
            echo $tarif1 . "\t";
            echo $pphDipotong1 . "\t";
            echo $JHU2 . "\t";
            echo $KOTP2 . "\t";
            echo $jumlahBruto2 . "\t";
            echo $tarif2 . "\t";
            echo $pphDipotong2 . "\t";
            echo $JHU3 . "\t";
            echo $KOTP3 . "\t";
            echo $jumlahBruto3 . "\t";
            echo $tarif3 . "\t";
            echo $pphDipotong3 . "\t";
            echo $JHU4 . "\t";
            echo $KOTP4 . "\t";
            echo $jumlahBruto4 . "\t";
            echo $tarif4 . "\t";
            echo $pphDipotong4 . "\t";
            echo $JHU5 . "\t";
            echo $KOTP5 . "\t";
            echo $jumlahBruto5 . "\t";
            echo $tarif5 . "\t";
            echo $pphDipotong5 . "\t";
            echo $JHU6 . "\t";
            echo $jumlahBruto6 . "\t";
            echo $tarif6 . "\t";
            echo $pphDipotong6 . "\t";
            echo $jumlahBruto7 . "\t";
            echo $tarif7 . "\t";
            echo $pphDipotong7 . "\t";
            echo $jenisPenghasilan8 . "\t";
            echo $jumlahBruto8 . "\t";
            echo $tarif8 . "\t";
            echo $pphDipotong8 . "\t";
            echo $jumlahPphDipotong . "\t";
            echo $TJTO . "\t";
            echo $TPO . "\t";
            echo $TPenO . "\t";
            echo $HPO . "\t";
            echo $TimePO . "\t";
            echo $jenisBp . "\t";
            echo $dibuatOleh . "\t";
            echo $nomorPembukuan;
            echo "\r\n";
        }

        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakExcelMandor() {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;

        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayIdPegawaiTemp = explode(",", $arrayRows);
            $arrayIdPegawai = [];
            for ($an = 0; $an < count($arrayIdPegawaiTemp); $an++) {            
                $arrayIdPegawai[] = $arrayIdPegawaiTemp[$an];
            };
            unset($session['sel-rows']);
        } else {
           if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT mandorId FROM master_mandor")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT mandorId FROM master_mandor WHERE userId = '$userId'")->queryAll();
            }
            $arrayIdPegawai = array_column($getId, 'mandorId');
        }

        $filename = 'Daftar Mandor' . '.xls';

        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../master-mandor/index");

        echo "Nama Mandor\t";
        echo "NPWP\t";
        echo "NIK\t";
        echo "Alamat\t";
        echo "Jenis Kelamin\t";
        echo "HP\t";
        echo "Email\t";
        echo "Dibuat Oleh";
        echo "\r\n";

        for ($node = 0; $node < count($arrayIdPegawai); $node++) {            
            $modelWp =  MasterMandor::find()->where(['mandorId' => $arrayIdPegawai[$node]])->one();

            $nama = $modelWp['nama'];
            $npwp = $modelWp['npwp'];
            $nik = $modelWp['nik'];
            $alamat = $modelWp['alamat'];
            $alamat = trim(preg_replace('/\s\s+/', ' ', $alamat));
            $hp = $modelWp['hp'];
            $email = $modelWp['email'];
            $modelJk = PphJenisKelamin::find()->where(['jenisKelaminId' => $modelWp['jenisKelamin']])->one();
            $jenisKelamin = ucfirst($modelJk->jenis);
            $modelUser = User::find()->where(['id' => $modelWp['userId']])->one();
            $dibuatOleh = ucfirst($modelUser->username);

            echo $nama . "\t";
            echo $npwp . "\t";
            echo $nik . "\t";
            echo $alamat . "\t";
            echo $jenisKelamin . "\t";
            echo $hp . "\t";
            echo $email . "\t";
            echo $dibuatOleh . "\t";
            echo "\r\n";

        }
        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download-pasal-21'] = $notification;
        if (!isset($session['notification-success-download-pasal-21'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }
    
    public function actionCetakExcelWp() {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;

        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayIdPegawaiTemp = explode(",", $arrayRows);
            $arrayIdPegawai = [];
            for ($an = 0; $an < count($arrayIdPegawaiTemp); $an++) {            
                $arrayIdPegawai[] = $arrayIdPegawaiTemp[$an];
            };
            unset($session['sel-rows']);
        } else {
           if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE user_id = '$userId'")->queryAll();
            }
            $arrayIdPegawai = array_column($getId, 'wajibPajakId');
        }

        $filename = 'Daftar Wajib Pajak' . '.xls';

        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-wajib-pajak/index");

        echo "Nama Wajib Pajak\t";
        echo "NPWP\t";
        echo "Penanggung Jawab\t";
        echo "Alamat\t";
        echo "IUJK\t";
        echo "Tanggal Berakhir IUJK\t";
        echo "BUJPK\t";
        echo "Tanggal Berakhir BUJPK\t";
        echo "HP\t";
        echo "Telepon\t";
        echo "Fax\t";
        echo "Email\t";
        echo "Website\t";
        echo "Dibuat Oleh";
        echo "\r\n";

        for ($node = 0; $node < count($arrayIdPegawai); $node++) {            
            $modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $arrayIdPegawai[$node]])->one();

            $penanggungJawab = $modelWp['penanggungJawab'];
            $nama = $modelWp['nama'];
            $npwp = $modelWp['npwp'];
            $iujkId = $modelWp['iujkId'];
            if($iujkId != null){
                $modelIujk = PphIujk::find()->where(['iujkId' => $iujkId])->one();
                $nomorIujk = $modelIujk['nomorIujk'];
                $tanggalBerakhirIujk = $modelIujk['expired_at'];
            } else {
                $nomorIujk = '-';
                $tanggalBerakhirIujk = '-';
            }
            $bujpkId = $modelWp['bujpkId'];
            if($bujpkId != null){
                $modelBujpk = PphBujpk::find()->where(['bujpkId' => $bujpkId])->one();
                $nomorBujpk = $modelBujpk['nomorBujpk'];
                $tanggalBerakhirBujpk = $modelBujpk['expired_at'];
            } else {
                $nomorBujpk = '-';
                $tanggalBerakhirBujpk = '-';
            }
            $alamat = $modelWp['alamat'];
            $alamat = trim(preg_replace('/\s\s+/', ' ', $alamat));
            $hp = $modelWp['hp'];
            $telepon = $modelWp['telepon'];
            $fax = $modelWp['fax'];
            $email = $modelWp['email'];
            $website = $modelWp['website'];
            $modelUser = User::find()->where(['id' => $modelWp['user_id']])->one();
            $dibuatOleh = ucfirst($modelUser->username);
            
            echo $nama . "\t";
            echo $npwp . "\t";
            echo $penanggungJawab . "\t";
            echo $alamat . "\t";
            echo $nomorIujk . "\t";
            echo $tanggalBerakhirIujk . "\t";
            echo $nomorBujpk . "\t";
            echo $tanggalBerakhirBujpk . "\t";
            echo $hp . "\t";
            echo $telepon . "\t";
            echo $fax . "\t";
            echo $email . "\t";
            echo $website . "\t";
            echo $dibuatOleh . "\t";
            echo "\r\n";

        }
        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download-pasal-21'] = $notification;
        if (!isset($session['notification-success-download-pasal-21'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }
    
    public function actionCetakExcelPasal21Akumulasi() {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $bulan = \Yii::t('app', $arrayMasa[0]);
        if (strlen($bulan) == 1) {
            $bulan = '0' . $bulan;
        }
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayIdPegawaiTemp = explode(",", $arrayRows);
            $arrayIdPegawai = [];
            for ($an = 0; $an < count($arrayIdPegawaiTemp); $an++) {
                $modelPenghasilan = MasterPenghasilan::find()->where(['penghasilanId' => $arrayIdPegawaiTemp[$an]])->one();
                $arrayIdPegawai[] = $modelPenghasilan['pegawaiId'];
            }
            $arrayIdPegawai = array_unique($arrayIdPegawai);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT penghasilanId, pegawaiId FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT penghasilanId, pegawaiId FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun' AND userId = '$userId'")->queryAll();
            }
            $arrayIdPegawai = array_column($getId, 'pegawaiId');
        }

        $arrayIdPegawai = array_unique($arrayIdPegawai);

        if (!isset($arrayIdPegawai[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . $bulan . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/master-penghasilan/index']);
        }

        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $bulan = \Yii::t('app', $bulan);
        $filename = 'Laporan Penghasilan Pegawai PPh 21 - Akumulasi' . $bulan . ' ' . $tahun . '.xls';

        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../master-penghasilan/index");

        echo "Masa Pajak\t";
        echo "Tahun Pajak\t";
        echo "Pembetulan\t";
        echo "NPWP\t";
        echo "Nama\t";
        echo "Kode Pajak\t";
        echo "Jumlah Bruto\t";
        echo "Jumlah PPh\t";
        echo "Kode Negara\t";
        echo "\r\n";

        for ($node = 0; $node < count($arrayIdPegawai); $node++) {
            $jumlahBruto = 0;
            $jumlahPph = 0;
            $getDataPenghasilan = Yii::$app->db->createCommand("SELECT masaPajak, tahunPajak, kodePajak, bruto, pph FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun' AND pegawaiId = '$arrayIdPegawai[$node]'")->queryAll();

            for ($en = 0; $en < count($getDataPenghasilan); $en++) {
                $jumlahBruto = $jumlahBruto + $getDataPenghasilan[$en]['bruto'];
                $jumlahPph = $jumlahPph + $getDataPenghasilan[$en]['pph'];
                $masaPajak = $getDataPenghasilan[$en]['masaPajak'];
                $tahunPajak = $getDataPenghasilan[$en]['tahunPajak'];
                $kodePajak = $getDataPenghasilan[$en]['kodePajak'];
            }

            $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $arrayIdPegawai[$node]])->one();
            $npwp = $modelPegawai['npwp'];
            $nama = $modelPegawai['nama'];
            $pembetulan = 0;

            echo $masaPajak . "\t";
            echo $tahunPajak . "\t";
            echo $pembetulan . "\t";
            echo $npwp . "\t";
            echo $nama . "\t";
            echo $kodePajak . "\t";
            echo $jumlahBruto . "\t";
            echo $jumlahPph . "\t";
            echo "" . "\t";
            echo "\r\n";

        }
        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download-pasal-21'] = $notification;
        if (!isset($session['notification-success-download-pasal-21'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakExcelPasal21Individu() {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $bulan = \Yii::t('app', $arrayMasa[0]);
        if (strlen($bulan) == 1) {
            $bulan = '0' . $bulan;
        }
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayIdPegawai = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT penghasilanId, pegawaiId FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT penghasilanId, pegawaiId FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun' AND userId = '$userId'")->queryAll();
            }
            $arrayIdPegawai = array_column($getId, 'penghasilanId');
//            $arraySort = ksort($array)
        }
//        echo '<pre>';
//        print_r($arrayIdPegawai);

        if (!isset($arrayIdPegawai[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . $bulan . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/master-penghasilan/index']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $bulan = \Yii::t('app', $bulan);
        $filename = 'Laporan Penghasilan Pegawai PPh 21 - Individu ' . $bulan . ' ' . $tahun . '.xls';
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../master-penghasilan/index");
        echo "Masa Pajak\t";
        echo "Tahun Pajak\t";
        echo "Pembetulan\t";
        echo "NPWP\t";
        echo "Nama\t";
        echo "Kode Pajak\t";
        echo "Jumlah Bruto\t";
        echo "Jumlah PPh\t";
        echo "Kode Negara\t";
        echo "Jenis Penghasilan";
        echo "\r\n";


        for ($node = 0; $node < count($arrayIdPegawai); $node++) {
            $modelData = MasterPenghasilan::find()->where(['penghasilanId' => $arrayIdPegawai[$node]])->one();
            $pegawaiId = $modelData->pegawaiId;
            $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $pegawaiId])->one();
            $bruto = $modelData->bruto;
            $pph = $modelData->pph;
            $pembetulan = 0;
            $npwp = $modelPegawai->npwp;
            $nama = $modelPegawai->nama;
            $kode_pajak = $modelData->kodePajak;
            $masaPajak = $modelData->masaPajak;
            $tahunPajak = $modelData->tahunPajak;
            $kodeNegara = "";
            $jenisPenghasilanId = $modelData->jenisPenghasilanId;
            if ($jenisPenghasilanId == null) {
                $jenisPenghasilanId = 1;
            }
            $createdById = $modelData->created_by;
            $modelUser = User::find()->where(['id' => $createdById])->one();
            $dibuatOleh = ucfirst($modelUser->username);
            $modelJP = PphJenisPenghasilan::find()->where(['jenisPenghasilanId' => $jenisPenghasilanId])->one();
            $jenisPenghasilan = $modelJP->nama;
            echo $masaPajak . "\t";
            echo $tahunPajak . "\t";
            echo $pembetulan . "\t";
            echo $npwp . "\t";
            echo $nama . "\t";
            echo $kode_pajak . "\t";
            echo $bruto . "\t";
            echo $pph . "\t";
            echo $kodeNegara . "\t";            
            echo $jenisPenghasilan . "\t";
            echo $dibuatOleh;
            echo "\r\n";
        }

        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download-pasal-21'] = $notification;
        if (!isset($session['notification-success-download-pasal-21'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakExcelPasal23($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 3;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        $arrayId = [];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }

        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
//        echo '<pre>';
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $tempDate = DateTime::createFromFormat('Y-m-d', $downloadedTime)->format('d/m/Y');
        $bulan = \Yii::t('app', $idBulan);
        $filename = 'Bukti Potong PPh Pasal 23 ' . $bulan . ' ' . $tahun . '.xls';
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index-pasal-23");
        echo "Kode Form Bukti Potong\t";
        echo "Masa Pajak\t";
        echo "Tahun Pajak\t";
        echo "Pembetulan\t";
        echo "NPWP WP yang Dipotong\t";
        echo "Nama WP yang Dipotong\t";
        echo "Alamat WP yang Dipotong\t";
        echo "Nomor Bukti Potong\t";
        echo "Tanggal Bukti Potong\t";
        echo "Nilai Bruto 1\t";
        echo "Tarif 1\t";
        echo "PPh yang Dipotong 1\t";
        echo "Nilai Bruto 2\t";
        echo "Tarif 2\t";
        echo "PPh Yang Dipotong 2\t";
        echo "Nilai Bruto 3\t";
        echo "Tarif 3\t";
        echo "PPh Yang Dipotong  3\t";
        echo "Nilai Bruto 4\t";
        echo "Tarif 4\t";
        echo "PPh Yang Dipotong 4\t";
        echo "Nilai Bruto 5\t";
        echo "Tarif 5\t";
        echo "PPh Yang Dipotong 5\t";
        echo "Nilai Bruto 6a/Nilai Bruto 6\t";
        echo "Tarif 6a/Tarif 6\t";
        echo "PPh Yang Dipotong 6a/PPh Yang Dipotong 6\t";
        echo "Nilai Bruto 6b/Nilai Bruto 7\t";
        echo "Tarif 6b/Tarif 7\t";
        echo "PPh Yang Dipotong 6b/PPh Yang Dipotong 7\t";
        echo "Nilai Bruto 6c/Nilai Bruto 8\t";
        echo "Tarif 6c/Tarif 8\t";
        echo "PPh Yang Dipotong 6c/PPh Yang Dipotong 8\t";
        echo "Nilai Bruto 9\t";
        echo "Tarif 9\t";
        echo "PPh Yang Dipotong 9\t";
        echo "Nilai Bruto 10\t";
        echo "Perkiraan Penghasilan Netto10\t";
        echo "Tarif 10\t";
        echo "PPh Yang Dipotong 10\t";
        echo "Nilai Bruto 11\t";
        echo "Perkiraan Penghasilan Netto11\t";
        echo "Tarif 11\t";
        echo "PPh Yang Dipotong 11\t";
        echo "Nilai Bruto 12\t";
        echo "Perkiraan Penghasilan Netto12\t";
        echo "Tarif 12\t";
        echo "PPh Yang Dipotong 12\t";
        echo "Nilai Bruto 13\t";
        echo "Tarif 13\t";
        echo "PPh Yang Dipotong 13\t";
        echo "Kode Jasa 6d1 PMK-244/PMK.03/2008\t";
        echo "Nilai Bruto 6d1\t";
        echo "Tarif 6d1\t";
        echo "PPh Yang Dipotong 6d1\t";
        echo "Kode Jasa 6d2 PMK-244/PMK.03/2008\t";
        echo "Nilai Bruto 6d2\t";
        echo "Tarif 6d2\t";
        echo "PPh Yang Dipotong 6d2\t";
        echo "Kode Jasa 6d3 PMK-244/PMK.03/2008\t";
        echo "Nilai Bruto 6d3\t";
        echo "Tarif 6d3\t";
        echo "PPh Yang Dipotong 6d3\t";
        echo "Kode Jasa 6d4 PMK-244/PMK.03/2008\t";
        echo "Nilai Bruto 6d4\t";
        echo "Tarif 6d4\t";
        echo "PPh Yang Dipotong 6d4\t";
        echo "Kode Jasa 6d5 PMK-244/PMK.03/2008\t";
        echo "Nilai Bruto 6d5\t";
        echo "Tarif 6d5\t";
        echo "PPh Yang Dipotong 6d5\t";
        echo "Kode Jasa 6d6 PMK-244/PMK.03/2008\t";
        echo "Nilai Bruto 6d6\t";
        echo "Tarif 6d6\t";
        echo "PPh Yang Dipotong 6d6\t";
        echo "Jumlah Nilai Bruto\t";
        echo "Jumlah PPh Yang Dipotong\t";
        echo "Jenis Bukti Potong\t";
        echo "Dibuat Oleh" . "\t";
        echo "Nomor Pembukuan";
        echo "\r\n";
        $tarif = '';

        for ($node = 0; $node < count($arrayId); $node++) {
            $kodeForm = 'F113316';
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            $statusWpId = $modelData->statusWpId;
            $wpId = $modelData->wajibPajakId;
            $remove_character = array('\n', '\r\n', '\r');
            if ($wpId != NULL) {
                $npwpWp = $modelVendor->npwp;
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
            } else {
                $npwpWp = '000000000000000';
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
            }

            if ($statusWpId == 3) {
                $NB6d1 = $modelData->jumlahBruto;
                $PPhDP6D1 = $modelData->jumlahPphDiPotong;
                $NB5 = '0';
                $Tarif5 = '2';
                $PPhDP5 = '0';
                if ($wpId != NULL) {
                    $Tarif6D1 = 2;
                } else {
                    $Tarif6D1 = 4;
                }
                $jumlahBruto = $NB6d1;
                $jumlahPph = $PPhDP6D1;
            } else {
                $NB6d1 = '0';
                $PPhDP6D1 = '0';
                $Tarif6D1 = '2';
                $NB5 = $modelData->jumlahBruto;
                $PPhDP5 = $modelData->jumlahPphDiPotong;
                if ($wpId != NULL) {
                    $Tarif5 = 2;
                } else {
                    $Tarif5 = 4;
                }
                $jumlahBruto = $NB5;
                $jumlahPph = $PPhDP5;
            }

            $noBp = $modelData->nomorBuktiPotong;
            $tanggalPungut = $modelData->tanggal;
            $NB1 = '0';
            $Tarif1 = '15';
            $PPhDP1 = '0';
            $NB2 = '0';
            $Tarif2 = '15';
            $PPhDP2 = '0';
            $NB3 = '0';
            $Tarif3 = '15';
            $PPhDP3 = '0';
            $NB4 = '0';
            $Tarif4 = '15';
            $PPhDP4 = '0';
            $NB5 = $NB5;
            $Tarif5 = $Tarif5;
            $PPhDP5 = $PPhDP5;
            $NB6a = '0';
            $Tarif6a = '2';
            $PPhDP6a = '0';
            $NB6b = '0';
            $Tarif6b = '2';
            $PPhDP6b = '0';
            $NB6c = '0';
            $Tarif6c = '2';
            $PPhDP6c = '0';
            $NB9 = '';
            $Tarif9 = '';
            $PPhDP9 = '';
            $NB10 = '';
            $Net10 = '';
            $Tarif10 = '';
            $PPhDP10 = '';
            $NB11 = '';
            $Net11 = '';
            $Tarif11 = '';
            $PPhDP11 = '';
            $NB12 = '';
            $Net12 = '';
            $Tarif12 = '';
            $PPhDP12 = '';
            $NB13 = '';
            $Tarif13 = '';
            $PPhDP13 = '';
            $KodeJasa6d1 = '0';
            $NB6d1 = $NB6d1;
            $Tarif6D1 = $Tarif6D1;
            $PPhDP6d1 = $PPhDP6D1;
            $KodeJasa6d2 = '0';
            $NB6d2 = '0';
            $Tarif6D2 = '2';
            $PPhDP6d2 = '0';
            $KodeJasa6d3 = '0';
            $NB6d3 = '0';
            $Tarif6D3 = '2';
            $PPhDP6d3 = '0';
            $KodeJasa6d4 = '0';
            $NB6d4 = '0';
            $Tarif6D4 = '2';
            $PPhDP6d4 = '0';
            $KodeJasa6d5 = '0';
            $NB6d5 = '0';
            $Tarif6D5 = '2';
            $PPhDP6d5 = '0';
            $KodeJasa6d6 = '0';
            $NB6d6 = '0';
            $Tarif6D6 = '2';
            $PPhDP6d6 = '0';
            $jumlahBruto = $jumlahBruto;
            $jumlahPph = $jumlahPph;
            $createdById = $modelData->created_by;
            $nomorPembukuan = $modelData->nomorPembukuan;
            $modelUser = User::find()->where(['id' => $createdById])->one();
            $dibuatOleh = ucfirst($modelUser->username);
            if ($modelData->statusWpId == 3) {
                $jenisBp = 'Jasa';
            } else {
                $jenisBp = 'Sewa';
            }

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalPungut)->format('d/m/Y');
            echo $kodeForm . "\t";
            echo $masaPajak . "\t";
            echo $tahunPajak . "\t";
            echo $pembetulan . "\t";
            echo $npwpWp . "\t";
            echo $namaWp . "\t";
            echo $alamatWp . "\t";
            echo $noBp . "\t";
            echo $tanggal . "\t";
            echo $NB1 . "\t";
            echo $Tarif1 . "\t";
            echo $PPhDP1 . "\t";
            echo $NB2 . "\t";
            echo $Tarif2 . "\t";
            echo $PPhDP2 . "\t";
            echo $NB3 . "\t";
            echo $Tarif3 . "\t";
            echo $PPhDP3 . "\t";
            echo $NB4 . "\t";
            echo $Tarif4 . "\t";
            echo $PPhDP4 . "\t";
            echo $NB5 . "\t";
            echo $Tarif5 . "\t";
            echo $PPhDP5 . "\t";
            echo $NB6a . "\t";
            echo $Tarif6a . "\t";
            echo $PPhDP6a . "\t";
            echo $NB6b . "\t";
            echo $Tarif6b . "\t";
            echo $PPhDP6b . "\t";
            echo $NB6c . "\t";
            echo $Tarif6c . "\t";
            echo $PPhDP6c . "\t";
            echo $NB9 . "\t";
            echo $Tarif9 . "\t";
            echo $PPhDP9 . "\t";
            echo $NB10 . "\t";
            echo $Net10 . "\t";
            echo $Tarif10 . "\t";
            echo $PPhDP10 . "\t";
            echo $NB11 . "\t";
            echo $Net11 . "\t";
            echo $Tarif11 . "\t";
            echo $PPhDP11 . "\t";
            echo $NB12 . "\t";
            echo $Net12 . "\t";
            echo $Tarif12 . "\t";
            echo $PPhDP12 . "\t";
            echo $NB13 . "\t";
            echo $Tarif13 . "\t";
            echo $PPhDP13 . "\t";
            echo $KodeJasa6d1 . "\t";
            echo $NB6d1 . "\t";
            echo $Tarif6D1 . "\t";
            echo $PPhDP6d1 . "\t";
            echo $KodeJasa6d2 . "\t";
            echo $NB6d2 . "\t";
            echo $Tarif6D2 . "\t";
            echo $PPhDP6d2 . "\t";
            echo $KodeJasa6d3 . "\t";
            echo $NB6d3 . "\t";
            echo $Tarif6D3 . "\t";
            echo $PPhDP6d3 . "\t";
            echo $KodeJasa6d4 . "\t";
            echo $NB6d4 . "\t";
            echo $Tarif6D4 . "\t";
            echo $PPhDP6d4 . "\t";
            echo $KodeJasa6d5 . "\t";
            echo $NB6d5 . "\t";
            echo $Tarif6D5 . "\t";
            echo $PPhDP6d5 . "\t";
            echo $KodeJasa6d6 . "\t";
            echo $NB6d6 . "\t";
            echo $Tarif6D6 . "\t";
            echo $PPhDP6d6 . "\t";
            echo $jumlahBruto . "\t";
            echo $jumlahPph . "\t";
            echo $jenisBp . "\t";
            echo $dibuatOleh . "\t";
            echo $nomorPembukuan;
            echo "\r\n";
        }
//        die();
        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakExcelPasal21Ptt($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 5;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        $arrayId = [];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }

        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $tempDate = DateTime::createFromFormat('Y-m-d', $downloadedTime)->format('d/m/Y');
        $bulan = \Yii::t('app', $idBulan);
        
        $filename = 'Bukti Potong PPh Pasal 21 - PTT ' . $bulan . ' ' . $tahun . '.xls';
       header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index-pasal-21");
        echo "Masa Pajak\t";
        echo "Tahun Pajak\t";
        echo "Pembetulan\t";
        echo "Nomor Bukti Potong\t";
        echo "NPWP\t";
        echo "NIK\t";
        echo "Nama\t";
        echo "Alamat\t";
        echo "WP Luar Negeri\t";
        echo "Kode Negara\t";
        echo "Kode Pajak\t";
        echo "Jumlah Bruto\t";
        echo "Jumlah DPP\t";
        echo "Tanpa NPWP\t";
        echo "Tarif\t";
        echo "Jumlah PPh\t";
        echo "NPWP Pemotong\t";
        echo "Nama Pemotong\t";
        echo "Tanggal Bukti Potong";
        echo "\r\n";
        
        for ($node = 0; $node < count($arrayId); $node++) {
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = MasterMandor::find()->where(['mandorId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            $tarif = $modelData->tarif;
            if ($modelData->wajibPajakId != null) {
                $npwpWp = preg_replace("/[^0-9]/", "", $modelVendor->npwp);
                $nik = preg_replace("/[^0-9]/", "", $modelVendor->nik);
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
                $tanpaNpwp = 'N';
            } else {
                $npwpWp = '000000000000000';
                $nonId = $modelData->wajibPajakNonId;
                $modelNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $nonId])->one();
                $nik = $modelNon->nik;
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
                $tanpaNpwp = 'Y';
            }

            $arrayUserId = [];
            for ($ax = 0; $ax < count($arrayId); $ax++) {
                $bid = $arrayId[$ax];
                $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
                $uid = $modelBP->userId;
                $arrayUserId[$ax] = $uid;
            }

            $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$node]])->asArray()->one();
            $statusPemungutPajakPtt = $dataSetting['pasal21_ptt'];
            
            if ($statusPemungutPajakPtt == 0) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 1) {
                $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
            } else {
                $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
            }

            $namaPemungut = $dataSettingPP['namaPemungutPajak'];
            $wpLuarNegeri = 'N';
            $kodeNegara = '';
            if($modelData->statusKerjaId == 2){
                $kodePajak = '21-100-03';
            } else {
                $kodePajak = '21-100-09';
            }
            $noBp = $modelData->nomorBuktiPotong;
            $tanggalBp = $modelData->tanggal;
            $jumlahBruto = $modelData->jumlahBruto;
            $jumlahDpp = $modelData->dpp;
            $pph = $modelData->jumlahPphDiPotong;
            $createdById = $modelData->created_by;
            $modelUser = User::find()->where(['id' => $createdById])->one();
            $dibuatOleh = ucfirst($modelUser->username);
            $nomorPembukuan = $modelData->nomorPembukuan;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalBp)->format('d/m/Y');
            echo $masaPajak . "\t";
            echo $tahunPajak . "\t";
            echo $pembetulan . "\t";
            echo $noBp . "\t";
            echo $npwpWp . "\t";
            echo $nik . "\t";
            echo $namaWp . "\t";
            echo $alamatWp . "\t";
            echo $wpLuarNegeri . "\t";
            echo $kodeNegara . "\t";
            echo $kodePajak . "\t";
            echo $jumlahBruto . "\t";
            echo $jumlahDpp . "\t";
            echo $tanpaNpwp . "\t";
            echo $tarif . "\t";
            echo $pph . "\t";
            echo $npwpPemungut . "\t";
            echo $namaPemungut . "\t";
            echo $tanggal;
            echo "\r\n";
        }

        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakExcel($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 2;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        $arrayId = [];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }

        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $tempDate = DateTime::createFromFormat('Y-m-d', $downloadedTime)->format('d/m/Y');
        $bulan = \Yii::t('app', $idBulan);
        $filename = 'Bukti Potong PPh Pasal 22 ' . $bulan . ' ' . $tahun . '.xls';
//        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index");
        echo "Kode Form\t";
        echo "Masa Pajak\t";
        echo "Tahun Pajak\t";
        echo "Pembetulan\t";
        echo "NPWP WP\t";
        echo "Nama WP\t";
        echo "Alamat WP\t";
        echo "No. BP\t";
        echo "Tanggal Pungut\t";
        echo "B13\t";
        echo "B15\t";
        echo "B16\t";
        echo "B23\t";
        echo "B25\t";
        echo "B26\t";
        echo "B33\t";
        echo "B35\t";
        echo "B36\t";
        echo "B43\t";
        echo "B45\t";
        echo "B46\t";
        echo "B52\t";
        echo "B53\t";
        echo "B55\t";
        echo "B56\t";
        echo "B62\t";
        echo "B63\t";
        echo "B65\t";
        echo "B66\t";
        echo "B72\t";
        echo "B73\t";
        echo "B75\t";
        echo "B76\t";
        echo "B82\t";
        echo "B83\t";
        echo "B85\t";
        echo "B86\t";
        echo "B92\t";
        echo "B93\t";
        echo "B95\t";
        echo "B96\t";
        echo "B102\t";
        echo "B103\t";
        echo "B105\t";
        echo "B106\t";
        echo "B112\t";
        echo "B113\t";
        echo "B115\t";
        echo "B116\t";
        echo "Jml. Bruto\t";
        echo "Jml. PPh\t";
        echo "Dibuat Oleh\t";
        echo "Nomor Pembukuan";
        echo "\r\n";
        $tarif = '';

        for ($node = 0; $node < count($arrayId); $node++) {
            $kodeForm = 'F113304A';
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            if ($modelData->wajibPajakId != null) {
                $npwpWp = preg_replace("/[^0-9]/", "", $modelVendor->npwp);
                $idStatusNpwp = $modelVendor->statusNpwpId;
                $modelStatusNpwp = PphStatusNpwp::find()->where(['npwpId' => $idStatusNpwp])->one();
                $idTarif = $modelStatusNpwp->tarifId;
                $modelTarif = PphTarif::find()->where(['tarifId' => $idTarif])->one();
                $tarif = $modelTarif->tarif;
            } else {
                $npwpWp = '000000000000000';
                $modelTarif = PphTarif::find()->where(['tarifId' => 2])->one();
                $tarif = $modelTarif->tarif;
            }
            if ($modelData->wajibPajakId != null) {
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
            } else {
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
            }

            $noBp = $modelData->nomorBuktiPotong;
            $tanggalPungut = $modelData->tanggal;
            $b13 = 0;
            $b15 = '0.25';
            $b16 = 0;
            $b23 = 0;
            $b25 = '0.1';
            $b26 = 0;
            $b33 = 0;
            $b35 = '0.3';
            $b36 = 0;
            $b43 = 0;
            $b45 = '0.45';
            $b46 = 0;
            $b52 = '';
            $b53 = 0;
            $b55 = 0;
            $b56 = 0;
            $b62 = '';
            $b63 = 0;
            $b65 = 0;
            $b66 = 0;
            $b72 = '';
            $b73 = 0;
            $b75 = 0;
            $b76 = 0;
            $b82 = '';
            $b83 = 0;
            $b85 = $tarif; //prediksi tarif
            $b86 = 0;
            $b92 = '';
            $b93 = 0;
            $b95 = $tarif; //prediksi tarif
            $b96 = 0;
            $b102 = 'BUMN TERTENTU';
            $b103 = $modelData->jumlahBruto;
            $b105 = $tarif; //prediksi tarif
            $b106 = $modelData->jumlahPphDiPotong;
            $b112 = '';
            $b113 = 0;
            $b115 = 0;
            $b116 = 0;
            $jumlahBruto = $modelData->jumlahBruto;
            $jumlahPph = $modelData->jumlahPphDiPotong;
            $nomorPembukuan = $modelData->nomorPembukuan;

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $createdById = $modelData->created_by;
            $modelUser = User::find()->where(['id' => $createdById])->one();
            $dibuatOleh = ucfirst($modelUser->username);
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalPungut)->format('d/m/Y');
            echo $kodeForm . "\t";
            echo $masaPajak . "\t";
            echo $tahunPajak . "\t";
            echo $pembetulan . "\t";
            echo $npwpWp . "\t";
            echo $namaWp . "\t";
            echo $alamatWp . "\t";
            echo $noBp . "\t";
            echo $tanggal . "\t";
            echo $b13 . "\t";
            echo $b15 . "\t";
            echo $b16 . "\t";
            echo $b23 . "\t";
            echo $b25 . "\t";
            echo $b26 . "\t";
            echo $b33 . "\t";
            echo $b35 . "\t";
            echo $b36 . "\t";
            echo $b43 . "\t";
            echo $b45 . "\t";
            echo $b46 . "\t";
            echo $b52 . "\t";
            echo $b53 . "\t";
            echo $b55 . "\t";
            echo $b56 . "\t";
            echo $b62 . "\t";
            echo $b63 . "\t";
            echo $b65 . "\t";
            echo $b66 . "\t";
            echo $b72 . "\t";
            echo $b73 . "\t";
            echo $b75 . "\t";
            echo $b76 . "\t";
            echo $b82 . "\t";
            echo $b83 . "\t";
            echo $b85 . "\t";
            echo $b86 . "\t";
            echo $b92 . "\t";
            echo $b93 . "\t";
            echo $b95 . "\t";
            echo $b96 . "\t";
            echo $b102 . "\t";
            echo $b103 . "\t";
            echo $b105 . "\t";
            echo $b106 . "\t";
            echo $b112 . "\t";
            echo $b113 . "\t";
            echo $b115 . "\t";
            echo $b116 . "\t";
            echo $jumlahBruto . "\t";
            echo $jumlahPph . "\t";
            echo $dibuatOleh . "\t";
            echo $nomorPembukuan;
            echo "\r\n";
        }

        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakCsvPasal4($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 4;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
//        die('Stoplah');
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index-pasal-4']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $bulan = \Yii::t('app', $idBulan);
        $filename = 'Bukti Potong PPh Pasal 4 Ayat (2) ' . $bulan . ' ' . $tahun . '.csv';
//        echo '<pre>';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index-pasal-23");

        $modelUser = User::find()->where(['id' => $userId])->one();
        $delimiter = $modelUser->separatedBy;

        $quote = '';
        echo $e_header = 'Kode Form Bukti Potong / Kode Form Input PPh Yang Dibayar Sendiri' . $delimiter . 'Masa Pajak' . $delimiter . 'Tahun Pajak' . $delimiter . 'Pembetulan' . $delimiter . 'NPWP WP yang Dipotong' . $delimiter . 'Nama WP yang Dipotong' . $delimiter . 'Alamat WP yang Dipotong' . $delimiter . 'Nomor Bukti Potong / NTPN' . $delimiter . 'Tanggal Bukti Potong/Tanggal SSP' . $delimiter . 'Jenis Hadiah Undian 1 / Lokasi Tanah dan atau Bangunan / Nama Obligasi' . $delimiter . 'Kode Option Tempat Penyimpanan 1 (Khusus F113310)' . $delimiter . 'Jumlah Nilai Bruto 1 / Jumlah Nilai Nominal Obligasi Yg Diperdagangkan Di Bursa Efek / Jumlah Penghasilan Pada Form Input Yang Dibayar Sendiri' . $delimiter . 'Tarif 1 / Tingkat Bunga per Tahun' . $delimiter . 'PPh Yang Dipotong  1 /Jumlah PPh Pada Form Input Yang Dibayar Sendiri' . $delimiter . 'Jenis Hadiah Undian 2 / Nomor Seri Obligasi ' . $delimiter . 'Kode Option Tempat Penyimpanan 2' . $delimiter . 'Jumlah Nilai Bruto 2 / Jumlah Harga Perolehan Bersih (tanpa Bunga) Pada Obligasi Yg Diperdagangkan Di Bursa Efek' . $delimiter . 'Tarif 2' . $delimiter . 'PPh Yang Dipotong  2' . $delimiter . 'Jenis Hadiah Undian 3' . $delimiter . 'Kode Option Tempat Penyimpanan 3' . $delimiter . 'Jumlah Nilai Bruto 3 / Jumlah Harga Penjualan Bersih (tanpa Bunga) Pada Obligasi Yg Diperdagangkan Di Bursa Efek' . $delimiter . 'Tarif 3' . $delimiter . 'PPh Yang Dipotong 3' . $delimiter . 'Jenis Hadiah Undian 4' . $delimiter . 'Kode Option Tempat Penyimpanan 4 / Kode Option Perencanaan (1) atau Pengawasan (2) atau selainnya (0) untuk BP Jasa Konstruksi poin 4' . $delimiter . 'Jumlah Nilai Bruto 4 / Jumlah Diskonto Pada Obligasi Yg Diperdagangkan Di Bursa Efek' . $delimiter . 'Tarif 4' . $delimiter . 'PPh Yang Dipotong  4' . $delimiter . 'Jenis Hadiah Undian 5' . $delimiter . 'Kode Option Tempat Penyimpanan 5 / Kode Option Perencanaan (1) atau Pengawasan (2) atau selainnya (0) untuk BP Jasa Konstruksi poin 5' . $delimiter . 'Jumlah Nilai Bruto 5 / Jumlah Bunga Pada Obligasi Yg Diperdagangkan Di Bursa Efek' . $delimiter . 'Tarif 5' . $delimiter . 'PPh Yang Dipotong  5' . $delimiter . 'Jenis Hadiah Undian 6' . $delimiter . 'Jumlah Nilai Bruto 6 / Jumlah Total Bunga atau Diskonto Obligasi Yang Diperdagangkan' . $delimiter . 'Tarif 6 / Tarif PPh Final Pada Obligasi Yang Diperdagangkan Di Bursa Efek' . $delimiter . 'PPh Yang Dipotong  6' . $delimiter . 'Jumlah Nilai Bruto 7' . $delimiter . 'Tarif 7' . $delimiter . 'PPh Yang Dipotong 7' . $delimiter . 'Jenis Penghasilan 8' . $delimiter . 'Jumlah Nilai Bruto 8' . $delimiter . 'Tarif 8' . $delimiter . 'PPh Yang Dipotong 8' . $delimiter . 'Jumlah PPh Yang Dipotong' . $delimiter . 'Tanggal Jatuh Tempo Obligasi' . $delimiter . 'Tanggal Perolehan Obligasi' . $delimiter . 'Tanggal Penjualan Obligasi' . $delimiter . 'Holding Periode Obligasi (Hari)' . $delimiter . 'Time Periode Obligasi (Hari)' . "\r\n";
        for ($node = 0; $node < count($arrayId); $node++) {            
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            $statusWpId = $modelData->statusWpId;
            $wpId = $modelData->wajibPajakId;
            if($modelVendor == null){
                $npwpWp = '000000000000000';
            } else {
                $npwpWp = $modelVendor->npwp;
            }
            if ($statusWpId == 3) {
                $kodeForm = 'F113316';
                $iujkId = $modelVendor->iujkId;
                $lokasi = '';
                if ($iujkId != NULL) {
                    $modelIujk = PphIujk::find()->where(['iujkId' => $iujkId])->one();
                    $is_approve = $modelIujk->is_approve;
                    if ($is_approve == 1) {
                        $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                        if ($tingkatKeuanganWp == 1) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 6])->one();
                            $tarif = $modelTarif->tarif;
                            $jumlahBruto1 = $modelData->jumlahBruto;
                            $tarif1 = $tarif;
                            $jumlahPphDipotong1 = $modelData->jumlahPphDiPotong;
                            $jumlahBruto2 = '0';
                            $tarif2 = '4';
                            $jumlahPphDipotong2 = '0';
                            $jumlahBruto3 = '0';
                            $tarif3 = '3';
                            $jumlahPphDipotong3 = '0';
                            $jumlahBruto4 = '0';
                            $tarif4 = '4';
                            $pphDipotong4 = '0';
                            $jumlahBruto5 = '0';
                            $tarif5 = '6';
                            $pphDipotong5 = '0';
//                            $jumlahPphDipotong = $jumlahPphDipotong3;
                        } else if ($tingkatKeuanganWp == 2) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                            $tarif = $modelTarif->tarif;
                            $jumlahBruto1 = '0';
                            $tarif1 = '2';
                            $jumlahPphDipotong1 = '0';
                            $jumlahBruto2 = '0';
                            $tarif2 = $tarif;
                            $jumlahPphDipotong2 = '0';
                            $jumlahBruto3 = $modelData->jumlahBruto;
                            $tarif3 = '3';
                            $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                            $jumlahBruto4 = '0';
                            $tarif4 = '4';
                            $pphDipotong4 = '0';
                            $jumlahBruto5 = '0';
                            $tarif5 = '6';
                            $pphDipotong5 = '0';
//                            $jumlahPphDipotong = $jumlahPphDipotong3;
                        } else if ($tingkatKeuanganWp == 3) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                            $tarif = $modelTarif->tarif;
                            $jumlahBruto1 = '0';
                            $tarif1 = '2';
                            $jumlahPphDipotong1 = '0';
                            $jumlahBruto2 = '0';
                            $tarif2 = $tarif;
                            $jumlahPphDipotong2 = '0';
                            $jumlahBruto3 = $modelData->jumlahBruto;
                            $tarif3 = '3';
                            $jumlahPphDipotong3 = $modelData->jumlahPphDiPotong;
                            $jumlahBruto4 = '0';
                            $tarif4 = '4';
                            $pphDipotong4 = '0';
                            $jumlahBruto5 = '0';
                            $tarif5 = '6';
                            $pphDipotong5 = '0';
//                            $jumlahPphDipotong = $jumlahPphDipotong3;
                        }
                    } else {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                        $tarif = $modelTarif->tarif;
                        $jumlahBruto1 = '0';
                        $tarif1 = '2';
                        $jumlahPphDipotong1 = '0';
                        $jumlahBruto2 = $modelData->jumlahBruto;
                        $tarif2 = '4';
                        $jumlahPphDipotong2 = $modelData->jumlahPphDiPotong;
                        $jumlahBruto3 = '0';
                        $tarif3 = $tarif;
                        $jumlahPphDipotong3 = '0';
                        $jumlahBruto4 = '0';
                        $tarif4 = '4';
                        $pphDipotong4 = '0';
                        $jumlahBruto5 = '0';
                        $tarif5 = '6';
                        $pphDipotong5 = '0';
                    }
                } else {                    
                    $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                    $tarif = $modelTarif->tarif;
                    $jumlahBruto1 = '0';
                    $tarif1 = '2';
                    $jumlahPphDipotong1 = '0';
                    $jumlahBruto2 = $modelData->jumlahBruto;
                    $tarif2 = '4';
                    $jumlahPphDipotong2 = $modelData->jumlahPphDiPotong;
                    $jumlahBruto3 = '0';
                    $tarif3 = $tarif;
                    $jumlahPphDipotong3 = '0';
                    $jumlahBruto4 = '0';
                    $tarif4 = '4';
                    $pphDipotong4 = '0';
                    $jumlahBruto5 = '0';
                    $tarif5 = '6';
                    $pphDipotong5 = '0';
                }
            } else if($statusWpId == 2){
                $kodeForm = 'F113312';
                $modelTarif = PphTarif::find()->where(['tarifId' => 3])->one();
                $tarif = $modelTarif->tarif;
                $jumlahBruto1 = $modelData->jumlahBruto;
                $tarif1 = $tarif;
                $jumlahPphDipotong1 = $modelData->jumlahPphDiPotong;
                $jumlahBruto2 = '0';
                $tarif2 = '4';
                $jumlahPphDipotong2 = '0';
                $jumlahBruto3 = '0';
                $tarif3 = '3';
                $jumlahPphDipotong3 = '0';
                $lokasi = $modelData->lokasi_tanah;
                $jumlahBruto4 = '0';
                $tarif4 = '4';
                $pphDipotong4 = '0';
                $jumlahBruto5 = '0';
                $tarif5 = '6';
                $pphDipotong5 = '0';
            } else {
                $bujpkId = $modelVendor->bujpkId;
                $kodeForm = 'F113316';
                if($bujpkId != null){
                    $modelBujpk = PphBujpk::find()->where(['bujpkId' => $bujpkId])->one();
                    $is_approve = $modelBujpk->is_approve;
                    if($is_approve == 1){
                        $modelTarif = PphTarif::find()->where(['tarifId' => 12])->one();
                        $tarif = '0';
                        $jumlahBruto1 = '0';
                        $tarif1 = '2';
                        $jumlahPphDipotong1 = '0';                        
                        $jumlahBruto2 = '0';
                        $tarif2 = '4';
                        $jumlahPphDipotong2 = '0';
                        $jumlahBruto3 = '0';
                        $tarif3 = '3';
                        $jumlahPphDipotong3 = '0';
                        $jumlahBruto4 = $modelData->jumlahBruto;
                        $tarif4 = $modelTarif->tarif;
                        $jumlahPphDipotong4 = $modelData->jumlahPphDiPotong;
                        $jumlahBruto5 = '0';
                        $tarif5 = '6';
                        $pphDipotong5 = '0';
                    } else {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 13])->one();
                        $tarif = '0';
                        $jumlahBruto1 = '0';
                        $tarif1 = '2';
                        $jumlahPphDipotong1 = '0';                        
                        $jumlahBruto2 = '0';
                        $tarif2 = '4';
                        $jumlahPphDipotong2 = '0';
                        $jumlahBruto3 = '0';
                        $tarif3 = '3';
                        $jumlahPphDipotong3 = '0';
                        $jumlahBruto4 = '0';
                        $tarif4 = '4';
                        $pphDipotong4 = '0';
                        $jumlahBruto5 = $modelData->jumlahBruto;
                        $tarif5 = $modelTarif->tarif;
                        $jumlahPphDipotong5 = $modelData->jumlahPphDiPotong;
                    }
                } else {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 13])->one();
                    $tarif = '0';
                    $jumlahBruto1 = '0';
                    $tarif1 = '2';
                    $jumlahPphDipotong1 = '0';                        
                    $jumlahBruto2 = '0';
                    $tarif2 = '4';
                    $jumlahPphDipotong2 = '0';
                    $jumlahBruto3 = '0';
                    $tarif3 = '3';
                    $jumlahPphDipotong3 = '0';
                    $jumlahBruto4 = '0';
                    $tarif4 = '4';
                    $pphDipotong4 = '0';                    
                    $jumlahBruto5 = $modelData->jumlahBruto;
                    $tarif5 = $modelTarif->tarif;
                    $jumlahPphDipotong5 = $modelData->jumlahPphDiPotong;
                }
            }

            $remove_character = array('\n', '\r\n', '\r');
            if ($modelData->wajibPajakId != null) {
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
            } else {
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
            }

            $noBp = $modelData->nomorBuktiPotong;
            $tanggalPungut = $modelData->tanggal;
            $JHU1 = $lokasi;
            $KOTP1 = '0';
            $jumlahBruto1 = $jumlahBruto1;
            $tarif1 = $tarif1;
            $pphDipotong1 = $jumlahPphDipotong1;
            $JHU2 = '';
            $KOTP2 = '0';
            $jumlahBruto2 = $jumlahBruto2;
            $tarif2 = $tarif2;
            $pphDipotong2 = $jumlahPphDipotong2;
            $JHU3 = '';
            $KOTP3 = '0';
            $jumlahBruto3 = $jumlahBruto3;
            $tarif3 = $tarif3;
            $pphDipotong3 = $jumlahPphDipotong3;
            $JHU4 = '';
            if($statusWpId == 3 || $statusWpId == 2){
                $KOTP4 = '0';
            } else {
                if($bujpkId != null){
                    $KOTP4 = '1';
                } else {
                    $KOTP4 = '0';
                }
            }
            $jumlahBruto4 = $jumlahBruto4;
            $tarif4 = $tarif4;
            $pphDipotong4 = $jumlahPphDipotong4;
            $JHU5 = '';
            if($statusWpId == 3 || $statusWpId == 2){
                $KOTP5 = '0';
            } else {
                if($bujpkId != null){
                    $KOTP5 = '0';
                } else {
                    $KOTP5 = '1';
                }
            }            
            $jumlahBruto5 = $jumlahBruto5;
            $tarif5 = $tarif5;
            $pphDipotong5 = $jumlahPphDipotong5;
            $JHU6 = '';
            $jumlahBruto6 = '0';
            $tarif6 = '0';
            $pphDipotong6 = '0';
            $jumlahBruto7 = '0';
            $tarif7 = '0';
            $pphDipotong7 = '0';
            $jenisPenghasilan8 = '';
            $jumlahBruto8 = '0';
            $tarif8 = '0';
            $pphDipotong8 = '0';
            $jumlahPphDipotong = $modelData->jumlahPphDiPotong;;
            $TJTO = '';
            $TPO = '';
            $TPenO = '';
            $HPO = '';
            $TimePO = '';

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalPungut)->format('d/m/Y');
            echo $bukti_potong_data = $quote . $kodeForm . $quote . $delimiter . $quote . $masaPajak . $quote . $delimiter . $quote . $tahunPajak . $quote . $delimiter . $quote . $pembetulan . $quote . $delimiter . $quote . $npwpWp . $quote . $delimiter . $quote . $namaWp . $quote . $delimiter . $quote . $alamatWp . $quote . $delimiter . $quote . $noBp . $quote . $delimiter . $quote . $tanggal . $quote . $delimiter . $quote . $JHU1 . $quote . $delimiter . $quote . $KOTP1 . $quote . $delimiter . $quote . $jumlahBruto1 . $quote . $delimiter . $quote . $tarif1 . $quote . $delimiter . $quote . $pphDipotong1 . $quote . $delimiter . $quote . $JHU2 . $quote . $delimiter . $quote . $KOTP2 . $quote . $delimiter . $quote . $jumlahBruto2 . $quote . $delimiter . $quote . $tarif2 . $quote . $delimiter . $quote . $pphDipotong2 . $quote . $delimiter . $quote . $JHU3 . $quote . $delimiter . $quote . $KOTP3 . $quote . $delimiter . $quote . $jumlahBruto3 . $quote . $delimiter . $quote . $tarif3 . $quote . $delimiter . $quote . $pphDipotong3 . $quote . $delimiter . $quote . $JHU4 . $quote . $delimiter . $quote . $KOTP4 . $quote . $delimiter . $quote . $jumlahBruto4 . $quote . $delimiter . $quote . $tarif4 . $quote . $delimiter . $quote . $pphDipotong4 . $quote . $delimiter . $quote . $JHU5 . $quote . $delimiter . $quote . $KOTP5 . $quote . $delimiter . $quote . $jumlahBruto5 . $quote . $delimiter . $quote . $tarif5 . $quote . $delimiter . $quote . $pphDipotong5 . $quote . $delimiter . $quote . $JHU6 . $quote . $delimiter . $quote . $jumlahBruto6 . $quote . $delimiter . $quote . $tarif6 . $quote . $delimiter . $quote . $pphDipotong6 . $quote . $delimiter . $quote . $jumlahBruto7 . $quote . $delimiter . $quote . $tarif7 . $quote . $delimiter . $quote . $pphDipotong7 . $quote . $delimiter . $quote . $jenisPenghasilan8 . $quote . $delimiter . $quote . $jumlahBruto8 . $quote . $delimiter . $quote . $tarif8 . $quote . $delimiter . $quote . $pphDipotong8 . $quote . $delimiter . $quote . $jumlahPphDipotong . $quote . $delimiter . $quote . $TJTO . $quote . $delimiter . $quote . $TPO . $quote . $delimiter . $quote . $TPenO . $quote . $delimiter . $quote . $HPO . $quote . $delimiter . $quote . $TimePO . "\r\n";
        }
//        die();
        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakCsvPasal23($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 3;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }

        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index-pasal-23']);
        }

        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $bulan = \Yii::t('app', $idBulan);
        $filename = 'Bukti Potong PPh Pasal 23 ' . $bulan . ' ' . $tahun . '.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index-pasal-23");
        
        $modelUser = User::find()->where(['id' => $userId])->one();
        $delimiter = $modelUser->separatedBy;

        $quote = '';
        echo $e_header = 'Kode Form Bukti Potong' .
        $delimiter . 'Masa Pajak' .
        $delimiter . 'Tahun Pajak' .
        $delimiter . 'Pembetulan' .
        $delimiter . 'NPWP WP yang Dipotong' .
        $delimiter . 'Nama WP yang Dipotong' .
        $delimiter . 'Alamat WP yang Dipotong' .
        $delimiter . 'Nomor Bukti Potong ' .
        $delimiter . 'Tanggal Bukti Potong' .
        $delimiter . 'Nilai Bruto 1' .
        $delimiter . 'Tarif 1' .
        $delimiter . 'PPh yang Dipotong 1' .
        $delimiter . 'Nilai Bruto 2' .
        $delimiter . 'Tarif 2' .
        $delimiter . 'PPh Yang Dipotong 2' .
        $delimiter . 'Nilai Bruto 3' .
        $delimiter . 'Tarif 3' .
        $delimiter . 'PPh Yang Dipotong 3' .
        $delimiter . 'Nilai Bruto 4' .
        $delimiter . 'Tarif 4' .
        $delimiter . 'PPh Yang Dipotong 4' .
        $delimiter . 'Nilai Bruto 5' .
        $delimiter . 'Tarif 5' .
        $delimiter . 'PPh Yang Dipotong 5' .
        $delimiter . 'Nilai Bruto 6a/Nilai Bruto 6' .
        $delimiter . 'Tarif 6a/Tarif 6' .
        $delimiter . 'PPh Yang Dipotong 6a/PPh Yang Dipotong 6' .
        $delimiter . 'Nilai Bruto 6b/Nilai Bruto 7' .
        $delimiter . 'Tarif 6b/Tarif 7' .
        $delimiter . 'PPh Yang Dipotong 6b/PPh Yang Dipotong 7' .
        $delimiter . 'Nilai Bruto 6c/Nilai Bruto 8' .
        $delimiter . 'Tarif 6c/Tarif 8' .
        $delimiter . 'PPh Yang Dipotong 6c/PPh Yang Dipotong 8' .
        $delimiter . 'Nilai Bruto 9' .
        $delimiter . 'Tarif 9' .
        $delimiter . 'PPh Yang Dipotong 9' .
        $delimiter . 'Nilai Bruto 10' .
        $delimiter . 'Perkiraan Penghasilan Netto10' .
        $delimiter . 'Tarif 10' .
        $delimiter . 'PPh Yang Dipotong 10' .
        $delimiter . 'Nilai Bruto 11' .
        $delimiter . 'Perkiraan Penghasilan Netto11' .
        $delimiter . 'Tarif 11' .
        $delimiter . 'PPh Yang Dipotong 11' .
        $delimiter . 'Nilai Bruto 12' .
        $delimiter . 'Perkiraan Penghasilan Netto12' .
        $delimiter . 'Tarif 12' .
        $delimiter . 'PPh Yang Dipotong 12' .
        $delimiter . 'Nilai Bruto 13' .
        $delimiter . 'Tarif 13' .
        $delimiter . 'PPh Yang Dipotong 13' .
        $delimiter . 'Kode Jasa 6d1 PMK-244/PMK.03/2008' .
        $delimiter . 'Nilai Bruto 6d1' .
        $delimiter . 'Tarif 6d1' .
        $delimiter . 'PPh Yang Dipotong 6d1' .
        $delimiter . 'Kode Jasa 6d2 PMK-244/PMK.03/2008' .
        $delimiter . 'Nilai Bruto 6d2' .
        $delimiter . 'Tarif 6d2' .
        $delimiter . 'PPh Yang Dipotong 6d2' .
        $delimiter . 'Kode Jasa 6d3 PMK-244/PMK.03/2008' .
        $delimiter . 'Nilai Bruto 6d3' .
        $delimiter . 'Tarif 6d3' .
        $delimiter . 'PPh Yang Dipotong 6d3' .
        $delimiter . 'Kode Jasa 6d4 PMK-244/PMK.03/2008' .
        $delimiter . 'Nilai Bruto 6d4' .
        $delimiter . 'Tarif 6d4' .
        $delimiter . 'PPh Yang Dipotong 6d4' .
        $delimiter . 'Kode Jasa 6d5 PMK-244/PMK.03/2008' .
        $delimiter . 'Nilai Bruto 6d5' .
        $delimiter . 'Tarif 6d5' .
        $delimiter . 'PPh Yang Dipotong 6d5' .
        $delimiter . 'Kode Jasa 6d6 PMK-244/PMK.03/2008' .
        $delimiter . 'Nilai Bruto 6d6' .
        $delimiter . 'Tarif 6d6' .
        $delimiter . 'PPh Yang Dipotong 6d6' .
        $delimiter . 'Jumlah Nilai Bruto' .
        $delimiter . 'Jumlah PPh Yang Dipotong' . "\r\n";

        for ($node = 0; $node < count($arrayId); $node++) {
            $kodeForm = 'F113306';
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            $statusWpId = $modelData->statusWpId;
            $wpId = $modelData->wajibPajakId;

            $remove_character = array('\n', '\r\n', '\r');
            if ($wpId != NULL) {
                $npwpWp = "'" . $modelVendor->npwp;
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
            } else {
                $npwpWp = "'000000000000000";
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
            }

            if ($statusWpId == 3) {
                $NB6d1 = $modelData->jumlahBruto;
                $PPhDP6D1 = $modelData->jumlahPphDiPotong;
                $NB5 = '0';
                $Tarif5 = '2';
                $PPhDP5 = '0';
                $kodeJasa6d1 = '12';
                if ($wpId != NULL) {
                    $Tarif6D1 = 2;
                } else {
                    $Tarif6D1 = 4;
                }
                $jumlahBruto = $NB6d1;
                $jumlahPph = $PPhDP6D1;
            } else {
                $NB6d1 = '0';
                $PPhDP6D1 = '0';
                $Tarif6D1 = '2';
                $NB5 = $modelData->jumlahBruto;
                $PPhDP5 = $modelData->jumlahPphDiPotong;
                $kodeJasa6d1 = '';
                if ($wpId != NULL) {
                    $Tarif5 = 2;
                } else {
                    $Tarif5 = 4;
                }
                $jumlahBruto = $NB5;
                $jumlahPph = $PPhDP5;
            }

            $noBp = $modelData->nomorBuktiPotong;
            $tanggalPungut = $modelData->tanggal;
            $NB1 = '0';
            $Tarif1 = '15';
            $PPhDP1 = '0';
            $NB2 = '0';
            $Tarif2 = '15';
            $PPhDP2 = '0';
            $NB3 = '0';
            $Tarif3 = '15';
            $PPhDP3 = '0';
            $NB4 = '0';
            $Tarif4 = '15';
            $PPhDP4 = '0';
            $NB5 = $NB5;
            $Tarif5 = $Tarif5;
            $PPhDP5 = $PPhDP5;
            $NB6a = '0';
            $Tarif6a = '2';
            $PPhDP6a = '0';
            $NB6b = '0';
            $Tarif6b = '2';
            $PPhDP6b = '0';
            $NB6c = '0';
            $Tarif6c = '2';
            $PPhDP6c = '0';
            $NB9 = '';
            $Tarif9 = '';
            $PPhDP9 = '';
            $NB10 = '';
            $Net10 = '';
            $Tarif10 = '';
            $PPhDP10 = '';
            $NB11 = '';
            $Net11 = '';
            $Tarif11 = '';
            $PPhDP11 = '';
            $NB12 = '';
            $Net12 = '';
            $Tarif12 = '';
            $PPhDP12 = '';
            $NB13 = '';
            $Tarif13 = '';
            $PPhDP13 = '';
//            $KodeJasa6d1 = $kodeJasa6d1;
            $NB6d1 = $NB6d1;
            $Tarif6D1 = $Tarif6D1;
            $PPhDP6d1 = $PPhDP6D1;
            $KodeJasa6d2 = '0';
            $NB6d2 = '0';
            $Tarif6D2 = '2';
            $PPhDP6d2 = '0';
            $KodeJasa6d3 = '0';
            $NB6d3 = '0';
            $Tarif6D3 = '0';
            $PPhDP6d3 = '0';
            $KodeJasa6d4 = '0';
            $NB6d4 = '0';
            $Tarif6D4 = '2';
            $PPhDP6d4 = '0';
            $KodeJasa6d5 = '0';
            $NB6d5 = '0';
            $Tarif6D5 = '2';
            $PPhDP6d5 = '0';
            $KodeJasa6d6 = '0';
            $NB6d6 = '0';
            $Tarif6D6 = '2';
            $PPhDP6d6 = '0';
            $jumlahBruto = $jumlahBruto;
            $jumlahPph = $jumlahPph;

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalPungut)->format('d/m/Y');
            echo $bukti_potong_data = $quote . $kodeForm . $quote .
            $delimiter . $quote . $masaPajak . $quote .
            $delimiter . $quote . $tahunPajak . $quote .
            $delimiter . $quote . $pembetulan . $quote .
            $delimiter . $quote . $npwpWp . $quote .
            $delimiter . $quote . $namaWp . $quote .
            $delimiter . $quote . $alamatWp . $quote .
            $delimiter . $quote . $noBp . $quote .
            $delimiter . $quote . $tanggal . $quote .
            $delimiter . $quote . $NB1 . $quote .
            $delimiter . $quote . $Tarif1 . $quote .
            $delimiter . $quote . $PPhDP1 . $quote .
            $delimiter . $quote . $NB2 . $quote .
            $delimiter . $quote . $Tarif2 . $quote .
            $delimiter . $quote . $PPhDP2 . $quote .
            $delimiter . $quote . $NB3 . $quote .
            $delimiter . $quote . $Tarif3 . $quote .
            $delimiter . $quote . $PPhDP3 . $quote .
            $delimiter . $quote . $NB4 . $quote .
            $delimiter . $quote . $Tarif4 . $quote .
            $delimiter . $quote . $PPhDP4 . $quote .
            $delimiter . $quote . $NB5 . $quote .
            $delimiter . $quote . $Tarif5 . $quote .
            $delimiter . $quote . $PPhDP5 . $quote .
            $delimiter . $quote . $NB6a . $quote .
            $delimiter . $quote . $Tarif6a . $quote .
            $delimiter . $quote . $PPhDP6a . $quote .
            $delimiter . $quote . $NB6b . $quote .
            $delimiter . $quote . $Tarif6b . $quote .
            $delimiter . $quote . $PPhDP6b . $quote .
            $delimiter . $quote . $NB6c . $quote .
            $delimiter . $quote . $Tarif6c . $quote .
            $delimiter . $quote . $PPhDP6c . $quote .
            $delimiter . $quote . $NB9 . $quote .
            $delimiter . $quote . $Tarif9 . $quote .
            $delimiter . $quote . $PPhDP9 . $quote .
            $delimiter . $quote . $NB10 . $quote .
            $delimiter . $quote . $Net10 . $quote .
            $delimiter . $quote . $Tarif10 . $quote .
            $delimiter . $quote . $PPhDP10 . $quote .
            $delimiter . $quote . $NB11 . $quote .
            $delimiter . $quote . $Net11 . $quote .
            $delimiter . $quote . $Tarif11 . $quote .
            $delimiter . $quote . $PPhDP11 . $quote .
            $delimiter . $quote . $NB12 . $quote .
            $delimiter . $quote . $Net12 . $quote .
            $delimiter . $quote . $Tarif12 . $quote .
            $delimiter . $quote . $PPhDP12 . $quote .
            $delimiter . $quote . $NB13 . $quote .
            $delimiter . $quote . $Tarif13 . $quote .
            $delimiter . $quote . $PPhDP13 . $quote .
            $delimiter . $quote . $kodeJasa6d1 . $quote .
            $delimiter . $quote . $NB6d1 . $quote .
            $delimiter . $quote . $Tarif6D1 . $quote .
            $delimiter . $quote . $PPhDP6d1 . $quote .
            $delimiter . $quote . $KodeJasa6d2 . $quote .
            $delimiter . $quote . $NB6d2 . $quote .
            $delimiter . $quote . $Tarif6D2 . $quote .
            $delimiter . $quote . $PPhDP6d2 . $quote .
            $delimiter . $quote . $KodeJasa6d3 . $quote .
            $delimiter . $quote . $NB6d3 . $quote .
            $delimiter . $quote . $Tarif6D3 . $quote .
            $delimiter . $quote . $PPhDP6d3 . $quote .
            $delimiter . $quote . $KodeJasa6d4 . $quote .
            $delimiter . $quote . $NB6d4 . $quote .
            $delimiter . $quote . $Tarif6D4 . $quote .
            $delimiter . $quote . $PPhDP6d4 . $quote .
            $delimiter . $quote . $KodeJasa6d5 . $quote .
            $delimiter . $quote . $NB6d5 . $quote .
            $delimiter . $quote . $Tarif6D5 . $quote .
            $delimiter . $quote . $PPhDP6d5 . $quote .
            $delimiter . $quote . $KodeJasa6d6 . $quote .
            $delimiter . $quote . $NB6d6 . $quote .
            $delimiter . $quote . $Tarif6D6 . $quote .
            $delimiter . $quote . $PPhDP6d6 . $quote .
            $delimiter . $quote . $jumlahBruto . $quote .
            $delimiter . $quote . $jumlahPph . "\r\n";
        }
//        die();

        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakCsvPasal21() {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $bulan = \Yii::t('app', $arrayMasa[0]);
        if (strlen($bulan) == 1) {
            $bulan = '0' . $bulan;
        }
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayIdPegawaiTemp = explode(",", $arrayRows);
            $arrayIdPegawai = [];
            for ($an = 0; $an < count($arrayIdPegawaiTemp); $an++) {
                $modelPenghasilan = MasterPenghasilan::find()->where(['penghasilanId' => $arrayIdPegawaiTemp[$an]])->one();
                $arrayIdPegawai[] = $modelPenghasilan['pegawaiId'];
            }
            $arrayIdPegawai = array_unique($arrayIdPegawai);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT penghasilanId, pegawaiId FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT penghasilanId, pegawaiId FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun' AND userId = '$userId'")->queryAll();
            }
            $arrayIdPegawai = array_column($getId, 'pegawaiId');
        }

        $arrayIdPegawai = array_unique($arrayIdPegawai);

        if (!isset($arrayIdPegawai[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . $bulan . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/master-penghasilan/index']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $bulan = \Yii::t('app', $bulan);
        $filename = 'Laporan PPh Pasal 21 ' . $bulan . ' ' . $tahun . '.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=index");
        $modelUser = User::find()->where(['id' => $userId])->one();
        $delimiter = $modelUser->separatedBy;

        $quote = '';
        echo $e_header = 'Masa Pajak' . $delimiter . 'Tahun Pajak' . $delimiter . 'Pembetulan' . $delimiter . 'NPWP' . $delimiter . 'Nama' . $delimiter . 'Kode Pajak' . $delimiter . 'Jumlah Bruto' . $delimiter . 'Jumlah PPh' . $delimiter . 'Kode Negara' . "\r\n";
        for ($node = 0; $node < count($arrayIdPegawai); $node++) {
            $jumlahBruto = 0;
            $jumlahPph = 0;
//            if ($node == $cekNode) {
            $getDataPenghasilan = Yii::$app->db->createCommand("SELECT masaPajak, tahunPajak, kodePajak, bruto, pph FROM master_penghasilan WHERE masaPajak = '$bulan' AND tahunPajak = '$tahun' AND pegawaiId = '$arrayIdPegawai[$node]'")->queryAll();

            for ($en = 0; $en < count($getDataPenghasilan); $en++) {
                $jumlahBruto = $jumlahBruto + $getDataPenghasilan[$en]['bruto'];
                $jumlahPph = $jumlahPph + $getDataPenghasilan[$en]['pph'];
                $masaPajak = $getDataPenghasilan[$en]['masaPajak'];
                $tahunPajak = $getDataPenghasilan[$en]['tahunPajak'];
                $kodePajak = $getDataPenghasilan[$en]['kodePajak'];
            }

            $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $arrayIdPegawai[$node]])->one();
            $npwp = $modelPegawai['npwp'];
            $nama = $modelPegawai['nama'];
            $pembetulan = 0;

            echo $bukti_potong_data = $quote . $masaPajak . $quote . $delimiter . $quote . $tahunPajak . $quote . $delimiter . $quote . $pembetulan . $quote . $delimiter . $quote . $npwp . $quote . $delimiter . $quote . $nama . $quote . $delimiter . $quote . $kodePajak . $quote . $delimiter . $quote . $jumlahBruto . $quote . $delimiter . $quote . $jumlahPph . "\r\n";
        }
        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download-pasal-21'] = $notification;
        if (!isset($session['notification-success-download-pasal-21'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakCsv($idBulan) {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $pasal = 2;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId == 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }

        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $downloadTZ = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
        $downloadedTime = $downloadTZ->format('Y-m-d');
        $bulan = \Yii::t('app', $idBulan);
        $filename = 'Bukti Potong PPh Pasal 22 ' . $bulan . ' ' . $tahun . '.csv';
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-Type: application/force-download");
        header("refresh:0;url=../pph-bukti-potong/index");

        $modelUser = User::find()->where(['id' => $userId])->one();
        $delimiter = $modelUser->separatedBy;

        $quote = '';
        echo $e_header = 'Kode Form' . $delimiter . 'Masa Pajak' . $delimiter . 'Tahun Pajak' . $delimiter . 'Pembetulan' . $delimiter . 'NPWP WP' . $delimiter . 'Nama WP' . $delimiter . 'Alamat WP' . $delimiter . 'No. BP' . $delimiter . 'Tanggal Pungut' . $delimiter . 'B13' . $delimiter . 'B15' . $delimiter . 'B16' . $delimiter . 'B23' . $delimiter . 'B25' . $delimiter . 'B26' . $delimiter . 'B33' . $delimiter . 'B35' . $delimiter . 'B36' . $delimiter . 'B43' . $delimiter . 'B45' . $delimiter . 'B46' . $delimiter . 'B52' . $delimiter . 'B53' . $delimiter . 'B55' . $delimiter . 'B56' . $delimiter . 'B62' . $delimiter . 'B63' . $delimiter . 'B65' . $delimiter . 'B66' . $delimiter . 'B72' . $delimiter . 'B73' . $delimiter . 'B75' . $delimiter . 'B76' . $delimiter . 'B82' . $delimiter . 'B83' . $delimiter . 'B85' . $delimiter . 'B86' . $delimiter . 'B92' . $delimiter . 'B93' . $delimiter . 'B95' . $delimiter . 'B96' . $delimiter . 'B102' . $delimiter . 'B103' . $delimiter . 'B105' . $delimiter . 'B106' . $delimiter . 'B112' . $delimiter . 'B113' . $delimiter . 'B115' . $delimiter . 'B116' . $delimiter . 'Jml. Bruto' . $delimiter . 'Jml. PPh' . "\r\n";

        for ($node = 0; $node < count($arrayId); $node++) {
            $kodeForm = 'F113304A';
            $modelData = PphBuktiPotong::find()->where(['buktiPotongId' => $arrayId[$node]])->one();
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $modelData->wajibPajakId])->one();
            $masa = $session['masaPajak'];
            $arrayMasa = explode(' ', $masa);
            $masaPajak = \Yii::t('app', $arrayMasa[0]);
            $tahunPajak = $arrayMasa[1];
            $pembetulan = 0;
            if ($modelData->wajibPajakId != null) {
                $npwpWp = preg_replace("/[^0-9]/", "", $modelVendor->npwp);
                $idStatusNpwp = $modelVendor->statusNpwpId;
                $modelStatusNpwp = PphStatusNpwp::find()->where(['npwpId' => $idStatusNpwp])->one();
                $idTarif = $modelStatusNpwp->tarifId;
                $modelTarif = PphTarif::find()->where(['tarifId' => $idTarif])->one();
                $tarif = $modelTarif->tarif;
            } else {
                $npwpWp = '000000000000000';
                $modelTarif = PphTarif::find()->where(['tarifId' => 2])->one();
                $tarif = $modelTarif->tarif;
            }

            $remove_character = array('\n', '\r\n', '\r');
            if ($modelData->wajibPajakId != null) {
                $namaWp = $modelVendor->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelVendor->alamat));
            } else {
                $namaWp = $modelData->nama;
                $alamatWp = trim(preg_replace('/\s\s+/', ' ', $modelData->alamat));
            }

            $noBp = $modelData->nomorBuktiPotong;
            $tanggalPungut = $modelData->tanggal;
            $b13 = 0;
            $b15 = '0,25';
            $b16 = 0;
            $b23 = 0;
            $b25 = '0,1';
            $b26 = 0;
            $b33 = 0;
            $b35 = '0,3';
            $b36 = 0;
            $b43 = 0;
            $b45 = '0,45';
            $b46 = 0;
            $b52 = '';
            $b53 = 0;
            $b55 = 0;
            $b56 = 0;
            $b62 = '';
            $b63 = 0;
            $b65 = 0;
            $b66 = 0;
            $b72 = '';
            $b73 = 0;
            $b75 = 0;
            $b76 = 0;
            $b82 = '';
            $b83 = 0;
            $b85 = number_format($tarif, 1, ",", "");
            $b86 = 0;
            $b92 = '';
            $b93 = 0;
            $b95 = number_format($tarif, 1, ",", "");
            $b96 = 0;
            $b102 = 'BUMN Tertentu';
            $b103 = $modelData->jumlahBruto;
            $b105 = '1,5';
            $b106 = $modelData->jumlahPphDiPotong;
            $b112 = '';
            $b113 = 0;
            $b115 = 0;
            $b116 = 0;
            $jumlahBruto = $modelData->jumlahBruto;
            $jumlahPph = $modelData->jumlahPphDiPotong;

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $modelData->updated_at = $date->format('Y:m:d H:i:s');
            $modelData->updated_by = $userId;
            $modelData->save(false);
            $tanggal = DateTime::createFromFormat('Y-m-d', $tanggalPungut)->format('d/m/Y');
            echo $bukti_potong_data = $quote . $kodeForm . $quote . $delimiter . $quote . $masaPajak . $quote . $delimiter . $quote . $tahunPajak . $quote . $delimiter . $quote . $pembetulan . $quote . $delimiter . $quote . $npwpWp . $quote . $delimiter . $quote . $namaWp . $quote . $delimiter . $quote . $alamatWp . $quote . $delimiter . $quote . $noBp . $quote . $delimiter . $quote . $tanggal . $quote . $delimiter . $quote . $b13 . $quote . $delimiter . $quote . $b15 . $quote . $delimiter . $quote . $b16 . $quote . $delimiter . $quote . $b23 . $quote . $delimiter . $quote . $b25 . $quote . $delimiter . $quote . $b26 . $quote . $delimiter . $quote . $b33 . $quote . $delimiter . $quote . $b35 . $quote . $delimiter . $quote . $b36 . $quote . $delimiter . $quote . $b43 . $quote . $delimiter . $quote . $b45 . $quote . $delimiter . $quote . $b46 . $quote . $delimiter . $quote . $b52 . $quote . $delimiter . $quote . $b53 . $quote . $delimiter . $quote . $b55 . $quote . $delimiter . $quote . $b56 . $quote . $delimiter . $quote . $b62 . $quote . $delimiter . $quote . $b63 . $quote . $delimiter . $quote . $b65 . $quote . $delimiter . $quote . $b66 . $quote . $delimiter . $quote . $b72 . $quote . $delimiter . $quote . $b73 . $quote . $delimiter . $quote . $b75 . $quote . $delimiter . $quote . $b76 . $quote . $delimiter . $quote . $b82 . $quote . $delimiter . $quote . $b83 . $quote . $delimiter . $quote . $b85 . $quote . $delimiter . $quote . $b86 . $quote . $delimiter . $quote . $b92 . $quote . $delimiter . $quote . $b93 . $quote . $delimiter . $quote . $b95 . $quote . $delimiter . $quote . $b96 . $quote . $delimiter . $quote . $b102 . $quote . $delimiter . $quote . $b103 . $quote . $delimiter . $quote . $b105 . $quote . $delimiter . $quote . $b106 . $quote . $delimiter . $quote . $b112 . $quote . $delimiter . $quote . $b113 . $quote . $delimiter . $quote . $b115 . $quote . $delimiter . $quote . $b116 . $quote . $delimiter . $quote . $jumlahBruto . $quote . $delimiter . $quote . $jumlahPph . "\r\n";
        }

        $notification = "Dokumen" . "<strong>" . " berhasil didownload" . "</strong>" . ".";
        $session['notification-success-download'] = $notification;
        if (!isset($session['notification-success-download'])) {
            Yii::$app->session->setFlash('success', $notification);
        }
    }

    public function actionCetakPdfPasal21Ptt($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 5;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }
        
        $curMonth = date('m');
        $curYear = date('y');
        $tarif = [];
        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = MasterMandor::find()->where(['mandorId' => $idVendor])->asArray()->one();
            
            $statusPemungutPajakPtt = $dataSetting['pasal21_ptt'];

            if ($statusPemungutPajakPtt == 0) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 1) {
                $npwpPemungutPajak = $dataSettingPP['npwpPemungutPajak'];
                $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            } else {
                $npwpPemungutPajak = $dataSetting['npwpPemungutPajakCabang'];
                $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            }            
            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $nik[] = $dataVendor['nik'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
                $statusNpwpMandor[] = 1;
            } else {
                $nama[] = $data[0]['nama'];
                $nik[] = '000000000000000';
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
                $statusNpwpMandor[] = 0;
            }            
            $statusKerja[] = $data[0]['statusKerjaId'];
            $tarif[] = $data[0]['tarif'];
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $tanggal[] = $data[0]['tanggal'];
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $dpp[] = $data[0]['dpp'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong Pasal 21 Mandor - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-pasal-21-mandor', [
                'model' => $model,
                'curMonth' => $curMonth,
                'curYear' => $curYear,
                'nama' => $nama,
                'npwp' => $npwp,
                'nik' => $nik,
                'namaPemungutPajak' => $namaPemungutPajak,                
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'dpp' => $dpp,
                'tarif' => $tarif,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpMandor' => $statusNpwpMandor,
                'statusKerja' => $statusKerja,
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }
    public function actionSptInduk21Pdf($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 5;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }
        
        $curMonth = date('m');
        $curYear = date('y');
        $tarif = [];
        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = MasterMandor::find()->where(['mandorId' => $idVendor])->asArray()->one();
            
            $statusPemungutPajakPtt = $dataSetting['pasal21_ptt'];

            if ($statusPemungutPajakPtt == 0) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 1) {
                $npwpPemungutPajak = $dataSettingPP['npwpPemungutPajak'];
                $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            } else {
                $npwpPemungutPajak = $dataSetting['npwpPemungutPajakCabang'];
                $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            }            
            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $nik[] = $dataVendor['nik'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
                $statusNpwpMandor[] = 1;
            } else {
                $nama[] = $data[0]['nama'];
                $nik[] = '000000000000000';
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
                $statusNpwpMandor[] = 0;
            }            
            $statusKerja[] = $data[0]['statusKerjaId'];
            $tarif[] = $data[0]['tarif'];
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $tanggal[] = $data[0]['tanggal'];
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $dpp[] = $data[0]['dpp'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL21 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-spt-21', [
                'model' => $model,
                'curMonth' => $curMonth,
                'curYear' => $curYear,
                'nama' => $nama,
                'npwp' => $npwp,
                'nik' => $nik,
                'namaPemungutPajak' => $namaPemungutPajak,                
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'dpp' => $dpp,
                'tarif' => $tarif,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpMandor' => $statusNpwpMandor,
                'statusKerja' => $statusKerja,
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 21 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }
    
    public function actionCetakNpwpWp(){
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;        
        $userId = Yii::$app->user->id;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE userId = '$userId' AND approvalStatus = 1")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE approvalStatus = 1")->queryAll();
            }
            $arrayId = array_column($getId, 'wajibPajakId');
        }
       
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);
        
        $npwp = [];
        $nama = [];
        $path = [];
        for($ns = 0; $ns < count($arrayId); $ns++){
            $data = PphWajibPajak::find()->where(['wajibPajakId' => $arrayId[$ns]])->asArray()->one();
            $nama[] = $data['nama'];
            $npwp[] = $data['npwp'];
            $path[] = $data['buktiNpwp'];            
        }   
//echo '<pre>';
//		print_r($path);
//		die();
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'NPWP Wajib Pajak.pdf',
            'content' => $this->renderPartial('cetak-npwp-wp', [
                'model' => $model,
                'nama' => $nama,
                'npwp' => $npwp,
                'path' => $path,                
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();        
    }
    
    public function actionCetakNikMandor(){
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;        
        $userId = Yii::$app->user->id;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT mandorId FROM master_mandor WHERE userId = '$userId' AND approvalStatus = 1")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT mandorId FROM master_mandor WHERE approvalStatus = 1")->queryAll();
            }
            $arrayId = array_column($getId, 'mandorId');
        }
               
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);
        
        $npwp = [];
        $nama = [];
        $path = [];
        for($ns = 0; $ns < count($arrayId); $ns++){
            $data = MasterMandor::find()->where(['mandorId' => $arrayId[$ns]])->asArray()->one();
            $nama[] = $data['nama'];
            $npwp[] = $data['npwp'];
            $path[] = $data['buktiNik'];            
        }

        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'NIK Mandor.pdf',
            'content' => $this->renderPartial('cetak-nik-mandor', [
                'model' => $model,
                'nama' => $nama,
                'npwp' => $npwp,
                'path' => $path,                
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();        
    }
    
    public function actionCetakNpwpMandor(){
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;        
        $userId = Yii::$app->user->id;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT mandorId FROM master_mandor WHERE userId = '$userId' AND approvalStatus = 1")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT mandorId FROM master_mandor WHERE approvalStatus = 1")->queryAll();
            }
            $arrayId = array_column($getId, 'mandorId');
        }
               
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);
        
        $npwp = [];
        $nama = [];
        $path = [];
        for($ns = 0; $ns < count($arrayId); $ns++){
            $data = MasterMandor::find()->where(['mandorId' => $arrayId[$ns]])->asArray()->one();
            $nama[] = $data['nama'];
            $npwp[] = $data['npwp'];
            $path[] = $data['buktiNpwp'];            
        }
//        echo '<pre>';
//        print_r($path);
//        die();
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'NPWP Mandor.pdf',
            'content' => $this->renderPartial('cetak-npwp-mandor', [
                'model' => $model,
                'nama' => $nama,
                'npwp' => $npwp,
                'path' => $path,                
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();        
    }
    
    public function actionCetakSppkpWp(){
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;        
        $userId = Yii::$app->user->id;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE userId = '$userId' AND approvalStatus = 1")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE approvalStatus = 1")->queryAll();
            }
            $arrayId = array_column($getId, 'wajibPajakId');
        }
  
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);
        
        $npwp = [];
        $nama = [];
        $path = [];
        for($ns = 0; $ns < count($arrayId); $ns++){
            $data = PphWajibPajak::find()->where(['wajibPajakId' => $arrayId[$ns]])->asArray()->one();
            $nama[] = $data['nama'];
            $npwp[] = $data['npwp'];
            $path[] = $data['buktiSppkp'];            
        }

        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'SPPKP Wajib Pajak.pdf',
            'content' => $this->renderPartial('cetak-sppkp-wp', [
                'model' => $model,
                'nama' => $nama,
                'npwp' => $npwp,
                'path' => $path,                
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();        
    }
    
    public function actionCetakIujkWp(){
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;        
        $userId = Yii::$app->user->id;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE userId = '$userId' AND approvalStatus = 1")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE approvalStatus = 1")->queryAll();
            }
            $arrayId = array_column($getId, 'wajibPajakId');
        }
        
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);
        
        $npwp = [];
        $nama = [];
        $path = [];
        for($ns = 0; $ns < count($arrayId); $ns++){
            $data = PphWajibPajak::find()->where(['wajibPajakId' => $arrayId[$ns]])->asArray()->one();
            $nama[] = $data['nama'];
            $npwp[] = $data['npwp'];
            $datas = PphIujk::find()->where(['wajibPajakId' => $arrayId[$ns], 'is_expired' => 0])->asArray()->one();
            $path[] = $datas['bukti'];            
        }
//        echo '<pre>';
//        print_r($path);
//        die();
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'SPPKP Wajib Pajak.pdf',
            'content' => $this->renderPartial('cetak-iujk-wp', [
                'model' => $model,
                'nama' => $nama,
                'npwp' => $npwp,
                'path' => $path,                
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();        
    }
    
    public function actionCetakBujpkWp(){
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;        
        $userId = Yii::$app->user->id;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE userId = '$userId' AND approvalStatus = 1")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT wajibPajakId FROM pph_wajib_pajak WHERE approvalStatus = 1")->queryAll();
            }
            $arrayId = array_column($getId, 'wajibPajakId');
        }
        
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);
        
        $npwp = [];
        $nama = [];
        $path = [];
        for($ns = 0; $ns < count($arrayId); $ns++){
            $data = PphWajibPajak::find()->where(['wajibPajakId' => $arrayId[$ns]])->asArray()->one();
            $nama[] = $data['nama'];
            $npwp[] = $data['npwp'];
            $datas = PphBujpk::find()->where(['wajibPajakId' => $arrayId[$ns], 'is_expired' => 0])->asArray()->one();
            $path[] = $datas['bukti'];            
        }
//        echo '<pre>';
//        print_r($path);
//        die();
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'SPPKP Wajib Pajak.pdf',
            'content' => $this->renderPartial('cetak-bujpk-wp', [
                'model' => $model,
                'nama' => $nama,
                'npwp' => $npwp,
                'path' => $path,                
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();        
    }
    
    public function actionCetakPdf($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 2;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }

        $tarif = [];
        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = PphWajibPajak::find()->where(['wajibPajakId' => $idVendor])->asArray()->one();
            $modelStatus = PphStatusNpwp::find()->where(['npwpId' => $data[0]['statusNpwpId']])->asArray()->one();
            $tarifId = $modelStatus['tarifId'];
            $modelTarif = PphTarif::find()->where(['tarifId' => $tarifId])->asArray()->one();

            $statusPemungutPajak22 = $dataSetting['pasal22'];
            if ($statusPemungutPajak22 == 1) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 0) {
                $npwpPemungutPajak = $dataSettingPP['npwpPemungutPajak'];
                $kota[] = $dataSettingPP['kota'];
                $kppTerdaftar[] = $dataSettingPP['kppTerdaftar'];
                $namaPenanggungJawab[] = $dataSettingPP['nama'];
                $jabatan[] = $dataSettingPP['jabatan'];
            } else {
                $npwpPemungutPajak = $dataSetting['npwpPemungutPajakCabang'];
                $kota[] = $dataSetting['kota'];
                $kppTerdaftar[] = $dataSetting['kppTerdaftar'];
                $jabatan[] = $dataSetting['jabatan'];
                $namaPenanggungJawab[] = $dataSetting['nama'];
            }

            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
                $statusNpwpWp[] = 1;
            } else {
                $nama[] = $data[0]['nama'];
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
                $statusNpwpWp[] = 0;
            }
            $tarif[] = $modelTarif['tarif'];
            $kppTerdaftar[] = $dataSetting['kppTerdaftar'];
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $tanggal[] = $data[0]['tanggal'];
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL22 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak', [
                'model' => $model,
                'nama' => $nama,
                'kota' => $kota,
                'namaPenanggungJawab' => $namaPenanggungJawab,
                'namaPemungutPajak' => $namaPemungutPajak,
                'npwp' => $npwp,
                'jabatan' => $jabatan,
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'tarif' => $tarif,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpWp' => $statusNpwpWp,
                'kppTerdaftar' => $kppTerdaftar
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
//            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
            'methods' => [
//                'SetHeader' => $header,
//                'SetFooter' => $footer,
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }
    
    public function actionSptInduk4Pdf($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 2;
        $masa = $session['masaPajak'];
        
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        $masaIdPembetulan = \Yii::t('app', $arrayMasa[0]);

        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }

        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        $jumBruto = 0;
        $jumPph = 0;
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = PphWajibPajak::find()->where(['wajibPajakId' => $idVendor])->asArray()->one();
            $modelStatus = PphStatusNpwp::find()->where(['npwpId' => $data[0]['statusNpwpId']])->asArray()->one();

            //cek pembetulan
            $dataPembetulan = LogPembetulan::find()->where(['masa' => $masaIdPembetulan, 'tahun' => $tahun, 'pasalId' => $pasal])->one();
            if($dataPembetulan != null){
                $nilaiPembetulan = $dataPembetulan->pembetulan;
            } else {
                $nilaiPembetulan = null;
            }

            $statusPemungutPajak22 = $dataSetting['pasal22'];
            if ($statusPemungutPajak22 == 1) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 0) {
                $npwpPemungutPajak = $dataSettingPP['npwpPemungutPajak'];
                $namaPenanggungJawab = $dataSettingPP['nama'];             
            } else {
                $npwpPemungutPajak = $dataSetting['npwpPemungutPajakCabang'];
                $namaPenanggungJawab = $dataSetting['nama'];
            }

            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
                $statusNpwpWp[] = 1;
            } else {
                $nama[] = $data[0]['nama'];
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
                $statusNpwpWp[] = 0;
            }
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $tanggal[] = date('d/m/Y', strtotime($data[0]['tanggal']));
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
            
            $jumBruto = $jumBruto + $data[0]['jumlahBruto'];
            $jumPph = $jumPph + $data[0]['jumlahPphDiPotong'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL22 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-spt-4', [
                'model' => $model,
                'nama' => $nama,
                'namaPenanggungJawab' => $namaPenanggungJawab,
                'namaPemungutPajak' => $namaPemungutPajak,
                'npwp' => $npwp,
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumBruto' => $jumBruto,
                'jumPph' => $jumPph,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpWp' => $statusNpwpWp,
                'nilaiPembetulan' => $nilaiPembetulan
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    public function actionSptInduk23Pdf($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 3;
        $masa = $session['masaPajak'];
        
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];

        $masaIdPembetulan = \Yii::t('app', $arrayMasa[0]);
        
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
           unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }

        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        $jumBruto = 0;
        $jumPph = 0;
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $statusWp = $data[0]['statusWpId'];            
            $dataVendor = PphWajibPajak::find()->where(['wajibPajakId' => $idVendor])->asArray()->one();
            $modelStatus = PphStatusNpwp::find()->where(['npwpId' => $data[0]['statusNpwpId']])->asArray()->one();

            //cek pembetulan
            $dataPembetulan = LogPembetulan::find()->where(['masa' => $masaIdPembetulan, 'tahun' => $tahun, 'pasalId' => $pasal])->one();
            if($dataPembetulan != null){
                $nilaiPembetulan = $dataPembetulan->pembetulan;
            } else {
                $nilaiPembetulan = null;
            }
            // echo '<pre>';
            // print_r($dataPembetulan);
            // die();

            if($statusWp == 3){
                $statusPemungutPajak22 = $dataSetting['pasal23_jasa'];
            } else {
                $statusPemungutPajak22 = $dataSetting['pasal23_sewa'];
            }
            if ($statusPemungutPajak22 == 1) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 0) {
                $npwpPemungutPajak = $dataSettingPP['npwpPemungutPajak'];
                $namaPenanggungJawab = $dataSettingPP['nama'];             
            } else {
                $npwpPemungutPajak = $dataSetting['npwpPemungutPajakCabang'];
                $namaPenanggungJawab = $dataSetting['nama'];
            }

            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
                $statusNpwpWp[] = 1;
            } else {
                $nama[] = $data[0]['nama'];
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
                $statusNpwpWp[] = 0;
            }
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $tanggal[] = date('d/m/Y', strtotime($data[0]['tanggal']));
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
            
            $jumBruto = $jumBruto + $data[0]['jumlahBruto'];
            $jumPph = $jumPph + $data[0]['jumlahPphDiPotong'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL23 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-spt-23', [
                'model' => $model,
                'nama' => $nama,
                'namaPenanggungJawab' => $namaPenanggungJawab,
                'namaPemungutPajak' => $namaPemungutPajak,
                'npwp' => $npwp,
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumBruto' => $jumBruto,
                'jumPph' => $jumPph,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpWp' => $statusNpwpWp,
                'nilaiPembetulan' => $nilaiPembetulan
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    public function actionSptInduk22Pdf($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 2;
        $masa = $session['masaPajak'];
        
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        $masaIdPembetulan = \Yii::t('app', $arrayMasa[0]);
        
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }

        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        $jumBruto = 0;
        $jumPph = 0;
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = PphWajibPajak::find()->where(['wajibPajakId' => $idVendor])->asArray()->one();
            $modelStatus = PphStatusNpwp::find()->where(['npwpId' => $data[0]['statusNpwpId']])->asArray()->one();

            //cek pembetulan
            $dataPembetulan = LogPembetulan::find()->where(['masa' => $masaIdPembetulan, 'tahun' => $tahun, 'pasalId' => $pasal])->one();
            if($dataPembetulan != null){
                $nilaiPembetulan = $dataPembetulan->pembetulan;
            } else {
                $nilaiPembetulan = null;
            }

            $statusPemungutPajak22 = $dataSetting['pasal22'];
            if ($statusPemungutPajak22 == 1) {
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 0;
            }

            if ($statusPemungutPajak == 0) {
                $npwpPemungutPajak = $dataSettingPP['npwpPemungutPajak'];
                $namaPenanggungJawab = $dataSettingPP['nama'];             
            } else {
                $npwpPemungutPajak = $dataSetting['npwpPemungutPajakCabang'];
                $namaPenanggungJawab = $dataSetting['nama'];
            }

            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
                $statusNpwpWp[] = 1;
            } else {
                $nama[] = $data[0]['nama'];
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
                $statusNpwpWp[] = 0;
            }
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $tanggal[] = date('d/m/Y', strtotime($data[0]['tanggal']));
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
            
            $jumBruto = $jumBruto + $data[0]['jumlahBruto'];
            $jumPph = $jumPph + $data[0]['jumlahPphDiPotong'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL22 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-spt-22', [
                'model' => $model,
                'nama' => $nama,
                'namaPenanggungJawab' => $namaPenanggungJawab,
                'namaPemungutPajak' => $namaPemungutPajak,
                'npwp' => $npwp,
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumBruto' => $jumBruto,
                'jumPph' => $jumPph,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpWp' => $statusNpwpWp,
                'nilaiPembetulan' => $nilaiPembetulan
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 22 - WASKITA'
            ],
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    public function actionCetakPdfPasal23($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        $pasal = 3;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index-pasal-23']);
        }
        $session['pdfCount'] = count($arrayId);
        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }

        $tarif = [];
        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = PphWajibPajak::find()->where(['wajibPajakId' => $idVendor])->asArray()->one();
            $modelStatus = PphStatusNpwp::find()->where(['npwpId' => $data[0]['statusNpwpId']])->asArray()->one();

            $tarifId = $modelStatus['tarifId'];
            $modelTarif = PphTarif::find()->where(['tarifId' => $tarifId])->asArray()->one();
            $statusPemungutPajakJasa = $dataSetting['pasal23_jasa'];
            $statusPemungutPajakSewa = $dataSetting['pasal23_sewa'];
            $statusWajibPajak = $data[0]['statusWpId'];

            if ($statusPemungutPajakJasa == 0 && $statusPemungutPajakSewa == 0) {
                //keduanya pusat
                $statusPemungutPajak = 1;
            } else if ($statusPemungutPajakJasa == 1 && $statusPemungutPajakSewa == 0) {
                //hanya jasa yang make cabang
                $statusPemungutPajak = 2;
            } else if ($statusPemungutPajakJasa == 0 && $statusPemungutPajakSewa == 1) {
                //hanya sewa yang make cabang
                $statusPemungutPajak = 3;
            } else if ($statusPemungutPajakJasa == NULL && $statusPemungutPajakSewa == NULL) {
                //keduanya make pusat
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 4;
            }

            if ($statusPemungutPajak == 1) {
                $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
                $kotaPusat = $dataSettingPP['kota'];
                $kppPusat = $dataSettingPP['kppTerdaftar'];
                $namaPusat = $dataSettingPP['nama'];
                $jabatanPusat = $dataSettingPP['jabatan'];
            } else if ($statusPemungutPajak == 2) {
                if ($statusWajibPajak == 3) {
                    $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
                } else {
                    $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
                }
            } else if ($statusPemungutPajak == 3) {
                if ($statusWajibPajak == 2) {
                    $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
                } else {
                    $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
                }
            } else {
                $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
            }

            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
            } else {
                $nama[] = $data[0]['nama'];
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
            }
            $tarif[] = $modelTarif['tarif'];
            $statusNpwpWp[] = $data[0]['statusNpwpId'];
            $npwpPemungutPajak[] = $npwpPemungut;
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;

            if ($statusPemungutPajak == 1) {
                $kota[] = $kotaPusat;
                $kppTerdaftar[] = $kppPusat;
                $jabatan[] = $jabatanPusat;
                $namaPenanggungJawab[] = $namaPusat;
            } else {
                $kota[] = $dataSetting['kota'];
                $kppTerdaftar[] = $dataSetting['kppTerdaftar'];
                $jabatan[] = $dataSetting['jabatan'];
                $namaPenanggungJawab[] = $dataSetting['nama'];
            }
            $tanggal[] = $data[0]['tanggal'];
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
            $jenisBp[] = $data[0]['statusWpId'];
        }

        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL 23 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-pasal-23', [
                'model' => $model,
                'nama' => $nama,
                'kota' => $kota,
                'namaPenanggungJawab' => $namaPenanggungJawab,
                'namaPemungutPajak' => $namaPemungutPajak,
                'npwp' => $npwp,
                'jabatan' => $jabatan,
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'tarif' => $tarif,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'statusNpwpWp' => $statusNpwpWp,
                'kppTerdaftar' => $kppTerdaftar,
                'jenisBp' => $jenisBp
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 23 - WASKITA'
            ],
            'methods' => [
            ]
        ]);

        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    public function actionCetakPdfPasal4($idBulan) {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $setting = new Setting();
        $userId = Yii::$app->user->id;
        $pasal = 4;
        $masa = $session['masaPajak'];
        $arrayMasa = explode(' ', $masa);
        $tahun = $arrayMasa[1];
        if (isset($session['sel-rows'])) {
            $rows = $session['sel-rows'];
            $arrayRows = json_decode($rows);
            $arrayId = explode(",", $arrayRows);
            unset($session['sel-rows']);
        } else {
            if ($userId != 1) {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            } else {
                $getId = Yii::$app->db->createCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$idBulan' AND YEAR(tanggal) = '$tahun' AND pasalId = '$pasal'")->queryAll();
            }
            $arrayId = array_column($getId, 'buktiPotongId');
        }
        if (!isset($arrayId[0])) {
            Yii::$app->session->setFlash('error', "Data dengan Masa Pajak " . "<strong>" . \Yii::t('app', $idBulan) . "</strong>" . " untuk tahun " . "<strong>" . $tahun . "</strong>" . " tidak tersedia.");
            return Yii::$app->response->redirect(['/pph-bukti-potong/index-pasal-4']);
        }
        $session = Yii::$app->session;
        $session['pdfCount'] = count($arrayId);

        $arrayUserId = [];
        for ($ax = 0; $ax < count($arrayId); $ax++) {
            $bid = $arrayId[$ax];
            $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $bid])->one();
            $uid = $modelBP->userId;
            $arrayUserId[$ax] = $uid;
        }

        $tarif = [];
        $dataSettingPP = Setting::find()->where(['userId' => 1])->asArray()->one();
        for ($n = 0; $n < count($arrayId); $n++) {
            $dataSetting = Setting::find()->where(['userId' => $arrayUserId[$n]])->asArray()->one();
            $data = Yii::$app->db->createCommand("SELECT * FROM pph_bukti_potong WHERE buktiPotongId = '$arrayId[$n]'")->queryAll();
            $nomorBP = $data[0]['nomorBuktiPotong'];
            $idVendor = $data[0]['wajibPajakId'];
            $dataVendor = PphWajibPajak::find()->where(['wajibPajakId' => $idVendor])->asArray()->one();
            $modelStatusVendor = PphStatusVendor::find()->where(['statusVendorId' => $data[0]['statusWpId']])->asArray()->one();
            $statusPemungutPajakJasa = $dataSetting['pasal4_jasa'];
            $statusPemungutPajakSewa = $dataSetting['pasal4_sewa'];
            $statusPemungutPajakPerencana = $dataSetting['pasal4_perencana'];
            $statusWajibPajak = $data[0]['statusWpId'];
            if ($statusPemungutPajakJasa == 0 && $statusPemungutPajakSewa == 0 && $statusPemungutPajakPerencana == 0) {
                //ketiganya pusat
                $statusPemungutPajak = 1;
            } else if ($statusPemungutPajakJasa == 1 && $statusPemungutPajakSewa == 0 && $statusPemungutPajakPerencana == 0) {
                //hanya jasa yang make cabang
                $statusPemungutPajak = 2;
            } else if ($statusPemungutPajakJasa == 0 && $statusPemungutPajakSewa == 1 && $statusPemungutPajakPerencana == 0) {
                //hanya sewa yang make cabang
                $statusPemungutPajak = 3;
            } else if ($statusPemungutPajakJasa == 0 && $statusPemungutPajakSewa == 0 && $statusPemungutPajakPerencana == 1) {
                //hanya perencana yang make cabang
                $statusPemungutPajak = 4;
            } else if ($statusPemungutPajakJasa == NULL && $statusPemungutPajakSewa == NULL && $statusPemungutPajakPerencana == NULL) {
                //ketiganya make pusat
                $statusPemungutPajak = 1;
            } else {
                $statusPemungutPajak = 5;
            }

            if ($statusPemungutPajak == 1) {
                $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
            } else if ($statusPemungutPajak == 2) {
                if ($statusWajibPajak == 3) {
                    $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
                } else {
                    $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
                }
            } else if ($statusPemungutPajak == 3) {
                if ($statusWajibPajak == 2) {
                    $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
                } else {
                    $npwpPemungut = $dataSettingPP['npwpPemungutPajak'];
                }
            } else {
                $npwpPemungut = $dataSetting['npwpPemungutPajakCabang'];
            }

            $statusWpId = $data[0]['statusWpId'];
            if ($statusWpId == 3) {
                //ini Jasa Konstruksi
                $iujkId = $dataVendor['iujkId'];
                if ($iujkId != NULL) {
                    $bujpkExist = 0;
                    $iujkExist = 1;
                    $modelIujk = PphIujk::find()->where(['iujkId' => $iujkId])->one();
                    $is_approve = $modelIujk->is_approve;
                    if ($is_approve == 1) {
                        $statusIujk = 1;
                        $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                        if ($tingkatKeuanganWp == 1) {
                            $tarifId = 6;
                            $level = 1;
                        } else {
                            $tarifId = 7;
                            $level = 2;
                        }
                    } else {
                        $tarifId = 8;
                        $statusIujk = 0;
                        $level = 0;
                    }
                    $statusBujpk = 0;
                } else {
                    $tarifId = 8;
                    $iujkExist = 0;
                    $bujpkExist = 0;
                    $statusIujk = 0;
                    $level = 0;
                    $statusBujpk = 0;
                }
            } else if ($statusWpId == 2) {
                $tarifId = 3;
                $iujkExist = 0;
                $bujpkExist = 0;
                $statusIujk = 0;
                $level = 0;
                $statusBujpk = 0;
            } else {
                $bujpkId = $dataVendor['bujpkId'];
                $bujpkExist = 1;
                $iujkExist = 0;
                $statusIujk = 0;
                $level = 0;
                if ($bujpkId != null) {
                    $modelBujpk = PphBujpk::find()->where(['bujpkId' => $bujpkId])->one();
                    $is_approve = $modelBujpk->is_approve;
                    if ($is_approve == 1) {
                        $statusBujpk = 1;
                        $tarifId = 12;
                    } else {
                        $statusBujpk = 0;
                        $tarifId = 13;
                    }
                } else {
                    $statusBujpk = 0;
                    $tarifId = 13;
                }
            }

            $modelTarif = PphTarif::find()->where(['tarifId' => $tarifId])->asArray()->one();
            if ($idVendor != null) {
                $nama[] = $dataVendor['nama'];
                $npwp[] = preg_replace("/[^0-9]/", "", $dataVendor['npwp']);
                $alamat[] = $dataVendor['alamat'];
            } else {
                $nama[] = $data[0]['nama'];
                $npwp[] = '000000000000000';
                $alamat[] = $data[0]['alamat'];
            }
            $tarif[] = $modelTarif['tarif'];
            $jenisBp[] = $statusWpId;
            $arrayStatusIujk[] = $statusIujk;
            $arrayStatusBujpk[] = $statusBujpk;
            $arrayLevel[] = $level;
            $arrayIujkExist[] = $iujkExist;
            $arrayBujpkExist[] = $bujpkExist;
            $npwpPemungutPajak[] = $npwpPemungut;
            $namaPemungutPajak = $dataSettingPP['namaPemungutPajak'];
            $nomorBuktiPotong[] = $nomorBP;
            $jabatan[] = $dataSetting['jabatan'];
            $kota[] = $dataSetting['kota'];
            $namaPenanggungJawab[] = $dataSetting['nama'];
            $kppTerdaftar[] = $dataSetting['kppTerdaftar'];
            $tanggal[] = $data[0]['tanggal'];
            $jumlahBruto[] = $data[0]['jumlahBruto'];
            $pphDipungut[] = $data[0]['jumlahPphDiPotong'];
            $lokasiTanah[] = $data[0]['lokasi_tanah'];
        }
        $header = '<div><img src="img/header.png"></div>';
        $footer = '<div><img src="img/footer.png"></div>';
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE, // leaner size using standard fonts
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'filename' => 'Bukti Potong PASAL22 - ' . \Yii::t('app', $idBulan) . ' ' . $tahun . '.pdf',
            'content' => $this->renderPartial('cetak-pasal-4', [
                'model' => $model,
                'nama' => $nama,
                'kota' => $kota,
                'namaPenanggungJawab' => $namaPenanggungJawab,
                'namaPemungutPajak' => $namaPemungutPajak,
                'npwp' => $npwp,
                'jabatan' => $jabatan,
                'npwpPemungutPajak' => $npwpPemungutPajak,
                'alamat' => $alamat,
                'tanggal' => $tanggal,
                'jumlahBruto' => $jumlahBruto,
                'pphDipungut' => $pphDipungut,
                'tarif' => $tarif,
                'nomorBuktiPotong' => $nomorBuktiPotong,
                'jenisBp' => $jenisBp,
                'arrayLevel' => $arrayLevel,
                'arrayStatusIujk' => $arrayStatusIujk,
                'arrayStatusBujpk' => $arrayStatusBujpk,
                'arrayIujkExist' => $arrayIujkExist,
                'arrayBujpkExist' => $arrayBujpkExist,
                'kppTerdaftar' => $kppTerdaftar,
                'lokasiTanah' => $lokasiTanah
            ]),
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'options' => [
                'title' => 'Privacy Policy - Krajee.com',
                'subject' => 'PPh Pasal 4 Ayat (2) - WASKITA'
            ],
            'methods' => [
//                'SetHeader' => $header,
            ]
        ]);
//        echo '<pre>';
//        print_r($jumlahBruto);
//        die();
        Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'application/pdf');
        return $pdf->render();
    }

    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PphBerkas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new PphBerkas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->berkasId]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PphBerkas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->berkasId]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PphBerkas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PphBerkas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PphBerkas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PphBerkas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

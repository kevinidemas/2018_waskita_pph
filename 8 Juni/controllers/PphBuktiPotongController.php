<?php

namespace app\controllers;

use Yii;
use app\models\PphBuktiPotong;
use app\models\Options;
use app\search\PphBuktiPotongSearch;
//use dektrium\user\models\User;
use app\models\User;
use app\models\Setting;
use app\models\PphCalendar;
use app\models\PphTarif;
use app\models\PphNomorBuktiPotong;
use app\models\MasterWajibPajakNon;
use app\models\PphStatusNpwp;
use app\models\PphStatusVendor;
use app\models\PphIujk;
use app\models\PphBujpk;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\PphWajibPajak;
use app\models\PphHistoryUpdate;
use app\models\LogPembetulan;
use DateTime;

/**
 * PphBuktiPotongController implements the CRUD actions for PphBuktiPotong model.
 */
class PphBuktiPotongController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
//                        'actions' => ['create', 'update', 'index', 'view'],
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all PphBuktiPotong models.
     * @return mixed
     */
    public function actionIndex() {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        $searchModel = new PphBuktiPotongSearch();
        if (isset($session['sel-rows'])) {
            unset($session['sel-rows']);
        }
        $session['pasal-terpilih'] = 2;
        //attach roles == 2        

        $checkRoles = Yii::$app->db->CreateCommand("SELECT id FROM arranger WHERE user = $userId")->execute();
        if ($checkRoles == 0) {
            $updateRoles = Yii::$app->db->CreateCommand("INSERT INTO arranger (role, user) VALUES('2', '$userId')")->execute();
        }

        //berikan peringatan $is_expired_warning jika sudah dalam kurun waktu 3 bulan menjelang expired
        $updateWarningIujk = Yii::$app->db->CreateCommand("UPDATE pph_iujk SET is_expired_warning = 1 WHERE expired_at <= NOW() + INTERVAL 90 DAY")->execute();

        //update masa berlaku IUJK
        $updateIujk = Yii::$app->db->CreateCommand("UPDATE pph_iujk SET is_expired = 1, is_active = 0, is_expired_warning = 0 WHERE CURDATE() > expired_at")->execute();

        //berikan peringatan $is_expired_warning jika sudah dalam kurun waktu 3 bulan menjelang expired
        $updateWarningBujpk = Yii::$app->db->CreateCommand("UPDATE pph_bujpk SET is_expired_warning = 1 WHERE expired_at <= NOW() + INTERVAL 90 DAY")->execute();

        //update masa berlaku IUJK
        $updateBujpk = Yii::$app->db->CreateCommand("UPDATE pph_bujpk SET is_expired = 1, is_active = 0, is_expired_warning = 0 WHERE CURDATE() > expired_at")->execute();

        $arrayMasa = explode(' ', $session['masaPajak']);
        $monthUpdate = \Yii::t('app', $arrayMasa[0]);
        $yearUpdate = $arrayMasa[1];
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if ($bln == date('m')) {
            $tgl = date('d');
        }

        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        if ($userId == 1) {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]' AND pasalId = '2'")->queryAll();
        } else {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND pasalId = '2' AND MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]'")->queryAll();
        }
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        if ($getDataCount != null) {
            $session['dataCount-pasal-22'] = 1;
        }
        $status = $session['expiredDate'];

        if ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
        }

        $checkIujkPeriod = Yii::$app->db->CreateCommand("SELECT nomorIujk from pph_iujk WHERE created_by = $userId AND expired_at <= NOW() + INTERVAL 90 DAY AND expired_at >= NOW()")->queryAll();

        if ($checkIujkPeriod != null) {
            $iujkExpiredArr = [];
            for ($ie = 0; $ie < count($checkIujkPeriod); $ie++) {
                $iujkExpiredArr[] = "Masa berlaku IUJK dengan nomor " . "<strong>" . $checkIujkPeriod[$ie]['nomorIujk'] . "</strong> akan segera berakhir";
            }

            $iujkExpiredArrJson = json_encode($iujkExpiredArr);
            $session['iujkExpiredArr'] = $iujkExpiredArrJson;
        }

        $checkbujpkPeriod = Yii::$app->db->CreateCommand("SELECT nomorbujpk from pph_bujpk WHERE created_by = $userId AND expired_at <= NOW() + INTERVAL 90 DAY AND expired_at >= NOW()")->queryAll();

        if ($checkbujpkPeriod != null) {
            $bujpkExpiredArr = [];
            for ($ie = 0; $ie < count($checkbujpkPeriod); $ie++) {
                $bujpkExpiredArr[] = "Masa berlaku BUJPK dengan nomor " . "<strong>" . $checkbujpkPeriod[$ie]['nomorbujpk'] . "</strong> akan segera berakhir";
            }

            $bujpkExpiredArrJson = json_encode($bujpkExpiredArr);
            $session['bujpkExpiredArr'] = $bujpkExpiredArrJson;
        }

        if (Yii::$app->user->id == 1) {
            $arrayId = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate' AND nomorBuktiPotong IS NOT NULL")->queryAll();
            $arrayId = array_column($arrayId, 'buktiPotongId');
            $arrayIdCreate = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate'")->queryAll();
            $arrayIdCreate = array_column($arrayIdCreate, 'buktiPotongId');
            $arrayCrNoBp = null;
            $arrayRmNoBp = null;
            if ($arrayIdCreate == null) {
                $arrayCrNoBp = false;
            } else {
                $arrayCrNoBp = true;
            }
            if ($arrayId == null) {
                $arrayRmNoBp = true;
            } else {
                $arrayRmNoBp = false;
            }
        } else {
            $arrayRmNoBp = true;
            $arrayCrNoBp = false;
        };
        if ($userId != 1) {
            $searchModel->userId = $userId;
        }


        $searchModel->tanggal = $monthUpdate;
        $searchModel->pasalId = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;


        return $this->render('index', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'arrayCrNoBp' => $arrayCrNoBp,
                    'arrayRmNoBp' => $arrayRmNoBp,
        ]);
    }

    public function actionIndexPasal23() {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $searchModel = new PphBuktiPotongSearch();
        if (isset($session['sel-rows'])) {
            unset($session['sel-rows']);
        }
        $session['pasal-terpilih'] = 3;
        $arrayMasa = explode(' ', $session['masaPajak']);
        $monthUpdate = \Yii::t('app', $arrayMasa[0]);
        $yearUpdate = $arrayMasa[1];
        $userId = Yii::$app->user->id;
        $session = Yii::$app->session;
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if ($bln == date('m')) {
            $tgl = date('d');
        }

        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        if ($userId == 1) {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]' AND pasalId = '3'")->queryAll();
        } else {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND pasalId = '3' AND MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]'")->queryAll();
        }
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        if ($getDataCount != null) {
            $session['dataCount-pasal-23'] = 1;
        }
        $status = $session['expiredDate'];

        if ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
        }
        if (Yii::$app->user->id == 1) {
            $arrayId = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate' AND nomorBuktiPotong IS NOT NULL")->queryAll();
            $arrayId = array_column($arrayId, 'buktiPotongId');
            $arrayIdCreate = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate'")->queryAll();
            $arrayIdCreate = array_column($arrayIdCreate, 'buktiPotongId');
            $arrayCrNoBp = null;
            $arrayRmNoBp = null;
            if ($arrayIdCreate == null) {
                $arrayCrNoBp = false;
            } else {
                $arrayCrNoBp = true;
            }
            if ($arrayId == null) {
                $arrayRmNoBp = true;
            } else {
                $arrayRmNoBp = false;
            }
        } else {
            $arrayRmNoBp = true;
            $arrayCrNoBp = false;
        };
        if ($userId != 1) {
            $searchModel->userId = $userId;
        }
        $searchModel->tanggal = $monthUpdate;
        $searchModel->pasalId = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 25;

        return $this->render('index-pasal-23', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'arrayCrNoBp' => $arrayCrNoBp,
                    'arrayRmNoBp' => $arrayRmNoBp,
        ]);
    }

    public function actionIndexPasal21() {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $searchModel = new PphBuktiPotongSearch();
        if (isset($session['sel-rows'])) {
            unset($session['sel-rows']);
        }

        $session['pasal-terpilih'] = 5;
        $arrayMasa = explode(' ', $session['masaPajak']);
        $monthUpdate = \Yii::t('app', $arrayMasa[0]);
        $yearUpdate = $arrayMasa[1];
        $userId = Yii::$app->user->id;
        $session = Yii::$app->session;
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if ($bln == date('m')) {
            $tgl = date('d');
        }

        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        if ($userId == 1) {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]'")->queryAll();
        } else {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]'")->queryAll();
        }
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        if ($getDataCount != null) {
            $session['dataCount'] = 1;
        }
        $status = $session['expiredDate'];

        if ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
        }
        if (Yii::$app->user->id == 1) {
            $arrayId = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate' AND nomorBuktiPotong IS NOT NULL")->queryAll();
            $arrayId = array_column($arrayId, 'buktiPotongId');
            $arrayIdCreate = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate'")->queryAll();
            $arrayIdCreate = array_column($arrayIdCreate, 'buktiPotongId');
            $arrayCrNoBp = null;
            $arrayRmNoBp = null;
            if ($arrayIdCreate == null) {
                $arrayCrNoBp = false;
            } else {
                $arrayCrNoBp = true;
            }
            if ($arrayId == null) {
                $arrayRmNoBp = true;
            } else {
                $arrayRmNoBp = false;
            }
        } else {
            $arrayRmNoBp = true;
            $arrayCrNoBp = false;
        };
        if ($userId != 1) {
            $searchModel->userId = $userId;
        }
        $searchModel->tanggal = $monthUpdate;
        $searchModel->pasalId = 5;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        return $this->render('index-pasal-21', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'arrayCrNoBp' => $arrayCrNoBp,
                    'arrayRmNoBp' => $arrayRmNoBp,
        ]);
    }

    public function actionIndexPasal4() {
        $model = new PphBuktiPotong();
        $session = Yii::$app->session;
        $searchModel = new PphBuktiPotongSearch();
        if (isset($session['sel-rows'])) {
            unset($session['sel-rows']);
        }
        $session['pasal-terpilih'] = 4;
        $arrayMasa = explode(' ', $session['masaPajak']);
        $monthUpdate = \Yii::t('app', $arrayMasa[0]);
        $yearUpdate = $arrayMasa[1];
        $userId = Yii::$app->user->id;

        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if ($bln == date('m')) {
            $tgl = date('d');
        }
        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        if ($userId == 1) {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]' AND pasalId = '4'")->queryAll();
        } else {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE userId = '$userId' AND pasalId = '4' AND MONTH(tanggal) = '$masaBulan' AND YEAR(tanggal) = '$array[1]'")->queryAll();
        }
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        //cek apakah ada IUJK yang dalam waktu 1 bulan lagi habis masa laku
        $nextMonth = date('Y-m-d', strtotime('+1 month')) . '<br>';
        $today = date('Y-m-d');
        $iujkYear = date('Y');
        $getIujk = Yii::$app->db->CreateCommand("SELECT iujkId FROM pph_iujk WHERE (expired_at BETWEEN '$today' AND '$nextMonth')")->queryAll();
        $arrayIujkId = array_column($getIujk, 'iujkId');

        if ($getIujk != null) {
            $arrayWarnIujk = [];
            for ($ai = 0; $ai < count($arrayIujkId); $ai++) {
                $modelIujk = PphIujk::find()->where(['iujkId' => $arrayIujkId[$ai]])->one();
                $expiredDate = date("d-m-Y", strtotime($modelIujk->expired_at));
                $nomorIujk = $modelIujk->nomorIujk;
                $arrayWarnIujk[] = "Masa berlaku Surat " . "<strong>" . "IUJK" . "</strong>" . " dengan nomor " . "<strong>" . $nomorIujk . "</strong>" . " akan berakhir pada tanggal " . "<strong>" . $expiredDate . "</strong>" . ".";
            }
            Yii::$app->session->setFlash('warning', array_unique($arrayWarnIujk));
        }

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        if ($getDataCount != null) {
            $session['dataCount-pasal-4'] = 1;
        }
        $status = $session['expiredDate'];

        if ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
        }
        if (Yii::$app->user->id == 1) {
            $arrayId = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate' AND nomorBuktiPotong IS NOT NULL")->queryAll();
            $arrayId = array_column($arrayId, 'buktiPotongId');
            $arrayIdCreate = Yii::$app->db->CreateCommand("SELECT buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate'")->queryAll();
            $arrayIdCreate = array_column($arrayIdCreate, 'buktiPotongId');
            $arrayCrNoBp = null;
            $arrayRmNoBp = null;
            if ($arrayIdCreate == null) {
                $arrayCrNoBp = false;
            } else {
                $arrayCrNoBp = true;
            }
            if ($arrayId == null) {
                $arrayRmNoBp = true;
            } else {
                $arrayRmNoBp = false;
            }
        } else {
            $arrayRmNoBp = true;
            $arrayCrNoBp = false;
        };
        if ($userId != 1) {
            $searchModel->userId = $userId;
        }
        $searchModel->tanggal = $monthUpdate;
        $searchModel->pasalId = 4;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider->pagination->pageSize = 100;

        return $this->render('index-pasal-4', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'arrayCrNoBp' => $arrayCrNoBp,
                    'arrayRmNoBp' => $arrayRmNoBp,
        ]);
    }

    /**
     * Displays a single PphBuktiPotong model.
     * @param integer $id
     * @return mixed
     */
    public function actionViewPasal21Honorer($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-21-honorer', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionViewPasal21Mandor($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-21-mandor', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionView($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionViewPasal23Jasa($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-23-jasa', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionViewPasal23Sewa($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-23-sewa', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionViewPasal4Jasa($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-4-jasa', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionViewPasal4Sewa($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-4-sewa', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionViewPasal4Perencana($id) {
        $modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();
        $created_by = $modelBp->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            return $this->render('view-pasal-4-perencana', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Creates a new PphBuktiPotong model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGenerateBp() {
        $rolesName = Yii::$app->rbaccomponent->myRole();
        $rulesName = Yii::$app->rbaccomponent->ableTo('generate_numb_bp');
        if ($rulesName == true) {
            $model = new PphBuktiPotong();
            $userId = Yii::$app->user->id;
            $session = Yii::$app->session;
            $connection = \Yii::$app->db;
            $arrayMasa = explode(' ', $session['masaPajak']);
            $namaMasa = $arrayMasa[0];
            $monthUpdate = \Yii::t('app', $arrayMasa[0]);
            $yearUpdate = $arrayMasa[1];
            $pasal = $session['pasal-terpilih'];
            
//            unset($session['pasal-terpilih']);

            $arrayId = Yii::$app->db->CreateCommand("SELECT userId, buktiPotongId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate' AND pasalId = '$pasal' AND nomorBuktiPotong IS NULL ORDER BY userId ASC, buktiPotongId ASC")->queryAll();
            $arrayBuktiPotong = [];
   
            if (!isset($arrayId[0])) {
                $notification = "Seluruh Baris data dengan masa pajak " . "<strong>" . $session['masaPajak'] . "</strong>" . "  telah memiliki Nomor Bukti Potong";
                $session['notification-failed-generate'] = $notification;
                return $this->redirect(['index']);
            }
            $user = $arrayId[0]['userId'];

            $element = 0;
            for ($a = 0; $a < count($arrayId); $a++) {
                if ($arrayId[$a]['userId'] == $user) {
                    if (!isset($arrayBuktiPotong[$element])) {
                        $arrayBuktiPotong[$element][] = $arrayId[$a]['userId'];
                    }
                    $arrayBuktiPotong[$element][] = $arrayId[$a]['buktiPotongId'];
                } else {
                    $user = $arrayId[$a]['userId'];
                    $element = $element + 1;
                    if (!isset($arrayBuktiPotong[$element])) {
                        $arrayBuktiPotong[$element][] = $arrayId[$a]['userId'];
                    }
                    $arrayBuktiPotong[$element][] = $arrayId[$a]['buktiPotongId'];
                }
            }

            $modelCalendar = PphCalendar::find()->where(['created_by' => $userId])->one();
            $countArray = count($arrayBuktiPotong);

            $years = DateTime::createFromFormat('Y', $yearUpdate);
            $year = $years->format('y');
            if ($monthUpdate < 10) {
                $month = '0' . $monthUpdate;
            } else {
                $month = $monthUpdate;
            }
            $noBP = null;

            $emptySetting = [];
            for ($n = 0; $n < $countArray; $n++) {
                $selectedUser = $arrayBuktiPotong[$n][0];
                if($pasal == 5){
                    $arrayNomorBP = Yii::$app->db->CreateCommand("SELECT nomorBuktiPotongId FROM pph_nomor_bukti_potong WHERE tahun = '$yearUpdate' AND userId = '$selectedUser' ORDER BY userId ASC")->queryAll();
                } else {
                    $arrayNomorBP = Yii::$app->db->CreateCommand("SELECT nomorBuktiPotongId FROM pph_nomor_bukti_potong WHERE bulan = '$monthUpdate' AND tahun = '$yearUpdate' AND userId = '$selectedUser' ORDER BY userId ASC")->queryAll();
                }                               

                $dataSetting = Setting::find()->where(['userId' => $arrayBuktiPotong[$n][0]])->asArray()->one();
                $statusNoBP21 = $dataSetting['pasal21_ptt'];
                if ($statusNoBP21 == 1) {                    
                    $statusNoBP = 1;
                } else {                    
                    if($selectedUser == 1){
                        $statusNoBP = 1;
                    } else {
                        $statusNoBP = 0;
                    }
                }
                        
                $userIndukId = $dataSetting['userIndukId'];
                if($statusNoBP == 0 && $userIndukId == NULL){
                    $userIndukId = 1;   
                } else if($selectedUser == 1){
                    $userIndukId = 1;  
                } else if($userIndukId != NULL){
                    $userIndukId = $dataSetting['userIndukId']; 
                }
                
//                echo $n.' = ';
//                var_dump($userIndukId);
//                echo '<br>';
                //cek apakah array daftar nomor BP untuk user ini kosong atau tidak
//                echo $statusNoBP.' = '.$selectedUser.'<br>';
                if ($arrayNomorBP == null) {
                    //jika kosong maka tambahkan
                    $dateNomorBP = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $createdNomorBP = $dateNomorBP->format('Y:m:d H:i:s');
                    if($pasal == 5 && $statusNoBP != 0){  
                        $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP_21, userId, created_at) VALUES('$yearUpdate', NULL, NULL, '$selectedUser', '$createdNomorBP')";
                        $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();  
                    } else if($pasal == 2){
                        $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP, userId, created_at) VALUES('$yearUpdate', '$monthUpdate', NULL, '$selectedUser', '$createdNomorBP')";
                        $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();
                    } else if($pasal == 3){
                        $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP_23, userId, created_at) VALUES('$yearUpdate', '$monthUpdate', NULL, '$selectedUser', '$createdNomorBP')";
                        $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();
                    } else if($pasal == 4){
                        $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP_4, userId, created_at) VALUES('$yearUpdate', '$monthUpdate', NULL, '$selectedUser', '$createdNomorBP')";
                        $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();
                    } else if($pasal == 5){
                        if($userIndukId == 0){                            
                            $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP_21, userId, created_at) VALUES('$yearUpdate', NULL, NULL, '$selectedUser', '$createdNomorBP')";
                            $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();  
                        } else if($userIndukId == $selectedUser){
                            $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP_21, userId, created_at) VALUES('$yearUpdate', NULL, NULL, '$userIndukId', '$createdNomorBP')";
                            $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();  
                        } else if($userIndukId != NULL && $userIndukId != 1){
                            $commandCreate = "INSERT INTO pph_nomor_bukti_potong (tahun, bulan, lastNomorBP_21, userId, created_at) VALUES('$yearUpdate', NULL, NULL, '$userIndukId', '$createdNomorBP')";
                            $execInsert = Yii::$app->db->createCommand($commandCreate)->execute();  
                        }
                    }                   
                }  
                
                if($pasal == 5){
                    if($statusNoBP == 0){
                        $modelNomorBP = PphNomorBuktiPotong::find()->where(['userId' => 1, 'tahun' => $yearUpdate])->one();
                    } else {
                        $modelNomorBP = PphNomorBuktiPotong::find()->where(['userId' => $selectedUser, 'tahun' => $yearUpdate])->one(); 
                    }
                } else {
                    $modelNomorBP = PphNomorBuktiPotong::find()->where(['userId' => $selectedUser, 'bulan' => $monthUpdate,'tahun' => $yearUpdate])->one();
                }
                
                $modelSetting = Setting::find()->where(['userId' => $selectedUser])->one();
                if (!isset($modelSetting)) {                    
                    $namaSetting = $model->getNamaUser($selectedUser);
                    $emptySetting[] = "Nomor bukti bukti untuk Pengguna " . "<strong>" . $namaSetting . "</strong>" . " tidak berhasil, dikarenakan pengguna belum memiliki data pada halaman Setting";
                    continue;
                }
                
                if($pasal == 5 && $statusNoBP == 0){                    
                    $modelSetting = Setting::find()->where(['userId' => 1])->one();
                    if (!isset($modelSetting)) {
                        $namaSetting = $model->getNamaUser($selectedUser);
                        $emptySetting[] = "Nomor bukti bukti untuk Pengguna " . "<strong>" . $namaSetting . "</strong>" . " tidak berhasil, dikarenakan pengguna belum memiliki data pada halaman Setting";
                        continue;
                    }
                }                               
                                
                if($pasal == 5){
                    $getCalendarModel = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE YEAR(startDate) = '$yearUpdate' AND status = '0'")->queryAll();
                } else {
                    $getCalendarModel = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE calendarNote = '$namaMasa' AND YEAR(startDate) = '$yearUpdate' AND status = '0'")->queryAll();
                }
                
                $calendarStatus = null;
                $calId = $getCalendarModel[0]['calendarId'];
                if ($pasal == 2) {
                    $qcalendarStatus = $getCalendarModel[0]['statusBP'];
                    $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP = '1' WHERE calendarId = '$calId'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
                } else if ($pasal == 3) {
                    $calendarStatus = $getCalendarModel[0]['statusBP_23'];
                    $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP_23 = '1' WHERE calendarId = '$calId'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
                } else if ($pasal == 5) {                     
                    $calendarStatus = $getCalendarModel[0]['statusBP_21'];
                    $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP_21 = '1' WHERE YEAR(startDate) = '$yearUpdate'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
                } else {
                    $calendarStatus = $getCalendarModel[0]['statusBP_4'];
                    $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP_4 = '1' WHERE calendarId = '$calId'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
                }
                
//                if ($calendarStatus == NULL) {                    
//                    if ($pasal == 2) {
//                        $prefixBP = $modelSetting->prefixNomorBP;
//                        $lastBP = null;
//                        $startBP = $modelSetting->startNomorBP;
//                        $length = $modelSetting->digitNomorBP;
//                    } else if ($pasal == 3) {
//                        $prefixBP = $modelSetting->prefixNomorBP_23;
//                        $lastBP = null;
//                        $startBP = $modelSetting->startNomorBP_23;
//                        $length = $modelSetting->digitNomorBP_23;
//                    } else if ($pasal == 5) {
//                        $prefixBP = $modelSetting->prefixNomorBP_21;
//                        $lastBP = null;
//                        $startBP = $modelSetting->startNomorBP_21;
//                        $length = $modelSetting->endNomorBP_21;
//                        if($userIndukId != NULL){                            
//                            $modelSettingInduk = Setting::find()->where(['userId' => $userIndukId])->asArray()->one();
//                            $prefixBP = $modelSettingInduk->prefixNomorBP_21;
//                            $lastBP = null;
//                            $startBP = $modelSettingInduk->startNomorBP_21;
//                            $length = $modelSettingInduk->endNomorBP_21;
//                        }
//                    } else {
//                        $prefixBP = $modelSetting->prefixNomorBP_4;
//                        $lastBP = null;
//                        $startBP = $modelSetting->startNomorBP_4;
//                        $length = $modelSetting->digitNomorBP_4;
//                    }
//                } else {
                    if ($pasal == 2) {
                        $prefixBP = $modelSetting->prefixNomorBP;
                        $lastBP = $modelNomorBP->lastNomorBP;
                        $startBP = $modelSetting->startNomorBP;
                        $length = $modelSetting->digitNomorBP;
                    } else if ($pasal == 3) {
                        $prefixBP = $modelSetting->prefixNomorBP_23;
                        $lastBP = $modelNomorBP->lastNomorBP_23;
                        $startBP = $modelSetting->startNomorBP_23;
                        $length = $modelSetting->digitNomorBP_23;
                    } else if ($pasal == 5) {
                        $prefixBP = $modelSetting->prefixNomorBP_21;
                        $lastBP = $modelNomorBP->lastNomorBP_21;
                        $startBP = $modelSetting->startNomorBP_21;
                        $length = $modelSetting->endNomorBP_21;
                        if($userIndukId != null){                            
                            $modelSettingInduk = Setting::find()->where(['userId' => $userIndukId])->asArray()->one();
                            $prefixBP = $modelSettingInduk['prefixNomorBP_21'];
                            $modelNomorBPInduk = PphNomorBuktiPotong::find()->where(['userId' => $userIndukId, 'tahun' => $yearUpdate])->one();
                            $lastBP = $modelNomorBPInduk['lastNomorBP_21'];
                            $startBP = $modelSettingInduk['startNomorBP_21'];
                            $length = $modelSettingInduk['endNomorBP_21'];
                        }
                    } else {
                        $prefixBP = $modelSetting->prefixNomorBP_4;
                        $lastBP = $modelNomorBP->lastNomorBP_4;
                        $startBP = $modelSetting->startNomorBP_4;
                        $length = $modelSetting->digitNomorBP_4;
                    }
//                }
               
                for ($z = 1; $z < count($arrayBuktiPotong[$n]); $z++) {
                    if($pasal == 5){                        
                        if ($lastBP == null) {
                                $noBP = $startBP;
                                $lastBP = $noBP;
                            } else {
                                $noBP = $lastBP + 1;
                                $lastBP = $noBP;
                            }
                    } else {
                            if ($lastBP == null) {
                                $noBP = $startBP;
                                $lastBP = $noBP;
                            } else {
                                $noBP = $lastBP + 1;
                                $pre = null;
                                if (strlen($noBP) < $length) {
                                    $less = $length - strlen($noBP);
                                    for ($m = 1; $m <= $less; $m++) {
                                        $pre = $pre . '0';
                                    }
                                    $noBP = $pre . $noBP;
                                }
                                $lastBP = $noBP;
                            }
                    }
                    if ($pasal == 5) {
                        $nomorBuktiPotong = $prefixBP . '-' . $month . '.' . $year . '-' . $noBP;
                    } else {
                        $nomorBuktiPotong = $noBP . '/' . $prefixBP . '/' . $month . '/' . $year;
                    }
                    $bpId = $arrayBuktiPotong[$n][$z];
                    if($pasal == 5){
                        if($userIndukId != NULL){
                            $indukId = $userIndukId;
                        } else if($statusNoBP == 0){
                            $indukId = 1;
                        } else {
                            $indukId = 'NULL';
                        }
                    } else {
                        $indukId = 'NULL';
                    }
                    $commandUpdateBuktiPotong = "UPDATE pph_bukti_potong SET nomorBuktiPotong = '$nomorBuktiPotong', userIndukId = '$indukId' WHERE buktiPotongId = '$bpId'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();
                }
                
                $idUser = $arrayBuktiPotong[$n][0];
                if ($modelNomorBP->lastNomorBP != null) {
                    $prev = $modelNomorBP->lastNomorBP;
                } else {
                    $prev = 'NULL';
                }
                
                if($pasal == 5){
                    if($statusNoBP == 0 && $userIndukId == null){
                        $commandUpdateBuktiPotong = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_21 = '$noBP' WHERE userId = '1' AND tahun = '$yearUpdate'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();   
                    } else if($userIndukId != null){
                        $commandUpdateBuktiPotong = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_21 = '$noBP' WHERE userId = '$userIndukId' AND tahun = '$yearUpdate'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();                        
                    } else {
                        $commandUpdateBuktiPotong = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_21 = '$noBP' WHERE userId = '$idUser' AND tahun = '$yearUpdate'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();
                    }                   
                } else if($pasal == 4) {
                    $commandUpdateBuktiPotong = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_23 = '$noBP' WHERE userId = '$idUser' AND bulan = '$monthUpdate' AND tahun = '$yearUpdate'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();
                } else if($pasal == 3) {
                    $commandUpdateBuktiPotong = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_4 = '$noBP' WHERE userId = '$idUser' AND bulan = '$monthUpdate' AND tahun = '$yearUpdate'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();
                } else {
                    $commandUpdateBuktiPotong = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_4 = '$noBP' WHERE userId = '$idUser' AND bulan = '$monthUpdate' AND tahun = '$yearUpdate'";
                    $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();
                } 
                
            }
//            die();
            if (count($emptySetting)) {
                Yii::$app->session->setFlash('info', $emptySetting);
            }

            Yii::$app->session->setFlash('success', "Nomor Bukti Potong untuk Masa Pajak " . "<strong>" . $session['masaPajak'] . "</strong>" . " berhasil di " . "<strong>" . "Generate" . "</strong>");
            if ($pasal == 2) {
                return $this->redirect(['index']);
            } else if ($pasal == 3) {
                return $this->redirect(['index-pasal-23']);
            } else if ($pasal == 4) {
                return $this->redirect(['index-pasal-4']);
            } else if ($pasal == 5) {
                return $this->redirect(['index-pasal-21']);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionRemoveBp() {
        $rolesName = Yii::$app->rbaccomponent->myRole();
        $rulesName = Yii::$app->rbaccomponent->ableTo('remove_numb_bp');
        if ($rulesName == true) {
            $userId = Yii::$app->user->id;
            $session = Yii::$app->session;
            $monthUpdate = $session['masaId'];
            $namaMasa = \Yii::t('app', $monthUpdate);
            $yearUpdate = preg_replace('/[^0-9]/', '', $session['masaPajak']);;
            $pasal = $session['pasal-terpilih'];
            unset($session['pasal-terpilih']);
            
            $action = Yii::$app->request->post('action');
            $selection = (array) Yii::$app->request->post('selection');
            $countSelected = count($selection);            

            $getCalendarModel = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE calendarNote = '$namaMasa' AND YEAR(startDate) AND status = '0'")->queryAll();
            $calendarStatus = null;
            $calId = $getCalendarModel[0]['calendarId'];
            if ($pasal == 2) {
                $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP = NULL WHERE calendarId = '$calId'";
                $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
            } else if ($pasal == 3) {
                $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP_23 = NULL WHERE calendarId = '$calId'";
                $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
            } else if ($pasal == 5) {
                $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP_21 = NULL WHERE calendarId = '$calId'";
                $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
            } else {
                $commandUpdateCalendar = "UPDATE pph_calendar SET statusBP_4 = NULL WHERE calendarId = '$calId'";
                $execUpdate = Yii::$app->db->createCommand($commandUpdateCalendar)->execute();
            }

            //update nomor BP menjadi null            

            $getId = Yii::$app->db->CreateCommand("SELECT DISTINCT userIndukId, userId FROM pph_bukti_potong WHERE MONTH(tanggal) = '$monthUpdate' AND YEAR(tanggal) = '$yearUpdate' AND pasalId = '$pasal' ORDER BY userId")->queryAll();                                    
            for($an = 0; $an < count($getId); $an++){
                if($getId[$an]['userIndukId'] == 0){
                    $getId[$an]['userIndukId'] = $getId[$an]['userId'];
                }
                unset($getId[$an]['userId']);
            }
            
            $arrayUserId = array_column($getId, 'userIndukId');
            $arrayUserId = array_unique($arrayUserId);
            $arrayUserId= array_values($arrayUserId);
            
//            echo '<pre>';
//            print_r($countSelected);
//            die();
            
            for ($n = 0; $n < count($arrayUserId); $n++) {            
                $userIndukId = $arrayUserId[$n];
                $dataSetting = Setting::find()->where(['userId' => $userIndukId])->asArray()->one();
                $statusNoBP21 = $dataSetting['pasal21_ptt'];

                if($pasal == 5){                                                                             
                        $getDataBp = Yii::$app->db->CreateCommand("SELECT nomorBuktiPotong FROM pph_bukti_potong WHERE pasalId = '5' AND userIndukId = '$userIndukId' AND year(tanggal) = '$yearUpdate' AND month(tanggal) = '$monthUpdate' ORDER BY nomorBuktiPotong ASC")->queryAll();
                        if($getDataBp == NULL){
//                            echo 'A'.'<br>';
                            $getDataBp = Yii::$app->db->CreateCommand("SELECT nomorBuktiPotong FROM pph_bukti_potong WHERE pasalId = '5' AND userId = '$userIndukId' AND year(tanggal) = '$yearUpdate' AND month(tanggal) = '$monthUpdate' ORDER BY nomorBuktiPotong ASC")->queryAll();
//                            echo '<pre>';
//                            print_r($getDataBp);
//                            die();
                            $firstNBP = substr($getDataBp[0]['nomorBuktiPotong'], -7);
                            $getDataBp2 = Yii::$app->db->CreateCommand("SELECT nomorBuktiPotong FROM pph_bukti_potong WHERE pasalId = '5' AND userId = '$userIndukId' AND year(tanggal) = '$yearUpdate' AND month(tanggal) = '$monthUpdate' ORDER BY nomorBuktiPotong DESC")->queryAll();                     
                            $lastNBP = substr($getDataBp2[0]['nomorBuktiPotong'], -7);
                            echo $firstNBP.'<br>';
                        } else {
//                            echo 'B'.'<br>';
                            $firstNBP = substr($getDataBp[0]['nomorBuktiPotong'], -7);
                            $getDataBp2 = Yii::$app->db->CreateCommand("SELECT nomorBuktiPotong FROM pph_bukti_potong WHERE pasalId = '5' AND userIndukId = '$userIndukId' AND year(tanggal) = '$yearUpdate' AND month(tanggal) = '$monthUpdate' ORDER BY nomorBuktiPotong DESC")->queryAll();                     
                            $lastNBP = substr($getDataBp2[0]['nomorBuktiPotong'], -7);
//                            echo $firstNBP.'<br>';
                        }                        
                        $modelLastSetting = PphNomorBuktiPotong::find()->where(['userId' => $userIndukId, 'tahun' => $yearUpdate])->one();
                        $startBP = $dataSetting['startNomorBP_21'];
                        $lastNoBPInModel = (int)$modelLastSetting['lastNomorBP_21']; 
                        $renewBP = (int)$firstNBP;   
//                        echo $renewBP.' = '.$startBP.'<br>';
                        if($renewBP == $startBP && $lastNoBPInModel == $lastNBP){
//                            echo $renewBP.'<br>';
                            $commandUpdateNomorBP = "DELETE FROM pph_nomor_bukti_potong WHERE tahun = '$yearUpdate' AND userId = '$userIndukId'";
                            $execUpdateSetting = Yii::$app->db->createCommand($commandUpdateNomorBP)->execute();
                        } else if($lastNoBPInModel == $lastNBP){
//                            echo $renewBP.'<br>';
                            $commandUpdateNomorBP = "UPDATE pph_nomor_bukti_potong SET lastNomorBP_21 = '$renewBP' WHERE tahun = '$yearUpdate' AND userId = '$userIndukId'";
                            $execUpdateSetting = Yii::$app->db->createCommand($commandUpdateNomorBP)->execute();
                        }                        
                } else {
                    $commandUpdateNomorBP = "DELETE FROM pph_nomor_bukti_potong WHERE bulan = '$monthUpdate' AND tahun = '$yearUpdate'";
                    $execUpdateSetting = Yii::$app->db->createCommand($commandUpdateNomorBP)->execute();
                }                
                $commandUpdateBuktiPotong = "UPDATE pph_bukti_potong SET nomorBuktiPotong = NULL WHERE YEAR(tanggal) = '$yearUpdate' AND MONTH(tanggal) = '$monthUpdate' AND pasalId = '$pasal' AND (userId = '$userIndukId' OR userIndukId ='$userIndukId')";
                $execUpdate = Yii::$app->db->createCommand($commandUpdateBuktiPotong)->execute();
            }            
//            die();
            Yii::$app->session->setFlash('success', "Nomor Bukti Potong untuk Masa Pajak " . "<strong>" . $session['masaPajak'] . "</strong>" . " berhasil di " . "<strong>" . "Hapus" . "</strong>");
            if ($pasal == 2) {
                return $this->redirect(['index']);
            } else if ($pasal == 3) {
                return $this->redirect(['index-pasal-23']);
            } else if ($pasal == 4) {
                return $this->redirect(['index-pasal-4']);
            } else if ($pasal == 5) {
                return $this->redirect(['index-pasal-21']);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionCreatePasal21Mandor() {
        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP, digitNomorBP, startNomorBP, kppTerdaftar, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 21" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if (!isset($session['masaPajak'])) {
                return $this->redirect(['/pph-masa/masa']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / tidak terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index']);
                }
                if ($model->tanggal == null) {
                    $todate = date('d-m-Y');
                    $model->tanggal = $todate;
                }

                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 5;
                $model->statusKerjaId = 3;
                $model->nama = $model->nama;
                $model->alamat = $model->alamat;
                if (isset($_POST['npwp'])) {
                    if ($model->wajibPajakNonId == null) {
                        $error = "Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
                        Yii::$app->session->setFlash('error', $error);
                        return $this->redirect(['/pph-bukti-potong/index']);
                    }
                    $model->statusNpwpId = 2;
                    $model->wajibPajakId = NULL;
                    $modelWpNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $model->wajibPajakNonId])->one();
                    $model->nama = $modelWpNon->nama;
                    $model->alamat = $modelWpNon->alamat;
                    $tarifPlus = 20;
                } else {
                    $model->statusNpwpId = 1;
                    $model->wajibPajakId = $model->wajibPajakId;
                    $model->nama = null;
                    $model->alamat = null;
                    $tarifPlus = 0;
                    if (isset($session['idWp'])) {
                        $model->wajibPajakId = $session['idWp'];
                        unset($session['idWp']);
                    }
                }
                
                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(",", "", $model->jumlahBruto);
                $model->dpp = str_replace(",", "", $model->dpp);
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $model->dpp = str_replace(".", "", $model->dpp);
                $nilaiTagihan = str_replace(".", "", $model->nilaiTagihan);
                $potonganPegawai = str_replace(".", "", $model->potonganPegawai);
                $potonganMaterial = str_replace(".", "", $model->potonganMaterial);

                //tarif progressif
                $dpp = $model->dpp;
                $tarifProgressif = [];
                $range = [
                    '0' => '50000000',
                    '1' => '200000000',
                    '2' => '250000000',
                    '3' => '500000000',
                ];

                $tarif = 5;
                $arrayDpp = [];
                $totalPph = 0;
                $arrayTarif = [];
                if ($model->statusNpwpId == 1) {
                    for ($np = 0; $np < 4; $np++) {
                        if ($dpp != 0) {
                            $tarifAkhir = $tarif;
                        }
                        $arrayTarif[$np] = $tarif;
                        if ($dpp >= $range[$np]) {
                            $dpp = $dpp - $range[$np];
                            $tarifProgressif[$np] = ($range[$np] * $tarif) / 100;
                            $hasil = $range[$np];
                        } else if ($dpp < $range[$np]) {
                            $tarifProgressif[$np] = ($dpp * $tarif) / 100;
                            $hasil = $dpp;
                            $dpp = 0;
                        }
                        if ($np == 2) {
                            $tarif = $tarif + 5;
                        } else {
                            $tarif = $tarif + 10;
                        }
                        // echo $hasil;die();
                        $arrayDpp[$np] = number_format($hasil);
                        $totalPph = $totalPph + $tarifProgressif[$np];
                    }
                    $model->tarif = $tarifAkhir;
                } else {
                    $tarifDasar = 5;
                    for ($np = 0; $np < 4; $np++) {
                        if ($np > 0) {
                            $tarif = $tarifDasar;
                        }
                        $ext = ($tarif * $tarifPlus) / 100;
                        $tarif = $tarif + $ext;
                        if ($dpp != 0) {
                            $tarifAkhir = $tarif;
                        }
                        $arrayTarif[$np] = $tarif;
                        if ($dpp >= $range[$np]) {
                            $dpp = $dpp - $range[$np];
                            $tarifProgressif[$np] = ($range[$np] * $tarif) / 100;
                            $hasil = $range[$np];
                        } else if ($dpp < $range[$np]) {
                            $tarifProgressif[$np] = ($dpp * $tarif) / 100;
                            $hasil = $dpp;
                            $dpp = 0;
                        }
                        if ($np == 2) {
                            $tarifDasar = $tarifDasar + 5;
                        } else {
                            $tarifDasar = $tarifDasar + 10;
                        }
                        $arrayDpp[$np] = number_format($hasil);
                        $totalPph = $totalPph + $tarifProgressif[$np];
                    }
                    $model->tarif = $tarifAkhir;
                }

                $aT = implode(',', $arrayTarif);
                $jsonAT = json_encode($aT);
                $aD = implode('.', $arrayDpp);
                $jsonAD = json_encode($aD);
                $aTP = implode(',', $tarifProgressif);
                $jsonATP = json_encode($aTP);
                $session['at'] = $jsonAT;
                $session['ad'] = $jsonAD;
                $session['atp'] = $jsonATP;
                $session['total-pph'] = $totalPph;

                $model->jumlahPphDiPotong = $totalPph;
                $model->nilaiTagihan = str_replace(".", "", $model->nilaiTagihan);
                $model->potonganMaterial = str_replace(".", "", $model->potonganMaterial);
                $model->potonganPegawai = str_replace(".", "", $model->potonganPegawai);
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->is_calendar_close = 0;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }
//                echo '<pre>';
//                var_dump($model);
//                die();
                if ($status == true) {
                    $model->save(false);
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create']);
                }
                return $this->redirect(['view-pasal-21-mandor', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create-pasal-21-mandor', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

    public function actionCreatePasal21Honorer() {
        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP, digitNomorBP, startNomorBP, kppTerdaftar, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 21" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if (!isset($session['masaPajak'])) {
                return $this->redirect(['/pph-masa/masa']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index']);
                }
                if ($model->tanggal == null) {
                    $todate = date('d-m-Y');
                    $model->tanggal = $todate;
                }

                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 5;
                $model->statusKerjaId = 2;
                if (isset($_POST['npwp'])) {
                    if ($model->wajibPajakNonId == null) {
                        $error = "Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
                        Yii::$app->session->setFlash('error', $error);
                        return $this->redirect(['/pph-bukti-potong/index']);
                    }
                    $model->statusNpwpId = 2;
                    $model->wajibPajakId = NULL;
                    $modelWpNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $model->wajibPajakNonId])->one();
                    $model->nama = $modelWpNon->nama;
                    $model->alamat = $modelWpNon->alamat;
                    $tarifPlus = 20;
                } else {
                    $model->statusNpwpId = 1;
                    $model->wajibPajakId = $model->wajibPajakId;
                    $model->nama = null;
                    $model->alamat = null;
                    $tarifPlus = 0;
                    if (isset($session['idWp'])) {
                        $model->wajibPajakId = $session['idWp'];
                        unset($session['idWp']);
                    }
                }
                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $pph = str_replace(".", "", $model->jumlahBruto);

                //tarif progressif

                $tarifProgressif = [];
                $range = [
                    '0' => '50000000',
                    '1' => '200000000',
                    '2' => '250000000',
                    '3' => '500000000',
                ];

                $tarif = 5;
                $arrayPPh = [];
                $totalPph = 0;
                $arrayTarif = [];
                if ($model->statusNpwpId == 1) {
                    for ($np = 0; $np < 4; $np++) {
                        if ($pph != 0) {
                            $tarifAkhir = $tarif;
                        }
                        $arrayTarif[$np] = $tarif;
                        if ($pph >= $range[$np]) {
                            $pph = $pph - $range[$np];
                            $tarifProgressif[$np] = ($range[$np] * $tarif) / 100;
                            $hasil = $range[$np];
                        } else if ($pph < $range[$np]) {
                            $tarifProgressif[$np] = ($pph * $tarif) / 100;
                            $hasil = $pph;
                            $pph = 0;
                        }
                        if ($np == 2) {
                            $tarif = $tarif + 5;
                        } else {
                            $tarif = $tarif + 10;
                        }
                        
                        $arrayPPh[$np] = number_format($hasil);
                        $totalPph = $totalPph + $tarifProgressif[$np];
                    }
                    $model->tarif = $tarifAkhir;
                } else {
                    $tarifDasar = 5;
                    for ($np = 0; $np < 4; $np++) {
                        if ($np > 0) {
                            $tarif = $tarifDasar;
                        }
                        $ext = ($tarif * $tarifPlus) / 100;
                        $tarif = $tarif + $ext;
                        if ($pph != 0) {
                            $tarifAkhir = $tarif;
                        }
                        $arrayTarif[$np] = $tarif;
                        if ($pph >= $range[$np]) {
                            $pph = $pph - $range[$np];
                            $tarifProgressif[$np] = ($range[$np] * $tarif) / 100;
                            $hasil = $range[$np];
                        } else if ($pph < $range[$np]) {
                            $tarifProgressif[$np] = ($pph * $tarif) / 100;
                            $hasil = $pph;
                            $pph = 0;
                        }
                        if ($np == 2) {
                            $tarifDasar = $tarifDasar + 5;
                        } else {
                            $tarifDasar = $tarifDasar + 10;
                        }
                        $arrayPPh[$np] = number_format($hasil);
                        $totalPph = $totalPph + $tarifProgressif[$np];
                    }
                    $model->tarif = $tarifAkhir;
                }

                $aT = implode(',', $arrayTarif);
                $jsonAT = json_encode($aT);
                $aD = implode('.', $arrayPPh);
                $jsonAD = json_encode($aD);
                $aTP = implode(',', $tarifProgressif);
                $jsonATP = json_encode($aTP);
                $session['at'] = $jsonAT;
                $session['ad'] = $jsonAD;
                $session['atp'] = $jsonATP;
                $session['total-pph'] = $totalPph;

                $model->jumlahPphDiPotong = $totalPph;
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->is_calendar_close = 0;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }
//                echo '<pre>';
//                var_dump($model);
//                die();
                if ($status == true) {
                    $model->save(false);
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create']);
                }
                return $this->redirect(['view-pasal-21-honorer', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create-pasal-21-honorer', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

    public function actionCreate() {

        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelnon = new MasterWajibPajakNon();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP, digitNomorBP, startNomorBP, kppTerdaftar, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 22" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if (!isset($session['masaPajak'])) {
                return $this->redirect(['/pph-masa/masa']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();

                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index']);
                }
                if ($model->tanggal == null) {
                    $todate = date('d-m-Y');
                    $model->tanggal = $todate;
                }

                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 2;
                $model->nama = $model->nama;
                $model->alamat = $model->alamat;
                if (isset($_POST['npwp'])) {
                    if ($model->wajibPajakNonId == null) {
                        $error = "Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
                        Yii::$app->session->setFlash('error', $error);
                        return $this->redirect(['/pph-bukti-potong/index']);
                    }
                    $model->statusNpwpId = 2;
                    $model->wajibPajakId = NULL;
                    $modelWpNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $model->wajibPajakNonId])->one();
                    $model->nama = $modelWpNon->nama;
                    $model->alamat = $modelWpNon->alamat;
                    $modelTarif = PphTarif::find()->where(['tarifId' => 2])->one();
                    $tarif = $modelTarif->tarif;
                } else {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 1])->one();
                    $tarif = $modelTarif->tarif;
                    $model->statusNpwpId = 1;
                    $model->wajibPajakNonId = NULL;
//                    $model->wajibPajakId = $model->wajibPajakId;
                    $model->nama = null;
                    $model->alamat = null;
                    if (isset($session['idWp'])) {
                        $model->wajibPajakId = $session['idWp'];
                        unset($session['idWp']);
                    }
                }

                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $model->jumlahPphDiPotong = ($model->jumlahBruto * $tarif) / 100;
                $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->is_calendar_close = 0;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }
                if ($status == true) {
                    $model->save(false);

                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$model->pasalId' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '2', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create']);
                }
                return $this->redirect(['view', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create', [
                            'model' => $model,
                            'modelnon' => $modelnon,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

    public function actionCreatePasal23Jasa() {
        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP_23, digitNomorBP_23, startNomorBP_23, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP_23'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP_23'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP_23'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 23" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index-pasal-23']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
//            Yii::$app->session->setFlash('error', "Pendaftaran bukti potong untuk masa pajak " . "<strong>" . $masaPajak . "</strong>" . "  ditutup");
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index-pasal-23']);
                }
                $userId = Yii::$app->user->id;
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 3;
                $model->nama = $model->nama;
                $model->alamat = $model->alamat;
                if (isset($_POST['npwp'])) {
                    if ($model->wajibPajakNonId == null) {
                        $error = "Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
                        Yii::$app->session->setFlash('error', $error);
                        return $this->redirect(['/pph-bukti-potong/index']);
                    }
                    $modelWpNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $model->wajibPajakNonId])->one();
                    $model->nama = $modelWpNon->nama;
                    $model->alamat = $modelWpNon->alamat;
                    $modelTarif = PphTarif::find()->where(['tarifId' => 11])->one();
                    $tarif = $modelTarif->tarif;
                    $model->statusNpwpId = 4;
                    $model->wajibPajakId = NULL;
                } else {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 10])->one();
                    $tarif = $modelTarif->tarif;
                    $model->statusNpwpId = 3;
                    $model->wajibPajakId = $model->wajibPajakId;
                    $model->nama = null;
                    $model->alamat = null;
                    if (isset($session['idWp'])) {
                        $model->wajibPajakId = $session['idWp'];
                        unset($session['idWp']);
                    }
                }

                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $model->jumlahPphDiPotong = ($model->jumlahBruto * $tarif) / 100;
                $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->statusWpId = 3;
                $model->is_calendar_close = 0;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }
                if ($status == true) {
                    $model->save(false);
                    
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$model->pasalId' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '3', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create-pasal-23-jasa']);
                }
                return $this->redirect(['view-pasal-23-jasa', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create-pasal-23-jasa', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

    public function actionCreatePasal23Sewa() {
        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP_23, digitNomorBP_23, startNomorBP_23, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP_23'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP_23'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP_23'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 23" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index-pasal-23']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
//            Yii::$app->session->setFlash('error', "Pendaftaran bukti potong untuk masa pajak " . "<strong>" . $masaPajak . "</strong>" . "  ditutup");
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index']);
                }
                $userId = Yii::$app->user->id;
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 3;
                $model->nama = $model->nama;
                $model->alamat = $model->alamat;
                if (isset($_POST['npwp'])) {
                    if ($model->wajibPajakNonId == null) {
                        $error = "Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
                        Yii::$app->session->setFlash('error', $error);
                        return $this->redirect(['/pph-bukti-potong/index-pasal-23']);
                    }
                    $modelWpNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $model->wajibPajakNonId])->one();
                    $model->nama = $modelWpNon->nama;
                    $model->alamat = $modelWpNon->alamat;
                    $modelTarif = PphTarif::find()->where(['tarifId' => 11])->one();
                    $tarif = $modelTarif->tarif;
                    $model->statusNpwpId = 4;
                    $model->wajibPajakId = NULL;
                } else {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 10])->one();
                    $tarif = $modelTarif->tarif;
                    $model->statusNpwpId = 3;
                    $model->wajibPajakId = $model->wajibPajakId;
                    $model->nama = null;
                    $model->alamat = null;
                    if (isset($session['idWp'])) {
                        $model->wajibPajakId = $session['idWp'];
                        unset($session['idWp']);
                    }
                }
                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $model->jumlahPphDiPotong = ($model->jumlahBruto * $tarif) / 100;
                $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->statusWpId = 2;
                $model->is_calendar_close = 0;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }

                if ($status == true) {
                    $model->save(false);

                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$model->pasalId' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '3', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create-pasal-23-sewa']);
                }
                return $this->redirect(['view-pasal-23-sewa', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create-pasal-23-sewa', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

//    public function actionCreatePasal21() {
////        if(Yii::$app->user->can('admin')) {
//        $model = new PphBuktiPotong();
//        $modelWp = new PphWajibPajak();
//        $session = Yii::$app->session;
//        $userId = Yii::$app->user->id;
//        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
//
//        $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP_23, digitNomorBP_23, startNomorBP_23, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();
//        if ($getSettingNomorBp[0]['prefixNomorBP_21'] == null) {
//            $statusSettingError = 1;
//        } else if ($getSettingNomorBp[0]['digitNomorBP_21'] == null) {
//            $statusSettingError = 1;
//        } else if ($getSettingNomorBp[0]['startNomorBP_21'] == null) {
//            $statusSettingError = 1;
//        } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
//            $statusSettingError = 1;
//        } else {
//            $statusSettingError = 0;
//        }
//
//        if ($statusSettingError == 1) {
//            $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 21" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
//            Yii::$app->session->setFlash('error', $error);
//            return $this->redirect(['/pph-bukti-potong/index-pasal-21']);
//        }
//
//        $masaPajak = $session['masaPajak'];
//        $array = explode(' ', $masaPajak);
//        $bln = \Yii::t('app', $array[0]);
//        $tgl = '01';
//        if ($bln == date('m')) {
//            $tgl = date('d');
//        }
//
//        $masaBulan = $session['masaId'];
//        $date = $array[1] . '-' . $bln . '-' . $tgl;
//        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
//        $expired = [];
//        $status = false;
//
//        if ($getCalendar == null) {
//            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
//        } else if ($checkCalender == null) {
//            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
//        } else {
//            for ($x = 0; $x < count($getCalendar); $x++) {
//                $calId = $getCalendar[$x]['calendarId'];
//                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
//                if ($checkRange != null) {
//                    $status = true;
//                    $expired = null;
//                    break;
//                } else {
//                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
//                }
//            }
//        }
//
//        if ($expired != null) {
//            $session['expiredDate'] = 1;
//        } else {
//            $session['expiredDate'] = 0;
//        }
//
//        $status = $session['expiredDate'];
//
//        if (isset($session['saveFailed'])) {
//            unset($session['saveFailed']);
//        } elseif ($status == 1) {
//            Yii::$app->session->setFlash('error', array_unique($expired));
////            Yii::$app->session->setFlash('error', "Pendaftaran bukti potong untuk masa pajak " . "<strong>" . $masaPajak . "</strong>" . "  ditutup");
//        }
//
//        if ($model->load(Yii::$app->request->post())) {
//
//            $givenMonth = date("m", strtotime($model->tanggal));
//            if ($bln != $givenMonth) {
//                $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
//                Yii::$app->session->setFlash('error', $error);
//                return $this->redirect(['/pph-bukti-potong/index']);
//            }
//
//            $model->userId = $userId;
//            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
//            $created_at = $date->format('Y:m:d H:i:s');
//            $modelUser = User::find()->where(['id' => $userId])->one();
//            $model->parentId = $modelUser->parentId;
//            $model->pasalId = 2;
//            $model->nama = $model->nama;
//            $model->alamat = $model->alamat;
//            if (isset($_POST['npwp'])) {
//                if ($model->nama == null || $model->alamat == null) {
//                    $error = "Nama dan Alamat Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
//                    Yii::$app->session->setFlash('error', $error);
//                    return $this->redirect(['/pph-bukti-potong/index']);
//                }
//                $modelTarif = PphTarif::find()->where(['tarifId' => 2])->one();
//                $tarif = $modelTarif->tarif;
//                $model->statusNpwpId = 2;
//                $model->wajibPajakId = NULL;
//            } else {
//                $modelTarif = PphTarif::find()->where(['tarifId' => 1])->one();
//                $tarif = $modelTarif->tarif;
//                $model->statusNpwpId = 1;
//                $model->wajibPajakId = $model->wajibPajakId;
//                $model->nama = null;
//                $model->alamat = null;
//            }
//            if (isset($session['idWp'])) {
//                $model->wajibPajakId = $session['idWp'];
//                unset($session['idWp']);
//            }
//            $model->nomorBuktiPotong = null;
//            $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
//            $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
//            $model->jumlahPphDiPotong = ($model->jumlahBruto * $tarif) / 100;
//            $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
//            $model->userId = $userId;
//            $model->parentId = $modelUser->parentId;
//            $model->created_at = $created_at;
//            $model->created_by = $userId;
//            $model->statusNpwpId = $model->statusNpwpId;
//            $model->tanggal = $model->tanggal;
//            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
//            $masaPajak = $session['masaPajak'];
//            $array = explode(' ', $masaPajak);
//            $bln = \Yii::t('app', $array[0]);
//            $tgl = '01';
//            if ($bln == date('m')) {
//                $tgl = date('d');
//            }
//            $date = $array[1] . '-' . $bln . '-' . $tgl;
//            $expired = [];
//            $status = false;
//            if ($getCalendar == null) {
//                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
//            }
//            for ($x = 0; $x < count($getCalendar); $x++) {
//                $calId = $getCalendar[$x]['calendarId'];
//                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
//                if ($checkRange == null) {
//                    $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
//                } else {
//                    $status = true;
//                    break;
//                }
//            }
////            echo '<pre>';
////            print_r($model);
////            die();
//            if ($status == true) {
//                $model->save(false);
//            } else {
//                Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
//                $session['saveFailed'] = true;
//                return $this->redirect(['create-pasal-21']);
//            }
//            return $this->redirect(['view-pasal-21', 'id' => $model->buktiPotongId]);
//        } else {
//            return $this->render('create-pasal-21', [
//                        'model' => $model,
//                        'modelWp' => $modelWp,
//            ]);
//        }
////        } else {
////            throw new \yii\web\ForbiddenHttpException;
////        }
//    }

    public function actionCreatePasal4Sewa() {
        $model = new PphBuktiPotong();
        $modelWp = new PphWajibPajak();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

        $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP_4, digitNomorBP_4, startNomorBP_4, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

        if ($getSettingNomorBp == null) {
            $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
            Yii::$app->session->setFlash('error', $error);
            return $this->redirect(['/pph-bukti-potong/index']);
        }

        if ($getSettingNomorBp[0]['prefixNomorBP_4'] == null) {
            $statusSettingError = 1;
        } else if ($getSettingNomorBp[0]['digitNomorBP_4'] == null) {
            $statusSettingError = 1;
        } else if ($getSettingNomorBp[0]['startNomorBP_4'] == null) {
            $statusSettingError = 1;
        } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
            $statusSettingError = 1;
        } else {
            $statusSettingError = 0;
        }



        //cek options pengaturan validasi
        $modelOptions = Options::find()->where(['optionsId' => 1])->one();
        $optionsValue = $modelOptions->value;

        if ($statusSettingError == 1 && $optionsValue == 1) {
            $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 4 Ayat (2)" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
            Yii::$app->session->setFlash('error', $error);
            return $this->redirect(['/pph-bukti-potong/index-pasal-4']);
        }

        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if ($bln == date('m')) {
            $tgl = date('d');
        }

        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        $status = $session['expiredDate'];

        if (isset($session['saveFailed'])) {
            unset($session['saveFailed']);
        } elseif ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
        }

        if ($model->load(Yii::$app->request->post())) {

            $givenMonth = date("m", strtotime($model->tanggal));
            if ($bln != $givenMonth) {
                $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }
            $model->userId = $userId;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $created_at = $date->format('Y:m:d H:i:s');
            $modelUser = User::find()->where(['id' => $userId])->one();
            $model->parentId = $modelUser->parentId;
            $model->pasalId = 4;
            $model->nama = $model->nama;
            $model->alamat = $model->alamat;
            if (isset($_POST['npwp'])) {
                    if ($model->wajibPajakNonId == null) {
                        $error = "Wajib Pajak " . "<strong>" . "tidak boleh kosong" . "</strong>";
                        Yii::$app->session->setFlash('error', $error);
                        return $this->redirect(['/pph-bukti-potong/index-pasal-4']);
                    }
                    $model->statusNpwpId = 2;
                    $model->wajibPajakId = NULL;
                    $modelWpNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $model->wajibPajakNonId])->one();
                    $model->nama = $modelWpNon->nama;
                    $model->alamat = $modelWpNon->alamat;
                } else {
                    $model->statusNpwpId = 1;
                    $model->wajibPajakNonId = NULL;
//                    $model->wajibPajakId = $model->wajibPajakId;
                    $model->nama = null;
                    $model->alamat = null;
                    if (isset($session['idWp'])) {
                        $model->wajibPajakId = $session['idWp'];
                        unset($session['idWp']);
                    }
                }
            $model->nomorBuktiPotong = null;
            $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
            $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
            $model->jumlahPphDiPotong = ($model->jumlahBruto * 10) / 100;
            $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
            $model->userId = $userId;
            $model->parentId = $modelUser->parentId;
            $model->created_at = $created_at;
            $model->created_by = $userId;
            $model->statusNpwpId = $model->statusNpwpId;
            $model->tanggal = $model->tanggal;
            $model->lokasi_tanah = $model->lokasi_tanah;
            $model->statusWpId = 2;
            $model->is_calendar_close = 0;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $expired = [];
            $status = false;
            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            }
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange == null) {
                    $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                } else {
                    $status = true;
                    break;
                }
            }
            if ($status == true) {
                $model->save(false);

                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$model->pasalId' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '4', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
            } else {
                Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                $session['saveFailed'] = true;
                return $this->redirect(['create-pasal-4-sewa']);
            }
            return $this->redirect(['view-pasal-4-sewa', 'id' => $model->buktiPotongId]);
        } else {
            return $this->render('create-pasal-4-sewa', [
                        'model' => $model,
                        'modelWp' => $modelWp,
            ]);
        }
    }

    public function actionCreatePasal4Perencana() {
        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP_4, digitNomorBP_4, startNomorBP_4, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP_4'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP_4'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP_4'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 4 Ayat (2)" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index-pasal-4']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index']);
                }
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 4;
                $model->nama = $model->nama;
                $model->alamat = $model->alamat;
                $model->wajibPajakId = $session['wp-terpilih-perencana'];
                $modelBujpkWp = PphWajibPajak::find()->where(['wajibPajakId' => $model->wajibPajakId])->one();
                $bujpkWp = $modelBujpkWp->bujpkId;
                if ($bujpkWp == null) {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 13])->one();
                    $tarif = $modelTarif->tarif;
                } else {
                    $modelBujpk = PphBujpk::find()->where(['bujpkId' => $bujpkWp])->one();
                    $keuanganWp = $modelBujpk->kemampuanKeuangan;
                    $is_approve = $modelBujpk->is_approve;
                    if ($is_approve == 1) {
                        $is_expired = $modelBujpk->is_expired;
                        if ($is_expired == 1) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 13])->one();
                            $tarif = $modelTarif->tarif;
                        } else {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 12])->one();
                            $tarif = $modelTarif->tarif;
                        }
                    } else {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 13])->one();
                        $tarif = $modelTarif->tarif;
                    }
                }
                $model->wajibPajakId = $model->wajibPajakId;
                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $model->jumlahPphDiPotong = ($model->jumlahBruto * $tarif) / 100;
                $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->statusWpId = 4;
                $model->is_calendar_close = 0;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }
                if ($status == true) {
                    $model->save(false);

                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$model->pasalId' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '4', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create-pasal-4-perencana']);
                }
                return $this->redirect(['view-pasal-4-perencana', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create-pasal-4-perencana', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

    public function actionCreatePasal4Jasa() {
        if (isset(Yii::$app->user->id)) {
            $model = new PphBuktiPotong();
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            $userId = Yii::$app->user->id;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();

            $getSettingNomorBp = Yii::$app->db->CreateCommand("SELECT prefixNomorBP_4, digitNomorBP_4, startNomorBP_4, kppTerdaftar FROM setting WHERE userId = '$userId'")->queryAll();

            if ($getSettingNomorBp == null) {
                $error = "Data profil user " . "<strong>" . ucfirst(Yii::$app->user->identity->username) . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index']);
            }

            if ($getSettingNomorBp[0]['prefixNomorBP_4'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['digitNomorBP_4'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['startNomorBP_4'] == null) {
                $statusSettingError = 1;
            } else if ($getSettingNomorBp[0]['kppTerdaftar'] == null) {
                $statusSettingError = 1;
            } else {
                $statusSettingError = 0;
            }

            //cek options pengaturan validasi
            $modelOptions = Options::find()->where(['optionsId' => 1])->one();
            $optionsValue = $modelOptions->value;

            if ($statusSettingError == 1 && $optionsValue == 1) {
                $error = "Pengaturan Nomor Bukti Potong anda untuk Pasal 4 Ayat (2)" . "<strong>" . " masih kosong" . "</strong>" . " silahkan isi terlebih dahulu melalui Administrator";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bukti-potong/index-pasal-4']);
            }

            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }

            $masaBulan = $session['masaId'];
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
            $expired = [];
            $status = false;

            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            } else if ($checkCalender == null) {
                $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
            } else {
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange != null) {
                        $status = true;
                        $expired = null;
                        break;
                    } else {
                        $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                    }
                }
            }

            if ($expired != null) {
                $session['expiredDate'] = 1;
            } else {
                $session['expiredDate'] = 0;
            }

            $status = $session['expiredDate'];

            if (isset($session['saveFailed'])) {
                unset($session['saveFailed']);
            } elseif ($status == 1) {
                Yii::$app->session->setFlash('error', array_unique($expired));
            }

            if ($model->load(Yii::$app->request->post())) {

                $givenMonth = date("m", strtotime($model->tanggal));
                if ($bln != $givenMonth) {
                    $error = "Masa pada tanggal Bukti Potong " . "<strong>" . "tidak sesuai" . "</strong>" . " dengan Masa Pajak yang telah dipilih saat Login";
                    Yii::$app->session->setFlash('error', $error);
                    return $this->redirect(['/pph-bukti-potong/index']);
                }
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $created_at = $date->format('Y:m:d H:i:s');
                $modelUser = User::find()->where(['id' => $userId])->one();
                $model->parentId = $modelUser->parentId;
                $model->pasalId = 4;
                $model->nama = $model->nama;
                $model->alamat = $model->alamat;
                $model->wajibPajakId = $session['wp-terpilih'];
                $modelIujkWp = PphWajibPajak::find()->where(['wajibPajakId' => $model->wajibPajakId])->one();
                $iujkWp = $modelIujkWp->iujkId;
                if ($iujkWp == null) {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                    $tarif = $modelTarif->tarif;
                } else {
                    $modelIujk = PphIujk::find()->where(['iujkId' => $iujkWp])->one();
                    $keuanganWp = $modelIujk->kemampuanKeuangan;
                    $is_approve = $modelIujk->is_approve;
                    if ($is_approve == 1) {
                        $is_expired = $modelIujk->is_expired;
                        if ($is_expired == 1) {
                            $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                            $tarif = $modelTarif->tarif;
                        } else {
                            $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                            if ($tingkatKeuanganWp == 1) {
                                $modelTarif = PphTarif::find()->where(['tarifId' => 6])->one();
                                $tarif = $modelTarif->tarif;
                            } else if ($tingkatKeuanganWp == 2) {
                                $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                                $tarif = $modelTarif->tarif;
                            } else if ($tingkatKeuanganWp == 3) {
                                $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                                $tarif = $modelTarif->tarif;
                            }
                        }
                    } else {
                        $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                        $tarif = $modelTarif->tarif;
                    }
                }
                $model->wajibPajakId = $model->wajibPajakId;
                $model->nomorBuktiPotong = null;
                $model->jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $model->jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $model->jumlahPphDiPotong = ($model->jumlahBruto * $tarif) / 100;
                $model->jumlahPphDiPotong = floor($model->jumlahPphDiPotong);
                $model->userId = $userId;
                $model->parentId = $modelUser->parentId;
                $model->created_at = $created_at;
                $model->created_by = $userId;
                $model->statusNpwpId = $model->statusNpwpId;
                $model->tanggal = $model->tanggal;
                $model->statusWpId = 3;
                $model->is_calendar_close = 0;
//            $model->jenisBuktiPotong = 1;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $status = false;
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $status = true;
                        break;
                    }
                }
                if ($status == true) {
                    //$updateIujkQuery = "UPDATE pph_iujk SET is_active = '1' WHERE iujkId = '$iujkWp'";
                    //$updateIujkExec = Yii::$app->db->createCommand($updateIujkQuery)->execute();
                    $model->save(false);

                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();

                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$model->pasalId' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {                                
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '4', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
                } else {
                    Yii::$app->session->setFlash('error', "Bukti Potong tidak berhasil ditambahkan karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup");
                    $session['saveFailed'] = true;
                    return $this->redirect(['create-pasal-4-jasa']);
                }
                unset($session['tarif-pasal-4']);
                unset($session['wp-terpilih']);
                unset($session['submit-wp-nama']);
                return $this->redirect(['view-pasal-4-jasa', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('create-pasal-4-jasa', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            return $this->redirect(['/user/login']);
        }
    }

    /**
     * Updates an existing PphBuktiPotong model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionRemoveData() {
        $rolesName = Yii::$app->rbaccomponent->myRole();
        $rulesName = Yii::$app->rbaccomponent->ableTo('remove_data_bp');
        if ($rulesName == true) {
            $userId = Yii::$app->user->identity->id;
            $session = Yii::$app->session;
            $action = Yii::$app->request->post('action');
            $selection = (array) Yii::$app->request->post('selection');
            $countSelected = count($selection);
            $pasal = $session['pasal-terpilih'];
            $deleteSuccess = 0;
            $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
            $masaPajak = $session['masaPajak'];
            $array = explode(' ', $masaPajak);
            $bln = \Yii::t('app', $array[0]);
            $tgl = '01';
            if ($bln == date('m')) {
                $tgl = date('d');
            }
            $date = $array[1] . '-' . $bln . '-' . $tgl;
            $expired = [];
            $masaBulan = $session['masaId'];
            $statusRemoveData = false;
            if ($getCalendar == null) {
                $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
            }
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                if ($checkRange == null) {
                    $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                } else {
                    $statusRemoveData = true;
                    break;
                }
            }
            if ($statusRemoveData == true) {                                               
                $statusFailed = false;
                $deleteFailed = 0;
                for ($i = 0; $i < $countSelected; $i++) {
                    $modelBP = PphBuktiPotong::find()->where(['buktiPotongId' => $selection[$i]])->one();
                    $pasalId = $modelBP->pasalId;        

                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$bln' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$bln' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    if($checkCalender != null){
                        $lastClosedCalendar = $checkCalender[0]['lastClosed'];
                        if($lastClosedCalendar != null){
                            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                            $created_at_up = $date->format('Y:m:d H:i:s');
                            $uId = Yii::$app->user->id;
                            $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasal' AND userId = '$uId'")->queryAll();                
                            if($cekLogPembetulan != null){
                                $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                                $timeStampLog = strtotime($lastOpenedLog);
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $timeStampCalendar = strtotime($lastOpenedCalendar);
                                $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                                $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                                $pembCount = $pembetulanNumb;
                                if($timeStampLog != $timeStampCalendar){                                                        
                                    $newPembetulanNumb = $pembetulanNumb + 1;
                                    $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                                    $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                            
                                    $pembCount = $newPembetulanNumb;
                                }                        
                                $lastId = $pembetulanId;                        
                            } else {
                                $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                                $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasal', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                                $lastId = Yii::$app->db->getLastInsertID();
                                $pembCount = 1;
                            }
                        }
                    }
                    $nomorBP = $modelBP->nomorBuktiPotong;
                    if ($nomorBP == null) {
                        if ($userId == 1) {
                            $commandDeleteDetail = "DELETE FROM pph_bukti_potong WHERE buktiPotongId='$selection[$i]'";
                        } else {
                            $commandDeleteDetail = "DELETE FROM pph_bukti_potong WHERE buktiPotongId='$selection[$i]' AND userId='$userId'";
                        }
                        $execDeleteDetail = Yii::$app->db->createCommand($commandDeleteDetail)->execute();
                        if ($execDeleteDetail == true) {
                            $deleteSuccess = $deleteSuccess + 1;
                        }
                    } else {       
                        if($pasal == 5){
                           if ($userId == 1) {
                            $commandDeleteDetail = "DELETE FROM pph_bukti_potong WHERE buktiPotongId='$selection[$i]'";
                            } else {
                                $commandDeleteDetail = "DELETE FROM pph_bukti_potong WHERE buktiPotongId='$selection[$i]' AND userId='$userId'";
                            } 
                            $execDeleteDetail = Yii::$app->db->createCommand($commandDeleteDetail)->execute();
                            if ($execDeleteDetail == true) {
                                $deleteSuccess = $deleteSuccess + 1;
                            }
                        } else {
                            $statusFailed = true;
                            if ($statusFailed == true) {
                                $deleteFailed++;
                            }
                        }
                    }
                }
            } else {
                $failedMessage = "Bukti Potong tidak berhasil dihapus karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                $session['delete-bp-failed'] = $failedMessage;
                return $this->redirect(['index']);
            }

            if ($deleteSuccess > 0) {
                if ($deleteFailed == 0) {
                    $success = "Sebanyak " . "<strong>" . $deleteSuccess . "</strong>" . " Data berhasil dihapus.";
                    Yii::$app->session->setFlash('success', $success);
                } else {
                    $success = "Sebanyak " . "<strong>" . $deleteSuccess . "</strong>" . " Data berhasil dihapus.";
                    Yii::$app->session->setFlash('success', $success);
                    $failed = "Sebanyak " . "<strong>" . $deleteFailed . "</strong>" . " Data tidak berhasil dihapus.";
                    Yii::$app->session->setFlash('error', $failed);
                }
            } else {
                if ($deleteFailed > 0) {
                    $failed = "Sebanyak " . "<strong>" . $deleteFailed . "</strong>" . " Data tidak berhasil dihapus.";
                    Yii::$app->session->setFlash('error', $failed);
                }
            }
            $pasal = $session['pasal-terpilih'];
            unset($session['pasal-terpilih']);
            if ($pasal == 2) {
                return $this->redirect(['index']);
            } else if ($pasal == 3) {
                return $this->redirect(['index-pasal-23']);
            } else if ($pasal == 4) {
                return $this->redirect(['index-pasal-4']);
            } else if ($pasal == 5) {
                return $this->redirect(['index-pasal-21']);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $masaBulan = $session['masaId'];
                $pasalId = $model->pasalId;
                $statusWpId = $model->statusWpId;
                $wajibPajakId = $model->wajibPajakId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $trfId = null;
                if (isset($_POST['npwp'])) {
                    if ($pasalId == 2) {
                        $trfId = 2;
                    } else if ($pasalId == 3) {
                        $trfId = 11;
                    } else if ($pasalId == 4) {
                        $trfId = 3;
                    }
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 2;
                    $wajibPajakId = 'NULL';
                    $nama = $model->nama;
                    $alamat = $model->alamat;
                } else {
                    if ($pasalId == 2) {
                        $trfId = 1;
                    } else if ($pasalId == 3) {
                        $trfId = 10;
                    } else if ($pasalId == 4) {
                        if ($statusWpId == 3) {
                            $modelWaPa = PphWajibPajak::find()->where(['wajibPajakId' => $wajibPajakId])->one();
                            $iujkId = $modelWaPa->iujkId;
                            if ($iujkId != null) {
                                $modelIujk = PphIujk::find()->where(['iujkId' => $iujkId])->one();
                                $is_approve = $modelIujk->is_approve;
                                if ($is_approve == 1) {
                                    $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                                    if ($tingkatKeuanganWp == 1) {
                                        $trfId = 6;
                                    } else if ($tingkatKeuanganWp == 2) {
                                        $trfId = 7;
                                    } else if ($tingkatKeuanganWp == 3) {
                                        $trfId = 7;
                                    }
                                } else {
                                    $trfId = 8;
                                }
                            } else {
                                $trfId = 8;
                            }
                        } else if ($statusWpId == 2) {
                            $modelStatusVendor = PphStatusVendor::find()->where(['statusstatusWpId' => $statusWpId])->one();
                            $modelTarif = PphTarif::find()->where(['tarifId' => $modelStatusVendor->tarifId])->one();
                            $trfId = $modelTarif->tarifId;
                        } else {
                            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $wajibPajakId])->one();
                            if ($modelVendor->bujpkId == NULL) {
                                $trfId = 12;
                            } else {
                                $modelBujpk = PphBujpk::find()->where(['bujpkId' => $modelVendor->bujpkId])->one();
                                if ($modelBujpk->is_approve == 1) {
                                    $trfId = 13;
                                } else {
                                    $trfId = 13;
                                }
                            }
                        }
                    }
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 1;
                    $wajibPajakId = $model->wajibPajakId;
                    $nama = NULL;
                    $alamat = NULL;
                }
                if (isset($session['idWp'])) {
                    $model->wajibPajakId = $session['idWp'];
                    unset($session['idWp']);
                }
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $jumlahPphDiPotong = floor($jumlahPphDiPotong);
                $updated_at = $date->format('Y:m:d H:i:s');
                $tanggal = Yii::$app->formatter->asDatetime(strtotime($model->tanggal), "php:Y-m-d");
                $lokasi_tanah = $model->lokasi_tanah;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $statusUpdate = false;

                //jika ada perubahan maka tambahakan ke history update
                $prevData = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();

                $nomorPembukuanPrev = $prevData->nomorPembukuan;
                $tanggalPrev = $prevData->tanggal;
                $jumlahBrutoPrev = $prevData->jumlahBruto;
                $jumlahPphDiPotongPrev = $prevData->jumlahPphDiPotong;
                $pasalIdPrev = $prevData->pasalId;
                $statusWpIdPrev = $prevData->statusWpId;
                if($prevData->wajibPajakId != null){
                    $wajibPajakId = $prevData->wajibPajakId;
                    $wajibPajakNonId = NULL;
                } else {
                    $wajibPajakId = NULL;
                    $wajibPajakNonId = $prevData->wajibPajakNonId;
                }
                
                //compare
                $pembetulanFlag = 0;
                if($nomorPembukuanPrev != $model->nomorPembukuan){
                    $pembetulanFlag = 1;
                } else if($tanggalPrev != $tanggal){
                    $pembetulanFlag = 1;
                } else if($jumlahBrutoPrev != $model->jumlahBruto){
                    $pembetulanFlag = 1;
                } else if($jumlahPphDiPotongPrev != $model->jumlahPphDiPotong){
                    $pembetulanFlag = 1;
                } else if($wajibPajakId != $model->wajibPajakId){
                    $pembetulanFlag = 1;
                } else if($wajibPajakNonId != $model->wajibPajakNonId){
                    $pembetulanFlag = 1;
                } 
                
                //cek log Pembetulan
                if($pembetulanFlag == 1){
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $masaPajak = $session['masaPajak'];
                    $array = explode(' ', $masaPajak);                    
                    $masaBulan = $session['masaId'];
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    
                    $uId = Yii::$app->user->id;
                    $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasalIdPrev' AND userId = '$uId'")->queryAll();                
                    if($cekLogPembetulan != null){
                        $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                        $timeStampLog = strtotime($lastOpenedLog);
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $timeStampCalendar = strtotime($lastOpenedCalendar);
                        $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                        $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                        $pembCount = $pembetulanNumb;
                        if($timeStampLog != $timeStampCalendar){                                                        
                            $newPembetulanNumb = $pembetulanNumb + 1;
                            $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                            $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                                  
                            $pembCount = $newPembetulanNumb;
                        }                        
                        $lastId = $pembetulanId;                        
                    } else {                        
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasalIdPrev', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                        $lastId = Yii::$app->db->getLastInsertID();
                        $pembCount = 1;
                    }
                }
                
                $insertNewUpdate = Yii::$app->db->CreateCommand("INSERT INTO pph_history_update (nomorPembukuan, tanggal, jumlahBruto, jumlahPphDiPotong, userId, pembetulanId, buktiPotongId, pembetulanCount, pasalId) VALUES('$nomorPembukuanPrev', '$tanggalPrev', '$jumlahBrutoPrev', '$jumlahPphDiPotongPrev','$uId', '$lastId', '$id', '$pembCount', '$pasalId')")->execute();

                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $statusUpdate = true;
                        break;
                    }
                }
                if ($statusUpdate == true) {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                } else {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                    $failedMessage = "Bukti Potong tidak berhasil diubah karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                    $session['update-bp-failed'] = $failedMessage;
                    return $this->redirect(['index']);
                }

                return $this->redirect(['view', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        }
    }

    public function actionUpdatePasal23($id) {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post())) {
                $masaBulan = $session['masaId'];
                $pasalId = $model->pasalId;
                $statusWpId = $model->statusWpId;
                $wajibPajakId = $model->wajibPajakId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                if (isset($_POST['npwp'])) {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 11])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 4;
                    $wajibPajakId = 'NULL';
                    $nama = $model->nama;
                    $alamat = $model->alamat;
                } else {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 10])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 3;
                    $wajibPajakId = $model->wajibPajakId;
                    $nama = NULL;
                    $alamat = NULL;
                }
                if (isset($session['idWp'])) {
                    $model->wajibPajakId = $session['idWp'];
                    unset($session['idWp']);
                }
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahPphDiPotong = str_replace(",", "", $model->jumlahPphDiPotong);
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $jumlahPphDiPotong = floor($jumlahPphDiPotong);
                $updated_at = $date->format('Y:m:d H:i:s');
                $tanggal = Yii::$app->formatter->asDatetime(strtotime($model->tanggal), "php:Y-m-d");
                $lokasi_tanah = $model->lokasi_tanah;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $statusUpdate = false;

                //jika ada perubahan maka tambahakan ke history update
                $prevData = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();

                $nomorPembukuanPrev = $prevData->nomorPembukuan;
                $tanggalPrev = $prevData->tanggal;
                $jumlahBrutoPrev = $prevData->jumlahBruto;
                $jumlahPphDiPotongPrev = $prevData->jumlahPphDiPotong;
                $pasalIdPrev = $prevData->pasalId;
                $statusWpIdPrev = $prevData->statusWpId;
                if($prevData->wajibPajakId != null){
                    $wajibPajakId = $prevData->wajibPajakId;
                    $wajibPajakNonId = NULL;
                } else {
                    $wajibPajakId = NULL;
                    $wajibPajakNonId = $prevData->wajibPajakNonId;
                }
                
                //compare
                $pembetulanFlag = 0;
                if($nomorPembukuanPrev != $model->nomorPembukuan){
                    $pembetulanFlag = 1;
                } else if($tanggalPrev != $tanggal){
                    $pembetulanFlag = 1;
                } else if($jumlahBrutoPrev != $model->jumlahBruto){
                    $pembetulanFlag = 1;
                } else if($jumlahPphDiPotongPrev != $model->jumlahPphDiPotong){
                    $pembetulanFlag = 1;
                } else if($wajibPajakId != $model->wajibPajakId){
                    $pembetulanFlag = 1;
                } else if($wajibPajakNonId != $model->wajibPajakNonId){
                    $pembetulanFlag = 1;
                } 
                
                //cek log Pembetulan
                if($pembetulanFlag == 1){
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $masaPajak = $session['masaPajak'];
                    $array = explode(' ', $masaPajak);                    
                    $masaBulan = $session['masaId'];
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    
                    $uId = Yii::$app->user->id;
                    $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasalIdPrev' AND userId = '$uId'")->queryAll();                
                    if($cekLogPembetulan != null){
                        $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                        $timeStampLog = strtotime($lastOpenedLog);
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $timeStampCalendar = strtotime($lastOpenedCalendar);
                        $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                        $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                        $pembCount = $pembetulanNumb;
                        if($timeStampLog != $timeStampCalendar){                                                        
                            $newPembetulanNumb = $pembetulanNumb + 1;
                            $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                            $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                                  
                            $pembCount = $newPembetulanNumb;
                        }                        
                        $lastId = $pembetulanId;                        
                    } else {                        
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasalIdPrev', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                        $lastId = Yii::$app->db->getLastInsertID();
                        $pembCount = 1;
                    }
                }
                
                $insertNewUpdate = Yii::$app->db->CreateCommand("INSERT INTO pph_history_update (nomorPembukuan, tanggal, jumlahBruto, jumlahPphDiPotong, userId, pembetulanId, buktiPotongId, pembetulanCount, pasalId) VALUES('$nomorPembukuanPrev', '$tanggalPrev', '$jumlahBrutoPrev', '$jumlahPphDiPotongPrev','$uId', '$lastId', '$id', '$pembCount', '$pasalId')")->execute();                
                
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $statusUpdate = true;
                        break;
                    }
                }
                if ($statusUpdate == true) {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = '$wajibPajakId', tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = '$wajibPajakId', tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                } else {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                    $failedMessage = "Bukti Potong tidak berhasil diubah karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                    $session['update-bp-failed'] = $failedMessage;
                    return $this->redirect(['index']);
                }

                return $this->redirect(['view', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('update-pasal-23', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
    
    public function actionUpdatePasal21($id) {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $masaBulan = $session['masaId'];
                $pasalId = $model->pasalId;
                $statusWpId = $model->statusWpId;
                $wajibPajakId = $model->wajibPajakId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                if (isset($_POST['npwp'])) {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 11])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 4;
                    $wajibPajakId = 'NULL';
                    $nama = $model->nama;
                    $alamat = $model->alamat;
                } else {
                    $modelTarif = PphTarif::find()->where(['tarifId' => 10])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 3;
                    $wajibPajakId = $model->wajibPajakId;
                    $nama = NULL;
                    $alamat = NULL;
                }
                if (isset($session['idWp'])) {
                    $model->wajibPajakId = $session['idWp'];
                    unset($session['idWp']);
                }
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $jumlahPphDiPotong = floor($jumlahPphDiPotong);
                $updated_at = $date->format('Y:m:d H:i:s');
                $tanggal = Yii::$app->formatter->asDatetime(strtotime($model->tanggal), "php:Y-m-d");
                $lokasi_tanah = $model->lokasi_tanah;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $statusUpdate = false;
                
                //jika ada perubahan maka tambahakan ke history update
                $prevData = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();

                $nomorPembukuanPrev = $prevData->nomorPembukuan;
                $tanggalPrev = $prevData->tanggal;
                $jumlahBrutoPrev = $prevData->jumlahBruto;
                $jumlahPphDiPotongPrev = $prevData->jumlahPphDiPotong;
                $pasalIdPrev = $prevData->pasalId;
                $statusWpIdPrev = $prevData->statusWpId;
                if($prevData->wajibPajakId != null){
                    $wajibPajakId = $prevData->wajibPajakId;
                    $wajibPajakNonId = NULL;
                } else {
                    $wajibPajakId = NULL;
                    $wajibPajakNonId = $prevData->wajibPajakNonId;
                }
                
                //compare
                $pembetulanFlag = 0;
                if($nomorPembukuanPrev != $model->nomorPembukuan){
                    $pembetulanFlag = 1;
                } else if($tanggalPrev != $tanggal){
                    $pembetulanFlag = 1;
                } else if($jumlahBrutoPrev != $model->jumlahBruto){
                    $pembetulanFlag = 1;
                } else if($jumlahPphDiPotongPrev != $model->jumlahPphDiPotong){
                    $pembetulanFlag = 1;
                } else if($wajibPajakId != $model->wajibPajakId){
                    $pembetulanFlag = 1;
                } else if($wajibPajakNonId != $model->wajibPajakNonId){
                    $pembetulanFlag = 1;
                } 
                
                //cek log Pembetulan
                if($pembetulanFlag == 1){
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $masaPajak = $session['masaPajak'];
                    $array = explode(' ', $masaPajak);                    
                    $masaBulan = $session['masaId'];
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    
                    $uId = Yii::$app->user->id;
                    $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasalIdPrev' AND userId = '$uId'")->queryAll();                
                    if($cekLogPembetulan != null){
                        $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                        $timeStampLog = strtotime($lastOpenedLog);
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $timeStampCalendar = strtotime($lastOpenedCalendar);
                        $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                        $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                        $pembCount = $pembetulanNumb;
                        if($timeStampLog != $timeStampCalendar){                                                        
                            $newPembetulanNumb = $pembetulanNumb + 1;
                            $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                            $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                                  
                            $pembCount = $newPembetulanNumb;
                        }                        
                        $lastId = $pembetulanId;                        
                    } else {                        
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasalIdPrev', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                        $lastId = Yii::$app->db->getLastInsertID();
                        $pembCount = 1;
                    }
                }
                
                $insertNewUpdate = Yii::$app->db->CreateCommand("INSERT INTO pph_history_update (nomorPembukuan, tanggal, jumlahBruto, jumlahPphDiPotong, userId, pembetulanId, buktiPotongId, pembetulanCount, pasalId) VALUES('$nomorPembukuanPrev', '$tanggalPrev', '$jumlahBrutoPrev', '$jumlahPphDiPotongPrev','$uId', '$lastId', '$id', '$pembCount', '$pasalId')")->execute();
                
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $statusUpdate = true;
                        break;
                    }
                }
                if ($statusUpdate == true) {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                } else {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                    $failedMessage = "Bukti Potong tidak berhasil diubah karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                    $session['update-bp-failed'] = $failedMessage;
                    return $this->redirect(['index']);
                }

                return $this->redirect(['view', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('update-pasal-23', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionUpdatePasal4Jasa($id) {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post())) {
                $masaBulan = $session['masaId'];
                $pasalId = $model->pasalId;
                $statusWpId = $model->statusWpId;
                $wajibPajakId = $model->wajibPajakId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $trfId = null;
                if (isset($_POST['npwp'])) {
                    $trfId = 3;                   
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 2;
                    $wajibPajakId = 'NULL';
                    $nama = $model->nama;
                    $alamat = $model->alamat;
                    } else {                    
                        $modelWaPa = PphWajibPajak::find()->where(['wajibPajakId' => $wajibPajakId])->one();
                        $iujkId = $modelWaPa->iujkId;
                        if ($iujkId != null) {
                            $modelIujk = PphIujk::find()->where(['iujkId' => $iujkId])->one();
                            $is_approve = $modelIujk->is_approve;
                            if ($is_approve == 1) {
                                $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                                if ($tingkatKeuanganWp == 1) {
                                    $trfId = 6;
                                } else if ($tingkatKeuanganWp == 2) {
                                    $trfId = 7;
                                } else if ($tingkatKeuanganWp == 3) {
                                    $trfId = 7;
                                }
                            } else {
                                $trfId = 8;
                            }
                        } else {
                            $trfId = 8;
                        }
                        $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                        $tarif = $modelTarif->tarif;
                        $statusNpwpId = 1;
                        $wajibPajakId = $model->wajibPajakId;
                        $nama = NULL;
                        $alamat = NULL;
                    }
                if (isset($session['idWp'])) {
                    $model->wajibPajakId = $session['idWp'];
                    unset($session['idWp']);
                }
                $nomorPembukuan = $model->nomorPembukuan;
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $jumlahPphDiPotong = floor($jumlahPphDiPotong);
                $updated_at = $date->format('Y:m:d H:i:s');
                $tanggal = Yii::$app->formatter->asDatetime(strtotime($model->tanggal), "php:Y-m-d");
                $lokasi_tanah = $model->lokasi_tanah;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $statusUpdate = false;
                
                //jika ada perubahan maka tambahakan ke history update
                $prevData = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();

                $nomorPembukuanPrev = $prevData->nomorPembukuan;
                $tanggalPrev = $prevData->tanggal;
                $jumlahBrutoPrev = $prevData->jumlahBruto;
                $jumlahPphDiPotongPrev = $prevData->jumlahPphDiPotong;
                $pasalIdPrev = $prevData->pasalId;
                $statusWpIdPrev = $prevData->statusWpId;
                if($prevData->wajibPajakId != null){
                    $wajibPajakId = $prevData->wajibPajakId;
                    $wajibPajakNonId = NULL;
                } else {
                    $wajibPajakId = NULL;
                    $wajibPajakNonId = $prevData->wajibPajakNonId;
                }
                
                //compare
                $pembetulanFlag = 0;
                if($nomorPembukuanPrev != $model->nomorPembukuan){
                    $pembetulanFlag = 1;
                } else if($tanggalPrev != $tanggal){
                    $pembetulanFlag = 1;
                } else if($jumlahBrutoPrev != $model->jumlahBruto){
                    $pembetulanFlag = 1;
                } else if($jumlahPphDiPotongPrev != $model->jumlahPphDiPotong){
                    $pembetulanFlag = 1;
                } else if($wajibPajakId != $model->wajibPajakId){
                    $pembetulanFlag = 1;
                } else if($wajibPajakNonId != $model->wajibPajakNonId){
                    $pembetulanFlag = 1;
                } 
                
                //cek log Pembetulan
                if($pembetulanFlag == 1){
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $masaPajak = $session['masaPajak'];
                    $array = explode(' ', $masaPajak);                    
                    $masaBulan = $session['masaId'];
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    
                    $uId = Yii::$app->user->id;
                    $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasalIdPrev' AND userId = '$uId'")->queryAll();                
                    if($cekLogPembetulan != null){
                        $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                        $timeStampLog = strtotime($lastOpenedLog);
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $timeStampCalendar = strtotime($lastOpenedCalendar);
                        $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                        $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                        $pembCount = $pembetulanNumb;
                        if($timeStampLog != $timeStampCalendar){                                                        
                            $newPembetulanNumb = $pembetulanNumb + 1;
                            $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                            $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                                  
                            $pembCount = $newPembetulanNumb;
                        }                        
                        $lastId = $pembetulanId;                        
                    } else {                        
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasalIdPrev', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                        $lastId = Yii::$app->db->getLastInsertID();
                        $pembCount = 1;
                    }
                }
                
                $insertNewUpdate = Yii::$app->db->CreateCommand("INSERT INTO pph_history_update (nomorPembukuan, tanggal, jumlahBruto, jumlahPphDiPotong, userId, pembetulanId, buktiPotongId, pembetulanCount, pasalId) VALUES('$nomorPembukuanPrev', '$tanggalPrev', '$jumlahBrutoPrev', '$jumlahPphDiPotongPrev','$uId', '$lastId', '$id', '$pembCount', '$pasalId')")->execute(); 
                
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $statusUpdate = true;
                        break;
                    }
                }
                if ($statusUpdate == true) {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = '$wajibPajakId', tanggal = '$tanggal', nomorPembukuan = '$nomorPembukuan',nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = '$wajibPajakId', nomorPembukuan = '$nomorPembukuan', tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                } else {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                    $failedMessage = "Bukti Potong tidak berhasil diubah karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                    $session['update-bp-failed'] = $failedMessage;
                    return $this->redirect(['index']);
                }

                return $this->redirect(['view-pasal-4-jasa', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('update-pasal-4-jasa', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
    
    public function actionUpdatePasal4Perencana($id) {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post())) {
                $masaBulan = $session['masaId'];
                $pasalId = $model->pasalId;
                $statusWpId = $model->statusWpId;
                $wajibPajakId = $model->wajibPajakId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $trfId = null;
                if (isset($_POST['npwp'])) {                    
                    $trfId = 3;                    
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 2;
                    $wajibPajakId = 'NULL';
                    $nama = $model->nama;
                    $alamat = $model->alamat;
                } else {                        
                    $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $wajibPajakId])->one();
                    if ($modelVendor->bujpkId == NULL) {
                        $trfId = 12;
                    } else {
                        $modelBujpk = PphBujpk::find()->where(['bujpkId' => $modelVendor->bujpkId])->one();
                        if ($modelBujpk->is_approve == 1) {
                            $trfId = 13;
                        } else {
                            $trfId = 13;
                        }
                    }
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 1;
                    $wajibPajakId = $model->wajibPajakId;
                    $nama = NULL;
                    $alamat = NULL;
                }
                if (isset($session['idWp'])) {
                    $model->wajibPajakId = $session['idWp'];
                    unset($session['idWp']);
                }
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $jumlahPphDiPotong = floor($jumlahPphDiPotong);
                $updated_at = $date->format('Y:m:d H:i:s');
                $tanggal = Yii::$app->formatter->asDatetime(strtotime($model->tanggal), "php:Y-m-d");
                $lokasi_tanah = $model->lokasi_tanah;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $statusUpdate = false;
                
                //jika ada perubahan maka tambahakan ke history update
                $prevData = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();

                $nomorPembukuanPrev = $prevData->nomorPembukuan;
                $tanggalPrev = $prevData->tanggal;
                $jumlahBrutoPrev = $prevData->jumlahBruto;
                $jumlahPphDiPotongPrev = $prevData->jumlahPphDiPotong;
                $pasalIdPrev = $prevData->pasalId;
                $statusWpIdPrev = $prevData->statusWpId;
                if($prevData->wajibPajakId != null){
                    $wajibPajakId = $prevData->wajibPajakId;
                    $wajibPajakNonId = NULL;
                } else {
                    $wajibPajakId = NULL;
                    $wajibPajakNonId = $prevData->wajibPajakNonId;
                }
                
                //compare
                $pembetulanFlag = 0;
                if($nomorPembukuanPrev != $model->nomorPembukuan){
                    $pembetulanFlag = 1;
                } else if($tanggalPrev != $tanggal){
                    $pembetulanFlag = 1;
                } else if($jumlahBrutoPrev != $model->jumlahBruto){
                    $pembetulanFlag = 1;
                } else if($jumlahPphDiPotongPrev != $model->jumlahPphDiPotong){
                    $pembetulanFlag = 1;
                } else if($wajibPajakId != $model->wajibPajakId){
                    $pembetulanFlag = 1;
                } else if($wajibPajakNonId != $model->wajibPajakNonId){
                    $pembetulanFlag = 1;
                } 
                
                //cek log Pembetulan
                if($pembetulanFlag == 1){
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $masaPajak = $session['masaPajak'];
                    $array = explode(' ', $masaPajak);                    
                    $masaBulan = $session['masaId'];
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    
                    $uId = Yii::$app->user->id;
                    $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasalIdPrev' AND userId = '$uId'")->queryAll();                
                    if($cekLogPembetulan != null){
                        $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                        $timeStampLog = strtotime($lastOpenedLog);
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $timeStampCalendar = strtotime($lastOpenedCalendar);
                        $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                        $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                        $pembCount = $pembetulanNumb;
                        if($timeStampLog != $timeStampCalendar){                                                        
                            $newPembetulanNumb = $pembetulanNumb + 1;
                            $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                            $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                                  
                            $pembCount = $newPembetulanNumb;
                        }                        
                        $lastId = $pembetulanId;                        
                    } else {                        
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasalIdPrev', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                        $lastId = Yii::$app->db->getLastInsertID();
                        $pembCount = 1;
                    }
                }
                
                $insertNewUpdate = Yii::$app->db->CreateCommand("INSERT INTO pph_history_update (nomorPembukuan, tanggal, jumlahBruto, jumlahPphDiPotong, userId, pembetulanId, buktiPotongId, pembetulanCount, pasalId) VALUES('$nomorPembukuanPrev', '$tanggalPrev', '$jumlahBrutoPrev', '$jumlahPphDiPotongPrev','$uId', '$lastId', '$id', '$pembCount', '$pasalId')")->execute();
                
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $statusUpdate = true;
                        break;
                    }
                }
                if ($statusUpdate == true) {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                } else {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                    $failedMessage = "Bukti Potong tidak berhasil diubah karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                    $session['update-bp-failed'] = $failedMessage;
                    return $this->redirect(['index']);
                }

                return $this->redirect(['view-pasal-4-perencana', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('update-pasal-4-perencana', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
    
    public function actionUpdatePasal4Sewa($id) {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            $modelWp = new PphWajibPajak();
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post())) {
                $masaBulan = $session['masaId'];
                $pasalId = $model->pasalId;
                $statusWpId = $model->statusWpId;
                $wajibPajakId = $model->wajibPajakId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $trfId = null;
                if (isset($_POST['npwp'])) {
                    $trfId = 3;
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 2;
                    $wajibPajakId = 'NULL';
                    $nama = $model->nama;
                    $alamat = $model->alamat;
                } else {
                    $modelStatusVendor = PphStatusVendor::find()->where(['statusstatusWpId' => $statusWpId])->one();
                    $modelTarif = PphTarif::find()->where(['tarifId' => $modelStatusVendor->tarifId])->one();
                    $trfId = $modelTarif->tarifId;                        
                    $modelTarif = PphTarif::find()->where(['tarifId' => $trfId])->one();
                    $tarif = $modelTarif->tarif;
                    $statusNpwpId = 1;
                    $wajibPajakId = $model->wajibPajakId;
                    $nama = NULL;
                    $alamat = NULL;
                }
                if (isset($session['idWp'])) {
                    $model->wajibPajakId = $session['idWp'];
                    unset($session['idWp']);
                }
                $jumlahBruto = str_replace(".", "", $model->jumlahBruto);
                $jumlahPphDiPotong = str_replace(".", "", $model->jumlahPphDiPotong);
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $jumlahPphDiPotong = floor($jumlahPphDiPotong);
                $updated_at = $date->format('Y:m:d H:i:s');
                $tanggal = Yii::$app->formatter->asDatetime(strtotime($model->tanggal), "php:Y-m-d");
                $lokasi_tanah = $model->lokasi_tanah;
                $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
                $masaPajak = $session['masaPajak'];
                $array = explode(' ', $masaPajak);
                $bln = \Yii::t('app', $array[0]);
                $tgl = '01';
                if ($bln == date('m')) {
                    $tgl = date('d');
                }
                $date = $array[1] . '-' . $bln . '-' . $tgl;
                $expired = [];
                $statusUpdate = false;
                
                //jika ada perubahan maka tambahakan ke history update
                $prevData = PphBuktiPotong::find()->where(['buktiPotongId' => $id])->one();

                $nomorPembukuanPrev = $prevData->nomorPembukuan;
                $tanggalPrev = $prevData->tanggal;
                $jumlahBrutoPrev = $prevData->jumlahBruto;
                $jumlahPphDiPotongPrev = $prevData->jumlahPphDiPotong;
                $pasalIdPrev = $prevData->pasalId;
                $statusWpIdPrev = $prevData->statusWpId;
                if($prevData->wajibPajakId != null){
                    $wajibPajakId = $prevData->wajibPajakId;
                    $wajibPajakNonId = NULL;
                } else {
                    $wajibPajakId = NULL;
                    $wajibPajakNonId = $prevData->wajibPajakNonId;
                }
                
                //compare
                $pembetulanFlag = 0;
                if($nomorPembukuanPrev != $model->nomorPembukuan){
                    $pembetulanFlag = 1;
                } else if($tanggalPrev != $tanggal){
                    $pembetulanFlag = 1;
                } else if($jumlahBrutoPrev != $model->jumlahBruto){
                    $pembetulanFlag = 1;
                } else if($jumlahPphDiPotongPrev != $model->jumlahPphDiPotong){
                    $pembetulanFlag = 1;
                } else if($wajibPajakId != $model->wajibPajakId){
                    $pembetulanFlag = 1;
                } else if($wajibPajakNonId != $model->wajibPajakNonId){
                    $pembetulanFlag = 1;
                } 
                
                //cek log Pembetulan
                if($pembetulanFlag == 1){
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at_up = $date->format('Y:m:d H:i:s');
                    $masaPajak = $session['masaPajak'];
                    $array = explode(' ', $masaPajak);                    
                    $masaBulan = $session['masaId'];
                    $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
                    
                    $uId = Yii::$app->user->id;
                    $cekLogPembetulan = Yii::$app->db->CreateCommand("SELECT * FROM log_pembetulan WHERE masa = '$masaBulan' AND tahun = '$array[1]' AND pasalId = '$pasalIdPrev' AND userId = '$uId'")->queryAll();                
                    if($cekLogPembetulan != null){
                        $lastOpenedLog = $cekLogPembetulan[0]['lastOpened'];
                        $timeStampLog = strtotime($lastOpenedLog);
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $timeStampCalendar = strtotime($lastOpenedCalendar);
                        $pembetulanId = $cekLogPembetulan[0]['pembetulanId'];
                        $pembetulanNumb = $cekLogPembetulan[0]['pembetulan'];
                        $pembCount = $pembetulanNumb;
                        if($timeStampLog != $timeStampCalendar){                                                        
                            $newPembetulanNumb = $pembetulanNumb + 1;
                            $commandUpdate = "UPDATE log_pembetulan SET pembetulan = '$newPembetulanNumb', lastOpened = '$lastOpenedCalendar', created_at = '$created_at_up' WHERE pembetulanId = '$pembetulanId'";
                            $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();                                  
                            $pembCount = $newPembetulanNumb;
                        }                        
                        $lastId = $pembetulanId;                        
                    } else {                        
                        $lastOpenedCalendar = $checkCalender[0]['lastOpened'];
                        $insertNewLog = Yii::$app->db->CreateCommand("INSERT INTO log_pembetulan (masa, tahun, pasalId, userId, lastOpened, pembetulan, created_at) VALUES('$masaBulan', '$array[1]', '$pasalIdPrev', '$uId','$lastOpenedCalendar', '1', '$created_at_up')")->execute();
                        $lastId = Yii::$app->db->getLastInsertID();
                        $pembCount = 1;
                    }
                }
                
                $insertNewUpdate = Yii::$app->db->CreateCommand("INSERT INTO pph_history_update (nomorPembukuan, tanggal, jumlahBruto, jumlahPphDiPotong, userId, pembetulanId, buktiPotongId, pembetulanCount, pasalId) VALUES('$nomorPembukuanPrev', '$tanggalPrev', '$jumlahBrutoPrev', '$jumlahPphDiPotongPrev','$uId', '$lastId', '$id', '$pembCount', '$pasalId')")->execute();
                
                if ($getCalendar == null) {
                    $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
                }
                for ($x = 0; $x < count($getCalendar); $x++) {
                    $calId = $getCalendar[$x]['calendarId'];
                    $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND MONTH(startDate) = '$masaBulan' AND status = '1'")->queryAll();
                    if ($checkRange == null) {
                        $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
                    } else {
                        $statusUpdate = true;
                        break;
                    }
                }
                if ($statusUpdate == true) {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET statusNpwpId = '$statusNpwpId', nama = '$nama', alamat = '$alamat', lokasi_tanah= '$lokasi_tanah', updated_at = '$updated_at', jumlahBruto = '$jumlahBruto', jumlahPphDiPotong = '$jumlahPphDiPotong', wajibPajakId = $wajibPajakId, tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                } else {
                    if ($model->nomorBuktiPotong == null) {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal', nomorBuktiPotong = NULL WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    } else {
                        $commandUpdate = "UPDATE pph_bukti_potong SET tanggal = '$tanggal' WHERE buktiPotongId = '$id'";
                        $execUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    }
                    $failedMessage = "Bukti Potong tidak berhasil diubah karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
                    $session['update-bp-failed'] = $failedMessage;
                    return $this->redirect(['index']);
                }

                return $this->redirect(['view-pasal-4-sewa', 'id' => $model->buktiPotongId]);
            } else {
                return $this->render('update-pasal-4-sewa', [
                            'model' => $model,
                            'modelWp' => $modelWp,
                ]);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Deletes an existing PphBuktiPotong model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();               

        return $this->redirect(['index']);
    }

    /**
     * Finds the PphBuktiPotong model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PphBuktiPotong the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PphBuktiPotong::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

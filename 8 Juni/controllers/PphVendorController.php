<?php

namespace app\controllers;

use Yii;
use yii2mod\rbac\filters\AccessControl;
use app\models\PphVendor;
use app\search\PphVendorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * PphVendorController implements the CRUD actions for PphVendor model.
 */
class PphVendorController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
//            'access' => [
//                'class' => AccessControl::class,
//                'allowActions' => [
//                    'index',
//                    'create',
//                    // The actions listed here will be allowed to everyone including guests.
//                ]
//            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
                    [
                        'allow' => false,
                        'verbs' => ['POST']
                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all PphVendor models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PphVendorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PphVendor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PphVendor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new PphVendor();
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $statusNpwp = $model->statusNpwpId;
            $statusVendor = $model->statusVendorId;
            if ($statusNpwp == 0) {
                $model->statusNpwpId = 1;
            } else {
                $model->statusNpwpId = 2;
            }
            if ($statusVendor == 0) {
                $model->statusVendorId = 1;
            }
            $model->userId = $userId;
            $model->save();
            return $this->redirect(['view', 'id' => $model->wajibPajakId]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PphVendor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->wajibPajakId]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PphVendor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PphVendor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PphVendor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PphVendor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

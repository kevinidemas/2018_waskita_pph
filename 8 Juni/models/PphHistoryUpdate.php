<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_history_update".
 *
 * @property int $historyUpId
 * @property string $nomorPembukuan
 * @property string $tanggal
 * @property string $jumlahBruto
 * @property string $jumlahPphDiPotong
 * @property string $potonganPegawai
 * @property string $potonganMaterial
 * @property string $nilaiTagihan
 * @property string $dpp
 * @property int $pasalId
 * @property int $wajibPajakId
 * @property int $wajibPajakNonId
 * @property int $statusWpId
 * @property int $statusNpwpId
 * @property int $statusKerjaId
 * @property int $userId
 * @property int $parentId
 * @property string $updated_at
 * @property string $nama
 * @property string $alamat
 * @property string $lokasi_tanah
 * @property int $tarif
 * @property int $userIndukId
 * @property int $pembetulanId
 * @property int $buktiPotongId
 */
class PphHistoryUpdate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_history_update';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'updated_at'], 'safe'],
            [['jumlahBruto', 'jumlahPphDiPotong', 'potonganPegawai', 'potonganMaterial', 'nilaiTagihan', 'dpp'], 'number'],
            [['pasalId', 'wajibPajakId', 'wajibPajakNonId', 'statusWpId', 'statusNpwpId', 'statusKerjaId', 'userId', 'parentId', 'tarif', 'userIndukId', 'pembetulanId', 'buktiPotongId'], 'integer'],
            [['nomorPembukuan'], 'string', 'max' => 64],
            [['nama', 'lokasi_tanah'], 'string', 'max' => 128],
            [['alamat'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'historyUpId' => 'History Up ID',
            'nomorPembukuan' => 'Nomor Pembukuan',
            'tanggal' => 'Tanggal',
            'jumlahBruto' => 'Jumlah Bruto',
            'jumlahPphDiPotong' => 'Jumlah Pph Di Potong',
            'potonganPegawai' => 'Potongan Pegawai',
            'potonganMaterial' => 'Potongan Material',
            'nilaiTagihan' => 'Nilai Tagihan',
            'dpp' => 'Dpp',
            'pasalId' => 'Pasal ID',
            'wajibPajakId' => 'Wajib Pajak ID',
            'wajibPajakNonId' => 'Wajib Pajak Non ID',
            'statusWpId' => 'Status Wp ID',
            'statusNpwpId' => 'Status Npwp ID',
            'statusKerjaId' => 'Status Kerja ID',
            'userId' => 'User ID',
            'parentId' => 'Parent ID',
            'updated_at' => 'Updated At',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'lokasi_tanah' => 'Lokasi Tanah',
            'tarif' => 'Tarif',
            'userIndukId' => 'User Induk ID',
            'pembetulanId' => 'Pembetulan ID',
        ];
    }
}

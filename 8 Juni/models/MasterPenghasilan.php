<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_penghasilan".
 *
 * @property int $penghasilanId
 * @property int $pegawaiId
 * @property string $masaPajak
 * @property string $tahunPajak
 * @property string $kodePajak
 * @property string $jenisPenghasilanId
 * @property string $bruto
 * @property string $pph
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class MasterPenghasilan extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'master_penghasilan';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['pegawaiId', 'created_by', 'updated_by', 'jenisPenghasilanId'], 'integer'],
            [['bruto', 'pph'], 'number', 'numberPattern' => '/[0-9]|\./'],
            [['bruto', 'pph'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['masaPajak'], 'string', 'max' => 2],
            [['tahunPajak'], 'string', 'max' => 4],
            [['kodePajak'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'penghasilanId' => 'Penghasilan ID',
            'pegawaiId' => 'Pegawai ID',
            'masaPajak' => 'Masa Pajak',
            'tahunPajak' => 'Tahun Pajak',
            'kodePajak' => 'Kode Pajak',
            'bruto' => 'Bruto',
            'pph' => 'Pph',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'jenisPenghasilanId' => 'JP',
        ];
    }

    public function getNpwp($id) {
        $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $id])->one();
       // echo '<pre>';
       // print_r($id);
       // die();
        $npwp = $modelPegawai->npwp;

        return $npwp;
    }

    public function getNama($id) {
        $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $id])->one();
        $nama = $modelPegawai->nama;

        return $nama;
    }

    public function getPegawaiNama() {
        return $this->hasOne(MasterPegawai::className(), ['pegawaiId' => 'pegawaiId']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

}

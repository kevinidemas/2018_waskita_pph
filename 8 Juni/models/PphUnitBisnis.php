<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_unit_bisnis".
 *
 * @property int $unitBisnisId
 * @property string $nama
 * @property int $userId
 * @property int $parent
 */
class PphUnitBisnis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_unit_bisnis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'parent'], 'integer'],
            [['nama'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'unitBisnisId' => 'Unit Bisnis ID',
            'nama' => 'Nama',
            'userId' => 'User ID',
            'parent' => 'Parent',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_mandor".
 *
 * @property int $mandorId
 * @property string $nama
 * @property string $posisi
 * @property string $nik
 * @property string $npwp
 * @property string $alamat
 * @property string $email
 * @property int $jenisKelamin
 * @property int $hp
 * @property int $statusKerjaId
 * @property int $userId
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class MasterMandor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_mandor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenisKelamin', 'statusKerjaId', 'userId', 'created_by', 'updated_by', 'hp'], 'integer'],
            [['nama', 'alamat', 'npwp', 'nik', 'posisi', 'jenisKelamin', 'hp', 'email'], 'required'],
            [['buktiNpwp', 'buktiNik'], 'required', 'on' => 'create'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama', 'posisi', 'email'], 'string', 'max' => 64],
            [['nik', 'npwp'], 'string', 'max' => 32],
            [['alamat'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'mandorId' => 'Mandor ID',
            'nama' => 'Nama',
            'posisi' => 'Posisi',
            'nik' => 'NIK',
            'npwp' => 'NPWP',
            'alamat' => 'Alamat',
            'jenisKelamin' => 'Jenis Kelamin',
            'statusKerjaId' => 'Status Kerja ID',
            'userId' => 'User ID',
            'hp' => 'HP',
            'email' => 'E-mail',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}

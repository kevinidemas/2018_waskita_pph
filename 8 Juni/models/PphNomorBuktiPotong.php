<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_nomor_bukti_potong".
 *
 * @property int $nomorBuktiPotongId
 * @property string $tahun
 * @property int $bulan
 * @property string $lastNomorBP_21
 * @property string $lastNomorBP
 * @property string $lastNomorBP_23
 * @property string $lastNomorBP_4
 * @property int $userId
 * @property string $created_at
 */
class PphNomorBuktiPotong extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_nomor_bukti_potong';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bulan', 'userId'], 'integer'],
            [['created_at'], 'safe'],
            [['tahun'], 'string', 'max' => 4],
            [['lastNomorBP', 'lastNomorBP_23', 'lastNomorBP_4', 'lastNomorBP_21'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nomorBuktiPotongId' => 'Nomor Bukti Potong ID',
            'tahun' => 'Tahun',
            'bulan' => 'Bulan',
            'lastNomorBP' => 'Last Nomor Bp',
            'userId' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_jenis_penghasilan".
 *
 * @property int $jenisPenghasilanId
 * @property string $nama
 */
class PphJenisPenghasilan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_jenis_penghasilan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jenisPenghasilanId' => 'Jenis Penghasilan ID',
            'nama' => 'Nama',
        ];
    }
}

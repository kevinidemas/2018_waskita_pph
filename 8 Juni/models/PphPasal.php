<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_pasal".
 *
 * @property int $pasalId
 * @property string $nama
 */
class PphPasal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_pasal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pasalId' => 'Pasal ID',
            'nama' => 'Nama',
        ];
    }
}

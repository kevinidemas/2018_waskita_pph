<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphSsp;

/**
 * PphSspSearch represents the model behind the search form of `app\models\PphSsp`.
 */
class PphSspSearch extends PphSsp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sspId', 'created_by', 'updated_by', 'masa'], 'integer'],
            [['ntpn', 'tanggalSetor', 'created_at', 'updated_at', 'bukti'], 'safe'],
            [['nilai'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphSsp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'sspId' => $this->sspId,
            'tanggalSetor' => $this->tanggalSetor,
            'nilai' => $this->nilai,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updated_at' => $this->updated_at,
            'masa' => $this->masa,
        ]);

        $query->andFilterWhere(['like', 'ntpn', $this->ntpn])
            ->andFilterWhere(['like', 'bukti', $this->bukti]);

        return $dataProvider;
    }
}

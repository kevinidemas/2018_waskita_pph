<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphStatusKerja;

/**
 * PphStatusKerjaSearch represents the model behind the search form of `app\models\PphStatusKerja`.
 */
class PphStatusKerjaSearch extends PphStatusKerja
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusKerjaId', 'tarifId'], 'integer'],
            [['statusKerja', 'nomor'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphStatusKerja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'statusKerjaId' => $this->statusKerjaId,
            'tarifId' => $this->tarifId,
        ]);

        $query->andFilterWhere(['like', 'statusKerja', $this->statusKerja])
            ->andFilterWhere(['like', 'nomor', $this->nomor]);

        return $dataProvider;
    }
}

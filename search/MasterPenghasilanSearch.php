<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterPenghasilan;

/**
 * MasterPenghasilanSearch represents the model behind the search form of `app\models\MasterPenghasilan`.
 */
class MasterPenghasilanSearch extends MasterPenghasilan {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['penghasilanId', 'updated_by', 'userId'], 'integer'],
            [['masaPajak', 'tahunPajak', 'kodePajak', 'created_at', 'updated_at'], 'safe'],
            [['bruto', 'pph'], 'number'],
            [['created_by', 'pegawaiId'], 'string'],
            [['created_by', 'pegawaiId'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
//        $query = MasterPenghasilan::find();
        $query = MasterPenghasilan::find()->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'penghasilanId' => $this->penghasilanId,
//            'pegawaiId' => $this->pegawaiId,
            'bruto' => $this->bruto,
            'pph' => $this->pph,
            'created_at' => $this->created_at,
            'userId' => $this->userId,
//            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'masaPajak', $this->masaPajak])
//                ->andFilterWhere(['like', 'master_pegawai.nama', $this->pegawaiId])
                ->andFilterWhere(['like', 'tahunPajak', $this->tahunPajak])
                ->andFilterWhere(['like', 'user.username', $this->created_by])
                ->andFilterWhere(['like', 'kodePajak', $this->kodePajak]);

        return $dataProvider;
    }

}

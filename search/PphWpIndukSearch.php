<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphWpInduk;

/**
 * PphWpIndukSearch represents the model behind the search form of `app\models\PphWpInduk`.
 */
class PphWpIndukSearch extends PphWpInduk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wpInduk', 'wajibPajakId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphWpInduk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'wpInduk' => $this->wpInduk,
            'wajibPajakId' => $this->wajibPajakId,
        ]);

        return $dataProvider;
    }
}

<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphHistoryUpdate;

/**
 * PphHistoryUpdateSearch represents the model behind the search form of `app\models\PphHistoryUpdate`.
 */
class PphHistoryUpdateSearch extends PphHistoryUpdate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['historyUpId', 'pasalId', 'wajibPajakId', 'wajibPajakNonId', 'statusWpId', 'statusNpwpId', 'statusKerjaId', 'userId', 'parentId', 'tarif', 'userIndukId', 'pembetulanId'], 'integer'],
            [['nomorPembukuan', 'tanggal', 'updated_at', 'nama', 'alamat', 'lokasi_tanah'], 'safe'],
            [['jumlahBruto', 'jumlahPphDiPotong', 'potonganPegawai', 'potonganMaterial', 'nilaiTagihan', 'dpp'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphHistoryUpdate::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'historyUpId' => $this->historyUpId,
            'tanggal' => $this->tanggal,
            'jumlahBruto' => $this->jumlahBruto,
            'jumlahPphDiPotong' => $this->jumlahPphDiPotong,
            'potonganPegawai' => $this->potonganPegawai,
            'potonganMaterial' => $this->potonganMaterial,
            'nilaiTagihan' => $this->nilaiTagihan,
            'dpp' => $this->dpp,
            'pasalId' => $this->pasalId,
            'wajibPajakId' => $this->wajibPajakId,
            'wajibPajakNonId' => $this->wajibPajakNonId,
            'statusWpId' => $this->statusWpId,
            'statusNpwpId' => $this->statusNpwpId,
            'statusKerjaId' => $this->statusKerjaId,
            'userId' => $this->userId,
            'parentId' => $this->parentId,
            'updated_at' => $this->updated_at,
            'tarif' => $this->tarif,
            'userIndukId' => $this->userIndukId,
            'pembetulanId' => $this->pembetulanId,
        ]);

        $query->andFilterWhere(['like', 'nomorPembukuan', $this->nomorPembukuan])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'lokasi_tanah', $this->lokasi_tanah]);

        return $dataProvider;
    }
}

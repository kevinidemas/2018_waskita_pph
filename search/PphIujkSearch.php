<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphIujk;

/**
 * PphIujkSearch represents the model behind the search form of `app\models\PphIujk`.
 */
class PphIujkSearch extends PphIujk {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['iujkId', 'created_by', 'updated_by', 'is_active', 'is_approve', 'is_expired'], 'integer'],
            [['nomorIujk', 'penanggungJawab', 'expired_at', 'created_at', 'updated_at', 'bukti', 'keterangan', 'published_at'], 'safe'],
            [['kemampuanKeuangan'], 'number'],
            [['wajibPajakId'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PphIujk::find();
        $query->joinWith('wajibPajakNama');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iujkId' => $this->iujkId,
            // 'wajibPajakId' => $this->wajibPajakId,
            'kemampuanKeuangan' => $this->kemampuanKeuangan,
            'expired_at' => $this->expired_at,
            'published_at' => $this->published_at,
            'is_active' => $this->is_active,
            'is_approve' => $this->is_approve,
            'is_expired' => $this->is_expired,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'nomorIujk', $this->nomorIujk])
                ->andFilterWhere(['like', 'penanggungJawab', $this->penanggungJawab])
                ->andFilterWhere(['like', 'bukti', $this->bukti])
                ->andFilterWhere(['like', 'pph_wajib_pajak.wajibPajakId', $this->wajibPajakId])
                ->andFilterWhere(['like', 'keterangan', $this->keterangan]);
        return $dataProvider;
    }

}

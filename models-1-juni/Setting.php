<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $settingId
 * @property string $npwpPemungutPajak
 * @property string $npwpPemungutPajakCabang
 * @property string $namaPemungutPajak
 * @property string $kppTerdaftar
 * @property string $nama
 * @property string $jabatan
 * @property int $pasal21_pt
 * @property int $pasal21_ptt
 * @property int $pasal22
 * @property int $pasal23_jasa
 * @property int $pasal23_sewa
 * @property int $pasal4_jasa
 * @property int $pasal4_sewa
 * @property int $userId
 * @property int $pasalId
 * @property string $prefixNomorBP_21
 * @property string $prefixNomorBP
 * @property string $prefixNomorBP_23
 * @property string $prefixNomorBP_4
 * @property int $endNomorBP_21
 * @property int $digitNomorBP
 * @property int $digitNomorBP_23
 * @property int $digitNomorBP_4
 * @property int $userIndukId
 * @property string $startNomorBP_21
 * @property string $startNomorBP
 * @property string $startNomorBP_23
 * @property string $startNomorBP_4
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userId', 'pasalId', 'digitNomorBP', 'pasal21_ptt', 'pasal21_pt', 'pasal22', 'pasal23_jasa', 'pasal23_sewa', 'pasal4_jasa', 'pasal4_sewa', 'digitNomorBP_23', 'digitNomorBP_4', 'userIndukId'], 'integer'],
            [['prefixNomorBP', 'prefixNomorBP_21', 'prefixNomorBP_23', 'prefixNomorBP_4'], 'string', 'max' => 32],
            [['startNomorBP', 'startNomorBP_23', 'startNomorBP_4'], 'string', 'max' => 11],
            [['startNomorBP_21', 'endNomorBP_21'], 'string', 'max' => 7, 'min' => 7],
            [['namaPemungutPajak', 'kota'], 'string', 'max' => 128],
            [['nama', 'jabatan', 'kppTerdaftar'], 'string', 'max' => 64],
            [['nama', 'jabatan', 'kota','prefixNomorBP', 'digitNomorBP', 'startNomorBP'], 'required', 'message' => 'Kolom ini tidak boleh kosong'],
//            [['npwpPemungutPajak'], 'required', 'message' => 'NPWP tidak boleh kosong'],
            [['npwpPemungutPajak'], 'string', 'max' => 20, 'min' => 15, 'tooShort' => 'NPWP harus terdiri dari 15 digit angka'],
            [['npwpPemungutPajakCabang'], 'string', 'max' => 20, 'min' => 15, 'tooShort' => 'NPWP harus terdiri dari 15 digit angka'],
//            ['npwpPemungutPajak', 'match', 'pattern' => '/^[0-9]{15}$/', 'message' => 'NPWP tidak boleh mengandung alfabet, tanda baca ataupun simbol'],
//            ['jabatan', 'match', 'pattern'=>'/^[a-zA-Z_.]$/', 'message' => "Jabatan tidak boleh mengandung tanda baca keculi '-'"],
//            ['jabatan', 'match', 'pattern'=>'/[^0-9@\#-,;]$/', 'message' => "Jabatan tidak boleh mengandung tanda baca keculi '-'"],
//            ['jabatan', 'match', 'pattern' => '/^([a-z]+(\s))*[a-z]+$/i', 'message' => "Jabatan tidak boleh mengandung tanda baca kecuali '-', hanya boleh menggunakan 1(satu) tanda '-' dan tidak boleh diawal/diakhir"],
            ['jabatan', 'match', 'pattern' => '/^[a-zA-Z.,-]+(?:\s[a-zA-Z.,-]+)*$/', 'message' => "Jabatan tidak boleh mengandung angka, simbol, dan tanda baca kecuali '-', '.', ',', harus diawali dan diakhiri dengan abjad"],
            ['nama', 'match', 'pattern' => '/^([a-z .,-])*[a-z]+$/i', 'message' => "Nama tidak boleh mengandung tanda baca dan angka"],
            ['kppTerdaftar', 'match', 'pattern' => '/^([a-z .,-])*[a-z]+$/i', 'message' => "Nama KPP tidak boleh mengandung tanda baca dan angka"],
//            ['prefixNomorBP', 'match', 'pattern' => '/^[a-z0-9 .\-]+$/i', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', dan ',' "],
            ['prefixNomorBP', 'match', 'pattern' => '/^[a-zA-Z0-9.,-/\s]+$/', 'message' => "Prefix tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/' "],
            ['namaPemungutPajak', 'match', 'pattern' => '/^([a-z0-9 ]+.)*[a-z0-9()]+$/i']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'settingId' => 'Setting ID',
            'npwpPemungutPajak' => 'NPWP Pemungut Pajak',
            'npwpPemungutPajakCabang' => 'NPWP Pemungut Pajak Cabang',
            'namaPemungutPajak' => 'Nama Pemungut Pajak',
            'kppTerdaftar' => 'KPP Terdaftar',
            'nama' => 'Nama',
            'kota' => 'Kota',
            'jabatan' => 'Jabatan',
            'userId' => 'User ID',
            'pasalId' => 'Pasal',
            'prefixNomorBP' => 'Prefix Nomor Bukti Potong Pasal 22',
            'digitNomorBP' => 'Jumlah Digit Nomor Bukti Potong Pasal 22',
            'startNomorBP' => 'Nomor BP Awal Pasal 22',
            'prefixNomorBP_23' => 'Prefix Nomor Bukti Potong Pasal 23',
            'digitNomorBP_23' => 'Jumlah Digit Nomor Bukti Potong Pasal 23',
            'startNomorBP_23' => 'Nomor BP Awal Pasal 23',
            'prefixNomorBP_4' => 'Prefix Nomor Bukti Potong Pasal 4 Ayat (2)',
            'digitNomorBP_4' => 'Jumlah Digit Nomor Bukti Potong Pasal 4 Ayat (2)',
            'startNomorBP_4' => 'Nomor BP Awal Pasal 4 Ayat (2)',
            'startNomorBP_21' => 'Nomor BP Awal Pasal 21 (Slot 1)',
            'endNomorBP_21' => 'Nomor BP Akhir Pasal 21 (Slot 1)',
            'startNomorBP_21_S2' => 'Nomor BP Awal Pasal 21 (Slot 2)',
            'endNomorBP_21_S2' => 'Nomor BP Akhir Pasal 21 (Slot 2)',
            'userIndukId' => 'Unit Bisnis Induk Pasal 21 - PTT'
        ];
    }
}

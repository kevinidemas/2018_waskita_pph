<?php

namespace app\models;

use Yii;
use app\models\PphWajibPajak;
use app\models\MasterMandor;
use app\models\User;

/**
 * This is the model class for table "pph_bukti_potong".
 *
 * @property int $buktiPotongId
 * @property string $nomorPembukuan
 * @property string $nomorBuktiPotong
 * @property string $tanggal
 * @property string $jumlahBruto
 * @property int $wajibPajakId
 * @property int $wajibPajakNonId
 * @property int $statusNpwpId
 * @property int $statusWpId
 * @property int $statusKerjaId
 * @property string $jumlahPphDiPotong
 * @property string $potonganPegawai
 * @property string $potonganMaterial
 * @property string $dpp
 * @property string $nilaiTagihan
 * @property int $userId
 * @property int $parentId
 * @property int $pasalId
 * @property int $tarif
 * @property int $userIndukId
 * @property string $nama
 * @property string $lokasi_tanah
 * @property string $alamat
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $is_calendar_close
 */
class PphBuktiPotong extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pph_bukti_potong';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['tanggal', 'created_at', 'updated_at'], 'safe'],
            [['tanggal', 'nomorPembukuan', 'jumlahBruto'], 'required'],
            [['wajibPajakId', 'wajibPajakNonId','jumlahBruto', 'jumlahPphDiPotong', 'potonganPegawai', 'potonganMaterial', 'dpp', 'nilaiTagihan'], 'number', 'numberPattern' => '/[0-9]|\./'],
            [['wajibPajakId', 'tarif', 'userId', 'parentId', 'pasalId', 'created_by', 'updated_by', 'is_calendar_close', 'userIndukId'], 'integer'],
            [['nomorPembukuan', 'nomorBuktiPotong'], 'string', 'max' => 64],
            [['nama', 'lokasi_tanah'], 'string', 'max' => 128],
            [['alamat'], 'string', 'max' => 1000],
//            ['nomorPembukuan', 'match', 'pattern' => '/^[a-z0-9 ,.\-]+$/i', 'message' => "Nomor tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
            ['nomorPembukuan', 'match', 'pattern' => '/^[a-zA-Z0-9.,\-\/\s]+$/', 'message' => "Nomor tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
            ['nama', 'match', 'pattern' => '/^[a-z0-9 ,.\-]+$/i', 'message' => "Nama tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
//            ['alamat', 'match', 'pattern' => '^[a-zA-Z0-9.,-/\s]+$', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
            ['alamat', 'match', 'pattern' => '/^[a-zA-Z0-9.,\-\/\s]+$/', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'buktiPotongId' => 'Bukti Potong ID',
            'nomorPembukuan' => 'Nomor Pembukuan',
            'nomorBuktiPotong' => 'Nomor Bukti Potong',
            'tanggal' => 'Tanggal',
            'nama' => 'Nama',
            'npwp' => 'NPWP',
            'jumlahBruto' => 'Jumlah Bruto',
            'wajibPajakId' => 'Wajib Pajak',
            'jumlahPphDiPotong' => 'Jumlah Pph Di Potong',
            'userId' => 'User',
            'parentId' => 'Parent',
            'pasalId' => 'Pasal',
            'created_at' => 'Created At',
            'created_by' => 'Dibuat Oleh',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',            
        ];
    }

    public function beforeSave($insert) {
        if ($insert) {
            $this->tanggal = Yii::$app->formatter->asDatetime(strtotime($this->tanggal), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }

    public function getNpwp($id) {
        $modelWaPa = PphWajibPajak::find()->where(['wajibPajakId' => $id])->one();
        $npwp = $modelWaPa->npwp;

        return $npwp;
    }

       public function getNamaHonorer($id) {
        $modelHonorer = MasterHonorer::find()->where(['honorerId' => $id])->one();
        $nama = $modelHonorer->nama;

        return $nama;
    }

        public function getNpwpHonorer($id) {
        $modelHonorer = MasterHonorer::find()->where(['honorerId' => $id])->one();
        $npwp = $modelHonorer->npwp;

        return $npwp;
    }
    
    public function getNpwpMandor($id) {
        $modelMandor = MasterMandor::find()->where(['mandorId' => $id])->one();
        $npwp = $modelMandor->npwp;

        return $npwp;
    }

    public function getNama($id) {
        $modelWaPa = PphWajibPajak::find()->where(['wajibPajakId' => $id])->one();
        $nama = $modelWaPa->nama;

        return $nama;
    }
    
    public function getNamaMandor($id) {
        $modelMandor = MasterMandor::find()->where(['mandorId' => $id])->one();
        $nama = $modelMandor->nama;

        return $nama;
    }
    
    public function getNamaUser($id) {
        $modelUser = User::find()->where(['id' => $id])->one();
        $nama = $modelUser->username;

        return $nama;
    }

    public function getWajibPajakNama()
    {
        return $this->hasOne(PphWajibPajak::className(), ['wajibPajakId' => 'wajibPajakId']);
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

}
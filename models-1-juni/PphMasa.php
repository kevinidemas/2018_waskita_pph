<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_masa".
 *
 * @property int $masaId
 * @property string $nama
 */
class PphMasa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_masa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'masaId' => 'Masa ID',
            'nama' => 'Nama',
        ];
    }
    
        public function getOptions() {
        for ($i = 0; $i <= 36; ++$i) {
            $time = strtotime(sprintf('-%d months', $i));
//            $value = date('Y-m', $time);
            $label = date('F Y', $time);
            if ($label != null) {
                $label = str_replace('January', 'Januari', $label);
                $label = str_replace('February', 'Februari', $label);
                $label = str_replace('March', 'Maret', $label);
                $label = str_replace('May', 'Mei', $label);
                $label = str_replace('June', 'Juni', $label);
                $label = str_replace('July', 'Juli', $label);
                $label = str_replace('August', 'Agustus', $label);
                $label = str_replace('October', 'Oktober', $label);
                $label = str_replace('November', 'Nopember', $label);
                $label = str_replace('December', 'Desember', $label);
            }
            $masa_awal[] = $label;
            foreach ($masa_awal as $key => $value) {
                $masa_akhir[$value] = $value;
            }
        }

        return $masa_akhir;
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_status_npwp".
 *
 * @property int $npwpId
 * @property int $tarifId
 * @property int $userId
 * @property int $parentId
 * @property int $pasalId
 */
class PphStatusNpwp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_status_npwp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarifId', 'userId', 'parentId', 'pasalId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'npwpId' => 'Npwp ID',
            'tarifId' => 'Tarif ID',
            'userId' => 'User ID',
            'parentId' => 'Parent ID',
            'pasalId' => 'Pasal ID',
        ];
    }
}

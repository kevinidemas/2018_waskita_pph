<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_wajib_pajak_non".
 *
 * @property int $wajibPajakNonId
 * @property string $nama
 * @property string $alamat
 * @property int $created_by
 */
class MasterWajibPajakNon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_wajib_pajak_non';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'statusKerja'], 'integer'],
            [['nama'], 'string', 'max' => 64],
            [['alamat'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wajibPajakNonId' => 'Wajib Pajak Non ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'created_by' => 'Created By',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_jenis_bisnis".
 *
 * @property int $jenisBisnisId
 * @property string $jenisBisnis
 * @property int $userId
 * @property int $parentId
 */
class PphJenisBisnis extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_jenis_bisnis';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'parentId'], 'integer'],
            [['jenisBisnis'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jenisBisnisId' => 'Jenis Bisnis ID',
            'jenisBisnis' => 'Jenis Bisnis',
            'userId' => 'User ID',
            'parentId' => 'Parent ID',
        ];
    }
}

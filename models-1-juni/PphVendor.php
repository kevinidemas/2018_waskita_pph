<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_vendor".
 *
 * @property int $wajibPajakId
 * @property string $nama
 * @property string $npwp
 * @property string $alamat
 * @property int $statusNpwpId
 * @property int $iujkId
 * @property int $statusVendorId
 * @property int $userId 
 */
class PphVendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statusNpwpId', 'iujkId', 'statusVendorId', 'userId'], 'integer'],
            [['nama', 'alamat'], 'required', 'message' => 'Kolom ini tidak boleh kosong'],
            [['nama'], 'string', 'max' => 128],
            [['npwp'], 'string', 'max' => 15, 'min' => 15, 'tooShort' => 'NPWP harus terdiri dari 15 digit angka (tanpa tanda baca / simbol)'],
            ['npwp', 'isNumbersOnly'],
            [['alamat'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wajibPajakId' => 'Vendor ID',
            'nama' => 'Nama',
            'npwp' => 'NPWP',
            'alamat' => 'Alamat',
            'statusNpwpId' => 'Status NPWP',
            'iujkId' => 'IUJK',
            'statusVendorId' => 'Status Vendor',
            'userId' => 'User ID',
        ];
    }
    
    public function isNumbersOnly($attribute) {
        if (!preg_match('/^[0-9]{15}$/', $this->$attribute)) {
            $this->addError($attribute, 'NPWP harus terdiri dari 15 digit angka (tanpa tanda baca / simbol)');
        }
    }
}

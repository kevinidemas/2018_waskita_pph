<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_user_induk".
 *
 * @property int $userIndukId
 * @property int $userId
 */
class PphUserInduk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_user_induk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'userIndukId' => 'User Induk ID',
            'userId' => 'User ID',
        ];
    }
}

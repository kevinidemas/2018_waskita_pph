<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_wajib_pajak".
 *
 * @property int $wajibPajakId
 * @property string $nama
 * @property string $npwp
 * @property string $alamat
 * @property string $website
 * @property string $email
 * @property int $hp
 * @property int $telepon
 * @property int $fax
 * @property string $penanggungJawab
 * @property int $statusNpwpId
 * @property int $iujkId
 * @property int $bujpkId
 * @property int $statusWpId
 * @property int $jenisWp
 * @property resource $buktiNpwp
 * @property resource $buktiSppkp
 * @property int $user_id
 * @property int $approvalStatus
 */
class PphWajibPajak extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pph_wajib_pajak';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['statusNpwpId', 'iujkId', 'bujpkId','statusWpId', 'jenisWp', 'user_id', 'hp', 'telepon', 'fax', 'approvalStatus'], 'integer'],
            [['nama'], 'string', 'max' => 128],
            [['buktiNpwp', 'buktiSppkp'], 'string'],
            [['website', 'email', 'penanggungJawab'], 'string', 'max' => 64],
            [['nama', 'alamat', 'npwp'], 'required'],
            [['buktiNpwp'], 'required', 'on' => 'create'],
            ['npwp', 'filter', 'skipOnArray' => true, 'filter' => function ($value) {
                    return preg_replace("/[^a-zA-Z 0-9]+/", "", $value);
                }],
            [['npwp'], 'string', 'max' => 32, 'min' => 15],
            [['npwp'], 'unique'],
            [['alamat'], 'string', 'max' => 1000],
            ['nama', 'match', 'pattern' => '/^[a-z0-9 ,.\-]+$/i', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',', dan '/'"],
//            ['nama', 'match', 'pattern' => '^[a-zA-Z0-9,.!? ]*$', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',', dan '/'"],
            ['alamat', 'match', 'pattern' => '/^[\r\na-z0-9 ,.\-]+$/i', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',', dan '/'"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'wajibPajakId' => 'Wajib Pajak ID',
            'nama' => 'Nama',
            'npwp' => 'NPWP',
            'alamat' => 'Alamat',
            'penanggungJawab' => 'Penanggungjawab',
            'hp' => 'Nomor HP',
            'telepon' => 'Telepon',
            'fax' => 'Nomor Fax',
            'email' => 'E-mail',
            'website' => 'Website Perusahaan',
            'statusNpwpId' => 'Status NPWP',
            'iujkId' => 'IUJK',
            'bujpkId' => 'BUJPK',
            'buktiNpwp' => 'Upload NPWP',
            'buktiSppkp' => 'Upload SPPKP',
            'statusWpId' => 'Status Wajib Pajak',
            'jenisWp' => 'Jenis Wajib Pajak',
            'user_id' => 'User ID',
        ];
    }

//    public function getPphBuktiPotongs()
//    {
//        return $this->hasMany(PphBuktiPotong::className(), ['wajibPajakId' => 'wajibPajakId']);
//    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_jenis_kelamin".
 *
 * @property int $jenisKelaminId
 * @property string $jenis
 */
class PphJenisKelamin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_jenis_kelamin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jenisKelaminId' => 'Jenis Kelamin ID',
            'jenis' => 'Jenis',
        ];
    }
}

<?php

namespace app\models;
use app\models\PphWajibPajak;

use Yii;

/**
 * This is the model class for table "pph_iujk".
 *
 * @property int $iujkId
 * @property int $wajibPajakId
 * @property string $nomorIujk
 * @property string $penanggungJawab
 * @property string $kemampuanKeuangan
 * @property string $expired_at
 * @property string $published_at
 * @property string $created_at
 * @property int $created_by
 * @property int $is_active
 * @property int $is_approve
 * @property int $is_expired
 * @property string $updated_at
 * @property int $updated_by
 * @property resource $bukti
 * @property string $keterangan
 * @property int $tingkatKeuangan
 */
class PphIujk extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'pph_iujk';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['kemampuanKeuangan', 'expired_at', 'nomorIujk', 'penanggungJawab', 'keterangan', 'wajibPajakId'], 'required'],
            [['published_at', 'expired_at', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'wajibPajakId', 'is_active', 'is_expired_warning', 'is_approve', 'is_expired', 'tingkatKeuangan'], 'integer'],
            [['bukti'], 'string'],
            [['nomorIujk'], 'string', 'max' => 32, 'min' => 15],
            ['nomorIujk', 'unique', 'targetAttribute' => ['nomorIujk'], 'message' => 'Nomor IUJK telah terdaftar.'],
            [['penanggungJawab'], 'string', 'max' => 32],
            [['keterangan'], 'string', 'max' => 2048],
            ['penanggungJawab', 'match', 'pattern' => '/^([a-z0-9 ]+.)*[a-z0-9()]+$/i'],
//            ['nomorIujk', 'match', 'pattern' => '/^[a-zA-Z0-9.,\-\/\s]+$/', 'message' => "Nomor IUJK tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
            ['nomorIujk', 'match', 'pattern' => '/^[0-9]+$/', 'message' => "Nomor IUJK hanya terdiri dari Angka"],
            ['keterangan', 'match', 'pattern' => '/^[a-zA-Z0-9.,\-\/\s]+$/', 'message' => "Keterangan tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'iujkId' => 'Iujk ID',
            'nomorIujk' => 'Nomor IUJK',
            'telepon' => 'Telepon',
            'penanggungJawab' => 'Penanggung Jawab',
            'kemampuanKeuangan' => 'Kemampuan Keuangan',
            'published_at' => 'Berlaku Sejak',
            'expired_at' => 'Berakhir Pada',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'bukti' => 'Bukti',
            'wajibPajakId' => 'Wajib Pajak',
            'keterangan' => 'Keterangan',
            'is_approve' => 'Disetujui',
            'is_active' => 'Aktif',
        ];
    }

    public function beforeSave($insert) {
        if ($insert) {
            $this->expired_at = Yii::$app->formatter->asDatetime(strtotime($this->expired_at), "php:Y-m-d");
            $this->published_at = Yii::$app->formatter->asDatetime(strtotime($this->published_at), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }
}

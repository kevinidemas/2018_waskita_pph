<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_pegawai".
 *
 * @property int $pegawaiId
 * @property string $nip
 * @property string $nama
 * @property string $jabatan
 * @property string $npwp
 * @property string $alamat
 * @property int $jenisKelamin
 * @property string $nik
 * @property string $gajiKotor
 * @property string $pph21
 * @property int $statusPerkawinan
 * @property int $statusKerjaId
 * @property string $created_at
 * @property int $userId
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class MasterPegawai extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_pegawai';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
//            [['nama', 'jabatan', 'nip', 'nik', 'alamat', 'jenisKelamin', 'statusPerkawinan', 'gajiKotor', 'pph21'], 'required'],
            [['nama', 'jabatan', 'nip', 'nik', 'alamat', 'jenisKelamin', 'statusPerkawinan'], 'required'],
            [['jenisKelamin', 'statusPerkawinan', 'statusKerjaId'], 'integer'],
            [['nip', 'npwp', 'nik'], 'string', 'max' => 32],
            [['nama', 'jabatan'], 'string', 'max' => 64],
            [['alamat'], 'string', 'max' => 1000],
            ['nama', 'match', 'pattern' => '/^[a-z0-9 ,.\-]+$/i', 'message' => "Nama tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
            ['jabatan', 'match', 'pattern' => '/^[a-z0-9 ,.\-]+$/i', 'message' => "Jabatan tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
            ['alamat', 'match', 'pattern' => '/^[a-zA-Z0-9.,\-\/\s]+$/', 'message' => "Alamat tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pegawaiId' => 'Pegawai',
            'nama' => 'Nama',
            'nip' => 'NIP',
            'npwp' => 'NPWP',
            'alamat' => 'Alamat',
            'jenisKelamin' => 'Jenis Kelamin',
            'nik' => 'NIK',
            'statusPerkawinan' => 'Status Perkawinan',
//            'gajiKotor' => 'Jumlah Gaji Kotor',
            'pph21' => 'Jumlah PPh DIpotong',
            'statusKerjaId' => 'Status Kerja',
        ];
    }
    
}

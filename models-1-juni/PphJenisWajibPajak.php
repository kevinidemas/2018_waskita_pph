<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_jenis_wajib_pajak".
 *
 * @property int $jenisWajibPajakId
 * @property string $nama
 */
class PphJenisWajibPajak extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_jenis_wajib_pajak';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jenisWajibPajakId' => 'Jenis Wajib Pajak ID',
            'nama' => 'Nama',
        ];
    }
}

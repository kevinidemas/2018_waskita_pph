<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_berkas".
 *
 * @property int $berkasId
 * @property resource $berkas
 * @property int $userId
 * @property int $parentId
 * @property string $created_at
 * @property string $created_by
 * @property int $is_exported
 * @property int $bulan
 * @property int $jenisBerkas
 * @property string $tahun
 */
class PphBerkas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_berkas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['berkas'], 'string'],
            [['userId', 'parentId', 'is_exported', 'bulan', 'jenisBerkas'], 'integer'],
            [['created_at'], 'safe'],
            [['created_by'], 'string', 'max' => 64],
//            [['bulan'], 'string', 'max' => 24],
            [['tahun'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'berkasId' => 'Berkas ID',
            'berkas' => 'Berkas',
            'userId' => 'User ID',
            'parentId' => 'Parent ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'is_exported' => 'Is Exported',
            'bulan' => 'Bulan',
            'jenisBerkas' => 'Jenis Berkas',
            'tahun' => 'Tahun',
        ];
    }
}

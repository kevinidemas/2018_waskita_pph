<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_wp_induk".
 *
 * @property int $wpInduk
 * @property int $wajibPajakId
 */
class PphWpInduk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_wp_induk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wajibPajakId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wpInduk' => 'Wp Induk',
            'wajibPajakId' => 'Wajib Pajak ID',
        ];
    }
}

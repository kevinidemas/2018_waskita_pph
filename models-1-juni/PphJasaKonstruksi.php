<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_jasa_konstruksi".
 *
 * @property int $jasaKonstruksiId
 * @property string $status
 * @property int $tarifId
 * @property int $userId
 * @property int $parentId
 */
class PphJasaKonstruksi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_jasa_konstruksi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarifId', 'userId', 'parentId'], 'integer'],
            [['status'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'jasaKonstruksiId' => 'Jasa Konstruksi ID',
            'status' => 'Status',
            'tarifId' => 'Tarif ID',
            'userId' => 'User ID',
            'parentId' => 'Parent ID',
        ];
    }
}

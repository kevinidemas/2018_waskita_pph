<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphJenisBisnis;

/**
 * PphJenisBisnisSearch represents the model behind the search form of `app\models\PphJenisBisnis`.
 */
class PphJenisBisnisSearch extends PphJenisBisnis
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenisBisnisId', 'userId', 'parentId'], 'integer'],
            [['jenisBisnis'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphJenisBisnis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'jenisBisnisId' => $this->jenisBisnisId,
            'userId' => $this->userId,
            'parentId' => $this->parentId,
        ]);

        $query->andFilterWhere(['like', 'jenisBisnis', $this->jenisBisnis]);

        return $dataProvider;
    }
}

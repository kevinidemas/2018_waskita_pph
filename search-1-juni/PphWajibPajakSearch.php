<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphWajibPajak;

/**
 * PphWajibPajakSearch represents the model behind the search form of `app\models\PphWajibPajak`.
 */
class PphWajibPajakSearch extends PphWajibPajak
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wajibPajakId', 'statusNpwpId', 'iujkId', 'statusWpId', 'jenisWp', 'user_id', 'approvalStatus'], 'integer'],
            [['nama', 'npwp', 'alamat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphWajibPajak::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'wajibPajakId' => $this->wajibPajakId,
            'statusNpwpId' => $this->statusNpwpId,
            'approvalStatus' => $this->approvalStatus,
            'iujkId' => $this->iujkId,
            'statusWpId' => $this->statusWpId,
            'jenisWp' => $this->jenisWp,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'npwp', $this->npwp])
            ->andFilterWhere(['like', 'alamat', $this->alamat]);

        return $dataProvider;
    }
}

<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphBuktiPotong;

//use app\models\User;

/**
 * PphBuktiPotongSearch represents the model behind the search form of `app\models\PphBuktiPotong`.
 */
class PphBuktiPotongSearch extends PphBuktiPotong {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['buktiPotongId', 'userId', 'parentId', 'pasalId', 'updated_by', 'is_calendar_close'], 'integer'],
            [['nomorPembukuan', 'nomorBuktiPotong', 'tanggal', 'created_at', 'updated_at', 'nama', 'alamat'], 'safe'],
            [['jumlahBruto', 'jumlahPphDiPotong'], 'number'],
            [['wajibPajakId', 'nama', 'created_by'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = PphBuktiPotong::find();
        $query->joinWith('user');
        $query->joinWith('wajibPajakNama');

//        $querys = PphBuktiPotong::find()->joinWith('wajibPajakNama');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
//        $query->joinWith(['wajibPajak']);
        // grid filtering conditions
        $query->andFilterWhere([
            'buktiPotongId' => $this->buktiPotongId,
//            'tanggal' => $this->tanggal,
            'jumlahBruto' => $this->jumlahBruto,
//            'wajibPajakId' => $this->wajibPajakId,
            'jumlahPphDiPotong' => $this->jumlahPphDiPotong,
            'userId' => $this->userId,
            'parentId' => $this->parentId,
            'pasalId' => $this->pasalId,
            'created_at' => $this->created_at,
//            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'is_calendar_close' => $this->is_calendar_close,
        ]);

        $query->andFilterWhere(['MONTH(tanggal)' => $this->tanggal])
//                ->andFilterWhere(['like', 'npwp', $this->wajibPajakId])
                ->andFilterWhere(['like', 'nomorPembukuan', $this->nomorPembukuan])
                ->andFilterWhere(['like', 'nomorBuktiPotong', $this->nomorBuktiPotong])
                ->andFilterWhere(['like', 'pph_wajib_pajak.nama', $this->wajibPajakId])
                ->andFilterWhere(['like', 'user.username', $this->created_by])
//                ->andFilterWhere(['like', 'pph_wajib_pajak.nama', $this->wajibPajakId])
                ->andFilterWhere(['like', 'alamat', $this->alamat]);
        return $dataProvider;
    }

}

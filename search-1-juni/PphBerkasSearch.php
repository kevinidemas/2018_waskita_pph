<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphBerkas;

/**
 * PphBerkasSearch represents the model behind the search form of `app\models\PphBerkas`.
 */
class PphBerkasSearch extends PphBerkas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['berkasId', 'userId', 'parentId', 'is_exported', 'bulan', 'jenisBerkas'], 'integer'],
            [['berkas', 'created_at', 'created_by', 'tahun'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphBerkas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'berkasId' => $this->berkasId,
            'userId' => $this->userId,
            'parentId' => $this->parentId,
            'created_at' => $this->created_at,
            'is_exported' => $this->is_exported,
            'bulan' => $this->bulan,
            'jenisBerkas' => $this->jenisBerkas,
        ]);

        $query->andFilterWhere(['like', 'berkas', $this->berkas])
            ->andFilterWhere(['like', 'created_by', $this->created_by])
            ->andFilterWhere(['like', 'tahun', $this->tahun]);
//            ->andFilterWhere(['like', 'bulan', $this->bulan]);

        return $dataProvider;
    }
}

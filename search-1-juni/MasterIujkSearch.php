<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MasterIujk;

/**
 * MasterIujkSearch represents the model behind the search form of `app\models\MasterIujk`.
 */
class MasterIujkSearch extends MasterIujk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['iujkId', 'userId', 'parentId', 'tarifId'], 'integer'],
            [['nomorIujk', 'validDate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterIujk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iujkId' => $this->iujkId,
            'validDate' => $this->validDate,
            'userId' => $this->userId,
            'parentId' => $this->parentId,
            'tarifId' => $this->tarifId,
        ]);

        $query->andFilterWhere(['like', 'nomorIujk', $this->nomorIujk]);

        return $dataProvider;
    }
}

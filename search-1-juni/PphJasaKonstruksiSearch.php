<?php

namespace app\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PphJasaKonstruksi;

/**
 * PphJasaKonstruksiSearch represents the model behind the search form of `app\models\PphJasaKonstruksi`.
 */
class PphJasaKonstruksiSearch extends PphJasaKonstruksi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jasaKonstruksiId', 'tarifId', 'userId', 'parentId'], 'integer'],
            [['status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PphJasaKonstruksi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'jasaKonstruksiId' => $this->jasaKonstruksiId,
            'tarifId' => $this->tarifId,
            'userId' => $this->userId,
            'parentId' => $this->parentId,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

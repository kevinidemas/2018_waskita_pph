<?php

namespace app\controllers;

use Yii;
use app\models\MasterWajibPajakNon;
use app\search\MasterWajibPajakNonSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MasterWajibPajakNonController implements the CRUD actions for MasterWajibPajakNon model.
 */
class MasterWajibPajakNonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterWajibPajakNon models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterWajibPajakNonSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterWajibPajakNon model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterWajibPajakNon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterWajibPajakNon();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->wajibPajakNonId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionCreateModal22() {
        $model = new MasterWajibPajakNon();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {            
            $model->created_by = $userId;
            $model->statusKerja = 1;
            $model->jenisWp = 2;
            $model->save();
            $session['status-wp-pasal-22'] = 0;
            return $this->redirect(['/pph-bukti-potong/create',
                        'id' => $model->wajibPajakNonId,
            ]);
        } else {
            return $this->renderAjax('create-modal-22', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionCreateModal23Jasa() {
        $model = new MasterWajibPajakNon();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {            
            $model->created_by = $userId;
            $model->statusKerja = 1;
            $model->jenisWp = 2;
            $model->save();
            $session['status-wp-pasal-23-jasa'] = 0;
            return $this->redirect(['/pph-bukti-potong/create-pasal-23-jasa',
                        'id' => $model->wajibPajakNonId,
            ]);
        } else {
            return $this->renderAjax('create-modal-23-jasa', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionCreateModal23Sewa() {
        $model = new MasterWajibPajakNon();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {            
            $model->created_by = $userId;
            $model->statusKerja = 1;
            $model->jenisWp = 2;
            $model->save();
            $session['status-wp-pasal-23-sewa'] = 0;
            return $this->redirect(['/pph-bukti-potong/create-pasal-23-sewa',
                        'id' => $model->wajibPajakNonId,
            ]);
        } else {
            return $this->renderAjax('create-modal-23-sewa', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionCreateModal4Sewa() {
        $model = new MasterWajibPajakNon();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {            
            $model->created_by = $userId;
            $model->statusKerja = 1;
            $model->jenisWp = 2;
            $model->save();
            $session['status-wp-pasal-4-sewa'] = 0;
            return $this->redirect(['/pph-bukti-potong/create-pasal-4-sewa',
                        'id' => $model->wajibPajakNonId,
            ]);
        } else {
            return $this->renderAjax('create-modal-4-sewa', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionCreateModal21Mandor() {
        $model = new MasterWajibPajakNon();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {            
            $model->created_by = $userId;
            $model->statusKerja = 2;
            $model->jenisWp = 1;
            $model->save();
            $session['status-wp-pasal-21-mandor'] = 0;
            return $this->redirect(['/pph-bukti-potong/create-pasal-21-mandor',
                        'id' => $model->wajibPajakNonId,
            ]);
        } else {
            return $this->renderAjax('create-modal-21-mandor', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionCreateModal21Honorer() {
        $model = new MasterWajibPajakNon();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {            
            $model->created_by = $userId;
            $model->statusKerja = 3;
            $model->jenisWp = 1;
            $model->save();
            $session['status-wp-pasal-21-honorer'] = 0;
            return $this->redirect(['/pph-bukti-potong/create-pasal-21-honorer',
                        'id' => $model->wajibPajakNonId,
            ]);
        } else {
            return $this->renderAjax('create-modal-21-honorer', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MasterWajibPajakNon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->wajibPajakNonId]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterWajibPajakNon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterWajibPajakNon model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterWajibPajakNon the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterWajibPajakNon::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

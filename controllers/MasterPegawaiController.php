<?php

namespace app\controllers;

use Yii;
use app\models\MasterPegawai;
use app\models\PphStatusKawin;
use app\models\PphJenisKelamin;
use app\models\MasterPenghasilan;
use app\search\MasterPegawaiSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use PHPExcel_Cell;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use DateTime;

/**
 * MasterPegawaiController implements the CRUD actions for MasterPegawai model.
 */
class MasterPegawaiController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all MasterPegawai models.
     * @return mixed
     */
    public function removeNonNumeric($string) {
        $string = str_replace('-', '', $string); // Menghapus karakter "-"
        return preg_replace('/[^0-9\-]/', '', $string); // Menghapus karakter selain numerik
    }

    public function validationNpwp($noNpwp, $nama) {
        $strNpwp = MasterPegawaiController::RemoveNonNumeric($noNpwp);
        $error = NULL;
        //Pengecekan jumlah digit no NPWP
        if (strlen($strNpwp) != 15 && strlen($strNpwp) > 0) {
            $error = "<strong>" . "Error!" . "</strong>" . " Nomor " . "<strong>" . " NPWP " . $noNpwp . "</strong>" . " dengan Nama " . "<strong>" . $nama . "</strong>" . " tidak sesuai ketentuan (harus 15 digit).";
        }
        return $error;
    }

    public function actionIndex() {
        $searchModel = new MasterPegawaiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterPegawai model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterPegawai model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new MasterPegawai();
        $connection = \Yii::$app->db;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $model->nama = $model->nama;
            $nama = $model->nama;
            $model->jabatan = $model->jabatan;
            $jabatan = $model->jabatan;
            $model->nik = $model->nik;
            $nik = $model->nik;
            $model->nip = $model->nip;
            $nip = $model->nip;
            $model->npwp = $model->npwp;
            if ($model->npwp == NULL) {
                $model->npwp = NULL;
            }
            $npwp = $model->npwp;
            $model->alamat = $model->alamat;
            $alamat = $model->alamat;
            $model->jenisKelamin = $model->jenisKelamin;
            $jenisKelamin = $model->jenisKelamin;
            $model->statusPerkawinan = $model->statusPerkawinan;
            $statusPerkawinan = $model->statusPerkawinan;
//            $model->gajiKotor = str_replace(".", "", $model->gajiKotor);
//            $gajiKotor = $model->gajiKotor;
//            $model->pph21 = str_replace(".", "", $model->pph21);
//            $pph21 = $model->pph21;
            $model->statusKerjaId = 1;
            $model->userId = $userId;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $model->created_at = $date->format('Y:m:d H:i:s');
            $created_at = $date->format('Y:m:d H:i:s');
            $model->created_by = $userId;
            $checkIfExist = $connection->createCommand("SELECT pegawaiId FROM master_pegawai WHERE nip = '$model->nip' AND nik = '$model->nik' AND nama = '$model->nama'")->queryAll();
            $arrayPegawaiId = array_column($checkIfExist, 'pegawaiId');
            if ($arrayPegawaiId != NULL) {
                $model = $this->findModel($arrayPegawaiId[0]);
                $model->nama = $nama;
                $model->jabatan = $jabatan;
                $model->nik = $nik;
                $model->nip = $nip;
                $model->npwp = $npwp;
                $model->alamat = $alamat;
                $model->jenisKelamin = $jenisKelamin;
                $model->statusPerkawinan = $statusPerkawinan;
//                $model->gajiKotor = $gajiKotor;
//                $model->pph21 = $pph21;
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $model->updated_at = $date->format('Y:m:d H:i:s');
                $model->updated_by = $userId;
                $model->save();
            } else {
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->pegawaiId]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionCreateUpload() {
        $model = new MasterPegawai();
        $connection = \Yii::$app->db;
        $userId = Yii::$app->user->id;
        $dirTrash = Yii::getAlias('@webroot/trash');
        $dirSubTrash = Yii::getAlias('@webroot/trash/trash_pegawai');

        if (!is_dir($dirTrash)) {
            mkdir(Yii::getAlias('@webroot/trash'));
            if (!is_dir($dirSubTrash)) {
                mkdir(Yii::getAlias('@webroot/trash/trash_pegawai'));
            }
        } else {
            if (!is_dir($dirSubTrash)) {
                mkdir(Yii::getAlias('@webroot/trash/trash_pegawai'));
            }
        }
        if (Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [];
            $fileMaster = UploadedFile::getInstancesByName('master_pegawai');
            $middleName = substr(md5(microtime() * 100000), rand(0, 9), 5);
            if ($fileMaster !== null) {
                $nameMaster = $userId . '_MP' . '_' . $middleName . '_' . date('Y-m-d') . '_' . $fileMaster[0]->getBaseName() . "." . $fileMaster[0]->getExtension();
                $pathMaster = Yii::getAlias('@webroot/trash/trash_pegawai/') . $nameMaster;
                $fileMaster[0]->saveAs($pathMaster);
            } else {
                $error[] = "Silahkan pilih terlebih dahulu file" . "<strong>" . " Excel " . "</strong>" . "yang di upload.";
                $result = [
                    'error' => $error
                ];
            }

            //Get file data
            $objPHPExcelMaster = new \PHPExcel();
            $fileNameMaster = Yii::getAlias('@webroot/trash/trash_pegawai/') . $nameMaster;
            $inputFilesMaster = fopen(Yii::getAlias('@webroot/trash/trash_pegawai/') . $nameMaster, "r");
            try {
                $inputFileTypeMaster = \PHPExcel_IOFactory::identify($fileNameMaster);
                $objReaderMaster = \PHPExcel_IOFactory::createReader($inputFileTypeMaster);
                $objPHPExcelMaster = $objReaderMaster->load($fileNameMaster);
            } catch (Exception $ex) {
                die('Error');
            }
            $sheetMaster = $objPHPExcelMaster->getSheet(0);
            $highestRowMaster = $sheetMaster->getHighestDataRow();
            $highestColumnMaster = $sheetMaster->getHighestDataColumn();
            $colNumberMaster = PHPExcel_Cell::columnIndexFromString($highestColumnMaster);
            $plus = 1;
            if ($colNumberMaster != 4) {
                $temp = $colNumberMaster;
                $nextTemp = $temp - 4;
                $plus = $nextTemp + 1;
            }
            $colMaster = $colNumberMaster - $plus;
            $arrayDataMaster = [];

            for ($row = 1; $row <= $highestRowMaster; ++$row) {
                $rowDataMaster = $sheetMaster->rangeToArray('A' . $row . ':' . $highestColumnMaster . $row, NULL, TRUE, NULL);
                if (!is_null($rowDataMaster[0][1])) {
                    $arrayDataMaster[] = array_map(function($values) {
                        $tempArrayKey = [];
                        foreach ($values as $key => $value) {
                            $tempArrayKey[] = $value;
                        }
                        return $tempArrayKey;
                    }, $rowDataMaster);
                }
            }

            $arrayHeader = array_shift($arrayDataMaster);
            $countMaster = count($arrayDataMaster);
            $countArrayHeader = count($arrayHeader[0]);
            $arrayStatusElement = [];
            $arrayErrorTidakLengkap = [];
            //check array element ada yang kosong
            for ($n = 0; $n < $countMaster; $n++) {
                foreach ($arrayDataMaster[$n][0] as $key => $value) {
                    if ($value == null && $key != 4) {
                        $arrayStatusElement[$n] = 0;
                        $arrayErrorTidakLengkap[] = 'Data dengan Nama ' . "<strong>" . $arrayDataMaster[$n][0][0] . "</strong>" . ' dan NIP ' . "<strong>" . $arrayDataMaster[$n][0][2] . "</strong>" . " Tidak berhasil ditambah karena data tidak lengkap.";
                        break;
                    } else {
                        $arrayStatusElement[$n] = 1;
                    }
                }
            }

            //hapus array yang tidak lengkap
            $arrayDataTidakLengkap = [];
            for ($m = 0; $m < $countMaster; $m++) {
                if ($arrayStatusElement[$m] == 0) {
                    unset($arrayDataMaster[$m]);
                    $arrayDataTidakLengkap[$m] = $m;
                }
            }
            $arrayDataMaster = array_values($arrayDataMaster);
            $error = [];
            $count = 0;
            $passStatus = null;
            $countUpdate = 0;
            $countSuccess = 0;
            for ($x = 0; $x < count($arrayDataMaster); $x++) {
                $error[] = MasterPegawaiController::validationNpwp($arrayDataMaster[$x][0][4], $arrayDataMaster[$x][0][0]);
                if (!is_null($error[0])) {
                    $passStatus = false;
                } else {
                    $passStatus = true;
                }
                if ($passStatus == true) {
                    $nama = $arrayDataMaster[$x][0][0];
                    $jabatan = $arrayDataMaster[$x][0][1];
                    $nip = $arrayDataMaster[$x][0][2];
                    $nik = $arrayDataMaster[$x][0][3];
                    $npwp = $arrayDataMaster[$x][0][4];
                    $alamat = $arrayDataMaster[$x][0][5];
                    $jenKel = $arrayDataMaster[$x][0][6];
                    if ($jenKel == 'LK') {
                        $jenKel = 1;
                    } else if ($jenKel == 'PR') {
                        $jenKel = 2;
                    } else {
                        $jenKel = 3;
                    }
                    $jenisKelamin = $jenKel;
                    $statKaw = $arrayDataMaster[$x][0][7];
                    if ($statKaw == 'TK/0') {
                        $statKaw = 1;
                    } else if ($statKaw == 'K/0') {
                        $statKaw = 2;
                    } else if ($statKaw == 'K/1') {
                        $statKaw = 3;
                    } else if ($statKaw == 'K/2') {
                        $statKaw = 4;
                    } else if ($statKaw == 'K/3') {
                        $statKaw = 5;
                    } else {
                        $statKaw = 6;
                    }
                    $statusPerkawinan = $statKaw;
                    $statusKerjaId = 1;
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at = $date->format('Y:m:d H:i:s');
                    $commandUpdate = "UPDATE master_pegawai SET nama = '$nama', jabatan = '$jabatan', nip = '$nip', nik = '$nik', npwp='$npwp', alamat='$alamat', jenisKelamin = '$jenisKelamin', statusPerkawinan = '$statusPerkawinan', updated_by='$userId', updated_at='$created_at' WHERE nip = $nip AND nik = $nik AND nama = '$nama'";
                    $queryUpdate = Yii::$app->db->createCommand($commandUpdate)->execute();
                    if ($queryUpdate == true) {
                        $countUpdate = $countUpdate + 1;
                    } else {
                        $commandInsert = "INSERT INTO master_pegawai(nama, jabatan, nip, nik, npwp, alamat, jenisKelamin, statusPerkawinan, statusKerjaId,userId, created_at, created_by) VALUES('$nama', '$jabatan', '$nip', '$nik', '$npwp', '$alamat', '$jenisKelamin', '$statusPerkawinan', '$statusKerjaId','$userId', '$created_at', '$userId')";
                        $queryInsert = Yii::$app->db->createCommand($commandInsert)->execute();
                        if ($queryInsert == true) {
                            $countSuccess = $countSuccess + 1;
                        }
                    }

                    $count = $count + 1;
                }
            }

            if ($countSuccess > 0) {
                $success = "Data baru sebanyak " . "<strong>" . $countSuccess . " baris" . "</strong>" . " berhasil ditambahkan ke " . "<strong>" . " Pusat Data Pegawai" . "</strong>" . ".";
                Yii::$app->session->setFlash('success', $success);
            }

            if ($countUpdate > 0) {
                $info = "Sebanyak " . "<strong>" . $countUpdate . "</strong>" . " Pegawai" . " berhasil diperbaharui.";
                Yii::$app->session->setFlash('info', $info);
            }

            if ($arrayErrorTidakLengkap) {
                Yii::$app->session->setFlash('error', $arrayErrorTidakLengkap);
            }

//            echo '<pre>';
//            print_r($arrayDataMaster);
//            die();
            $result = [
                'url' => Url::to(['index']),
            ];
            return $result;
        }

        return $this->render('create-upload', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterPegawai model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelWp = MasterPegawai::find()->where(['pegawaiId' => $id])->one();
        $user_id = $modelWp->userId;
        if (Yii::$app->user->id == $user_id) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->pegawaiId]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Deletes an existing MasterPegawai model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (Yii::$app->user->id == 1) {
            $modelPenghasilan = MasterPenghasilan::find()->where(['pegawaiId' => $id])->asArray()->one();
            if($modelPenghasilan == NULL){
                $this->findModel($id)->delete();
            } else {
                Yii::$app->session->setFlash('error', "<strong>"."GAGAL! "."</strong>"."Pegawai tidak berhasil dihapus karena memiliki"."<strong>"." Penghasilan "."</strong>"."yang terdaftar");
                return Yii::$app->response->redirect(['/master-pegawai/index']);
            }
            
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }
    
    public function actionRemoveSelected() {
        $userId = Yii::$app->user->identity->id;
        if($userId == 1){
            $session = Yii::$app->session;
            $action = Yii::$app->request->post('action');
            $selection = (array) Yii::$app->request->post('selection');
            $countSelected = count($selection);
            
            $deleteFailed = 0;
            $deleteSuccess = 0;
            for($i = 0; $i < $countSelected; $i++){

                $modelPenghasilan = MasterPenghasilan::find()->where(['pegawaiId' => $selection[$i]])->asArray()->one();
                if($modelPenghasilan == NULL){
                    $commandDeletePegawai = "DELETE FROM master_pegawai WHERE pegawaiId='$selection[$i]'";                            
                    $execDeletePegawai = Yii::$app->db->createCommand($commandDeletePegawai)->execute();
                } else {
                     $deleteFailed = $deleteFailed + 1;
                }                
                
                if (isset($execDeletePegawai) && $execDeletePegawai == true) {
                    $deleteSuccess = $deleteSuccess + 1;
                }
            }                       
            
            if ($deleteSuccess > 0) {
                if ($deleteFailed == 0) {
                    $success = "Sebanyak " . "<strong>" . $deleteSuccess . "</strong>" . " Data berhasil dihapus.";
                    Yii::$app->session->setFlash('success', $success);
                } else {
                    $success = "Sebanyak " . "<strong>" . $deleteSuccess . "</strong>" . " Data berhasil dihapus.";
                    Yii::$app->session->setFlash('success', $success);
                    $failed = "Sebanyak " . "<strong>" . $deleteFailed . "</strong>" . " Data tidak berhasil dihapus.";
                    Yii::$app->session->setFlash('error', $failed);
                }
            } else {
                if ($deleteFailed > 0) {
                    $failed = "Sebanyak " . "<strong>" . $deleteFailed . "</strong>" . " Data tidak berhasil dihapus.";
                    Yii::$app->session->setFlash('error', $failed);
                }
            }
            
            return $this->redirect(['index']);            
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Finds the MasterPegawai model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterPegawai the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterPegawai::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

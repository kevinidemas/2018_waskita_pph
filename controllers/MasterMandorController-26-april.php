<?php

namespace app\controllers;

use Yii;
use app\models\MasterMandor;
use app\search\MasterMandorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\PphJenisKelamin;
use yii\filters\AccessControl;
use yii\web\Response;
use DateTime;

/**
 * MasterMandorController implements the CRUD actions for MasterMandor model.
 */
class MasterMandorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterMandor models.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new MasterMandorSearch();
        $searchModel->approvalStatus = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDownloadNpwp($id) {
        $wp = MasterMandor::findOne($id);
        $path = Yii::getAlias('@webroot/bukti/') . $wp->buktiNpwp;
        if (file_exists($path)) {            
            return Yii::$app->response->sendFile($path);
        } else {
            $error = "File " . "<strong>" . "NPWP " . "</strong>" . "tidak ditemukan";
            Yii::$app->session->setFlash('error', $error);
            return $this->redirect(['/master-mandor/index-approval-mandor']);
        }
    }
    
    public function actionDownloadKtp($id) {
        $wp = MasterMandor::findOne($id);
        $path = Yii::getAlias('@webroot/bukti/') . $wp->buktiNik;
        if (file_exists($path)) {            
            return Yii::$app->response->sendFile($path);
        } else {
            $error = "File " . "<strong>" . "KTP " . "</strong>" . "tidak ditemukan";
            Yii::$app->session->setFlash('error', $error);
            return $this->redirect(['/master-mandor/index-approval-mandor']);
        }
    }
    
    public function actionIndexApprovalMandor()
    {
        $searchModel = new MasterMandorSearch();
        $searchModel->approvalStatus = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index-approval-mandor', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApproveMandor($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $model->approvalStatus = 1;
        $model->save(false);
        
        return $this->redirect(['index-approval-mandor']);        
    }
    
    public function actionRejectMandor($id) {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index-approval-mandor']);        
    }

    /**
     * Displays a single MasterMandor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterMandor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterMandor();
        $connection = \Yii::$app->db;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $model->buktiNpwp = UploadedFile::getInstance($model, 'buktiNpwp');
           $model->buktiNik = UploadedFile::getInstance($model, 'buktiNik');
           if ($model->buktiNpwp && $model->buktiNik) {
                $imgIdNpwp = mt_rand(100000, 999999);
                $nameNpwp = 'NPWP' . $imgIdNpwp . '-' . $model->buktiNpwp->baseName . '.' . $model->buktiNpwp->extension;
                $pathNpwp = Yii::getAlias('@webroot/bukti/') . $nameNpwp;
                $model->buktiNpwp->saveAs($pathNpwp);
                $model->buktiNpwp = $nameNpwp;
                $imgIdNik = mt_rand(100000, 999999);
                $nameNik = 'NIK' . $imgIdNik . '-' . $model->buktiNik->baseName . '.' . $model->buktiNik->extension;
                $pathNik = Yii::getAlias('@webroot/bukti/') . $nameNik;
                $model->buktiNik->saveAs($pathNik);
                $model->buktiNik = $nameNik;
            }
            $model->nama = $model->nama;
            $nama = $model->nama;
            $model->posisi = $model->posisi;
            $posisi = $model->posisi;
            $model->nik = $model->nik;
            $nik = $model->nik;
            $model->npwp = preg_replace("/[^0-9]/", "", $model->npwp);
            if ($model->npwp == NULL) {
                $model->npwp = NULL;
            }
            $npwp = preg_replace("/[^0-9]/", "", $model->npwp);            
            $model->alamat = $model->alamat;
            $alamat = $model->alamat;
            $hp = $model->hp;
            $email = $model->email;
            $model->jenisKelamin = $model->jenisKelamin;
            $jenisKelamin = $model->jenisKelamin;
            $model->statusKerjaId = 3;
            $model->approvalStatus = 2;
            $model->userId = $userId;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $model->created_at = $date->format('Y:m:d H:i:s');
            $created_at = $date->format('Y:m:d H:i:s');
            $model->created_by = $userId;
            $checkIfExist = $connection->createCommand("SELECT mandorId FROM master_mandor WHERE nik = '$model->nik' AND nama = '$model->nama'")->queryAll();
            $arrayMandorId = array_column($checkIfExist, 'pegawaiId');
            if ($arrayMandorId != NULL) {
                $model = $this->findModel($arrayMandorId[0]);
                $model->nama = $nama;
                $model->posisi = $posisi;
                $model->nik = $nik;
                $model->hp = $hp;
                $model->email = $email;
                $model->npwp = preg_replace("/[^0-9]/", "", $npwp);
                $model->alamat = $alamat;
                $model->jenisKelamin = $jenisKelamin;
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $model->updated_at = $date->format('Y:m:d H:i:s');
                $model->updated_by = $userId;
                $model->save();
            } else {
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->mandorId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterMandor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->mandorId]);
            }        
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }               
    }

    /**
     * Deletes an existing MasterMandor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {   
        if (Yii::$app->user->id == 1) {
            $this->findModel($id)->delete();        
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }   
    }

    /**
     * Finds the MasterMandor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterMandor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterMandor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

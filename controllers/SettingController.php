<?php

namespace app\controllers;

use Yii;
use app\models\Setting;
use app\models\User;
use app\models\ChangePasswordForm;
use app\search\SettingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\PphWpInduk;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all Setting models.
     * @return mixed
     */
    public function actionChange() {
        $id = \Yii::$app->user->id;
        try {
            $model = new ChangePasswordForm($id);
        } catch (InvalidParamException $e) {
            throw new \yii\web\BadRequestHttpException($e->getMessage());
        }

        if ($model->load(\Yii::$app->request->post()) && $model->validate() && $model->changePassword()) {
            \Yii::$app->session->setFlash('success', 'Password berhasil diubah!');
        }

        return $this->render('changePassword', [
                    'model' => $model,
        ]);
    }

    public function actionIndex() {
        $searchModel = new SettingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Setting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $model = Setting::find()->where(['userId' => $id])->one();
        $id = $model->settingId;
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
//        return $this->render('view', [
//                    'model' => $model,
//                    'id' => $id
//        ]);
    }

    /**
     * Creates a new Setting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id) {
        $getSetting = Yii::$app->db->CreateCommand("SELECT userId FROM setting WHERE userId = '$id'")->queryAll();
        if ($getSetting != null) {
            return $this->redirect(['view', 'id' => $id]);
        }
        $model = new Setting();
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $model->userId = $id;
            if (strlen($model->startNomorBP) > $model->digitNomorBP) {
                Yii::$app->session->setFlash('error', "Jumlah Digit Nomor Bukti Potong yang anda masukkan sebesar " . "<strong>" . $model->digitNomorBP . "</strong>" . " silahkan masukkan Nomor Bukti Potong Awal dengan jumlah digit maksimal " . "<strong>" . $model->digitNomorBP . "</strong>");
                return Yii::$app->response->redirect(['/setting/create']);
            }
            
            if (strlen($model->startNomorBP) < $model->digitNomorBP) {
                $less = $model->digitNomorBP - strlen($model->startNomorBP);
                $count = null;
                for ($n = 1; $n <= $less; $n++) {
                    $count = $count . '0';
                }
                $start = $count . $model->startNomorBP;
                $model->startNomorBP = $start;
            } else {
                $model->startNomorBP = $model->startNomorBP;
            }                       
            
            if (strlen($model->startNomorBP_23) < $model->digitNomorBP_23) {
                $less = $model->digitNomorBP_23 - strlen($model->startNomorBP_23);
                $count = null;
                for ($n = 1; $n <= $less; $n++) {
                    $count = $count . '0';
                }
                $start = $count . $model->startNomorBP_23;
                $model->startNomorBP_23 = $start;
            } else {
                $model->startNomorBP_23 = $model->startNomorBP_23;
            }
            
            if (strlen($model->startNomorBP_4) < $model->digitNomorBP_4) {
                $less = $model->digitNomorBP_4 - strlen($model->startNomorBP_4);
                $count = null;
                for ($n = 1; $n <= $less; $n++) {
                    $count = $count . '0';
                }
                $start = $count . $model->startNomorBP_4;
                $model->startNomorBP_4 = $start;
            } else {
                $model->startNomorBP_4 = $model->startNomorBP_4;
            }
            
            if (isset($model->npwpPemungutPajak)) {
                $model->npwpPemungutPajak = preg_replace("/[^0-9]/", "", $model->npwpPemungutPajak);
                $model->namaPemungutPajak = $model->namaPemungutPajak;
            }
            
            if($model->startNomorBP_21 != NULL && $model->endNomorBP_21 != NULL){
                $commandInsertUserInduk = "INSERT INTO pph_user_induk userId VALUES('$model->userId')";
                $execUpdate = Yii::$app->db->createCommand($commandInsertUserInduk)->execute();
            }
            
            if (isset($_POST['pasal-21-pt'])) {
                $model->pasal21_pt = 1;
            } else {
                $model->pasal21_pt = 0;
            }
            
            if (isset($_POST['pasal-21-ptt'])) {
                $model->pasal21_ptt = 1;
            } else {
                $model->pasal21_ptt = 0;
            }
            
            if (isset($_POST['pasal-22'])) {
                $model->pasal22 = 1;
            } else {
                $model->pasal22 = 0;
            }
            
            if (isset($_POST['pasal-23-jasa'])) {
                $model->pasal23_jasa = 1;
            } else {
                $model->pasal23_jasa = 0;
            }
            
            if (isset($_POST['pasal-23-sewa'])) {
                $model->pasal23_sewa = 1;
            } else {
                $model->pasal23_sewa = 0;
            }
            
            if (isset($_POST['pasal-4-jasa'])) {
                $model->pasal4_jasa = 1;
            } else {
                $model->pasal4_jasa = 0;
            }
            
            if (isset($_POST['pasal-4-sewa'])) {
                $model->pasal4_sewa = 1;
            } else {
                $model->pasal4_sewa = 0;
            }
            
            
            
            $model->npwpPemungutPajakCabang = preg_replace("/[^0-9]/", "", $model->npwpPemungutPajakCabang);
            $model->kppTerdaftar = $model->kppTerdaftar;
            $model->nama = $model->nama;
            $model->npwp = $model->npwp;
            $model->jabatan = $model->jabatan;
            $model->kota = $model->kota;
            $model->save(false);
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Setting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $userId = Yii::$app->user->id;
        $modelSetting = Setting::find()->where(['userId' => $userId])->one();
        $startNBP = $modelSetting->startNomorBP;
        if ($model->load(Yii::$app->request->post())) {
            if (strlen($model->startNomorBP) > $model->digitNomorBP) {
                Yii::$app->session->setFlash('error', "Jumlah Digit Nomor Bukti Potong yang anda masukkan sebesar " . "<strong>" . $model->digitNomorBP . "</strong>" . " silahkan masukkan Nomor BP Awal dengan jumlah digit maksimal " . "<strong>" . $model->digitNomorBP . "</strong>");
                return Yii::$app->response->redirect(['/setting/update', 'id' => $id]);
            }
            
            if (strlen($model->startNomorBP) < $model->digitNomorBP) {
                $less = $model->digitNomorBP - strlen($model->startNomorBP);
                $count = null;
                for ($n = 1; $n <= $less; $n++) {
                    $count = $count . '0';
                }
                $start = $count . $model->startNomorBP;
                $model->startNomorBP = $start;
            } else {
                $model->startNomorBP = $model->startNomorBP;
            }                   
            
            if (strlen($model->startNomorBP_23) < $model->digitNomorBP_23) {
                $less = $model->digitNomorBP_23 - strlen($model->startNomorBP_23);
                $count = null;
                for ($n = 1; $n <= $less; $n++) {
                    $count = $count . '0';
                }
                $start = $count . $model->startNomorBP_23;
                $model->startNomorBP_23 = $start;
            } else {
                $model->startNomorBP_23 = $model->startNomorBP_23;
            }
            
            if (strlen($model->startNomorBP_4) < $model->digitNomorBP_4) {
                $less = $model->digitNomorBP_4 - strlen($model->startNomorBP_4);
                $count = null;
                for ($n = 1; $n <= $less; $n++) {
                    $count = $count . '0';
                }
                $start = $count . $model->startNomorBP_4;
                $model->startNomorBP_4 = $start;
            } else {
                $model->startNomorBP_4 = $model->startNomorBP_4;
            }
            
            if (isset($model->npwpPemungutPajak)) {
                $model->npwpPemungutPajak = preg_replace("/[^0-9]/", "", $model->npwpPemungutPajak);
                $model->namaPemungutPajak = $model->namaPemungutPajak;
            }
            
            if (isset($_POST['pasal-21-pt'])) {
                $model->pasal21_pt = 1;
            } else {
                $model->pasal21_pt = 0;
            }
            
            if (isset($_POST['pasal-21-ptt'])) {
                $model->pasal21_ptt = 1;
            } else {
                $model->pasal21_ptt = 0;
            }
            
            if (isset($_POST['pasal-22'])) {
                $model->pasal22 = 1;
            } else {
                $model->pasal22 = 0;
            }
            
            if (isset($_POST['pasal-23-jasa'])) {
                $model->pasal23_jasa = 1;
            } else {
                $model->pasal23_jasa = 0;
            }
            
            if (isset($_POST['pasal-23-sewa'])) {
                $model->pasal23_sewa = 1;
            } else {
                $model->pasal23_sewa = 0;
            }
            
            if (isset($_POST['pasal-4-jasa'])) {
                $model->pasal4_jasa = 1;
            } else {
                $model->pasal4_jasa = 0;
            }
            
            if (isset($_POST['pasal-4-sewa'])) {
                $model->pasal4_sewa = 1;
            } else {
                $model->pasal4_sewa = 0;
            }
            
            if (isset($_POST['pasal-4-perencana'])) {
                $model->pasal4_perencana = 1;
            } else {
                $model->pasal4_perencana = 0;
            }
            
            if($model->startNomorBP_21 != NULL && $model->endNomorBP_21 != NULL){
                $commandInsertUserInduk = "INSERT INTO pph_user_induk (userId) VALUES('$model->userId')";
                $execUpdate = Yii::$app->db->createCommand($commandInsertUserInduk)->execute();
            } else {                
                $commandDeleteUserInduk = "DELETE FROM pph_user_induk WHERE userId = '$model->userId'";
                $execUpdate = Yii::$app->db->createCommand($commandDeleteUserInduk)->execute();
            }
            
            $model->npwpPemungutPajakCabang = preg_replace("/[^0-9]/", "", $model->npwpPemungutPajakCabang);
            $model->userIndukId = $model->userIndukId;
            $model->kppTerdaftar = $model->kppTerdaftar;
            $model->nama = $model->nama;
            $model->npwp= preg_replace("/[^0-9]/", "", $model->npwp);
            $model->jabatan = $model->jabatan;
            $model->kota = $model->kota;
            $model->save(false);
            $modelId = Setting::find()->where(['settingId' => $id])->one();
            $id = $modelId->userId;
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Setting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Setting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Setting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

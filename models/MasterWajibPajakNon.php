<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_wajib_pajak_non".
 *
 * @property int $wajibPajakNonId
 * @property string $nama
  * @property string $nik
 * @property string $alamat
 * @property int $created_by
 * @property int $statusKerja
 * @property int $jenisWp
 */
class MasterWajibPajakNon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_wajib_pajak_non';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'statusKerja', 'jenisWp'], 'integer'],
            [['nama'], 'string', 'max' => 64],
            [['nik'], 'string', 'max' => 16],            
            [['alamat'], 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wajibPajakNonId' => 'Wajib Pajak Non ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'created_by' => 'Created By',
            'nik' => 'NIK',
        ];
    }
}

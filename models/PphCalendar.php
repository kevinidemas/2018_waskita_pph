<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_calendar".
 *
 * @property int $calendarId
 * @property string $startDate
 * @property string $endDate
 * @property string $lastOpened
 * @property string $lastClosed
 * @property string $calendarNote
 * @property string $created_at
 * @property int $created_by
 * @property int $status
 */
class PphCalendar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_calendar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['startDate', 'endDate', 'created_at', 'lastOpened', 'lastClosed'], 'safe'],
            [['created_by', 'status', 'is_permanent_close'], 'integer'],
            [['calendarNote'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'calendarId' => 'Calendar ID',
            'startDate' => 'Start Date',
            'endDate' => 'End Date',
            'calendarNote' => 'Masa',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'status' => 'Status',
        ];
    }
    
    public function beforeSave($insert) {
        if ($insert) {
            $this->startDate = Yii::$app->formatter->asDatetime(strtotime($this->startDate), "php:Y-m-d");
            $this->endDate = Yii::$app->formatter->asDatetime(strtotime($this->endDate), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }
}
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "master_honorer".
 *
 * @property int $honorerId
 * @property string $nama
 * @property string $posisi
 * @property string $nik
 * @property string $npwp
 * @property string $alamat
 * @property int $jenisKelamin
 * @property int $statusKerjaId
 * @property int $userId
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $hp
 * @property string $email
 * @property int $approvalStatus
 */
class MasterHonorer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_honorer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'alamat', 'npwp', 'nik', 'posisi', 'jenisKelamin', 'hp', 'email'], 'required'],
            [['honorerId', 'jenisKelamin', 'statusKerjaId', 'userId', 'created_by', 'updated_by', 'hp', 'approvalStatus'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama', 'posisi', 'email'], 'string', 'max' => 64],
            [['nik', 'npwp'], 'string', 'max' => 32],
            [['alamat'], 'string', 'max' => 1000],
            [['honorerId'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'honorerId' => 'Honorer ID',
            'nama' => 'Nama',
            'posisi' => 'Posisi',
            'nik' => 'NIK',
            'npwp' => 'NPWP',
            'alamat' => 'Alamat',
            'jenisKelamin' => 'Jenis Kelamin',
            'statusKerjaId' => 'Status Kerja',
            'userId' => 'User ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'hp' => 'HP',
            'email' => 'Email',
            'approvalStatus' => 'Approval Status',
        ];
    }
}

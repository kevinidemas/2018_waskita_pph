<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_status_kawin".
 *
 * @property int $statusKawinId
 * @property string $status
 */
class PphStatusKawin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_status_kawin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statusKawinId' => 'Status Kawin ID',
            'status' => 'Status',
        ];
    }
}

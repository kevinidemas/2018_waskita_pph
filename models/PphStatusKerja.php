<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_status_kerja".
 *
 * @property int $statusKerjaId
 * @property int $tarifId
 * @property string $statusKerja
 * @property string $nomor
 */
class PphStatusKerja extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_status_kerja';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarifId'], 'integer'],
            [['statusKerja'], 'string', 'max' => 64],
            [['nomor'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tarifId' => 'Tarif ID',
            'statusKerjaId' => 'Status Kerja ID',
            'statusKerja' => 'Status Kerja',
            'nomor' => 'Nomor',
        ];
    }
}

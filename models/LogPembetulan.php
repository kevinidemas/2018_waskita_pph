<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log_pembetulan".
 *
 * @property int $pembetulanId
 * @property string $lastOpened
 * @property string $created_at
 * @property string $masa
 * @property string $tahun
 * @property int $pasalId
 * @property int $pembetulan
 * @property int $userId
 * @property int $prevId
 * @property int $currId
 */
class LogPembetulan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_pembetulan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pasalId', 'userId', 'prevId', 'currId', 'pembetulan'], 'integer'],
            [['lastOpened', 'created_at'], 'safe'],
            [['masa'], 'string', 'max' => 2],
            [['tahun'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pembetulanId' => 'Pembetulan',
            'masa' => 'Masa',
            'tahun' => 'Tahun',
            'pasalId' => 'Pasal',
            'userId' => 'Pengguna',
            'prevId' => 'Prev',
            'currId' => 'Curr',
            'created_at' => 'Dibuat Pada',
        ];
    }

    public function beforeSave($insert) {
        if ($insert) {
            $this->created_at = Yii::$app->formatter->asDatetime(strtotime($this->created_at), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }
}

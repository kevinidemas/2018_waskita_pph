<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_status_vendor".
 *
 * @property int $statusVendorId
 * @property int $tarifId
 */
class PphStatusVendor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_status_vendor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tarifId'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statusVendorId' => 'Status Vendor ID',
            'tarifId' => 'Tarif ID',
        ];
    }
}

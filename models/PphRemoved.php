<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_removed".
 *
 * @property int $removedId
 * @property string $start
 * @property string $end
 * @property string $tahun
 * @property int $userId
 * @property int $userIndukId
 */
class PphRemoved extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_removed';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'userIndukId'], 'integer'],
            [['start', 'end'], 'string', 'max' => 32],
            [['tahun'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'removedId' => 'Removed ID',
            'start' => 'Start',
            'end' => 'End',
            'tahun' => 'Tahun',
            'userId' => 'User ID',
            'userIndukId' => 'User Induk ID',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_bujpk".
 *
 * @property int $bujpkId
 * @property string $nomorBujpk
 * @property int $wajibPajakId
 * @property string $penanggungJawab
 * @property string $kemampuanKeuangan
 * @property string $published_at
 * @property string $expired_at
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property resource $bukti
 * @property string $keterangan
 * @property int $is_active
 * @property int $is_approve
 * @property int $is_expired
 * @property int $tingkatKeuangan
 * @property int $is_expired_warning
 */
class PphBujpk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_bujpk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kemampuanKeuangan', 'expired_at', 'nomorBujpk', 'penanggungJawab', 'keterangan', 'wajibPajakId'], 'required'],
            [['wajibPajakId', 'created_by', 'updated_by', 'is_active', 'is_approve', 'is_expired', 'tingkatKeuangan', 'is_expired_warning'], 'integer'],
//            [['kemampuanKeuangan'], 'number'],
            [['published_at', 'expired_at', 'created_at', 'updated_at'], 'safe'],
            [['bukti'], 'string'],
            [['nomorBujpk'], 'string', 'max' => 32, 'min' => 15],
            [['penanggungJawab'], 'string', 'max' => 32],
            ['nomorBujpk', 'unique', 'targetAttribute' => ['nomorBujpk'], 'message' => 'Nomor BUJPK telah terdaftar.'],
            [['keterangan'], 'string', 'max' => 2048],
            ['penanggungJawab', 'match', 'pattern' => '/^([a-z0-9 ]+.)*[a-z0-9()]+$/i'],
            ['nomorBujpk', 'match', 'pattern' => '/^[0-9]+$/', 'message' => "Nomor IUJK hanya terdiri dari Angka"],
            ['keterangan', 'match', 'pattern' => '/^[a-zA-Z0-9.,\-\/\s]+$/', 'message' => "Keterangan tidak boleh mengandung simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bujpkId' => 'BUJPK',
            'nomorBujpk' => 'Nomor BUJPK',
            'wajibPajakId' => 'Wajib Pajak',
            'penanggungJawab' => 'Penanggung Jawab',
            'kemampuanKeuangan' => 'Kemampuan Keuangan',
            'published_at' => 'Berlaku Sejak',
            'expired_at' => 'Berakhir Pada',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'bukti' => 'Bukti',
            'keterangan' => 'Keterangan',
            'is_active' => 'Is Active',
            'is_approve' => 'Is Approve',
            'is_expired' => 'Is Expired',
            'tingkatKeuangan' => 'Tingkat Keuangan',
            'is_expired_warning' => 'Is Expired Warning',
        ];
    }
    
    public function beforeSave($insert) {
        if ($insert) {
            $this->expired_at = Yii::$app->formatter->asDatetime(strtotime($this->expired_at), "php:Y-m-d");
            $this->published_at = Yii::$app->formatter->asDatetime(strtotime($this->published_at), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_ssp".
 *
 * @property int $sspId
 * @property string $ntpn
 * @property string $tanggalSetor
 * @property string $nilai
 * @property int $created_by
 * @property int $masa
 * @property int $pasalId
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 * @property string $lokasi
 * @property resource $bukti
 */
class PphSsp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_ssp';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggalSetor', 'created_at', 'updated_at'], 'safe'],
            [['nilai'], 'number', 'numberPattern' => '/[0-9]|\./'],
            [['created_by', 'updated_by', 'masa', 'pasalId'], 'integer'],
            [['bukti'], 'string'],
            ['ntpn', 'match', 'pattern' => '/^[0-9.,\-\/\s]+$/', 'message' => "Nomor tidak boleh mengandung huruf, simbol, dan tanda baca kecuali '-', '.', ',',dan '/'"],            
            [['ntpn'], 'string', 'max' => 32],
            [['lokasi'], 'string', 'max' => 64],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sspId' => 'SSP',
            'ntpn' => 'Nomor NTPN / PBK',
            'tanggalSetor' => 'Tanggal Setor',
            'nilai' => 'Nilai',
            'created_by' => 'Dibuat Oleh',
            'created_at' => 'Dibuat Pada',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'bukti' => 'Upload SSP',
            'lokasi' => 'Lokasi',
            'pasalId' => 'Pasal',
        ];
    }
    
    public function beforeSave($insert) {
        if ($insert) {
            $this->tanggalSetor = Yii::$app->formatter->asDatetime(strtotime($this->tanggalSetor), "php:Y-m-d");
        }
        return parent::beforeSave($insert);
    }
}

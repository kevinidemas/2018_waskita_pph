<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property int $optionsId
 * @property string $name
 * @property int $value
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'integer'],
            [['name'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'optionsId' => 'Options ID',
            'name' => 'Name',
            'value' => '',
        ];
    }
}

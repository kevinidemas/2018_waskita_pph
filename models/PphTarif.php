<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pph_tarif".
 *
 * @property int $tarifId
 * @property string $status
 * @property string $tarif
 * @property int $userId
 * @property int $parentId
 */
class PphTarif extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pph_tarif';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'parentId'], 'integer'],
            [['status'], 'string', 'max' => 64],
            [['tarif'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tarifId' => 'Tarif ID',
            'status' => 'Status',
            'tarif' => 'Tarif',
            'userId' => 'User ID',
            'parentId' => 'Parent ID',
        ];
    }
    
    public function getStatusNpwpOpt($param) {
        
    }
}

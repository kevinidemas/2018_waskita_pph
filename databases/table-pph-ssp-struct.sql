/*
SQLyog Ultimate
MySQL - 10.1.25-MariaDB : Database - vatout_waskita
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`vatout_waskita` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `vatout_waskita`;

/*Table structure for table `pph_ssp` */

CREATE TABLE `pph_ssp` (
  `sspId` int(11) NOT NULL AUTO_INCREMENT,
  `ntpn` varchar(32) DEFAULT NULL,
  `tanggalSetor` date DEFAULT NULL,
  `nilai` decimal(30,0) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `bukti` longblob,
  `masa` int(11) DEFAULT NULL,
  PRIMARY KEY (`sspId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

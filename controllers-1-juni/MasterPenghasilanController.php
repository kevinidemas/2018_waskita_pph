<?php

namespace app\controllers;

use Yii;
use app\models\MasterPenghasilan;
use app\models\MasterPegawai;
use app\models\User;
use app\search\MasterPenghasilanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\helpers\Url;
use DateTime;
use yii\web\UploadedFile;
use PHPExcel_Cell;

/**
 * MasterPenghasilanController implements the CRUD actions for MasterPenghasilan model.
 */
class MasterPenghasilanController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    public function validationNpwp($noNpwp, $nama) {
        $strNpwp = MasterPegawaiController::RemoveNonNumeric($noNpwp);
        $error = NULL;
        //Pengecekan jumlah digit no NPWP
        if (strlen($strNpwp) != 15 && strlen($strNpwp) > 0) {
            $error = "<strong>" . "Error!" . "</strong>" . " Nomor " . "<strong>" . " NPWP " . $noNpwp . "</strong>" . " dengan Nama " . "<strong>" . $nama . "</strong>" . " tidak sesuai ketentuan (harus 15 digit).";
        }
        return $error;
    }

    public function validationMasaPajak($masa, $tahun, $nama) {
        $error = NULL;
        $session = Yii::$app->session;
        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        if (strlen($bln) == 1) {
            $bln = '0' . $bln;
        }
        if (strlen($masa) == 1) {
            $masa = '0' . $masa;
        }
        $thn = $array[1];
        if ($masa != $bln) {
            if ($tahun != $thn) {
                $error = "<strong>" . "Error!" . "</strong>" . " Masa dan Tahun Pajak " . "<strong>" . " Pegawai " . $nama . "</strong>" . " tidak sesuai dengan Masa Pajak yang dipilih";
            } else {
                $error = "<strong>" . "Error!" . "</strong>" . " Masa Pajak " . "<strong>" . " Pegawai " . $nama . "</strong>" . " tidak sesuai dengan Masa Pajak yang dipilih";
            }
        } else {
            if ($tahun != $thn) {
                $error = "<strong>" . "Error!" . "</strong>" . " Tahun Pajak " . "<strong>" . " Pegawai " . $nama . "</strong>" . " tidak sesuai dengan Masa Pajak yang dipilih";
            }
        }

        return $error;
    }

    /**
     * Lists all MasterPenghasilan models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new MasterPenghasilanSearch();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $session['pasal-terpilih'] = 1;
        if (strlen($bln) == 1) {
            $bln = '0' . $bln;
        }
        $thn = $array[1];
        if ($userId == 1) {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT penghasilanId FROM master_penghasilan WHERE masaPajak = '$bln' AND tahunPajak = '$thn'")->queryAll();
        } else {
            $getDataCount = Yii::$app->db->CreateCommand("SELECT penghasilanId FROM master_penghasilan WHERE userId = '$userId' AND masaPajak = '$bln' AND tahunPajak = '$thn'")->queryAll();
        }
        if ($getDataCount != null) {
            $session['dataCount-pasal-21'] = 1;
        }
        $searchModel->masaPajak = $bln;
        $searchModel->userId = $userId;
        $searchModel->tahunPajak = $thn;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterPenghasilan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterPenghasilan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRemoveData() {
        $userId = Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $action = Yii::$app->request->post('action');
        $selection = (array) Yii::$app->request->post('selection');
        $countSelected = count($selection);

        $deleteSuccess = 0;
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if ($bln == date('m')) {
            $tgl = date('d');
        }
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        $expired = [];
        $statusRemoveData = false;
        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        }
        for ($x = 0; $x < count($getCalendar); $x++) {
            $calId = $getCalendar[$x]['calendarId'];
            $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND '$date' between startDate and endDate")->queryAll();
            if ($checkRange == null) {
                $expired[] = "Data anda tidak berhasil disimpan, karena Masa berlaku pendaftaran Bukti Potong telah ditutup";
            } else {
                $statusRemoveData = true;
                break;
            }
        }
        if ($statusRemoveData == true) {
            $statusFailed = false;
            $deleteFailed = 0;
            for ($i = 0; $i < $countSelected; $i++) {
                $modelBP = MasterPenghasilan::find()->where(['penghasilanId' => $selection[$i]])->one();
                $commandDeleteDetail = "DELETE FROM master_penghasilan WHERE penghasilanId='$selection[$i]' AND userId='$userId'";
                $execDeleteDetail = Yii::$app->db->createCommand($commandDeleteDetail)->execute();
                if ($execDeleteDetail == true) {
                    $deleteSuccess = $deleteSuccess + 1;
                }
            }
        } else {
            $failedMessage = "Laporan Penghasilan tidak berhasil dihapus karena Calendar untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " telah ditutup";
            $session['delete-bp-failed'] = $failedMessage;
            return $this->redirect(['index']);
        }

        if ($deleteSuccess > 0) {
            if ($deleteFailed == 0) {
                $success = "Sebanyak " . "<strong>" . $deleteSuccess . "</strong>" . " Data berhasil dihapus.";
                Yii::$app->session->setFlash('success', $success);
            } else {
                $success = "Sebanyak " . "<strong>" . $deleteSuccess . "</strong>" . " Data berhasil dihapus.";
                Yii::$app->session->setFlash('success', $success);
                $failed = "Sebanyak " . "<strong>" . $deleteFailed . "</strong>" . " Data tidak berhasil dihapus karena memiliki telah memiliki " . "<strong>" . "Nomor Bukti Potong" . "</strong>";
                Yii::$app->session->setFlash('error', $failed);
            }
        } else {
            if ($deleteFailed > 0) {
                $failed = "Sebanyak " . "<strong>" . $deleteFailed . "</strong>" . " Data tidak berhasil dihapus karena memiliki telah memiliki " . "<strong>" . "Nomor Bukti Potong" . "</strong>";
                Yii::$app->session->setFlash('error', $failed);
            }
        }
        return $this->redirect(['index']);
    }

    public function actionCreateUpload() {
        $model = new MasterPegawai();
        $connection = \Yii::$app->db;
        $userId = Yii::$app->user->id;
        $session = Yii::$app->session;
        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $thn = $array[1];
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if (strlen($bln) == 1) {
            $bln = '0' . $bln;
        }
        if ($bln == date('m')) {
            $tgl = date('d');
        }
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND '$date' between startDate and endDate")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        $status = $session['expiredDate'];

        if (isset($session['saveFailed'])) {
            unset($session['saveFailed']);
        } elseif ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
            return $this->redirect(['/master-penghasilan/index']);
        }
        $dirTrash = Yii::getAlias('@webroot/trash');
        $dirSubTrash = Yii::getAlias('@webroot/trash/trash_penghasilan');

        if (!is_dir($dirTrash)) {
            mkdir(Yii::getAlias('@webroot/trash'));
            if (!is_dir($dirSubTrash)) {
                mkdir(Yii::getAlias('@webroot/trash/trash_penghasilan'));
            }
        } else {
            if (!is_dir($dirSubTrash)) {
                mkdir(Yii::getAlias('@webroot/trash/trash_penghasilan'));
            }
        }
        if (Yii::$app->request->post()) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = [];
            $fileMaster = UploadedFile::getInstancesByName('master_penghasilan');
            $middleName = substr(md5(microtime() * 100000), rand(0, 9), 5);
            if ($fileMaster !== null) {
                $nameMaster = $userId . '_MP' . '_' . $middleName . '_' . date('Y-m-d') . '_' . $fileMaster[0]->getBaseName() . "." . $fileMaster[0]->getExtension();
                $pathMaster = Yii::getAlias('@webroot/trash/trash_penghasilan/') . $nameMaster;
                $fileMaster[0]->saveAs($pathMaster);
            } else {
                $error[] = "Silahkan pilih terlebih dahulu file" . "<strong>" . " Excel " . "</strong>" . "yang di upload.";
                $result = [
                    'error' => $error
                ];
            }

            //Get file data
            $objPHPExcelMaster = new \PHPExcel();
            $fileNameMaster = Yii::getAlias('@webroot/trash/trash_penghasilan/') . $nameMaster;
            $inputFilesMaster = fopen(Yii::getAlias('@webroot/trash/trash_penghasilan/') . $nameMaster, "r");
            try {
                $inputFileTypeMaster = \PHPExcel_IOFactory::identify($fileNameMaster);
                $objReaderMaster = \PHPExcel_IOFactory::createReader($inputFileTypeMaster);
                $objPHPExcelMaster = $objReaderMaster->load($fileNameMaster);
            } catch (Exception $ex) {
                die('Error');
            }
            $sheetMaster = $objPHPExcelMaster->getSheet(0);
            $highestRowMaster = $sheetMaster->getHighestDataRow();
            $highestColumnMaster = $sheetMaster->getHighestDataColumn();
            $colNumberMaster = PHPExcel_Cell::columnIndexFromString($highestColumnMaster);
            $colMaster = $colNumberMaster;
            $arrayDataMaster = [];

            for ($row = 1; $row <= $highestRowMaster; ++$row) {
                $rowDataMaster = $sheetMaster->rangeToArray('A' . $row . ':' . $highestColumnMaster . $row, NULL, TRUE, NULL);
                if (!is_null($rowDataMaster[0][1]) && !is_null($rowDataMaster[0][7])) {
                    $arrayDataMaster[] = array_map(function($values) {
                        $tempArrayKey = [];
                        foreach ($values as $key => $value) {
                            $tempArrayKey[] = $value;
                        }
                        return $tempArrayKey;
                    }, $rowDataMaster);
                }
            }           

            $arrayHeader = array_shift($arrayDataMaster);
            $countMaster = count($arrayDataMaster);
            $countArrayHeader = count($arrayHeader[0]);
            $arrayPegawaiTdkTerdaftar = [];

            //check apakah pegawai telah terdaftar di master data pegawai
            $statusPegawai = null;
            for ($n = 0; $n < $countMaster; $n++) {
                $statusPegawai = 1;
                $npwp = $arrayDataMaster[$n][0][3];
                $nama = $arrayDataMaster[$n][0][4];

                //check berdasarkan npwp
                if ($npwp != '000000000000000' || $npwp != null || $npwp != '0') {
                    $checkQuery = "SELECT pegawaiId FROM master_pegawai WHERE npwp = '$npwp'";
                    $execCheck = Yii::$app->db->createCommand($checkQuery)->execute();
                    if ($execCheck == 0) {
                        $statusPegawai = 0;
                    }
                } else {
                    if ($nama != null) {
                        //check berdasarkan nama
                        $checkQuery = "SELECT pegawaiId FROM master_pegawai WHERE nama = '$nama'";
//                    die($checkQuery);
                        $execCheck = Yii::$app->db->createCommand($checkQuery)->execute();
                        if ($execCheck == 0) {
                            $statusPegawai = 0;
                        }
                    }
                }

                $baris = $n + 2;

                if ($statusPegawai == 0) {
                    $arrayPegawaiTdkTerdaftar[$n] = 'Pegawai pada baris data ke-' . "<strong>" . $baris . "</strong>" . ' tidak terdaftar di ' . "<strong>" . "Master Pegawai" . "</strong>";
                }
            }

            $error = [];
            $count = 0;
            $passStatus = null;
            $countSuccess = 0;
            $countFailed = 0;
            
            for ($x = 0; $x < count($arrayDataMaster); $x++) {
                $error[] = MasterPenghasilanController::validationNpwp($arrayDataMaster[$x][0][3], $arrayDataMaster[$x][0][4]);
                $error[] = MasterPenghasilanController::validationMasaPajak($arrayDataMaster[$x][0][0], $arrayDataMaster[$x][0][1], $arrayDataMaster[$x][0][4]);
                $error = array_values(array_filter($error));
                
                if ($error != null) {
                    $passStatus = false;
                } else {
                    $passStatus = true;
                }

                if ($passStatus == true && !isset($arrayPegawaiTdkTerdaftar[$x])) {
                    $masa_pajak = $arrayDataMaster[$x][0][0];
                    if (strlen($masa_pajak) == 1) {
                        $masa_pajak = '0' . $masa_pajak;
                    }
                    $tahun_pajak = $arrayDataMaster[$x][0][1];
                    $pembetulan = $arrayDataMaster[$x][0][2];
                    $npwp = $arrayDataMaster[$x][0][3];
                    $nama = $arrayDataMaster[$x][0][4];
                    $modelPegawai = MasterPegawai::find()->where(['nama' => $nama])->one();
                    $pegawaiId = $modelPegawai['pegawaiId'];
                    $kode_pajak = $arrayDataMaster[$x][0][5];
                    $bruto = $arrayDataMaster[$x][0][6];
                    $pph = $arrayDataMaster[$x][0][7];
                    $jenis = $arrayDataMaster[$x][0][9];
                    if ($jenis == null) {
                        $jenis = 100;
                    }
                    $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                    $created_at = $date->format('Y:m:d H:i:s');
                    $commandInsert = "INSERT INTO master_penghasilan(pegawaiId, masaPajak, tahunPajak, kodePajak, bruto, pph, userId, created_at, created_by, jenisPenghasilanId) VALUES('$pegawaiId', '$masa_pajak', '$tahun_pajak', '$kode_pajak', '$bruto', '$pph', '$userId', '$created_at', '$userId', '$jenis')";
                    $queryInsert = Yii::$app->db->createCommand($commandInsert)->execute();
                    if ($queryInsert == true) {
                        $countSuccess = $countSuccess + 1;
                    }

                    $count = $count + 1;
                } else {
                    $countFailed = $countFailed + 1;
                }
            }

            if ($countSuccess > 0) {
                $success = "Data baru sebanyak " . "<strong>" . $countSuccess . " baris" . "</strong>" . " berhasil ditambahkan ke " . "<strong>" . " Pusat Data Penghasilan" . "</strong>" . ".";
                Yii::$app->session->setFlash('success', $success);
            }

            if ($countFailed > 0) {
                $errorNote = "Sebanyak " . "<strong>" . $countFailed . "</strong>" . " Data Penghasilan" . " tidak berhasil ditambahkan, antara lain:";
                $arrayError = array_merge($arrayPegawaiTdkTerdaftar, $error);
                array_unshift($arrayError, $errorNote);
                Yii::$app->session->setFlash('error', $arrayError);
            }
            $result = [
                'url' => Url::to(['index']),
            ];
            return $result;
        }

        return $this->render('create-upload', [
                    'model' => $model,
        ]);
    }

    public function actionCreate() {
        $model = new MasterPenghasilan();
        $modelPegawai = new MasterPegawai();
        $userId = Yii::$app->user->id;
        $session = Yii::$app->session;
        $masaPajak = $session['masaPajak'];
        $array = explode(' ', $masaPajak);
        $thn = $array[1];
        $bln = \Yii::t('app', $array[0]);
        $tgl = '01';
        if (strlen($bln) == 1) {
            $bln = '0' . $bln;
        }
        if ($bln == date('m')) {
            $tgl = date('d');
        }
        $getCalendar = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE status = '1'")->queryAll();
        $masaBulan = $session['masaId'];
        $date = $array[1] . '-' . $bln . '-' . $tgl;
        $checkCalender = Yii::$app->db->CreateCommand("SELECT * FROM pph_calendar WHERE MONTH(startDate) = '$masaBulan' AND YEAR(startDate) = '$array[1]' AND MONTH(endDate) = '$masaBulan' AND YEAR(endDate) = '$array[1]'")->queryAll();
        $expired = [];
        $status = false;

        if ($getCalendar == null) {
            $expired[] = "Tidak terdapat kalender yang sedang berstatus aktif atau terbuka, silahkan hubungi admin";
        } else if ($checkCalender == null) {
            $expired[] = "Kalender untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " belum / terdaftar";
        } else {
            for ($x = 0; $x < count($getCalendar); $x++) {
                $calId = $getCalendar[$x]['calendarId'];
                $checkRange = Yii::$app->db->CreateCommand("SELECT calendarId FROM pph_calendar WHERE calendarId = '$calId' AND '$date' between startDate and endDate")->queryAll();
                if ($checkRange != null) {
                    $status = true;
                    $expired = null;
                    break;
                } else {
                    $expired[] = "Masa berlaku Bukti Potong untuk Masa Pajak " . "<strong>" . $masaPajak . "</strong>" . " ditutup";
                }
            }
        }

        if ($expired != null) {
            $session['expiredDate'] = 1;
        } else {
            $session['expiredDate'] = 0;
        }

        $status = $session['expiredDate'];

        if (isset($session['saveFailed'])) {
            unset($session['saveFailed']);
        } elseif ($status == 1) {
            Yii::$app->session->setFlash('error', array_unique($expired));
            return $this->redirect(['/master-penghasilan/index']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->pegawaiId = $model->pegawaiId;
            if (strlen($bln) == 1) {
                $bln = '0' . $bln;
            }
            $model->masaPajak = $bln;
            $model->tahunPajak = $thn;
            $model->bruto = str_replace(".", "", $model->bruto);
            $model->pph = str_replace(".", "", $model->pph);

//            //kalkulasi jumlah Bruto dan PPh
//            $dataPegawai = MasterPegawai::find()->where(['pegawaiId' => $model->pegawaiId])->one();
////            echo '<pre>';
////            print_r($dataPegawai);
////            die();
////            $jumlahBruto = $dataPegawai['jumlahBruto'];
////            echo $jumlahBruto;
////            echo '<br>';
////            echo $model->bruto;
////            die();
//            $jumlahBruto = $dataPegawai['jumlahBruto'] + $model->bruto;
//            $jumlahPph = $dataPegawai['jumlahPph'] + $model->pph;
//            $commandInsert = "UPDATE master_pegawai SET jumlahBruto = '$jumlahBruto', jumlahPph = '$jumlahPph' WHERE pegawaiId = $model->pegawaiId";
//            $queryInsert = Yii::$app->db->createCommand($commandInsert)->execute();
            $model->kodePajak = '21-100-01';
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $model->created_at = $date->format('Y:m:d H:i:s');
            $created_at = $date->format('Y:m:d H:i:s');
            $model->created_by = $userId;
            $model->userId = $userId;
            $model->jenisPenghasilanId = $model->jenisPenghasilanId;
            $model->save();
            return $this->redirect(['view', 'id' => $model->penghasilanId]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterPenghasilan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->penghasilanId]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MasterPenghasilan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MasterPenghasilan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterPenghasilan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MasterPenghasilan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

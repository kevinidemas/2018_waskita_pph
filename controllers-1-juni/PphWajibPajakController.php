<?php

namespace app\controllers;

use Yii;
use app\models\PphWajibPajak;
use app\search\PphWajibPajakSearch;
use yii\widgets\ActiveForm;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PphWajibPajakController implements the CRUD actions for PphWajibPajak model.
 */
class PphWajibPajakController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all PphWajibPajak models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PphWajibPajakSearch();
        $userId = Yii::$app->user->id;
        $searchModel->approvalStatus = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 100;

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionDownloadNpwp($id) {
        $wp = PphWajibPajak::findOne($id);
        $session = Yii::$app->session;
        if($wp->buktiNpwp != null){
            $path = Yii::getAlias('@webroot/bukti/') . $wp->buktiNpwp;
        } else {        
            $notification = "File " . "<strong>" . "NPWP " . "</strong>" . "tidak ditemukan";
            $session['notification-not-found'] = $notification;
            return $this->redirect(['/pph-wajib-pajak/index-approval-wp']);
        }
        
        if (file_exists($path)) {           
            $notification = "File " . "<strong>" . "NPWP " . "</strong>" . "ditemukan";
            $session['notification-found'] = $notification;
            return Yii::$app->response->sendFile($path);
        } else {
            $notification = "File " . "<strong>" . "NPWP " . "</strong>" . "tidak ditemukan";
            $session['notification-not-found'] = $notification;
            return $this->redirect(['/pph-wajib-pajak/index-approval-wp']);
        }
    }
    
    public function actionDownloadSppkp($id) {
        $wp = PphWajibPajak::findOne($id);
        $session = Yii::$app->session;
         if($wp->buktiSppkp != null){
            $path = Yii::getAlias('@webroot/bukti/') . $wp->buktiSppkp;
        } else {        
            $notification = "File " . "<strong>" . "SPPKP " . "</strong>" . "tidak ditemukan";
            $session['notification-not-found'] = $notification;
            return $this->redirect(['/pph-wajib-pajak/index-approval-wp']);
        }    

        if (file_exists($path)) { 
            $notification = "File " . "<strong>" . "SPPKP " . "</strong>" . "ditemukan";
            $session['notification-found'] = $notification;           
            return Yii::$app->response->sendFile($path);
        } else {
            $notification = "File " . "<strong>" . "SPPKP " . "</strong>" . "tidak ditemukan";
            $session['notification-not-found'] = $notification;
            return $this->redirect(['/pph-wajib-pajak/index-approval-wp']);
        }
    }
    
    public function actionIndexApprovalWp() {
        $searchModel = new PphWajibPajakSearch();
        $userId = Yii::$app->user->id;
        $searchModel->approvalStatus = 2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index-approval-wp', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApproveWp($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $model->approvalStatus = 1;
        $model->save(false);
        
        return $this->redirect(['index-approval-wp']);        
    }
    
    public function actionRejectWp($id) {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index-approval-wp']);        
    }
    /**
     * Displays a single PphWajibPajak model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PphWajibPajak model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new PphWajibPajak();
        $model->scenario = 'create';
        $userId = Yii::$app->user->id;
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
           $model->buktiNpwp = UploadedFile::getInstance($model, 'buktiNpwp');
           $model->buktiSppkp = UploadedFile::getInstance($model, 'buktiSppkp');
           if ($model->buktiNpwp) {
                $imgIdNpwp = mt_rand(100000, 999999);
                $nameNpwp = 'NPWP' . $imgIdNpwp . '-' . $model->buktiNpwp->baseName . '.' . $model->buktiNpwp->extension;
                $pathNpwp = Yii::getAlias('@webroot/bukti/') . $nameNpwp;
                $model->buktiNpwp->saveAs($pathNpwp);
                $model->buktiNpwp = $nameNpwp;

                if($model->buktiSppkp){
                    $imgIdSppkp = mt_rand(100000, 999999);
                    $nameSppkp = 'SPPKP' . $imgIdSppkp . '-' . $model->buktiSppkp->baseName . '.' . $model->buktiSppkp->extension;
                    $pathSppkp = Yii::getAlias('@webroot/bukti/') . $nameSppkp;
                    $model->buktiSppkp->saveAs($pathSppkp);
                    $model->buktiSppkp = $nameSppkp;
                }
            }
           
            $model->npwp = preg_replace("/[^0-9]/", "", $model->npwp);
            $model->statusNpwpId = 1;
            $model->statusWpId = 1;
            $model->jenisWp = 2;
            $model->approvalStatus = 2;
            $model->user_id = $userId;
            $model->save();
            return $this->redirect(['view', 'id' => $model->wajibPajakId]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateModal() {
        $model = new PphWajibPajak();
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $statusNpwp = $model->npwp;
            $statusWp = $model->statusWpId;
            if ($statusNpwp == null) {
                $model->statusNpwpId = 2;
            } else {
                $model->statusNpwpId = 1;
            }
            if ($statusWp == 0) {
                $model->statusWpId = 1;
            }
            $model->user_id = $userId;
            $model->save();
            return $this->redirect(['/pph-bukti-potong/create',
                        'id' => $model->wajibPajakId,
            ]);
        } else {
            return $this->renderAjax('create-modal', [
                        'model' => $model,
            ]);
        }
    }

    public function actionSelectIujk() {
        $model = new PphWajibPajak();
        $session = Yii::$app->session;
        $id = $session['iujk-wpId'];
//        $modelUpdate = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $iujkId = $model->iujkId;
//            $modelUpdate->iujkId = $modelUpdate->iujkId;
//            $modelUpdate->save();
            $updateIujkQuery = "UPDATE pph_iujk SET wajibPajakId = '$id' WHERE iujkId = '$iujkId'";
            $updateIujkExec = Yii::$app->db->createCommand($updateIujkQuery)->execute();
            return $this->redirect(['/pph-iujk/index-iujk-wp',
                        'id' => $id,
            ]);
        } else {
            return $this->renderAjax('create-select', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateModal4() {
        $model = new PphWajibPajak();
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $statusNpwp = $model->npwp;
            $statusWp = $model->statusWpId;
            if ($statusNpwp == null) {
                $model->statusNpwpId = 2;
            } else {
                $model->statusNpwpId = 1;
            }
            if ($statusWp == 0) {
                $model->statusWpId = 1;
            }
            $model->jenisWp = 2;
            $model->iujkId = $model->iujkId;
            $model->user_id = $userId;
            $model->save();
            $wpId = $model->wajibPajakId;
            $updateIujkQuery = "UPDATE pph_iujk SET wajibPajakId = '$wpId' WHERE iujkId = '$model->iujkId'";
            $updateIujkExec = Yii::$app->db->createCommand($updateIujkQuery)->execute();
            return $this->redirect(['/pph-bukti-potong/create-pasal-4',
                        'id' => $model->wajibPajakId,
            ]);
        } else {
            return $this->renderAjax('create-modal-4', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateModal22() {
        $model = new PphWajibPajak();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $statusNpwp = $model->npwp;
            $statusWp = $model->statusWpId;
            if ($statusNpwp == null) {
                $model->statusNpwpId = 2;
            } else {
                $model->statusNpwpId = 1;
            }
            if ($statusWp == 0) {
                $model->statusWpId = 1;
            }
            $model->jenisWp = 2;
            $model->iujkId = $model->iujkId;
            $model->user_id = $userId;
            $model->save();
            $wpId = $model->wajibPajakId;
            $updateIujkQuery = "UPDATE pph_iujk SET wajibPajakId = '$wpId' WHERE iujkId = '$model->iujkId'";
            $session['status-wp-pasal-22'] = 1;
            $updateIujkExec = Yii::$app->db->createCommand($updateIujkQuery)->execute();
            return $this->redirect(['/pph-bukti-potong/create',
                        'id' => $model->wajibPajakId,
            ]);
        } else {
            return $this->renderAjax('create-modal-22', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateModal23Jasa() {
        $model = new PphWajibPajak();
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $statusNpwp = $model->npwp;
            $statusWp = $model->statusWpId;
            if ($statusNpwp == null) {
                $model->statusNpwpId = 2;
            } else {
                $model->statusNpwpId = 1;
            }
            if ($statusWp == 0) {
                $model->statusWpId = 1;
            }
            $model->approvalStatus = 2;
            $model->jenisWp = 2;
            $model->iujkId = $model->iujkId;
            $model->user_id = $userId;
            $model->save();
            $wpId = $model->wajibPajakId;
            $updateIujkQuery = "UPDATE pph_iujk SET wajibPajakId = '$wpId' WHERE iujkId = '$model->iujkId'";
            $updateIujkExec = Yii::$app->db->createCommand($updateIujkQuery)->execute();
            return $this->redirect(['/pph-bukti-potong/create-pasal-23-jasa',
                        'id' => $model->wajibPajakId,
            ]);
        } else {
            return $this->renderAjax('create-modal-23-jasa', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreateModal23Sewa() {
        $model = new PphWajibPajak();
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $statusNpwp = $model->npwp;
            $statusWp = $model->statusWpId;
            if ($statusNpwp == null) {
                $model->statusNpwpId = 2;
            } else {
                $model->statusNpwpId = 1;
            }
            if ($statusWp == 0) {
                $model->statusWpId = 1;
            }
            $model->jenisWp = 2;
            $model->iujkId = $model->iujkId;
            $model->user_id = $userId;
            $model->save();
            $wpId = $model->wajibPajakId;
            $updateIujkQuery = "UPDATE pph_iujk SET wajibPajakId = '$wpId' WHERE iujkId = '$model->iujkId'";
            $updateIujkExec = Yii::$app->db->createCommand($updateIujkQuery)->execute();
            return $this->redirect(['/pph-bukti-potong/create-pasal-23-sewa',
                        'id' => $model->wajibPajakId,
            ]);
        } else {
            return $this->renderAjax('create-modal-23-sewa', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PphWajibPajak model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $id])->one();
        $user_id = $modelWp->user_id;
        if (Yii::$app->user->id == $user_id || Yii::$app->user->id == 1) {
            $preNpwp = $model->buktiNpwp;
            $preSppkp = $model->buktiSppkp;
            if ($model->load(Yii::$app->request->post())) {
                $model->buktiNpwp = UploadedFile::getInstance($model, 'buktiNpwp');
                $model->buktiSppkp = UploadedFile::getInstance($model, 'buktiSppkp');
                if ($model->buktiNpwp && $model->buktiSppkp) {
                    $imgIdNpwp = mt_rand(100000, 999999);
                    $nameNpwp = 'NPWP' . $imgIdNpwp . '-' . $model->buktiNpwp->baseName . '.' . $model->buktiNpwp->extension;
                    $pathNpwp = Yii::getAlias('@webroot/bukti/') . $nameNpwp;
                    $model->buktiNpwp->saveAs($pathNpwp);
                    $model->buktiNpwp = $nameNpwp;
                    $imgIdSppkp = mt_rand(100000, 999999);
                    $nameSppkp = 'SPPKP' . $imgIdSppkp . '-' . $model->buktiSppkp->baseName . '.' . $model->buktiSppkp->extension;
                    $pathSppkp = Yii::getAlias('@webroot/bukti/') . $nameSppkp;
                    $model->buktiSppkp->saveAs($pathSppkp);
                    $model->buktiSppkp = $nameSppkp;
                    $model->update($id);
                } else if ($model->buktiNpwp == null && $model->buktiSppkp != null){
                    $imgIdSppkp = mt_rand(100000, 999999);
                    $nameSppkp = 'SPPKP' . $imgIdSppkp . '-' . $model->buktiSppkp->baseName . '.' . $model->buktiSppkp->extension;
                    $pathSppkp = Yii::getAlias('@webroot/bukti/') . $nameSppkp;
                    $model->buktiSppkp->saveAs($pathSppkp);
                    $model->buktiSppkp = $nameSppkp;
                    $model->update($id);
                } else if ($model->buktiNpwp != null && $model->buktiSppkp == null){
                    $imgIdNpwp = mt_rand(100000, 999999);
                    $nameNpwp = 'NPWP' . $imgIdNpwp . '-' . $model->buktiNpwp->baseName . '.' . $model->buktiNpwp->extension;
                    $pathNpwp = Yii::getAlias('@webroot/bukti/') . $nameNpwp;
                    $model->buktiNpwp->saveAs($pathNpwp);
                    $model->buktiNpwp = $nameNpwp;
                    $model->update($id);
                } else {
                    $model->buktiNpwp = $preNpwp;
                    $model->buktiSppkp = $preSppkp;
                    $model->update($id);
                }
                
                $updateWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET jenisWp = '2' WHERE wajibPajakId = '$id'")->execute();
                return $this->redirect(['view', 'id' => $model->wajibPajakId]);
            } else {                
                return $this->render('update', [                   
                            'model' => $model,
//                            'id' => $id
                ]);
            }
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Deletes an existing PphWajibPajak model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;  
        if (Yii::$app->user->id == 1) {
            
            $modelBp = PphBuktiPotong::find()->where(['wajibPajakId' => $id])->asArray()->one();
            $modelIujk = PphIujk::find()->where(['wajibPajakId' => $id])->asArray()->one();
            if($modelBp == NULL && $modelIujk == NULL){
                $this->findModel($id)->delete();
            } else if($modelBp != NULL && $modelIujk == NULL){
                Yii::$app->session->setFlash('error', "Wajib Pajak tidak berhasil dihapus karna memiliki"."<strong>"." Bukti Potong "."</strong>"."yang terdaftar");
                return Yii::$app->response->redirect(['/pph-wajib-pajak/index']);
            } else if($modelBp == NULL && $modelIujk != NULL){
                Yii::$app->session->setFlash('error', "Wajib Pajak tidak berhasil dihapus karna memiliki"."<strong>"." IUJK "."</strong>"."yang terdaftar");
                return Yii::$app->response->redirect(['/pph-wajib-pajak/index']);
            } else {
                Yii::$app->session->setFlash('error', "Wajib Pajak tidak berhasil dihapus karna memiliki"."<strong>"." Bukti Potong dan IUJK "."</strong>"."yang terdaftar");
                return Yii::$app->response->redirect(['/pph-wajib-pajak/index']);
            }
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Finds the PphWajibPajak model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PphWajibPajak the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PphWajibPajak::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

<?php

namespace app\controllers;

use Yii;
use app\models\MasterHonorer;
use app\search\MasterHonorerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PphJenisKelamin;
use yii\filters\AccessControl;
use yii\web\Response;
use DateTime;

/**
 * MasterHonorerController implements the CRUD actions for MasterHonorer model.
 */
class MasterHonorerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MasterHonorer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MasterHonorerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MasterHonorer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MasterHonorer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MasterHonorer();
        $connection = \Yii::$app->db;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
//            echo '1';
            $model->nama = $model->nama;
            $nama = $model->nama;
            $model->posisi = $model->posisi;
            $posisi = $model->posisi;
            $model->nik = $model->nik;
            $nik = $model->nik;
            $model->npwp = preg_replace("/[^0-9]/", "", $model->npwp);
            if ($model->npwp == NULL) {
                $model->npwp = NULL;
            }
            $npwp = preg_replace("/[^0-9]/", "", $model->npwp);    
            $model->alamat = $model->alamat;
            $alamat = $model->alamat;
            $model->jenisKelamin = $model->jenisKelamin;
            $jenisKelamin = $model->jenisKelamin;
            $model->statusKerjaId = 3;
//            $model->approvalStatus = 2;
            $model->userId = $userId;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $model->created_at = $date->format('Y:m:d H:i:s');
            $created_at = $date->format('Y:m:d H:i:s');
            $model->created_by = $userId;
            $checkIfExist = $connection->createCommand("SELECT honorerId FROM master_honorer WHERE nik = '$model->nik' AND nama = '$model->nama'")->queryAll();
            $arrayHonorerId = array_column($checkIfExist, 'pegawaiId');            
            if ($arrayHonorerId != NULL) {
//                die('A');
                $model = $this->findModel($arrayHonorerId[0]);
                $model->nama = $nama;
                $model->posisi = $posisi;
                $model->nik = $nik;
                $model->npwp = $npwp;
                $model->alamat = $alamat;
                $model->jenisKelamin = $jenisKelamin;
                $model->userId = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $model->updated_at = $date->format('Y:m:d H:i:s');
                $model->updated_by = $userId;
                $model->save(false);
            } else {
//                die('B');
//                echo '<pre>';
//                print_r($model);
//                die();
                $model->save(false);
            }
            return $this->redirect(['view', 'id' => $model->honorerId]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MasterHonorer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $created_by = $model->created_by;
        if ($created_by == Yii::$app->user->id || Yii::$app->user->id == 1) {
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->honorerId]);
        }
            return $this->render('update', [
                'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        } 
    }

    /**
     * Deletes an existing MasterHonorer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if (Yii::$app->user->id == 1) {
            $this->findModel($id)->delete();        
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }  
    }

    /**
     * Finds the MasterHonorer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MasterHonorer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MasterHonorer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

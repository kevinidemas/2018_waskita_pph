<?php

namespace app\controllers;

use Yii;
use app\models\PphCalendar;
use app\search\PphCalendarSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use DateTime;
use yii\filters\AccessControl;

/**
 * PphCalendarController implements the CRUD actions for PphCalendar model.
 */
class PphCalendarController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                        [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all PphCalendar models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PphCalendarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PphCalendar model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        $userId = Yii::$app->user->id;
        return $this->render('view', [
//                    'model' => $this->findModel($userId),
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PphCalendar model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
//        $rolesName = Yii::$app->rbaccomponent->myRole();
//        $rulesName = Yii::$app->rbaccomponent->ableTo('create_calendar');
//        if ($rulesName == true) {
        $connection = \Yii::$app->db;
        $model = new PphCalendar();
        $session = Yii::$app->session;
        $userId = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $start = $_POST['startDate'];
            $end = $_POST['endDate'];
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $created_at = $date->format('Y:m:d H:i:s');
            $model->startDate = $start;
            $model->endDate = $end;
            $startMonth = date("n", strtotime($model->startDate));
            $endMonth = date("n", strtotime($model->endDate));
            if ($startMonth != $endMonth) {
                $notification = "Tanggal mulai berlaku " . "dan tanggal berakhir " . "<strong>" ."harus berada pada masa bulan yang sama" . "</strong>";
                Yii::$app->session->setFlash('error', $notification);
                return $this->redirect(['create']);
            }

            $masa = \Yii::t('app', $startMonth);
            $model->calendarNote = $masa;
            $model->created_by = $userId;
            $model->created_at = $created_at;
            $model->status = 1;
            $model->is_permanent_close = 0;
            $model->save(false);
//            }
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
//        } else {
//            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
//        }
    }

    public function actionActive($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $rolesName = Yii::$app->rbaccomponent->myRole();
        $rulesName = Yii::$app->rbaccomponent->ableTo('change_calendar_status');
        if ($rulesName == true) {
            if ($model->status == 1 && $model->is_permanent_close == 0) {
                $model->status = 0;
            } else if ($model->status == 0 && $model->is_permanent_close == 0) {
                $model->status = 1;
            } else {
                $error = "<strong>" . 'GAGAL ' . "</strong>" . "dikarenakan Kalendar telah di tutup secara permanent";
                Yii::$app->session->setFlash('error', $error);
            }
            $model->save();
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionClose($id) {
        $model = $this->findModel($id);
        if ($model->is_permanent_close == 0) {
            if ($model->status == 0) {
                $startDate = $model->startDate;
                $endDate = $model->endDate;
                $updateBp = Yii::$app->db->CreateCommand("UPDATE pph_bukti_potong SET is_calendar_close = '1' WHERE tanggal BETWEEN '$startDate' AND '$endDate'")->execute();
                $model->is_permanent_close = 1;
                $model->save();
            } else {
                $error = "<strong>" . 'GAGAL ' . "</strong>" . "dikarenakan Kalendar dalam status Aktif";
                Yii::$app->session->setFlash('error', $error);
            }
        } else {
            $error = "<strong>" . 'GAGAL ' . "</strong>" . "dikarenakan Kalendar telah dalam status Permanent Closed";
            Yii::$app->session->setFlash('error', $error);
        }
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing PphCalendar model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post())) {
            $start = $_POST['startDate'];
            $end = $_POST['endDate'];
            $start = date('Y-m-d', strtotime($start));
            $end = date('Y-m-d', strtotime($end));
            $model->startDate = $start;
            $model->endDate = $end;
            $model->save();
            return $this->redirect(['view', 'id' => $id]);
        } else {
            $modelCalendar = PphCalendar::find()->where(['calendarId' => $id])->one();
            $startDate = date('d-m-Y', strtotime($modelCalendar->startDate));
            $endDate = date('d-m-Y', strtotime($modelCalendar->endDate));
            $session['updateCalendar'] = true;
            return $this->render('update', [
                        'model' => $model,
                        'endDate' => $endDate,
                        'startDate' => $startDate,
            ]);
        }
    }

    /**
     * Deletes an existing PphCalendar model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $rolesName = Yii::$app->rbaccomponent->myRole();
        $rulesName = Yii::$app->rbaccomponent->ableTo('remove_calendar');
        if ($rulesName == true) {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Finds the PphCalendar model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PphCalendar the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PphCalendar::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}

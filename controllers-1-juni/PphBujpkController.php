<?php

namespace app\controllers;

use Yii;
use app\models\PphBujpk;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\search\PphBujpkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use DateTime;
use yii\data\ActiveDataProvider;

/**
 * PphBujpkController implements the CRUD actions for PphBujpk model.
 */
class PphBujpkController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'update', 'index', 'view'],
                'rules' => [
                    // deny all POST requests
//                    [
//                        'allow' => false,
//                        'verbs' => ['POST']
//                    ],
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Lists all PphBujpk models.
     * @return mixed
     */
    public function check_in_range($start_date, $end_date, $today) {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $today_ts = strtotime($today);

        // Check that user date is between start & end
        return (($today_ts >= $start_ts) && ($today_ts <= $end_ts));
    }

    public function actionIndex() {
        $searchModel = new PphBujpkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataProvider->pagination->pageSize = 100;
        $dataProvider = new ActiveDataProvider([
            'query' => PphBujpk::find()->
                    where(['is_expired' => 0]),
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        $dataProvider->setSort([
            'defaultOrder' => ['is_expired_warning' => SORT_DESC]
        ]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexBujpkWp($id) {
        $searchModel = new PphBujpkSearch();
        $searchModel->wajibPajakId = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-bujpk-wp', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PphBujpk model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionApprove($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $wpId = $model->wajibPajakId;
        if ($model->is_approve != 1) {
            $arrayBp = Yii::$app->db->CreateCommand("SELECT buktiPotongId, jumlahBruto FROM pph_bukti_potong WHERE wajibPajakId = '$wpId' AND is_calendar_close = '0' AND pasalId = '4' AND statusWpId = '4'")->queryAll();
            $arrayBpID = array_column($arrayBp, 'buktiPotongId');
            $tarif = null;
            for ($n = 0; $n < count($arrayBpID); $n++) {
                $bpId = $arrayBpID[$n];
                $is_approve = $model->is_approve;
                $tingkatKeuangan = $model->tingkatKeuangan;
                if ($is_approve != 1) {
                    if ($tingkatKeuangan == 1) {
                        $tarif = 2;
                    } else {
                        $tarif = 3;
                    }
                }
                $jumlahBruto = $arrayBp[$n]['jumlahBruto'];
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $updateBp = Yii::$app->db->CreateCommand("UPDATE pph_bukti_potong SET jumlahPphDiPotong = '$jumlahPphDiPotong' WHERE buktiPotongId = '$bpId'")->execute();
            }
            $arrayBujpkAktif = Yii::$app->db->CreateCommand("SELECT bujpkId FROM pph_bujpk WHERE wajibPajakId = '$wpId' AND is_expired = '0' AND is_active = '1' AND is_approve = '1'")->queryAll();
//            echo '<pre>';
//            print_r($arrayBujpkAktif);
//            die();
            if ($arrayBujpkAktif == null) {
                $start = strtotime($model->published_at);
//                $start = $model->published_at;
                $end = strtotime($model->expired_at);
//                $end = $model->expired_at;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $today = strtotime($date->format('Y-m-d'));
//                $today = $date->format('Y-m-d');
                if ($today >= $start && $today <= $end) {
                    $model->is_active = 1;
                } else {
                    $model->is_active = 0;
                }
            } else {
                $model->is_active = 0;
            }
            $model->is_approve = 1;
            $model->save(false);
            $updateWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET bujpkId = '$id' WHERE wajibPajakId = '$wpId'")->execute();
        } else {
            $error = "<strong>" . 'GAGAL ' . "</strong>" . "dikarenakan BUJPK sudah dalam status diterima";
            Yii::$app->session->setFlash('error', $error);
        }
        return $this->redirect(['index']);
    }

    public function actionReject($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $wpId = $model->wajibPajakId;
        if ($model->is_approve != 0) {
            $getBujpk = Yii::$app->db->CreateCommand("SELECT bujpkId FROM pph_bujpk WHERE wajibPajakId = '$wpId' AND is_approve = '1' AND is_expired = 0")->queryAll();
            $bujpkArray = array_column($getBujpk, 'bujpkId');
            if ($bujpkArray != NULL) {
                if ($bujpkArray[0] == $id) {
                    $bujpkId = 'NULL';
                } else {
                    $bujpkId = $bujpkArray[0];
                }
            } else {
                $bujpkId = $id;
            }

            $arrayBp = Yii::$app->db->CreateCommand("SELECT buktiPotongId, jumlahBruto FROM pph_bukti_potong WHERE wajibPajakId = '$wpId' AND is_calendar_close = '0' AND pasalId = '4' AND statusWpId = '3'")->queryAll();
            $arrayBpID = array_column($arrayBp, 'buktiPotongId');
            for ($n = 0; $n < count($arrayBpID); $n++) {
                $bpId = $arrayBpID[$n];
                $tarif = 4;
                $jumlahBruto = $arrayBp[$n]['jumlahBruto'];
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $updateBp = Yii::$app->db->CreateCommand("UPDATE pph_bukti_potong SET jumlahPphDiPotong = '$jumlahPphDiPotong' WHERE buktiPotongId = '$bpId'")->execute();
            }
            $updateBujpk = Yii::$app->db->CreateCommand("UPDATE pph_bujpk SET is_active = '1' WHERE bujpkId = '$bujpkId' AND wajibPajakId = '$wpId'")->execute();
            $updateWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET bujpkId = $bujpkId WHERE wajibPajakId = '$wpId'")->execute();
            $model->is_approve = 0;
            $model->is_active = 0;
            $model->save(false);
        } else {
            $error = "<strong>" . 'GAGAL ' . "</strong>" . "dikarenakan BUJPK sudah dalam status ditolak";
            Yii::$app->session->setFlash('error', $error);
        }

        return $this->redirect(['index']);
    }

    public function actionActive($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $wpId = $model->wajibPajakId;
        if ($model->is_active == 1) {
            $updateWpQuery = "UPDATE pph_wajib_pajak SET bujpkId = NULL WHERE wajibPajakId = '$wpId'";
            $updateWpExec = Yii::$app->db->createCommand($updateWpQuery)->execute();
            $arrayBp = Yii::$app->db->CreateCommand("SELECT buktiPotongId, jumlahBruto FROM pph_bukti_potong WHERE wajibPajakId = '$wpId'")->queryAll();
            $arrayBpID = array_column($arrayBp, 'buktiPotongId');
            for ($n = 0; $n < count($arrayBpID); $n++) {
                $bpId = $arrayBpID[$n];
                $jumlahBruto = $arrayBp[$n]['jumlahBruto'];
                $jumlahPphDiPotong = ($jumlahBruto * 4) / 100;
                $updateBpQuery = "UPDATE pph_bukti_potong SET jumlahPphDiPotong = '$jumlahPphDiPotong' WHERE buktiPotongId = '$bpId'";
                $updateBpExec = Yii::$app->db->createCommand($updateBpQuery)->execute();
            }
            $model->is_active = 0;
        } else {
            $updateBujpkQuery = "UPDATE pph_bujpk SET is_active = '0' WHERE is_active = '1' AND wajibPajakId = '$wpId'";
            $updateBujpkExec = Yii::$app->db->createCommand($updateBujpkQuery)->execute();
            $updateWpQuery = "UPDATE pph_wajib_pajak SET bujpkId = '$id' WHERE wajibPajakId = '$wpId'";
            $updateWpExec = Yii::$app->db->createCommand($updateWpQuery)->execute();
            $arrayBp = Yii::$app->db->CreateCommand("SELECT buktiPotongId, jumlahBruto FROM pph_bukti_potong WHERE wajibPajakId = '$wpId'")->queryAll();
            $arrayBpID = array_column($arrayBp, 'buktiPotongId');
            if ($model->is_approve == 1) {
                $tarif = 2;
            } else {
                $tarif = 4;
            }
            for ($n = 0; $n < count($arrayBpID); $n++) {
                $bpId = $arrayBpID[$n];
                $jumlahBruto = $arrayBp[$n]['jumlahBruto'];
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $updateBpQuery = "UPDATE pph_bukti_potong SET jumlahPphDiPotong = '$jumlahPphDiPotong' WHERE buktiPotongId = '$bpId'";
                $updateBpExec = Yii::$app->db->createCommand($updateBpQuery)->execute();
            }
            $model->is_active = 1;
        }
        $model->save(false);
        return $this->redirect(['index']);
    }

    public function actionActiveByWp($id) {
        $model = $this->findModel($id);
        $session = Yii::$app->session;
        $wpId = $model->wajibPajakId;
        if ($model->is_active == 1) {
            $updateWpQuery = "UPDATE pph_wajib_pajak SET bujpkId = NULL WHERE wajibPajakId = '$wpId'";
            $updateWpExec = Yii::$app->db->createCommand($updateWpQuery)->execute();
            $arrayBp = Yii::$app->db->CreateCommand("SELECT buktiPotongId, jumlahBruto FROM pph_bukti_potong WHERE wajibPajakId = '$wpId'")->queryAll();
            $arrayBpID = array_column($arrayBp, 'buktiPotongId');
            for ($n = 0; $n < count($arrayBpID); $n++) {
                $bpId = $arrayBpID[$n];
                $jumlahBruto = $arrayBp[$n]['jumlahBruto'];
                $jumlahPphDiPotong = ($jumlahBruto * 4) / 100;
                $updateBpQuery = "UPDATE pph_bukti_potong SET jumlahPphDiPotong = '$jumlahPphDiPotong' WHERE buktiPotongId = '$bpId'";
                $updateBpExec = Yii::$app->db->createCommand($updateBpQuery)->execute();
            }
            $model->is_active = 0;
        } else {
            $updateBujpkQuery = "UPDATE pph_bujpk SET is_active = '0' WHERE is_active = '1' AND wajibPajakId = '$wpId'";
            $updateBujpkExec = Yii::$app->db->createCommand($updateBujpkQuery)->execute();
            $updateWpQuery = "UPDATE pph_wajib_pajak SET bujpkId = '$id' WHERE wajibPajakId = '$wpId'";
            $updateWpExec = Yii::$app->db->createCommand($updateWpQuery)->execute();
            $arrayBp = Yii::$app->db->CreateCommand("SELECT buktiPotongId, jumlahBruto FROM pph_bukti_potong WHERE wajibPajakId = '$wpId'")->queryAll();
            $arrayBpID = array_column($arrayBp, 'buktiPotongId');
            if ($model->is_approve == 1) {
                $tarif = 2;
            } else {
                $tarif = 4;
            }
            for ($n = 0; $n < count($arrayBpID); $n++) {
                $bpId = $arrayBpID[$n];
                $jumlahBruto = $arrayBp[$n]['jumlahBruto'];
                $jumlahPphDiPotong = ($jumlahBruto * $tarif) / 100;
                $updateBpQuery = "UPDATE pph_bukti_potong SET jumlahPphDiPotong = '$jumlahPphDiPotong' WHERE buktiPotongId = '$bpId'";
                $updateBpExec = Yii::$app->db->createCommand($updateBpQuery)->execute();
            }
            $model->is_active = 1;
        }
        $model->save(false);
        return $this->redirect(['index-bujpk-wp', 'id' => $model->wajibPajakId]);
    }

    /**
     * Creates a new PphBujpk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new PphBujpk();

        if ($model->load(Yii::$app->request->post())) {
            $model->bukti = UploadedFile::getInstance($model, 'bukti');
            $userId = Yii::$app->user->id;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $created_at = $date->format('Y:m:d H:i:s');
            if ($model->bukti) {
                $imgId = mt_rand(100000, 999999);
                $name = 'BUJPK' . $imgId . '-' . $model->bukti->baseName . '.' . $model->bukti->extension;
                $path = Yii::getAlias('@webroot/bukti/') . $name;
                $model->bukti->saveAs($path);
                $model->bukti = $name;
            }
            $model->kemampuanKeuangan = str_replace(".", "", $model->kemampuanKeuangan);
            $model->nomorBujpk = $model->nomorBujpk;
            $model->is_active = 0;
            $model->is_approve = 0;
            $model->penanggungJawab = $model->penanggungJawab;
            $model->created_at = $created_at;
            $model->created_by = $userId;
            $model->expired_at = $model->expired_at;
            $model->keterangan = $model->keterangan;

            $model->save(false);
            return $this->redirect(['view', 'id' => $model->bujpkId]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionCreateByWp($id) {
        $model = new PphBujpk();
        $session = Yii::$app->session;
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->bukti = UploadedFile::getInstance($model, 'bukti');
            $userId = Yii::$app->user->id;

            //cek apakah pada masa tersebut sudah ada bujpk untuk WP tersebut
            $checkWpBujpk = Yii::$app->db->CreateCommand("SELECT bujpkId FROM pph_bujpk WHERE wajibPajakId = '$id'")->execute();
            $statusPassBujpk = null;

//            for ($nbujpk = 0; $nbujpk < count($checkWpBujpk); $nbujpk++) {
            $bujpkId = $checkWpBujpk;
            if ($bujpkId != 0) {
                $modelCheckBujpk = PphBujpk::find()->where(['bujpkId' => $bujpkId])->one();
                $a = $modelCheckBujpk->published_at;
                $b = $modelCheckBujpk->expired_at;
                $c = $model->published_at;
                $d = $model->expired_at;

                if (strtotime($a) <= strtotime($c) && strtotime($c) <= strtotime($b)) {
                    $statusPassBujpk = 1;
                } else if (strtotime($a) <= strtotime($d) && strtotime($d) <= strtotime($b)) {
                    $statusPassBujpk = 1;
                } else if (strtotime($c) < strtotime($a) && strtotime($d) > strtotime($b)) {
                    $statusPassBujpk = 1;
                }
            }

            if ($statusPassBujpk == 1) {
                $error = "<strong>" . "BUJPK" . "</strong>" . " untuk masa tersebut telah terdaftar.";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['/pph-bujpk/index-bujpk-wp', 'id' => $id]);
            }
//            }

            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $created_at = $date->format('Y:m:d H:i:s');
            if ($model->bukti) {
                $imgId = mt_rand(100000, 999999);
                $name = 'BUJPK' . $imgId . '-' . $model->bukti->baseName . '.' . $model->bukti->extension;
                $path = Yii::getAlias('@webroot/bukti/') . $name;
                $model->bukti->saveAs($path);
                $model->bukti = $name;
            } else {
                $arrayData = [];
                $arrayData[0] = $model->nomorBujpk;
                $arrayData[1] = $model->kemampuanKeuangan;
                $arrayData[2] = $model->penanggungJawab;
                $arrayData[3] = $model->published_at;
                $arrayData[4] = $model->expired_at;
                $arrayData = implode(',', $arrayData);
                $arrayData = json_encode($arrayData);                
                $session['dataGagalBujpk'] = $arrayData;
                $error = "Anda wajib menyertakan " . "<strong>" . "Bukti" . "</strong>" . " berupa berkas BUJPK.";
                Yii::$app->session->setFlash('error', $error);
                return $this->redirect(['create-by-wp', 'id' => $id]);
            }

            //pengecekan BUJPK, apakah ada BUJPK yang aktif, sudah diapprove, dan masih berlaku
            $cekBujpkExist = Yii::$app->db->CreateCommand("SELECT COUNT(bujpkId) FROM pph_bujpk WHERE wajibPajakId = $id AND is_active = 1")->execute();
            if ($cekBujpkExist != 0) {
                //jika wp telah memiliki BUJPK, cek apakah aktif, sudah di approve dan belum expired
                $modelBujpkSatu = Yii::$app->db->CreateCommand("SELECT bujpkId FROM pph_bujpk WHERE wajibPajakId = '$id' AND is_active = '1' AND is_expired = '0'")->execute();
                if ($modelBujpkSatu != 0) {
                    $modelBujpkDua = Yii::$app->db->CreateCommand("SELECT bujpkId FROM pph_bujpk WHERE wajibPajakId = '$id' AND is_active = '1' AND is_approve = 0 AND is_expired = '0'")->execute();
                    if ($modelBujpkDua == 1) {
                        $statusActive = 1;
                    } else {
                        $statusActive = 0;
                    }
                } else {
                    $statusActive = 1;
                }
            } else {
                $statusActive = 1;
            }

            if ($statusActive == 1) {
                $updateBujpkActive = Yii::$app->db->CreateCommand("UPDATE pph_bujpk SET is_active = 0 WHERE wajibPajakId = $id")->execute();
            }

            //mengukur tingkat keuangan (kecil, menengah/besar)
            $keuanganWp = str_replace(".", "", $model->kemampuanKeuangan);
            if ($keuanganWp < 500000000) {
                $model->tingkatKeuangan = 1;
            } else if ($keuanganWp >= 500000000) {
                $model->tingkatKeuangan = 2;
            } else if ($keuanganWp >= 10000000000) {
                $model->tingkatKeuangan = 3;
            }

            //cek tanggal masih pada masa berlaku atau tidak
            $startDate = $model->published_at;
            $endDate = $model->expired_at;
            $today = date('d-m-Y');
            $isInRange = PphBujpkController::check_in_range($startDate, $endDate, $today);
            if ($isInRange != NULL) {
                //tanggal hari ini masih berada di range masa berlaku BUJPK
                $statusExpired = 0;
            } else {
                //tanggal hari ini diluar range masa berlaku BUJPK
                if (strtotime($today) < strtotime($startDate)) {
                    //range masa berlaku masih diwaktu akan datang
                    $statusExpired = 0;
                } else {
                    //range masa berlaku telah lewat
                    $statusExpired = 1;
                }
            }

            $model->kemampuanKeuangan = str_replace(".", "", $model->kemampuanKeuangan);
            $model->nomorBujpk = $model->nomorBujpk;
            $model->wajibPajakId = $id;
            $model->is_active = $statusActive;
            $model->is_approve = 2;
            $model->is_expired = $statusExpired;
            $model->penanggungJawab = $model->penanggungJawab;
            $model->created_at = $created_at;
            $model->created_by = $userId;
            $model->published_at = $model->published_at;
            $model->expired_at = $model->expired_at;
            $model->keterangan = $model->keterangan;

            $model->save(false);
            $bujpkId = Yii::$app->db->getLastInsertID();
            if ($statusActive == 1) {
                $updateBujpkWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET bujpkId = '$bujpkId' WHERE wajibPajakId = $id")->execute();
            }
            return $this->redirect(['view', 'id' => $model->bujpkId]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    public function actionCreateModal() {
        $model = new PphBujpk();
        $session = Yii::$app->session;
        if ($model->load(Yii::$app->request->post())) {
            $model->bukti = UploadedFile::getInstance($model, 'bukti');
            $userId = Yii::$app->user->id;
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $created_at = $date->format('Y:m:d H:i:s');

            if ($model->bukti) {
                $imgId = mt_rand(100000, 999999);
                $name = $imgId . '-' . $model->bukti->baseName . '.' . $model->bukti->extension;
                $path = Yii::getAlias('@webroot/bukti/') . $name;
                $model->bukti->saveAs($path);
                $model->bukti = $name;
            }
            $keuanganWp = str_replace(".", "", $model->kemampuanKeuangan);
            if ($keuanganWp < 500000000) {
                $model->tingkatKeuangan = 1;
            } else if ($keuanganWp >= 500000000) {
                $model->tingkatKeuangan = 2;
            } else if ($keuanganWp >= 10000000000) {
                $model->tingkatKeuangan = 3;
            }
            $startDate = $model->published_at;
            $endDate = $model->expired_at;
            $today = date('d-m-Y');
            $isInRange = PphBujpkController::check_in_range($startDate, $endDate, $today);
            if ($isInRange != NULL) {
                //tanggal hari ini masih berada di range masa berlaku BUJPK
                $statusExpired = 0;
            } else {
                //tanggal hari ini diluar range masa berlaku BUJPK
                if (strtotime($today) < strtotime($startDate)) {
                    //range masa berlaku masih diwaktu akan datang
                    $statusExpired = 0;
                } else {
                    //range masa berlaku telah lewat
                    $statusExpired = 1;
                }
            }
            $model->kemampuanKeuangan = str_replace(".", "", $model->kemampuanKeuangan);
            $model->nomorBujpk = $model->nomorBujpk;
            $model->penanggungJawab = $model->penanggungJawab;
            $model->created_at = $created_at;
            $model->created_by = $userId;
            $model->expired_at = $model->expired_at;
            $model->published_at = $model->published_at;
            $model->keterangan = $model->keterangan;
            $model->is_active = 0;
            $model->is_approve = 2;
            $model->is_expired = $statusExpired;

//            echo '<pre>';
//            print_r($model);
//            die();
            $model->save(false);
            return $this->redirect(['/pph-wajib-pajak/create',
                        'id' => $model->bujpkId,
            ]);
        }

        return $this->renderAjax('create-modal', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing PphBujpk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelBujpk = PphBujpk::find()->where(['bujpkId' => $id])->one();
        $user_id = $modelBujpk->created_by;

        if (Yii::$app->user->id == $user_id || Yii::$app->user->id == 1) {
            $userId = Yii::$app->user->id;

            if ($model->load(Yii::$app->request->post())) {
                $model->updated_by = $userId;
                $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
                $model->updated_at = $date->format('Y:m:d H:i:s');
                $model->bukti = UploadedFile::getInstance($model, 'bukti');
                if ($model->bukti) {
                    $imgId = mt_rand(100000, 999999);
                    $name = $imgId . '-' . $model->bukti->baseName . '.' . $model->bukti->extension;
                    $path = Yii::getAlias('@webroot/bukti/') . $name;
                    $model->bukti->saveAs($path);
                    $model->bukti = $name;
                }
                $model->kemampuanKeuangan = str_replace(".", "", $model->kemampuanKeuangan);
                $model->nomorBujpk = $model->nomorBujpk;
                $model->wajibPajakId = $model->wajibPajakId;
                $model->penanggungJawab = $model->penanggungJawab;
                $model->published_at = date("Y-m-d", strtotime($model->published_at));
                $model->expired_at = date("Y-m-d", strtotime($model->expired_at));
                $model->keterangan = $model->keterangan;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->bujpkId]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    /**
     * Deletes an existing PphBujpk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $modelBujpk = PphBujpk::find()->where(['bujpkId' => $id])->one();
        $user_id = $modelBujpk->created_by;

        if (Yii::$app->user->id == $user_id || Yii::$app->user->id == 1) {
            $modelBujpk = PphBujpk::find()->where(['bujpkId' => $id])->one();
            $wajibPajakId = $modelBujpk->wajibPajakId;
            $this->findModel($id)->delete();
            $date = new DateTime('now', new \DateTimeZone('Asia/Jakarta'));
            $today = $date->format('Y:m:d');
            $updateBujpk = Yii::$app->db->CreateCommand("UPDATE pph_bujpk SET is_active = 1 WHERE is_expired = 0 AND wajibPajakId = '$wajibPajakId' AND is_approve = '1' AND is_active = 0")->execute();
            if ($updateBujpk == 1) {
                $getBujpk = Yii::$app->db->CreateCommand("SELECT bujpkId FROM pph_bujpk WHERE wajibPajakId = '$wajibPajakId' AND is_active = 1")->queryAll();
                if ($getBujpk != NULL) {
                    $bujpkId = $getBujpk[0]['bujpkId'];
                    $updateWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET bujpkId = '$bujpkId' WHERE wajibPajakId = '$wajibPajakId'")->execute();
                } else {
                    $updateWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET bujpkId = NULL WHERE bujpkId = '$id'")->execute();
                }
            } else {
                $updateWp = Yii::$app->db->CreateCommand("UPDATE pph_wajib_pajak SET bujpkId = NULL WHERE bujpkId = '$id'")->execute();
            }
            return $this->redirect(['index']);
        } else {
            throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }
    }

    public function actionDownload($id) {
        $bujpk = PphBujpk::findOne($id);
        $path = Yii::getAlias('@webroot/bukti/') . $bujpk->bukti;
        if (file_exists($path)) {            
            return Yii::$app->response->sendFile($path);
        } else {
            $error = "File " . "<strong>" . "BUJPK " . "</strong>" . "tidak ditemukan";
            Yii::$app->session->setFlash('error', $error);
            return $this->redirect(['/pph-bujpk/index']);
        }
    }

    /**
     * Finds the PphBujpk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PphBujpk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = PphBujpk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}

<?php

//use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formatter = Yii::$app->formatter;
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];

function kekata($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = kekata($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x < 1000) {
        $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x < 1000000) {
        $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
    } else if ($x < 1000000000) {
        $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
    }
    return $temp;
}

function terbilang($x, $style = 4) {
    if ($x < 0) {
        $hasil = "minus " . trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];
$date = date('dmY');
$toDate = str_split($date, 1);
?>

<style>
    .header-notices{
        letter-spacing: 3px;
        position:fixed;
        top:-39;
        left: 21;
        margin: -3px 0px 0px -15px;  
        color: #d6d6c2;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;        
    }

    .header-f{
        width:4.9%;
        height:12px;
        background:#000;
        position:fixed;
        top:-21;
        left: 11;
        margin: -3px 0px 0px -15px;        
    }
    .header-s{
        width:2.6%;
        height:12px;
        background:#000;
        position:fixed;
        top:-21;
        left:662;
        margin: -3px 0px 0px 0px;
    }

    .footer-f{
        width:4.9%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:20;
        left: 9;
        margin: -52px 0px -52px -12px;
    }
    .footer-s{
        width:3%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:20;
        left:659;
        margin: -52px 0px -52px 0px;
    }

    .box-f{
        width:1.1%;
        height:0.8px;
        background:#000;
        position:fixed;
        top:-18;
        left:637;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .box-s{
        width:1.1%;
        height:0.8px;
        background:#fff;
        position:fixed;
        top:-18;
        left:648;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .box-t{
        width:1.1%;
        height:0.8px;
        background:#fff;
        position:fixed;
        top:-18;
        left:659;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .box-fo{
        width:1.1%;
        height:0.8px;
        background:#fff;
        position:fixed;
        top:-18;
        left:670;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .header{        
        position:fixed;
        width: 120px;
        /*top:-20;*/
        left: 425;
        font-family: fantasy;
        font-size: 8px;
        text-align: left;
        margin: -2px 0px 0px 64px;
    }
    .footer-m{
        font-weight: bold; 
        font-size: 9.7px;
        padding-bottom: -29px;
        position:fixed;
        bottom:-22;
        left:3;
        margin: 0px -50px 0px 2px;
    }

    .judul{
        width: 38%; 
        font-size: 10;
        font-weight: bold;
        padding: px 0px 0px 14px;
        border: 1.5px solid black;
        border-left-width: 1px;
    }

    .header-box{
        border-collapse: collapse;
        font-family: "Arial", Arial, sans-serif;
        margin-left: -4px; 
        margin-top: -50px;
        border-top-style: dotted;
        border-bottom-style: solid;
        border-width: 1.5px;
        padding: 4px 0px 4px 0px;

    }

    .logo-header-box{        
        width: 22%; 
        font-size: 10.8px;
        border-right-style: solid;
        border-width: 1.5px;
        padding: 6px 0px 0px 69px;
    }

    .judul-box{
        font-size: 12.7px;         
        height: 28px;
        padding: 0px 0px 0px 0px;
        border-right-style: solid;
        border-bottom-style: none;
        border-width: 1.5px;
    }

    .kode-form-header{
        width: 14%; 
        padding: 0px -160px 0px 0px;
        border-bottom-style: none;
        border-width: 1.5px;
        font-size: 13px;
    }
    
    .npwp-box {
        font-family: "Arial", Arial, sans-serif;
        border: 1.5px solid black; 
        font-size: 10px;
        height: 120%;
        margin: 0px 0px 0px -4px;
    }
    
    .npwp-box td{
        padding: 6px 0px 6px 4px;
    }

    .sub-a{
        font-family: "Arial", Arial, sans-serif;
        font-size: 11px;
        font-weight: bold;
        padding: 12px 0px 0px 0px;
    }

    .table-f{
        border-style: solid;
        border-width: 1.5px;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;
        margin: 4px 0px 0px -4px; 
        padding: 8px 4px 10px 4px;
    }

    .table-f td{
        height: 17px;
        padding: 2px 0px 2px 0px;
    }
    
    .table-s td{
        padding: 3px 0px 2px 0px;
    }
    
    .table-s{
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;
        margin: 4px 0px 0px -4px;  
        padding: 16px 0px 0px 6px;
        border-style: solid;
        border-width: 1.5px;
    }
    
    .table-t{
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;
        margin: 4px 0px 0px -4px;  
        padding: 16px 0px 0px 6px;
        border-style: solid;
        border-width: 1.5px;
    }
    
    .table-t td {
        font-size: 9px;
    }
    
    .table-fr{
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif;
        margin: 4px 0px 0px -4px;  
        padding: 16px 0px 0px 6px;
        border-style: solid;
        border-width: 1.5px;
    }
    
    .table-fr td {
        font-family: "Arial", Arial, sans-serif;         
        font-size: 9px;
    }

    tr.c-1 td.c-1-r-1{
        width: 11%;
    }

    tr.c-1 td.c-1-r-2{
        width: 1%;
    }

    tr.c-1 td.c-1-r-3{
        width: 3%;
    }

    tr.c-1 td.c-1-r-4{
        letter-spacing: 2px; 
        border-bottom-style: solid; 
        border-width: 0.5px;
        width: 32%;
        /*        background-color: red;*/
    }    

    tr.c-1 td.c-1-r-5{
        width: 22%;
    }

    tr.c-1 td.c-1-r-7{
        width: 3%;
    }

    tr.c-1 td.c-1-r-8{
        padding-left: 0px;
        width: 26%;
        letter-spacing: 2px;
    }

    tr.c-5 td.c-5-r-2{
        width: 18%;
    }

    tr.c-5 td.c-5-r-5{
        padding-left: -130px;
        width: 3%;
    }

    tr.c-5 td.c-5-r-6{

    }

    tr.c3-1 td.c3-1-r-1{
        width: 8%;
    }

    tr.c3-1 td.c3-1-r-2{
        width: 1%;
    }

    tr.c3-1 td.c3-1-r-3{
        width: 3%;
    }

    tr.c3-2 td.c3-2-r-5{
        width: 3%;
    }
    tr.c3-2 td.c3-2-r-6{
        width: 24%;
        font-size: 10px;
        text-decoration: underline;
    }

    .c3-1{
        font-size: 10px;   
    }

    .djp{
        padding: 4px 4px 3px 0px;
        font-size: 11px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-width: 1.5px;
        height: 40px;
    }
</style>

<?php
$masaSession = $session['masaPajak'];
$masaTahun = preg_replace('/[^0-9]/', '', $masaSession);
$masaBulan = $session['masaId'];
if (strlen($masaBulan) == 1) {
    $masaBulan = '0' . $masaBulan;
}
$arrayMasa = [];
$arrayMasa[0] = $masaBulan;
$arrayMasa[1] = $masaTahun;
?>

<html>
        <body>
            <div style="height: 25px"></div>
            <div class="header-f"></div>
            <div class="header-s"></div>
            <div class="box-f"></div>
            <div class="box-s"></div>
            <div class="box-t"></div>
            <div class="box-fo"></div>
            <table class="header-box">
            <tr>
                <td class="logo-header-box">
                            <?= Html::img("@web/img/djp.png", ['width' => '53px', 'height' => '55px']) ?>                        
                </td>                                                        
                <td class="judul-box" colspan="2">
                    <font align="left"><center><b>SURAT PEMBERITAHUAN (SPT) MASA</b></center>
                    <font align="left"><center><b>PAJAK PENGHASILAN</b></center>
                    <font align="left"><center><b>PASAL 21 DAN/ATAU PASAL 26</b></center>
                </td>                                                        
                <td class="kode-form-header">
                    <font><center><b>FORMULIR 1721</b></center>
                    <div class="header" style="padding: -4px 0px 0px 29px;"></div>
                </td>                                                                            
            </tr>
            <tr>                    
                <td class="djp">
                    <font align="left"><center><b>KEMENTERIAN KEUANGAN RI</b></center>
                    <font align="left"><center><b>DIREKTORAT JENDERAL PAJAK</b></center>
                </td>
                <td class="judul" colspan="2">
                    <font align="left"><center><b>Formulir ini digunakan untuk melaporkan</b></center>
                    <font align="left"><center><b>Pemotongan Pajak Penghasilan Pasal 21 dan/atau</b></center>
                    <font align="left"><center><b>Pasal 26</b></center>
                </td>    
                <td style="border-top-style: none; border-bottom-style: solid; border-bottom-width: 1.5px"></td>
            </tr>
            <tr>
                <td style="font-size: 9px; padding: 8px 0px 0px 12px; border-left-style: solid; border-right-style: solid; border-width: 1.5px"><b>MASA PAJAK</b></td>
                <td style="font-size: 9px; text-align: center; border-bottom-style: solid; border-width: 1.5px" colspan="2"><b>Bacalah petunjuk pengisian sebelum mengisi formulir ini</b></td>
                <td style="font-size: 8px; border-left-style: solid; border-right-style: none; border-width: 1.5px; padding-left: 12px; width: 100px"><b>JUMLAH LEMBAR SPT <br> TERMASUK LAMPIRAN:</b></td>
                <td style="padding-left: 4px; border: none; border-right-style: solid; border-top-style: solid; border-width: 1.5px; width: 66px"> __ &nbsp; __ </td>
            </tr>
            <tr>
                <td style="font-size: 10px; padding: 2px 0px 4px 17px; border-left-style: solid; border-right-style: solid; border-width: 1.5px">[mm - yyyy] &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <sub style="font-size: 6px">H. 01</sub>&nbsp;&nbsp; <u><?php echo $arrayMasa[0] ?></u> - &nbsp;&nbsp;&nbsp; <u><?php echo $arrayMasa[1] ?></u></td>
                <td colspan="2"><sub style="font-size: 6px">H.02 &nbsp;&nbsp;</sub>
                    <?php 
                        if($nilaiPembetulan != null){
                            echo Html::img("@web/img/uncheckbox-spt2-2.png", ['width' => '98px', 'height' => '13px',  'margin-top' => '10px', 'margin-left' => '6px']);                   
                        } else {
                            echo Html::img("@web/img/checkbox-spt2.png", ['width' => '130px', 'height' => '13px', 'margin-top' => '10px']);
                        }      
                    ?>
                    <sub style="font-size: 6px">H.03 &nbsp;&nbsp;</sub>
                    <?php
                        if($nilaiPembetulan != null){
                            echo Html::img("@web/img/checkbox-spt-pembetulan2-1.png", ['width' => '93px', 'height' => '18px']).$nilaiPembetulan;
                        } else {
                            echo Html::img("@web/img/checkbox-spt-pembetulan2.png", ['width' => '130px', 'height' => '13px', 'margin-top' => '10px']);
                        }
                    ?>
                    <sub style="font-size: 6px">H.04</sub>
                </td>
                <td style="font-size: 6px; border-left-style: solid; border-right-style: none; border-top-style: none; border-width: 1.5px; padding-left: 20px">(DIISI OLEH PETUGAS)</td>
                <td style="font-size: 6px; padding: 0px 0px 0px 6px; margin-bottom: 8px; border-top-style: none; border-left-style: none; border-right-style: solid; border-width: 1.5px; width: 66px">H.05 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; H.06</td>
            </tr>
    </table>

    <div class="sub-a">A. IDENTITAS PEMOTONG</div>

    <table class="table-f" height="100%" width="100%">
        <tr class="c-1">
            <td class="c-1-r-1">1. NPWP</td>
            <td class="c-1-r-2">:</td>
            <td class="c-1-r-3"><sub>A. 01</sub></td>            
            <td class="c-1-r-4" style="letter-spacing: 1.5px;" colspan="5">
                <?php
                if (isset($npwp[0])) {
                    echo substr($npwp[0], 0, 2) . ' . ' . substr($npwp[0], 2, 3) . '.' . substr($npwp[0], 5, 3) . ' . ' . substr($npwp[0], 8, 1) . ' - ' . substr($npwp[0], 9, 3) . ' . ' . substr($npwp[0], 12, 3);
                }
                ?>
            </td>            
        </tr>
        <tr class="c-2">
            <td>2. NAMA</td>
            <td>:</td>
            <td><sub>A. 02</sub></td>            
            <td colspan="5" style="border-bottom-style: solid; border-width: 0.5px">
                <?php
                if (isset($nama[0])) {
                    echo $nama[0];
                }
                ?>
            </td>
        </tr>
        <tr class="c-3">
            <td>3. ALAMAT</td>
            <td>:</td>
            <td><sub>A. 03</sub></td>
            <td colspan="5" rowspan="2" style="border-bottom-style: solid; border-width: 0.5px"><?php
                if (isset($alamat[0])) {
                    echo $alamat[0];
                }
                ?></td>
        </tr>
        <tr class="c-4">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="c-5">
            <td class="c-1-r-1">4. NO. TELEPON</td>
            <td class="c-1-r-2">:</td>
            <td class="c-1-r-3"><sub>A. 04</sub></td>            
            <td style="border-bottom-style: solid; width: 27%; letter-spacing: 1.5px">
                <?php
                if (isset($notelpon[0])) {
                    echo $notelpon[0];
                }
                ?>
            </td>
            <td style="width: 5px"></td>
            <td style="width: 19px">5. EMAIL:</td>
            <td style="width: 1.5px"><sub>A. 05</sub></td>
            <td style="border-bottom-style: solid; border-width: 0.5px">
                <?php
                if (isset($email[0])) {
                    echo $email[0];
                }
                ?>
            </td>
        </tr>
    </table>

    <div class="sub-a">B. OBJEK PAJAK</div>
    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse;">
        <tr>
            <th style="border: 0.5px solid black; width: 4%; font-size: 9px;">No</th>
            <th style="border: 0.5px solid black; width: 37%; font-size: 9px;">PENERIMAAN PENGHASILAN</th>
            <th style="border: 0.5px solid black; width: 10%; font-size: 9px;">KODE OBJEK PAJAK</th>
            <th style="border: 0.5px solid black; width: 10%; font-size: 6.5px">JUMLAH PENERIMA PENGHASILAN</th>
            <th style="border: 0.5px solid black; width: 21%; font-size: 9px;">JUMLAH PENGHASILAN BRUTO (Rp)</th>
            <th style="border: 0.5px solid black; font-size: 9px;">JUMLAH PAJAK DIPOTONG (Rp)</th>
        </tr>
        <tr>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(1)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(2)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(3)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(4)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(5)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(6)</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '1';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                PEGAWAI TETAP
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-01
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo $jumlahPegawai;
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                 <?php
                if (isset($jumBruto)) {
                    $bruto = $jumBruto;
                    echo number_format($bruto, '0', '', '.');
                } else {
                    echo $pph = '';
                }
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                if (isset($jumPph)) {
                    $pph = $jumPph;
                    echo number_format($pph, '0', '', '.');
                } else {
                    echo $pph = '';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '2';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                PENERIMA PENSIUN BERKALA
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-01
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '3';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                PEGAWAI TIDAK TETAP ATAU TENAGA KERJA LEPAS
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-01
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;" rowspan="7">                
                <?php
                    echo '4';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                BUKAN PEGAWAI
            </td>
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3; ">
                
            </td>
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3; "></td>
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3; ">

            </td>
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3; ">
 
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                4a. DISTRIBUTOR <i>MULTILEVEL MARKETING</i> (MLM)
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-04
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                4b. PETUGAS DINAS LUAR ASURANSI
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-05
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                4c. PENJAJA BARANG DAGANGAN
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-06
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                4d. TENAGA AHLI
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-07
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                4e. BUKAN PEGAWAI YANG MENERIMA IMBALAN YANG &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BERSIFAT BERKESINAMBUNGAN
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-08
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                4f. BUKAN PEGAWAI YANG MENERIMA IMBALAN YANG &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TIDAK BERSIFAT BERKESINAMBUNGAN
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-09
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '5';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                ANGGOTA DEWAN KOMISARIS ATAU DEWAN PENGAWAS YANG TIDAK MERANGKAP SEBAGAI PEGAWAI TETAP
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-10
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '6';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                MANTAN PEGAWAI YANG MENERIMA JASA PRODUKSI, TANTIEM, BONUS ATAU IMBALAN LAIN
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-11
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '7';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                PEGAWAI YANG MELAKUKAN PENARIKAN DANA PENSIUN
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-12
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '8';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                PESERTA KEGIATAN
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-12
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '9';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                PENERIMA PENGHASILAN YANG DIPOTONG PPh PASAL 21 TIDAK FINAL LAINNYA
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-99
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '10';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8px">
                PEGAWAI/PEMBERI JASA/PESERTA KEGIATAN/PENERIMA PENSIUN BERKALA SEBAGAI WAJIB PAJAK LUAR NEGERI
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                21-100-99
            </td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '11';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px">
                JUMLAH (PENJUMLAHAN ANGKA 1 S.D 10)
            </td>
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3;">
 
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo $jumlahPegawai;
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                 <?php
                if (isset($jumBruto)) {
                    $bruto = $jumBruto;
                    echo number_format($bruto, '0', '', '.');
                } else {
                    echo $pph = '';
                }
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                if (isset($jumPph)) {
                    $pph = $jumPph;
                    echo number_format($pph, '0', '', '.');
                } else {
                    echo $pph = '';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3; ">                
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: center; padding-left: 4px" colspan="4">
                PENGHITUNGAN PPh PASAL 21 DAN/ATAU PASAL 26 YANG KURANG (LEBIH) DISETOR
            </td>            
            <td style="border: 0.5px solid black; height: 21px;background-color: #ccccb3; ">
               JUMLAH (Rp)
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;"> 
                <?php
                    echo '12';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px" colspan="4">
                STP PPh PASAL 21 DAN/ATAU PASAL 26 (HANYA POKOK PAJAK)<sub style="font-size: 6px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    B.01</sub>
            </td>            
            <td style="border: 0.5px solid black; height: 21px; text-align: right">
               <?php
                    echo $pph = '0';
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;" rowspan="3"> 
                <?php
                    echo '13';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; border-bottom-style: none" colspan="4">
                KELEBIHAN PENYETORAN PPh PASAL 21 DAN/ATAU PASAL 26 DARI :
            </td>            
            <td style="border: 0.5px solid black; height: 21px; text-align: right" rowspan="3"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; border-top-style: none; border-bottom-style: none" colspan="4">
                <table style=" border-collapse: collapse;">
                    <tr>
                        <td style = "width: 96px; height: 7px"><font align="left" size="2">MASA PAJAK</font></td>
                        <td width="17px" style="height: 12px">:</td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"></td>
                        <td><sub style="font-size: 6px">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                B.02</sub>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 65px; text-align: center; font-size: 12px; border-top-style: none"></td>
                        <td><sub style="font-size: 6px">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;
                                B.03 </sub></td>
                    </tr>
                </table>
            </td>            
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 7px; text-align: left; padding-left: 4px; border-top-style: none" colspan="4">
                <table style="">
                    <tr>
                        <td style = "width: 96px; height: 7px"><font align="left" size="2"></font></td>
                        <td width="17px" style="height: 12px"></td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">01</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">02</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">03</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">04</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">05</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">06</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">07</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">08</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">09</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">10</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">11</td>
                        <td style="height: 7px; width: 17px; text-align: center; font-size: 8px">12</td>
                        <td><sub style="font-size: 6px">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;
                                </sub>
                        </td>
                        <td style="height: 7px; width: 100px; font-size: 6px">TAHUN KALENDER (yyyy)</td>
                    </tr>
                </table>
            </td>            
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;"> 
                <?php
                    echo '14';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px" colspan="4">
                JUMLAH (ANGKA 12 + ANGKA 13)<sub style="font-size: 6px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    B.04</sub>
            </td>            
            <td style="border: 0.5px solid black; height: 21px; text-align: right"><?php echo '0' ?></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;"> 
                <?php
                    echo '15';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px" colspan="4">
                PPh PASAL 21 DAN/ATAU PASAL 26 YANG KURANG (LEBIH) DISETOR (ANGKA 11 KOLOM 6 - ANGKA 14)<sub style="font-size: 6px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    B.05</sub>
            </td>            
            <td style="border: 0.5px solid black; height: 21px; text-align: right">
                <?php
                if (isset($jumPph)) {
                    $pph = $jumPph;
                    echo number_format($pph, '0', '', '.');
                } else {
                    echo $pph = '';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;"></td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 7px" colspan="4">
                <i>LANJUTKAN PENGISIAN PADA ANGKA 16 & 17 APABILA SPT PEMBETULAN DAN/ATAU PADA ANGKA 18 APABILA PPh LEBIH DISETOR</i>
            </td>            
            <td style="border: 0.5px solid black; height: 21px; text-align: right; background-color: #ccccb3;"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 10px;" rowspan="2"> 
                <?php
                    echo '16';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 10px; text-align: left; padding-left: 4px; font-size: 7px; border-bottom-style: none" colspan="4">
                PPh PASAL 21 DAN/ATAU PASAL 26 YANG KURANG (LEBIH) DISETOR PADA SPT YANG DIBETULKAN <sub style="font-size: 6px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                   
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                   
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                   
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                   
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;
                    B.06</sub>
            </td>            
            <td style="border: 0.5px solid black; height: 10px; text-align: right" rowspan="2"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 10px; text-align: left; margin-top: -10px; padding-left: 4px; font-size: 7px; border-top-style: none" colspan="4">
                (PINDAHAN DARI BAGIAN B ANGKA 15 DARI SPT YANG DIBETULKAN)
            </td>            
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 10px;" rowspan="2"> 
                <?php
                    echo '17';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 10px; text-align: left; padding-left: 4px; border-bottom-style: none" colspan="4">
                PPh PASAL 21 DAN/ATAU PASAL 26 YANG KURANG (LEBIH) DISETOR KARENA PEMBETULAN <sub style="font-size: 6px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                   
                    &nbsp;&nbsp;
                    B.07</sub>
            </td>            
            <td style="border: 0.5px solid black; height: 10px; text-align: right" rowspan="2"></td>
        </tr>        
        <tr>
            <td style="border: 0.5px solid black; height: 10px; text-align: left; margin-top: -10px; padding-left: 4px; font-size: 7px; border-top-style: none" colspan="4">
                (ANGKA 15 - ANGKA 16)
            </td>            
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 10px;"> 
                <?php
                    echo '18';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 10px; padding: 3px 0px 3px 0px;text-align: left; padding-left: 4px; border-right-style: none; font-size: 9.5px" colspan="4">
                KELEBIHAN SETOR PADA ANGKA 15 ATAU ANGKA 17 AKAN DIKOMPENSASIKAN KE MASA PAJAK (mm - yyyy) <sub style="font-size: 6px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    B.08</sub>
            </td>            
            <td style="border: 0.5px solid black; height: 10px; text-align: center; border-left-style: none">
                _____ &nbsp; - &nbsp;________
            </td>
        </tr>
        
    </table>
    
    <table class="npwp-box" width="60%">
        <tr>
            <td style="width: 105px"><b>NPWP PEMOTONG:</b></td>
            <td style="width: 20px"><sub style="font-size: 6px">B.09</sub></td>
            <td style="letter-spacing: 2px; font-size: 10px">
                <u>
                    <?php
                    if (isset($npwp[0])) {
                        echo substr($npwp[0], 0, 2) . ' . ' . substr($npwp[0], 2, 3) . ' . ' . substr($npwp[0], 5, 3) . ' . ' . substr($npwp[0], 8, 1) . ' - ' . substr($npwp[0], 9, 3) . ' . ' . substr($npwp[0], 12, 3);
                    }
                    ?>
                </u>
            </td> 
        </tr>
    </table>
    
    <div class="sub-a">C. OBJEK PAJAK FINAL</div>    
    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse;">
        <tr>
            <th style="border: 0.5px solid black; width: 4%; font-size: 9px;">No</th>
            <th style="border: 0.5px solid black; width: 40%; font-size: 9px;">PENERIMAAN PENGHASILAN</th>
            <th style="border: 0.5px solid black; width: 8%; font-size: 9px;">KODE OBJEK PAJAK</th>
            <th style="border: 0.5px solid black; width: 9%; font-size: 6.5px">JUMLAH PENERIMA PENGHASILAN</th>
            <th style="border: 0.5px solid black; width: 21%; font-size: 9px;">JUMLAH PENGHASILAN BRUTO (Rp)</th>
            <th style="border: 0.5px solid black; font-size: 9px;">JUMLAH PAJAK DIPOTONG (Rp)</th>
        </tr>
        <tr>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(1)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(2)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(3)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(4)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(5)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px">(6)</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '1.';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 8.5px">
                PENERIMA UANG PESANGON YANG DIBAYARKAN SEKALIGUS
            </td>
            <td style="border: 0.5px solid black; height: 21px; font-size: 10px;">
                21-401-01
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '2.';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 7px">
                PENERIMA UANG MANFAAT PENSIUN, TUNJANGAN HARI TUA ATAU JAMINAN HARI TUA DAN PEMBAYARAN SEJENIS YANG DIBAYARKAN SEKALIGUS
            </td>
            <td style="border: 0.5px solid black; height: 21px; font-size: 10px;">
                21-401-02
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '3.';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 7px">
                PEJABAT NEGARA, PEGAWAI NEGERI SIPIL, ANGGOTA TNI/POLRI DAN PENSIUNAN YANG MENERIMA HONORARIUM DAN IMBALAN LAIN YANG DIBEBANKAN KEPADA KEUANGAN NEGARA/DAERAH
            </td>
            <td style="border: 0.5px solid black; height: 21px; font-size: 10px;">
                21-402-01
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '4.';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 7px">
                PENERIMA PENGHASILAN YANG DIPOTONG PPh PASAL 21 FINAL LAINNYA
            </td>
            <td style="border: 0.5px solid black; height: 21px; font-size: 10px;">
                21-499-99
            </td>            
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 21px;">                
                <?php
                    echo '5.';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px; text-align: left; padding-left: 4px; font-size: 9px">
                <b>JUMLAH BAGIAN C</b> (PENJUMLAHAN ANGKA 1 S.D 5)
            </td>
            <td style="background-color: #ccccb3; border: 0.5px solid black; font-size: 7px"></td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 21px;">
                <?php
                    echo '0';
                ?>
            </td>
        </tr>
    </table>
    
    <div class="sub-a">D. LAMPIRAN</div>    
    <table class="table-t" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse;">
        <tr>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">1.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; width: 138px; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-bottom-style: none">FORMULIR 1721 - I</td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none;"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none;" rowspan="2">LEMBAR</td>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">5.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; width: 213px; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none" rowspan="2">FORMULIR 1721 - IV</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none;" rowspan="2">LEMBAR</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.01</sub></td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-top-style: none">(untuk Satu Masa Pajak)</td>
            <td style="border: 0.5px solid black; height: 13px; padding-left: 4px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.02</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 17px; border-left-style: none; border-right-style: none; border-top-style: none">_________</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.09</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; text-align: right; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.10</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 17px; text-align: right; border-left-style: none; border-right-style: none; border-top-style: none">_________</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none;" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">2.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-bottom-style: none">FORMULIR 1721 - I</td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px;; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none" rowspan="2">LEMBAR</td>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">6.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; width: 204px; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none" rowspan="2">FORMULIR 1721 - V</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none; border-right-style: none; border-bottom-style: none" rowspan="2"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.03</sub></td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-top-style: none">(untuk Satu Tahun Pajak)</td>
            <td style="border: 0.5px solid black; height: 13px; padding-left: 4px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.04</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 17px; border-left-style: none; border-right-style: none; border-top-style: none">_________</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.11</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-top-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; text-align: right; border-left-style: none; border-right-style: none; border-top-style: none"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-left-style: none; border-right-style: none; border-bottom-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">3.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; width: 138px; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-bottom-style: none" rowspan="2">FORMULIR 1721 - II</td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none" rowspan="2">LEMBAR</td>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">5.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; width: 213px; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-bottom-style: none">SURAT SETORAN PAJAK (SSP) DAN/ATAU</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none" rowspan="2">LEMBAR</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.05</sub></td>
            <td style="border: 0.5px solid black; height: 13px; padding-left: 4px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.06</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 17px; border-left-style: none; border-right-style: none; border-top-style: none">_________</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.12</sub></td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; padding-left: 4px; border-left-style: none; border-right-style: none; border-top-style: none">BUKTI PEMINDAHBUKUAN(Pbk)</td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; text-align: right; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.13</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 17px; text-align: right; border-left-style: none; border-right-style: none; border-top-style: none">_________</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">4.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none; border-bottom-style: none" rowspan="2">FORMULIR 1721 - III</td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none; border-right-style: none" rowspan="2">LEMBAR</td>
            <td style="border: 0.5px solid black; height: 20px; width: 4px; padding: 0px 2px 0px 5px; border-right-style: none" rowspan="2">
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-bottom-style: none">8.</td>
            <td style="border: 0.5px solid black; height: 13px; text-align: left; width: 204px; padding: 0px 0px 0px 4px; border-left-style: none; border-right-style: none" rowspan="2">SURAT KUASA KHUSUS</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-bottom-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; width: 30px; padding: 0px 4px 0px 6px; border-left-style: none; border-right-style: none" rowspan="2"></td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.07</sub></td>
            <td style="border: 0.5px solid black; height: 13px; padding-left: 4px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.08</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 17px; border-left-style: none; border-right-style: none; border-top-style: none">_________</td>
            <td style="border: 0.5px solid black; height: 13px; border-left-style: none; border-right-style: none; border-top-style: none"><sub style="font-size: 6px">D.14</sub></td>
            <td style="border: 0.5px solid black; height: 13px; width: 8px; border-left-style: none; border-right-style: none; border-top-style: none"></td>
            <td style="border: 0.5px solid black; height: 13px; text-align: right; border-left-style: none; border-right-style: none; border-top-style: none"></td>
        </tr>
    </table>
    
    <div class="sub-a">E. PERNYATAAN DAN TANDA TANGAN PEMOTONG</div> 
    <table class="table-fr" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse; margin-top: 4px;">
        <tr>
            <td style="text-align: center; border-bottom-style: solid; border-bottom-width: 1px; padding: 4px 9px 4px 15px" colspan="6">
                Dengan menyadari sepenuhnya atas segala akibatnya termasuk sanksi-sanksi sesuai dengan ketentuan yang berlaku, saya menyatakan bahwa apa yang telah saya beritahukan di atas beserta lampiran-lampirannya adalah benar, lengkap dan jelas.
            </td>
        </tr>
        <tr style="padding-top: 6px">
            <td style="width: 22px; height: 24px; padding-left: 4px">1.</td>
            <td style="text-align: left; padding-left: 4px; padding-top: 4px" colspan="3">
                <?php
                    echo Html::img("@web/img/checkbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
                <sub style="font-size: 6px">E.01</sub>&nbsp; PEMOTONG
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                
                <?php
                    echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '16px', 'height' => '16px',  'margin-top' => '10px', 'margin-left' => '6px']);
                ?>
                <sub style="font-size: 6px">E.02</sub>&nbsp; KUASA
            </td>
            <td style="width: 8px; padding-left: 6px; border-left-style: solid; border-left-width: 1px">6.</td>
            <td style="padding-left: 18px; text-align: left; width: 270px">TANDA TANGAN:</td>
        </tr>
        <tr>
            <td style="width: 18px; height: 24px; padding-left: 4px;">2.</td>
            <td style="padding-left: 2px; text-align: left">
                NPWP : 
                <sub style="font-size: 6px">E.03</sub>&nbsp;                
            </td>            
            <td style="text-align: left; width: 300px; letter-spacing: 3px; font-size: 10px; border-bottom-style: solid; border-bottom-width: 0.5px" colspan="2">
                <?php
                    if (isset($npwp[0])) {
                        echo substr($npwp[0], 0, 2) . ' . ' . substr($npwp[0], 2, 3) . '.' . substr($npwp[0], 5, 3) . ' . ' . substr($npwp[0], 8, 1) . ' - ' . substr($npwp[0], 9, 3) . ' . ' . substr($npwp[0], 12, 3);
                    }
                ?>
            </td>
            <td style="padding-left: 6px; border-left-style: solid; border-left-width: 1px"></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 18px; height: 24px; padding-left: 4px;">3.</td>
            <td style="padding-left: 2px; text-align: left">
                NAMA : 
                <sub style="font-size: 6px">E.04</sub>&nbsp;                
            </td>  
            <td style="text-align: left; width: 300px; letter-spacing: 0.6px; font-size: 10px; border-bottom-style: solid; border-bottom-width: 0.5px" colspan="2">
                <?php
                    if (isset($nama[0])) {
                        echo $nama[0];
                    }
                ?>
            </td>
            <td style="padding-left: 6px; border-left-style: solid; border-left-width: 1px"></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 18px; height: 24px; padding-left: 4px;">4.</td>
            <td style="padding-left: 2px; text-align: left">
                TANGGAL : 
                <sub style="font-size: 6px">E.05</sub>&nbsp;                
            </td>  
            <td style="text-align: left; width: 300px; letter-spacing: 2px; font-size: 10px; border-bottom-style: solid; border-bottom-width: 0.5px" colspan="2">
                <?php
                    echo $toDate[0].
                         $toDate[1].
                         '-'.
                         $toDate[2].
                         $toDate[3].
                         '-'.
                         $toDate[4].
                         $toDate[5].
                         $toDate[6].
                         $toDate[7];
                ?>
                (dd-mm-yyyy)
            </td>
            <td style="padding-left: 6px; border-left-style: solid; border-left-width: 1px"></td>
            <td></td>
        </tr>
        <tr>
            <td style="width: 18px; height: 24px; padding-left: 4px;">5.</td>
            <td style="padding-left: 2px; text-align: left">
                TEMPAT : 
                <sub style="font-size: 6px">E.06</sub>&nbsp;                
            </td>  
            <td style="text-align: left; width: 300px; border-bottom-style: solid; border-bottom-width: 0.5px" colspan="2"></td>
            <td style="padding-left: 6px; border-left-style: solid; border-left-width: 1px"></td>
            <td style="text-align: left; padding-left: 18px">____________________________________________</td>
        </tr>
        <tr>
            <td colspan="4" style="height: 6px;"></td>
            <td style="border-left-style: solid; border-left-width: 1px"></td>
            <td></td>
        </tr>
    </table>
    
    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid white; border-collapse: collapse; font-weight: bold; margin-top: 18px">
        <tr>
            <td style="height: 360px; letter-spacing: 2px"> 
        </tr>
    </table>

    <div class="footer-f"></div>
    <div class="footer-s"></div>
</body>
</html>

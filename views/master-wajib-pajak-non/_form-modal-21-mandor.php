<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-vendor-form">
    <?php
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);    
    
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [            
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Mandor',
                            'id' => 'wpn-nama'
                        ]
                    ],
                    'nik' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan NIK Mandor...', 'maxlength'=>16]],
                    ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ],
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
                            'class' => 'btn btn-default',
                        ]) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
                            'class' => 'btn btn-primary',
//                            'disabled' => true
                        ]) .
                        '</div>'
                    ],
                ],
            ],                
        ],
    ]);
    
    ?>
        
    <?php
    ActiveForm::end();
    ?>
</div>

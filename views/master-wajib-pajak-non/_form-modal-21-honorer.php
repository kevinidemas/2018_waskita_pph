<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
?>
    
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#nik").mask("999.999.999.999-9999");
    });
</script> -->

<div class="pph-vendor-form">
    <?php
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [            
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Honorer',
                            'id' => 'wpn-nama'
                        ]
                    ],
                    'nik' => [
                        'type' => Form::INPUT_TEXTAREA, 
                        'options' => [
                                'placeholder' => 'Masukkan NIK Honorer...',
                                'maxlength'=>16,
                                'id' => 'nik-wp',
                                ]
                    ],
                ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ],
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
                            'class' => 'btn btn-default',
                        ]) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
                               'class' => 'btn btn-primary',
//                            'disabled' => true
                        ]) .
                        '</div>'
                    ],
                ],
            ],                
        ],
    ]);
    ActiveForm::end();
    ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphStatusNpwp */

$this->title = 'Create Pph Status Npwp';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Npwps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-status-npwp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

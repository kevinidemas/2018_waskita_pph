<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusNpwp */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-status-npwp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tarifId')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'parentId')->textInput() ?>

    <?= $form->field($model, 'pasalId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

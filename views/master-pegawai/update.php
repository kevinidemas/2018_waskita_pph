<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPegawai */

$this->title = 'Update Master Pegawai: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Master Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pegawaiId, 'url' => ['view', 'id' => $model->pegawaiId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-pegawai-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

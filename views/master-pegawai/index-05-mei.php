<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\alert\Alert;
use app\models\MasterPegawai;
use app\models\PphJenisKelamin;
use app\models\PphStatusKawin;
use app\models\PphStatusKerja;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterPegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pusat Data Pegawai';
//$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->session->hasFlash('success')) {
    $msgSuccess = Yii::$app->session->getFlash('success');
    echo Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'title' => 'Berhasil!',
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => $msgSuccess,
        'showSeparator' => true,
        'delay' => 10000
    ]);
}
if (Yii::$app->session->hasFlash('info')) {
    $msgInfo = Yii::$app->session->getFlash('info');
    echo Alert::widget([
        'type' => Alert::TYPE_INFO,
        'title' => 'Pemberitahuan!',
        'icon' => 'glyphicon glyphicon-info-sign',
        'body' => $msgInfo,
        'showSeparator' => true,
        'delay' => 10000
    ]);
}
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    if (!is_array($msgErrors)) {
        $alertBootstrap = '<div class="alert-single alert-danger">';
    } else {
        $alertBootstrap = '<div class="alert alert-danger">';
    }
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
        echo $alertBootstrap;
}
?>
<div class="master-pegawai-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Upload', ['create-upload'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Manual', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'pegawai_id',
                'header' => 'Nama',
                'value' => function ($data) {
                    $modelUser = MasterPegawai::find()->where(['pegawaiId' => $data->pegawaiId])->one();
                    $nama = $modelUser->nama;
                    return ucfirst($nama);
                },
                'options' => [
                    'width' => '190px'
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'jabatan',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '100px',
                ],
            ],
            [
                'attribute' => 'nip',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'npwp',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'alamat',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '229px',
                ],
            ],
            [
                'attribute' => 'jenisKelamin',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelJK = PphJenisKelamin::find()->where(['jenisKelaminId' => $data->jenisKelamin])->one();
                    $jk = $modelJK->jenis;
                    return ucfirst($jk);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'statusPerkawinan',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelJP = PphStatusKawin::find()->where(['statusKawinId' => $data->statusPerkawinan])->one();
                    $jp = $modelJP->status;
                    return ucfirst($jp);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '70px',
                ],
            ],
            [
                'attribute' => 'statusKerjaId',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelSK = PphStatusKerja::find()->where(['statusKerjaId' => $data->statusKerjaId])->one();
                    $sk = $modelSK->statusKerja;
                    return ucfirst($sk);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'created_by',
                'header' => 'Dibuat Oleh',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = User::find()->where(['id' => $data->created_by])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
            ],
        ],
    ]);
    ?>
</div>

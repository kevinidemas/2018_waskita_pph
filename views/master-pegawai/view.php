<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\PphJenisKelamin;
use app\models\PphStatusKawin;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPegawai */

$this->title = $model->pegawaiId;
$this->params['breadcrumbs'][] = ['label' => 'Master Pegawais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-pegawai-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->pegawaiId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->pegawaiId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'pegawaiId',
            'nip',
            'npwp',
            'alamat',
            'nik',
            [
                'attribute' => 'jenisKelamin',
                'value' => function ($model) {
                    if ($model->jenisKelamin != null) {
                        $modelWp = PphJenisKelamin::find()->where(['jenisKelaminId' => $model->jenisKelamin])->one();
                        $nama = $modelWp->jenis;
                        return $nama;
                    } else {
                        return $npwp = 'Tidak Diketahui';
                    }
                },
            ],
            [
                'attribute' => 'statusPerkawinan',
                'value' => function ($model) {
                    if ($model->statusPerkawinan != null) {
                        $modelWp = PphstatusKawin::find()->where(['statusKawinId' => $model->statusPerkawinan])->one();
                        $nama = $modelWp->status;
                        return $nama;
                    } else {
                        return $npwp = 'Tidak Diketahui';
                    }
                },
            ],
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use app\models\PphJenisKelamin;
use app\models\PphStatusKawin;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPegawai */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#pegawai-nip").mask("999999999");
        $("#pegawai-nik").mask("999999999999999");
        $("#pegawai-npwp").mask("99.999.999.9-999.999");
    });
</script>
<div class="master-pegawai-form">
    <?php
    //ambil options untuk jenis kelamin
    $dataJenisKelamin = PphJenisKelamin::find()->asArray()->all();
    $arrayJenisKelamin = ArrayHelper::getColumn($dataJenisKelamin, 'jenisKelaminId');
    $countArrayJK = count($arrayJenisKelamin);
    $jenKelOptions = [];
    for ($n = 0; $n < $countArrayJK; $n++) {
        $m = $n + 1;
        if (isset($arrayJenisKelamin[$n]) && $n != 2) {
            $modelJK = PphJenisKelamin::find()->where(['jenisKelaminId' => $arrayJenisKelamin[$n]])->one();
            $jenKelOptions[$m] = $modelJK->jenis;
        }
    }
    
    //ambil options untuk status perkawinan
    $dataStatusKawin = PphStatusKawin::find()->asArray()->all();
    $arrayStatusKawin = ArrayHelper::getColumn($dataStatusKawin, 'statusKawinId');
    $countArraySK = count($arrayStatusKawin);
    $statKawOptions = [];
    for ($n = 0; $n < $countArraySK; $n++) {
        $m = $n + 1;
        if (isset($arrayStatusKawin[$n]) && $n != 5) {
            $modelSK = PphStatusKawin::find()->where(['statusKawinId' => $arrayStatusKawin[$n]])->one();
            $statKawOptions[$m] = $modelSK->status;
        }
    }
    
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Pegawai',
                            'id' => 'pegawai-nama'
                        ]
                    ],
                ],
            ],
            [
                'attributes' => [
                    'jenisKelamin' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $jenKelOptions,
                        'options' => [
//                            'value' => $iujkId,
                        ],
                        'hint' => 'Pilih Jenis Kelamin Wajib Pajak',
                    ],
                    'statusPerkawinan' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $statKawOptions,
                        'options' => [
//                            'value' => $iujkId,
                        ],
                        'hint' => 'Pilih Status Kawin Wajib Pajak',
                    ],
                ]
            ],
            [
                'attributes' => [
                    'jabatan' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Pegawai',
                            'id' => 'pegawai-jabatan'
                        ]
                    ],
                    'npwp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor NPWP Pegawai',
                            'id' => 'pegawai-npwp'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'nip' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor Induk Pegawai',
                            'id' => 'pegawai-nip'
                        ]
                    ],
                    'nik' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor Induk Kependudukan',
                            'id' => 'pegawai-nik'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
                            'class' => 'btn btn-default',
                        ]) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
                            'class' => 'btn btn-primary',
//                            'disabled' => true
                        ]) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphHistoryUpdate */

$this->title = $model->historyUpId;
$this->params['breadcrumbs'][] = ['label' => 'Pph History Updates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-history-update-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->historyUpId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->historyUpId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'historyUpId',
            'nomorPembukuan',
            'tanggal',
            'jumlahBruto',
            'jumlahPphDiPotong',
            'potonganPegawai',
            'potonganMaterial',
            'nilaiTagihan',
            'dpp',
            'pasalId',
            'wajibPajakId',
            'wajibPajakNonId',
            'statusWpId',
            'statusNpwpId',
            'statusKerjaId',
            'userId',
            'parentId',
            'updated_at',
            'nama',
            'alamat',
            'lokasi_tanah',
            'tarif',
            'userIndukId',
            'pembetulanId',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphHistoryUpdate */

$this->title = 'Update Pph History Update: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph History Updates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->historyUpId, 'url' => ['view', 'id' => $model->historyUpId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-history-update-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

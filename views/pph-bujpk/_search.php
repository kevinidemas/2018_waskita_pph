<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PphBujpkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-bujpk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'bujpkId') ?>

    <?= $form->field($model, 'nomorBujpk') ?>

    <?= $form->field($model, 'wajibPajakId') ?>

    <?= $form->field($model, 'penanggungJawab') ?>

    <?= $form->field($model, 'kemampuanKeuangan') ?>

    <?php // echo $form->field($model, 'published_at') ?>

    <?php // echo $form->field($model, 'expired_at') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'bukti') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'is_active') ?>

    <?php // echo $form->field($model, 'is_approve') ?>

    <?php // echo $form->field($model, 'is_expired') ?>

    <?php // echo $form->field($model, 'tingkatKeuangan') ?>

    <?php // echo $form->field($model, 'is_expired_warning') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

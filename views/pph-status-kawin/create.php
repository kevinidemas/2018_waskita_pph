<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphStatusKawin */

$this->title = 'Create Pph Status Kawin';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Kawins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-status-kawin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

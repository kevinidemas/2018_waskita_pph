<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusKawin */

$this->title = 'Update Pph Status Kawin: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Kawins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->statusKawinId, 'url' => ['view', 'id' => $model->statusKawinId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-status-kawin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

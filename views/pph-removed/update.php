<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphRemoved */

$this->title = 'Update Pph Removed: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Removeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->removedId, 'url' => ['view', 'id' => $model->removedId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-removed-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

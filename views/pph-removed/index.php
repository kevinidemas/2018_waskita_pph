<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphRemovedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Removeds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-removed-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Removed', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'removedId',
            'start',
            'end',
            'tahun',
            'userId',
            //'userIndukId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

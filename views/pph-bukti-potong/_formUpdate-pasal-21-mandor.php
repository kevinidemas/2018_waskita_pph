<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\builder\FormGrid;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\Url;
use kartik\alert\Alert;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use app\models\MasterMandor;
use app\models\MasterWajibPajakNon;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */
/* @var $form yii\widgets\ActiveForm */
?>

<?php
$session = Yii::$app->session;
if (isset($session['status-wp-pasal-21-mandor'])) {
    $statusCheckNpwp = $session['status-wp-pasal-21-mandor'];
} else {
    $statusCheckNpwp = 1;
}
unset($session['status-wp-pasal-21-mandor']);
?>

<div style="visibility: hidden">
    <input type="hidden" id="status-check-npwp" name="status-check-npwp" value=<?php echo $statusCheckNpwp ?>>      
</div>

<?php
$session = Yii::$app->session;
$dataVendor = MasterMandor::find()->where(['approvalStatus' => 1])->asArray()->all();
$arrayVendorId = ArrayHelper::getColumn($dataVendor, 'mandorId');

if ($arrayVendorId != null) {
    for ($n = 0; $n < count($arrayVendorId); $n++) {
        $mn = $arrayVendorId[$n];
        if (isset($arrayVendorId[$n])) {
            $modelVendor = MasterMandor::find()->where(['mandorId' => $arrayVendorId[$n]])->one();
            $vendorOptions[$mn] = $modelVendor->npwp . ' (' . $modelVendor->nama . ')';
        }
    }
} else {
    $vendorOptions[] = 'KOSONG';
}

$dataVendorNon = MasterWajibPajakNon::find()->where(['statusKerja' => 2])->asArray()->all();
$arrayVendorNonId = ArrayHelper::getColumn($dataVendorNon, 'wajibPajakNonId');

if ($arrayVendorNonId != null) {
    for ($n = 0; $n < count($arrayVendorNonId); $n++) {
        $mn = $arrayVendorNonId[$n];
        if (isset($arrayVendorNonId[$n])) {
            $modelVendorNon = MasterWajibPajakNon::find()->where(['wajibPajakNonId' => $arrayVendorNonId[$n]])->one();
            $vendorOptionsNon[$mn] = $modelVendorNon->nama . ' (' . $modelVendorNon->alamat . ')';
        }
    }
} else {
    $vendorOptionsNon[] = 'KOSONG';
}
//
//echo '<pre>';
//print_r($vendorOptionsNon);
//die();

if (isset($_GET['id'])) {
    $session = Yii::$app->session;
    $id = $_GET['id'];
    $session['idWp'] = $id;
} else {
    $id = null;
}
$status = false;
?>

<style>
    .field-pph-21-mandor-potong-tagihan {
        margin-bottom: 10px;
        float: right;
        width: 51.5%;
    }
</style>

<?php
Modal::begin([
    'header' => '<h4>Tambah Mandor Baru</h4>',
    'id' => 'modal',
    'size' => 'modal-lg'
]);

echo "<div id='modalContent'></div>";

Modal::end();
?>
<?php
if (isset($_GET['var'])) {
    $var = $_GET['var'];
    $var = base64_decode($var);
    $array = json_decode($var);
} else {
    $array = ['0' => '', '1' => '', '2' => ''];
}
$tanggalValue = null;
$tahun = date('Y');
$bulan = date('m');

if (isset($session['masaId'])) {
    $m = $session['masaId'];
    if (strlen($m) < 2) {
        $m = '0' . $m;
    }
    $Y = date('Y');
    $countDate = date('t', strtotime($Y . '-' . $m . '-01'));
    if ($tahun == $session['masaTahun']) {
        if ($bulan == $m) {
            $tanggalValue = date('d');
        } else {
            $tanggalValue = date('t', strtotime($tahun . '-' . $m));
        }
    }
}

if ($session['expiredDate'] == 1) {
    ?>
    <style type="text/css">#form{
            display:none;
        }</style>
        <?php
    }
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 4]]);
    ?>

<div class="pph-bukti-potong-21-mandor-update-form" id="form">
    <?= Html::beginForm(['pph-bukti-potong/create'], 'post'); ?>
    <div style="width: 100%; margin-left: 0px" id="checkbox-npwp">
        <input type="checkbox" id="npwp-21-mandor" name="npwp" value="1"> TANPA NPWP
    </div>
    <?= Html::endForm(); ?>   
     <div class="row" id='form-satu'>         
        <div class="col-sm-8" id="wajib-pajak">
            <?php
             if($model->wajibPajakId != null){
                $wpPasal21 = $model->wajibPajakId;
                echo '<label class="control-label">Wajib Pajak</label>';
                echo Select2::widget([
                'model' => $model,
                'attribute' => 'wajibPajakId',
                'data' => $vendorOptions,
                'options' => [
                    'value' => $wpPasal21,
                    'placeholder' => Yii::t('app', 'Pilih Wajib Pajak'),
                    'id' => 'pph-wp'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
             } else {
                 $wpPasal21 = $model->wajibPajakNonId;
                 echo '<label class="control-label">Wajib Pajak</label>';
                 echo Select2::widget([
                 'model' => $model,
                 'attribute' => 'wajibPajakNonId',
                 'data' => $vendorOptionsNon,
                 'options' => [
                     'value' => $wpPasal21,
                     'placeholder' => Yii::t('app', 'Pilih Wajib Pajak'),
//                     'id' => 'pph-wp'
                 ],
                 'pluginOptions' => [
                     'allowClear' => true
                 ],
             ]);
             }
            
            echo '<br>';
            ?> 
        </div>
        <div class="col-sm-4" style="margin-top: 11px" id="date-picker">
            <?php
            echo '<label class="control-label">Tanggal</label>';
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'tanggal',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => [
                    'value' => date('d-m-Y', strtotime(date($model->tanggal)))
                    ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startDate' => date('d-' . $m . '-Y', strtotime(date('Y-m-1'))),
                    'endDate' => date('d-' . $m . '-Y', strtotime('last day of this month')),
                    'format' => 'dd-mm-yyyy',
                ]
            ]);
            ?>
        </div>
    </div> 

    <?php
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nilaiTagihan' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai Tagihan...',
                            'id' => 'pph-21-mandor-potong-tagihan',
                            'value' => $model->nilaiTagihan,
                        ],
                        'label' => 'Nilai Tagihan',
                    ],
                ],
            ],
            [
                'attributes' => [
                    'nomorPembukuan' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor Pembukuan', 'value' => $model->nomorPembukuan, 'readOnly' => true], 'label' => 'Nomor Voucher'],
                    'potonganPegawai' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai Potongan Pegawai...',
                            'id' => 'pph-21-mandor-potong-pegawai',
                            'value' => $model->potonganPegawai,
                        ],
                        'label' => 'Potongan'],
                ]
            ],
            [
                'attributes' => [
                    'nomorBuktiPotong' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'No. Bukti Potong di Generate Otomatis oleh Admin', 'value' => $model->nomorBuktiPotong,'readOnly' => true]],
                    'potonganMaterial' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai Potongan Material...',
                            'id' => 'pph-21-mandor-potong-material',
                            'value' => $model->potonganMaterial,
                        ],
                        'label' => ''],
                ]
            ],
            [
                'attributes' => [
                    'jumlahBruto' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai (Tagihan - Jumlah Potongan)',
                            'id' => 'pph-21-mandor-bruto',
                            'readOnly' => true,
                            'value' => $model->jumlahBruto,
                        ],
                    ],
                    'dpp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai (Total Bruto x 50%)',
                            'id' => 'pph-21-mandor-dpp',
                            'readOnly' => true,
                            'value' => $model->dpp,
                        ],
                    ],                    
                ],
            ],
        ],
    ]);
    ?>

    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <?php
    echo '<div class="text-right">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
    ActiveForm::end();
    ?>

</div>

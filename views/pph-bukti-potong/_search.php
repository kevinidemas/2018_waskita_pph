<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PphBuktiPotongSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-bukti-potong-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'buktiPotongId') ?>

    <?= $form->field($model, 'nomorPembukuan') ?>

    <?= $form->field($model, 'nomorBuktiPotong') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'jumlahBruto') ?>

    <?php // echo $form->field($model, 'wajibPajakId') ?>

    <?php // echo $form->field($model, 'jumlahPphDiPotong') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <?php // echo $form->field($model, 'parentId') ?>

    <?php // echo $form->field($model, 'pasalId') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

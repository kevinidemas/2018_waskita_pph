<?php

use yii\helpers\Html;
use kartik\alert\Alert;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\builder\FormGrid;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\Url;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use app\models\PphWajibPajak;
use app\models\PphIujk;
use app\models\PphTarif;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$session = Yii::$app->session;
$dataVendor = PphWajibPajak::find()->where(['jenisWp' => 2])->asArray()->all();
$arrayVendorId = ArrayHelper::getColumn($dataVendor, 'wajibPajakId');

if ($arrayVendorId != null) {
    for ($n = 0; $n < count($arrayVendorId); $n++) {
        $mn = $arrayVendorId[$n];
        if (isset($arrayVendorId[$n])) {
            $modelVendor = PphWajibPajak::find()->where(['wajibPajakId' => $arrayVendorId[$n]])->one();
            $vendorOptions[$mn] = $modelVendor->npwp . ' (' . $modelVendor->nama . ')';
        }
    }
} else {
    $vendorOptions[] = 'KOSONG';
}

if (isset($_GET['id'])) {
    $session = Yii::$app->session;
    $id = $_GET['id'];
    $session['idWp'] = $id;
//    $status = 'true';
} else {
    $id = null;
//    $status = 'false';
}
$status = false;
?>

<form action="create-pasal-4-jasa" method="GET">
    <input type="submit" name="submit-wp" id="wpIdButton"/>
</form>

<form action="create-pasal-4-jasa" method="GET">
    <input type="submit" name="submit-wp-nama" id="wpNamaButton"/>
</form>
<?php
if ($_SERVER['REQUEST_METHOD'] == "GET" and isset($_GET['submit-wp'])) {
    $idWp = $_GET['submit-wp'];
    $session['wp-terpilih'] = $idWp;
    unset($_GET['submit-wp']);
    ?>
    <script>

        $(function () {
            var wp = $("#pph-wp").val();
            if (wp != null) {
                var as = "<?php
    $modelWpTerpilih = PphWajibPajak::find()->where(['wajibPajakId' => $idWp])->one();
    $nama = $modelWpTerpilih->nama;
    echo $nama;
    ?>";
                document.getElementById('wpNamaButton').value = as;
                $('#wpNamaButton').trigger('click');
            }
        });
    </script>
    <?php
}

if (isset($idWp)) {
    $modelIujkWp = PphWajibPajak::find()->where(['wajibPajakId' => $idWp])->one();
    $iujkWp = $modelIujkWp->iujkId;
    if ($iujkWp == null) {
        $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
        $tarif = $modelTarif->tarif;
    } else {
        $modelIujk = PphIujk::find()->where(['iujkId' => $iujkWp])->one();
        $keuanganWp = $modelIujk->kemampuanKeuangan;
        $is_approve = $modelIujk->is_approve;
        if ($is_approve == 1) {
            $is_expired = $modelIujk->is_expired;
            if ($is_expired == 1) {
                
                $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
                $tarif = $modelTarif->tarif;
            } else {
                $tingkatKeuanganWp = $modelIujk->tingkatKeuangan;
                if ($tingkatKeuanganWp == 1) {

                    $modelTarif = PphTarif::find()->where(['tarifId' => 6])->one();
                    $tarif = $modelTarif->tarif;
                } else if ($tingkatKeuanganWp == 2) {

                    $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                    $tarif = $modelTarif->tarif;
                } else if ($tingkatKeuanganWp == 3) {

                    $modelTarif = PphTarif::find()->where(['tarifId' => 7])->one();
                    $tarif = $modelTarif->tarif;
                }
            }
        } else {
            $modelTarif = PphTarif::find()->where(['tarifId' => 8])->one();
            $tarif = $modelTarif->tarif;
        }
    }
    $session['tarif-pasal-4'] = $tarif;
}
?>

<?php
if (isset($session['tarif-pasal-4'])) {
    $tarif = $session['tarif-pasal-4'];
    ?>
    <script>
        var tarif_var = "<?php echo $tarif; ?>";
        $(document).ready(function () {
            document.getElementById("pph-bruto").addEventListener("change", myFunction);
            function myFunction() {
                var bruto = $("#pph-bruto").val();
                bruto = bruto.split('.').join("");
                result = (bruto * tarif_var) / 100;
                result = Math.floor(result);
                result = result.toLocaleString(undefined, {maximumFractionDigits: 2});
                document.getElementById('pph-potong').value = result;
            }
        });
    </script>
    <?php
}
?>

<script>
    $(function () {
        if ($('div').is('.pph-bukti-potong-4-jasa-form')) {
            $('#btn-create-bp').prop('disabled', true);
            $("#pph-wp").change(function () {
                var wp = $('#pph-wp').val();
                document.getElementById('wpIdButton').value = wp;
                $('#wpIdButton').trigger('click');
            });
        }
    });

</script>

<?php
Modal::begin([
    'header' => '<h4>Tambah Wajib Pajak Baru</h4>',
    'id' => 'modal',
    'size' => 'modal-lg'
]);

echo "<div id='modalContent'></div>";

Modal::end();
?>
<?php
if (isset($_GET['var'])) {
    $var = $_GET['var'];
    $var = base64_decode($var);
    $array = json_decode($var);
} else {
    $array = ['0' => '', '1' => '', '2' => ''];
}
$tanggalValue = null;
$tahun = date('Y');
$bulan = date('m');

if (isset($session['masaId'])) {
    $m = $session['masaId'];
    if (strlen($m) < 2) {
        $m = '0' . $m;
    }
    $Y = date('Y');
    $countDate = date('t', strtotime($Y . '-' . $m . '-01'));
    if ($tahun == $session['masaTahun']) {
        if ($bulan == $m) {
            $tanggalValue = date('d');
        } else {
            $tanggalValue = date('t', strtotime($tahun . '-' . $m));
        }
    }
}

if ($session['expiredDate'] == 1) {
    ?>
    <style type="text/css">#form{
            display:none;
        }</style>
        <?php
    }
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 4]]);
    ?>

<div class="pph-bukti-potong-4-jasa-form" id="form">
    <?= Html::beginForm(['pph-bukti-potong/create'], 'post'); ?>
    <!--        <div style="width: 100%; margin-left: 0px" id="checkbox-npwp">
                <input type="checkbox" id="iujk-status" name="iujk" value="1"> Dengan IUJK
            </div>-->
    <?= Html::endForm(); ?>
    <div id="Show" style="width: 40%; margin-left: 33px">
        <div style="margin-left: -15px"><?php echo '<label class="control-label">Wajib Pajak</label>'; ?></div>
        <?= $form->field($model, 'nama')->textInput(['maxlength' => 128, 'placeholder' => 'Nama Wajib Pajak'])->label('Nama') ?> 
        <?= $form->field($model, 'alamat')->textarea(['maxlength' => 1000, 'placeholder' => 'Alamat Wajib Pajak'])->label('Alamat') ?> 
    </div>
    <div class="row" id='form-satu'>         
        <div class="col-sm-8" id="wajib-pajak">
            <!--<p style="margin-top: 0px"><?= Html::button('<i class="glyphicon glyphicon-plus"></i>', ['value' => Url::to('../pph-wajib-pajak/create-modal-4'), 'class' => 'btn btn-add', 'id' => 'modalButton']) ?></p>-->
            <?php
            echo '<label class="control-label">Wajib Pajak</label>';
            echo Select2::widget([
                'model' => $model,
                'attribute' => 'wajibPajakId',
                'data' => $vendorOptions,
                'options' => [
                    'value' => $id,
                    'placeholder' => Yii::t('app', 'Pilih Wajib Pajak'),
                    'id' => 'pph-wp'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            echo '<br>';
            ?> 
        </div>
        <div class="col-sm-4" style="margin-top: 11px" id="date-picker">
            <?php
            echo '<label class="control-label">Tanggal</label>';
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'tanggal',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['value' => date('d-' . $m . '-Y', strtotime(date('Y-m-' . $tanggalValue)))],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startDate' => date('d-' . $m . '-Y', strtotime(date('Y-m-1'))),
//                    'endDate' => date('d-' . $m . '-Y', strtotime('last day of this month')),
                    'endDate' => $countDate . '-' . $m . '-' . $Y,
                    'format' => 'dd-mm-yyyy',
                ]
            ]);
            ?>
        </div>
    </div> 

    <?php
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
                [
                'attributes' => [
                    'nomorPembukuan' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor Pembukuan', 'value' => $array[0],], 'label' => 'Nomor Voucher'],
                    'jumlahBruto' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai Total Bruto...',
                            'id' => 'pph-bruto',
                            'value' => $array[1],
                        ],
                        'label' => 'Jumlah',
                    ],
                ],
            ],
                [
                'attributes' => [
                    'nomorBuktiPotong' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'No. Bukti Potong di Generate Otomatis oleh Admin', 'readOnly' => true]],
                    'jumlahPphDiPotong' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Nilai Potong otomatis dari Total (Bruto x Tarif)',
                            'readOnly' => true,
                            'id' => 'pph-potong',
                            'value' => $array[2]
                        ],
                        'label' => ''],
                ]
            ],
        ],
    ]);
    ?>

    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
        </div>
    </div>
    <?php
    echo '<div class="text-right">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-floppy-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
    ActiveForm::end();
    ?>

</div>

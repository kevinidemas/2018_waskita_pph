<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$this->title = 'Ubah Bukti Potong Pasal 21 PTT - Honorer ' . $model->nomorBuktiPotong;
?>
<div class="pph-bukti-potong-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?=
    $this->render('_formUpdate-pasal-21-honorer', [
        'model' => $model,
        'modelWp' => $modelWp,
    ])
    ?>

</div>

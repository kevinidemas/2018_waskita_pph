<?php

//use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formatter = Yii::$app->formatter;
?>

<?php
//header('Content-Type: application/pdf');
//header("Pragma: no-cache");
//header("Expires: 0");
//header("Content-Type: application/force-download");
//header('Content-Disposition: attachment; filename="July Report.pdf"');
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];

function kekata($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = kekata($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x < 1000) {
        $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x < 1000000) {
        $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
    } else if ($x < 1000000000) {
        $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
    }
    return $temp;
}

function terbilang($x, $style = 4) {
    if ($x < 0) {
        $hasil = "minus " . trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}

//        $nilai = 1250;
//        
//        echo terbilang($nilai, $style=1); echo '<br/>';
////        SERIBU DUA RATUS LIMA PULUH
//        
//        echo terbilang($nilai, $style=2); echo '<br/>';
////        seribu dua ratus lima puluh
//        
//        echo terbilang($nilai, $style=3); echo '<br/>';
////        Seribu Dua Ratus Lima Puluh
//        
//        echo terbilang($nilai, $style=4); echo '<br/>';
?>
<style>
    .header-f{
        width:6.1%;
        height:10px;
        background:#000;
        position:fixed;
        top:0;
        left: 0;
        margin: -52px 0px 0px -12px;
    }
    .header-s{
        width:4.1%;
        height:10px;
        background:#000;
        position:fixed;
        top:0;
        left:660;
        margin: -52px 0px 0px 0px;
    }
    .footer-f{
        width:6%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:-9;
        left: 0;
        margin: -52px 0px -52px -12px;
    }
    .footer-s{
        width:4.1%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:-9;
        left:660;
        margin: -52px 0px -52px 0px;
    }
    .header{
        position:fixed;
        /*top:-20;*/
        left: 462;
        font-family: fantasy;
        font-size: 8px;
        font-weight: bold;
        margin: -55px 0px 0px 30px;
    }
    .footer-m{
        font-weight: bold; 
        font-size: 9.7px;
        padding-bottom: -29px;
        position:fixed;
        bottom:-22;
        left:3;
        margin: 0px -50px 0px 2px;
    }
</style>
<html>
    <?php for ($n = 0; $n < $countPdf; $n++) { ?>
        <style font-size="20px"></style>
        <body>
            <div style="height: 25px"></div>
            <div class="header-f"></div>
            <div class="header-s"></div>
            <div class="header">
                <p>Lembar ke-1 untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Wajib Pajak</p>
                <p>Lembar ke-2 untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Kantor Pelayanan Pajak</p>
                <p>Lembar ke-3 untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Pemungut Pajak</p>
            </div>
            <table style="margin-left: 36px; font-size: 11.7px; padding-top: -20px">
                <tr>
                    <td style="width: 18%">
                        <?= Html::img("@web/img/djp.png", ['width' => '60px', 'height' => '50px', 'margin-top' => '-10px']) ?>
                    </td>
                    <td style="padding-top: 12px; padding-left: 10px;">
                <font align="left"><center><b>DEPARTEMEN KEUANGAN REPUBLIK INDONESIA</b></center>
                <font align="left"><center><b>DIREKTORAT JENDERAL PAJAK</b></center>
                <font align="left"><center><b>KANTOR PELAYANAN PAJAK</b></center>
                <font align="left"><center><b><?php echo $kppTerdaftar[$n] ?></b></center>
            </td>
        </tr>
    </table>


                                <!--<table style="width: 75%; border: 1px solid black; border-collapse: collapse; margin-left: 135px; margin-top: 10px; background-color: #ccccb3">-->
    <table style="width: 75%; border: 1px solid black; border-collapse: collapse; margin-left: 159px; margin-top: 10px; background-color: #ccccb3">
        <tr style="width: 20%">
            <td>
        <font size="1"><center><b>BUKTI PEMUNGUTAN PPh PASAL 22</b></center>
    </td>
    </tr>
    <tr>
        <td>
    <font size="1"><center><b>(OLEH BADAN USAHA INDUSTRI/EKSPORTIR TERTENTU)</b></center>
    </td>
    </tr>
    <tr>        
        <td style="border-top: 1px solid black;">
    <font size="1"><center><b>NOMOR : <?php echo $nomorBuktiPotong[$n] ?></b></center>
    </td>

    </tr>
    </table>
    <!--<HR><HR>-->
    <!--<hr style="height: 5px" />-->

    <table class="good" border="none" width="100%" style="margin-left: 40px; margin-top: 6px; height: 3px;">
        <tr>
            <td width="280px" margin-right="1000px">
                <table style=" border-collapse: collapse;">
                    <tr>
                        <td style = "width: 96px; letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
                        <td width="17px" style="height: 12px">:</td>
                        <?php
//                        echo '<pre>';
//                        print_r($npwp);
//                        die();
                        ?>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][0])) {
                                echo $npwp[$n][0];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][1])) {
                                echo $npwp[$n][1];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][2])) {
                                echo $npwp[$n][2];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][3])) {
                                echo $npwp[$n][3];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][4])) {
                                echo $npwp[$n][4];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][5])) {
                                echo $npwp[$n][5];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][6])) {
                                echo $npwp[$n][6];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][7])) {
                                echo $npwp[$n][7];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][8])) {
                                echo $npwp[$n][8];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][9])) {
                                echo $npwp[$n][9];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][10])) {
                                echo $npwp[$n][10];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][11])) {
                                echo $npwp[$n][11];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][12])) {
                                echo $npwp[$n][12];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][13])) {
                                echo $npwp[$n][13];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($npwp[$n][14])) {
                                echo $npwp[$n][14];
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style = "width: 96px; height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
                        <td width="17px" style="height: 12px">:</td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][0])) {
                                echo $nama[$n][0];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][1])) {
                                echo $nama[$n][1];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][2])) {
                                echo $nama[$n][2];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][3])) {
                                echo $nama[$n][3];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][4])) {
                                echo $nama[$n][4];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][5])) {
                                echo $nama[$n][5];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][6])) {
                                echo $nama[$n][6];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][7])) {
                                echo $nama[$n][7];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][8])) {
                                echo $nama[$n][8];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][9])) {
                                echo $nama[$n][9];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][10])) {
                                echo $nama[$n][10];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][11])) {
                                echo $nama[$n][11];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][12])) {
                                echo $nama[$n][12];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][13])) {
                                echo $nama[$n][13];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][14])) {
                                echo $nama[$n][14];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][15])) {
                                echo $nama[$n][15];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][16])) {
                                echo $nama[$n][16];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][17])) {
                                echo $nama[$n][17];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($nama[$n][18])) {
                                echo $nama[$n][18];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($nama[$n][19])) {
                                echo $nama[$n][19];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][20])) {
                                echo $nama[$n][20];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][21])) {
                                echo $nama[$n][21];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($nama[$n][22])) {
                                echo $nama[$n][22];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($nama[$n][23])) {
                                echo $nama[$n][23];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][24])) {
                                echo $nama[$n][24];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][25])) {
                                echo $nama[$n][25];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][26])) {
                                echo $nama[$n][26];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][27])) {
                                echo $nama[$n][27];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($nama[$n][28])) {
                                echo $nama[$n][28];
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style = "width: 96px;height: 12px"><b><font align="left" size="2">Alamat</font></b></td>
                        <td width="17px" style="height: 12px">:</td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][0])) {
                                echo $alamat[$n][0];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][1])) {
                                echo $alamat[$n][1];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][2])) {
                                echo $alamat[$n][2];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][3])) {
                                echo $alamat[$n][3];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][4])) {
                                echo $alamat[$n][4];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][5])) {
                                echo $alamat[$n][5];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][6])) {
                                echo $alamat[$n][6];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][7])) {
                                echo $alamat[$n][7];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][8])) {
                                echo $alamat[$n][8];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][9])) {
                                echo $alamat[$n][9];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][10])) {
                                echo $alamat[$n][10];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][11])) {
                                echo $alamat[$n][11];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][12])) {
                                echo $alamat[$n][12];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][13])) {
                                echo $alamat[$n][13];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][14])) {
                                echo $alamat[$n][14];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][15])) {
                                echo $alamat[$n][15];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][16])) {
                                echo $alamat[$n][16];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][17])) {
                                echo $alamat[$n][17];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($alamat[$n][18])) {
                                echo $alamat[$n][18];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($alamat[$n][19])) {
                                echo $alamat[$n][19];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][20])) {
                                echo $alamat[$n][20];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][21])) {
                                echo $alamat[$n][21];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($alamat[$n][22])) {
                                echo $alamat[$n][22];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                            <?php
                            if (isset($alamat[$n][23])) {
                                echo $alamat[$n][23];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][24])) {
                                echo $alamat[$n][24];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][25])) {
                                echo $alamat[$n][25];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][26])) {
                                echo $alamat[$n][26];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][27])) {
                                echo $alamat[$n][27];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                            if (isset($alamat[$n][28])) {
                                echo $alamat[$n][28];
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="border: 1px solid black; border-collapse: collapse; font-size: 12px; margin-top: 8px; margin-left: 25px; width: 95.5%">
        <tr>
            <th style="background-color: #ccccb3; width: 6.6%; text-align: center; border: 1px solid black; padding: -2px 0px 5px 0px; height: 80px">No</th>
            <th style="background-color: #ccccb3; width: 35.7%; text-align: center; border: 1px solid black; padding: -2px 0px 5px 0px; height: 80px; letter-spacing: 3px">Uraian</th>
            <th style="background-color: #ccccb3; width: 23.5%; text-align: center; border: 1px solid black; padding: -2px 0px 5px 0px; height: 80px">Harga(Rp)</th>
            <th style="background-color: #ccccb3; width: 10.1%; text-align: center; font-size: 8.5px;border: 1px solid black; padding: 13px 5px 15px 5px; height: 80px">Tarif Lebih Tinggi 100% (Tdk ber-NPWP)</th>
            <th style="background-color: #ccccb3; width: 10.2%; text-align: center; border: 1px solid black; padding: 3px 12px 0px 12px; height: 80px">Tarif (%)</th>
            <th style="background-color: #ccccb3; text-align: center; border: 1px solid black; padding: 5px 12px 0px 12px; height: 80px">PPh yang dipungut (Rp)</th>
        </tr>
        <tr>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(1)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(2)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(3)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(4)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(5)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(6)</th>
        </tr>
        <tr>
            <td style="text-align: left; height: 18px; border-right: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <th style="height: 18px; padding: -1px 0px 4px 5px;">Jenis Industri : </th>
            <th style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 5px;">
                Penjualan Bruto :  
            </th>
            <td style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                1.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">Semen</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                2.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">Kertas</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                3.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">Baja</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                4.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">Otomotif</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                5.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">............................</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                6.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">............................</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
            <td style="height: 18px; padding: 2px 8px 0px 5px; font-weight: bold; font-size: 12px;">
                Penjualan Barang yang Tergolong Sangat Mewah: 
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 5px; font-weight: bold; font-size: 12.5px">
                Harga Jual :
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                7.
            </td>
            <td style="height: 20px; padding: 2px 0px 0px 5px; letter-spacing: 2px">............................</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>       
        <tr>
            <td style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
            <td style="height: 18px; padding: 0px 8px 0px 5px; font-weight: bold; font-size: 12px">
                Industri/Eksportir : 
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 5px; font-weight: bold; font-size: 12.5px">
                Pembelian Bruto :
            </td>
            <td style="text-align: center; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                8.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 1px">Sektor  .............................</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                9.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 1px">Sektor  .............................</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
            <td style="height: 18px; padding: 0px 8px 0px 5px; font-weight: bold; font-size: 12px">
                Badan Tertentu Lainnya : 
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 5px; font-weight: bold; font-size: 12.5px">
            </td>
            <td style="text-align: center; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                10.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px;">BUMN TERTENTU</td>
            <td style="height: 20px; border: 1px solid black; padding: 0px 4px 0px 0px; text-align: right;"><?php
                if (isset($jumlahBruto[$n])) {
                    $harga[10] = $jumlahBruto[$n];
                    echo number_format($harga[10], '0', '', '.');
                } else {
                    echo $hargaSepuluh[$n] = '';
                }
                ?>
            </td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?php
                if ($statusNpwpWp[$n] == 1) {
                    $a = Html::img("@web/img/square.png", ['width' => '15px', 'height' => '15px']);
                    echo $a;
                } else {
                    $b = Html::img("@web/img/square - selected.png", ['width' => '15px', 'height' => '15px']);
                    echo $b;
                }
                ?>
            </td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 2px 0px 2px 0px;"><?php
                echo number_format($tarif[$n], '2', ',', '');
                ?></td>
            <td style="text-align: right; height: 20px; border: 1px solid black; padding: 0px 4px 0px 0px;"><?php
                if (isset($pphDipungut[$n])) {
                    $pph[10] = $pphDipungut[$n];
                    echo number_format($pph[10], '0', '', '.');
                } else {
                    echo $pph[10] = '';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; border-bottom: 1px solid black; padding: 2px 0px 0px 0px;">
                11.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; border-bottom: 1px solid black;">............................</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px; letter-spacing: 2px; font-weight: bold">JUMLAH</td>
            <td style="height: 18px; padding: 2px 4px 0px 5px; text-align: right;"><?php
                $jumlahHarga = 0;
                for ($i = 1; $i <= 11; $i++) {
                    if (isset($harga[$i])) {
                        $jumlahHarga = $jumlahHarga + $harga[$i];
                    }
                }
                echo number_format($jumlahHarga, '0', '', '.');
                ?>            
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="height: 18px; padding: 2px 4px 0px 5px; text-align: right;"><?php
                $jumlahPph = 0;
                for ($j = 1; $j <= 11; $j++) {
                    if (isset($pph[$j])) {
                        $jumlahPph = $jumlahPph + $pph[$j];
                    }
                }
                echo number_format($jumlahPph, '0', '', '.');
                ?>        
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left; height: 20px; border-right: 1px solid black; border-top: 1px solid black; padding: 0px 0px 0px 4px;">
                Terbilang :  ==  <?php echo terbilang($jumlahPph, $style = 1) ?> RUPIAH ==
            </td>
        </tr>
    </table>

    <div style="overflow: hidden; margin: 2px 0px 0px 6px; padding: 0px 0px 2px 0px">
        <div class="floatLeft" style="width: 36.5%; float: left;">
            <table width="100%" style="font-size: 12px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left">
            </table>
        </div>
        <div class="floatRight" style="width: 63.5%; float: right;">
            <table width="89.4%" style="font-size: 12px; padding: 8px 4px 2px 4px; float: left; text-align: center">
                <tr>
                    <td style="font-weight: bold"><?php echo $kota[$n] ?>, <?php
                        $fixDate = date("d F Y", strtotime($tanggal[$n]));
                        $month = preg_replace("/[^A-Za-z?!]/", '', $fixDate);
                        $transMonth = \Yii::t('app', $month);
                        $fixDate = str_replace($month, $transMonth, $fixDate);
                        echo $fixDate;
                        ?></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; padding-top: 10px">Pemungut Pajak,</td>
                </tr>
            </table>
        </div>
    </div>
    <table style="margin-left: 245px; border-collapse: collapse; width: 40%">
        <tr>
            <td style = "letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
            <td width="17px" style="height: 12px; padding-right: 10px">:</td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[$n][0])) {
                    echo $npwpPemungutPajak[0];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[1])) {
                    echo $npwpPemungutPajak[1];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[2])) {
                    echo $npwpPemungutPajak[2];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[3])) {
                    echo $npwpPemungutPajak[3];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[4])) {
                    echo $npwpPemungutPajak[4];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[5])) {
                    echo $npwpPemungutPajak[5];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[6])) {
                    echo $npwpPemungutPajak[6];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[7])) {
                    echo $npwpPemungutPajak[7];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[8])) {
                    echo $npwpPemungutPajak[8];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[9])) {
                    echo $npwpPemungutPajak[9];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[10])) {
                    echo $npwpPemungutPajak[10];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[11])) {
                    echo $npwpPemungutPajak[11];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[12])) {
                    echo $npwpPemungutPajak[12];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[13])) {
                    echo $npwpPemungutPajak[13];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($npwpPemungutPajak[14])) {
                    echo $npwpPemungutPajak[14];
                }
                ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style = "height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
            <td width="17px" style="height: 12px; padding-right: 10px">:</td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[0])) {
                    echo $namaPemungutPajak[0];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[1])) {
                    echo $namaPemungutPajak[1];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[2])) {
                    echo $namaPemungutPajak[2];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[3])) {
                    echo $namaPemungutPajak[3];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[4])) {
                    echo $namaPemungutPajak[4];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[5])) {
                    echo $namaPemungutPajak[5];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[6])) {
                    echo $namaPemungutPajak[6];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[7])) {
                    echo $namaPemungutPajak[7];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[8])) {
                    echo $namaPemungutPajak[8];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[9])) {
                    echo $namaPemungutPajak[9];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[10])) {
                    echo $namaPemungutPajak[10];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[11])) {
                    echo $namaPemungutPajak[11];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[12])) {
                    echo $namaPemungutPajak[12];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[13])) {
                    echo $namaPemungutPajak[13];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[14])) {
                    echo $namaPemungutPajak[14];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[15])) {
                    echo $namaPemungutPajak[15];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[16])) {
                    echo $namaPemungutPajak[16];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                if (isset($namaPemungutPajak[17])) {
                    echo $namaPemungutPajak[17];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                <?php
                if (isset($namaPemungutPajak[18])) {
                    echo $namaPemungutPajak[18];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                <?php
                if (isset($namaPemungutPajak[19])) {
                    echo $namaPemungutPajak[19];
                }
                ?>
            </td>
        </tr>
    </table>
    <div style="overflow: hidden; margin: 2px 0px 0px 25px">
        <div class="floatLeft" style="width: 34.2%; float: left; margin-left: 15px">
            <table width="100%" style="font-size: 11px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left">
                <tr>
                    <td></td>
                    <td style="padding-left: -10px">Perhatian:</td>
                </tr>
                <tr>
                    <td style="width: 4px; padding: -3px 0px 0px 4px">1. </td>
                    <td rowspan="6" style="padding: 0px 0px 0px 6px">Jumlah PPh Pasal 22 yang dipungut di atas merupakan pembayaran di muka atas PPh yang terutang untuk tahun pajak yang bersangkutan. Simpanlah Bukti Pemungutan ini baik-baik untuk diperhitungkan sebagai kredit pajak dalam Surat Pemberitahuan (SPT) Tahunan PPh</td>
                </tr>
                <tr>
                    <td style="width: 4px;"> </td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px; padding-top: -10px">2. </td>
                    <td>Bukti Pemungutan ini dianggap sah apabila diisi dengan lengkap dan benar</td>
                </tr>

            </table>
        </div>
        <div class="floatRight" style="width: 63.5%; float: right;"> 
            <table width="89.4%" style="font-size: 12px; padding: 8px 4px 2px 4px; float: left; text-align: center;">
                <tr>
                    <td style="font-weight: bold">Tanda Tangan, Nama dan Cap</td>
                </tr>
                <tr>
                    <td style="padding-top: 98px"><?php
                        echo $namaPenanggungJawab[$n];
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php
                        echo $jabatan[$n];
                        ?></td>
                </tr>
            </table>
        </div>
    </div>
    <!--    <div class="footer-main">-->
    <!--<div class="floatLeft" style="width: 34.2%; float: left; font-weight: bold; font-size: 9.7px; margin: 0px 0px 0px 26px">-->

    <!--        <div style=" float: right; font-weight: bold; font-size: 8.4px;">
            <div class="floatRight" style="float: right; text-align: right; font-weight: bold; font-size: 8.4px; margin: 0px 0px 0px 0px; padding-right: 29px">
                Lampiran III.3 Peraturan Direktur Jenderal Pajak Nomor PER-53/PJ/2009
            </div>-->
    <!--</div>-->
    <div class="footer-m">
        F.1.1.33.04
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        Lampiran III.3 Peraturan Direktur Jenderal Pajak Nomor PER-53/PJ/2009
    </div>
    <div class="footer-f"></div>
    <div class="footer-s"></div>

<?php } ?>
</body>
</html>

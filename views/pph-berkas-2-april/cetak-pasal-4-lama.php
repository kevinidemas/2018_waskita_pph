<?php

//use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formatter = Yii::$app->formatter;
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];

function kekata($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = kekata($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x < 1000) {
        $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x < 1000000) {
        $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
    } else if ($x < 1000000000) {
        $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
    }
    return $temp;
}

function terbilang($x, $style = 4) {
    if ($x < 0) {
        $hasil = "minus " . trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}
?>
<style>
    .header-f{
        width:4.1%;
        height:10px;
        background:#000;
        position:fixed;
        top:0;
        left:23;
        margin: -52px 0px 0px 0px;
    }
    .header-s{
        width:4.1%;
        height:10px;
        background:#000;
        position:fixed;
        top:0;
        left:645;
        margin: -52px 0px 0px 0px;
    }
    .footer-f{
        width:4.1%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:-9;
        left:23;
        margin: -52px 0px -52px 0px;
    }
    .footer-s{
        width:4.1%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:-9;
        left:645;
        margin: -52px 0px -52px 0px;
    }
    .header{
        position:fixed;
        /*top:-20;*/
        left: 462;
        font-family: fantasy;
        font-size: 8px;
        font-weight: bold;
        margin: -55px 0px 0px 0px;
    }
    .footer-m{
        font-weight: bold; 
        font-size: 9.7px;
        padding-bottom: -29px;
        position:fixed;
        bottom:-22;
        left:23;
        margin: 0px -50px 0px 2px;
    }
</style>
<html>
    <?php for ($n = 0; $n < $countPdf; $n++) { ?>
        <style font-size="20px"></style>
        <body>
            <div style="height: 10px"></div>
            <div class="header-f"></div>
            <div class="header-s"></div>
            <div class="header">
                <p>Lembar ke-1 untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Wajib Pajak</p>
                <p>Lembar ke-2 untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Kantor Pelayanan Pajak</p>
                <p>Lembar ke-3 untuk &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;Pemungut Pajak</p>
            </div>
            <table style="margin-left: 36px; font-size: 11.7px; padding-top: 0px">
                <tr>
                    <td style="width: 18%">
                        <?= Html::img("@web/img/djp.png", ['width' => '60px', 'height' => '50px', 'margin-top' => '-10px']) ?>
                    </td>
                    <td style="padding-top: 12px; padding-left: 10px;">
                <font align="left"><center><b>DEPARTEMEN KEUANGAN REPUBLIK INDONESIA</b></center>
                <font align="left"><center><b>DIREKTORAT JENDERAL PAJAK</b></center>
                <font align="left"><center><b>KANTOR PELAYANAN PAJAK</b></center>
                <font align="left"><center><b><?php echo $kppTerdaftar[$n] ?></b></center>
            </td>
        </tr>
    </table>

    <?php
    if ($jenisBp[$n] == 3) {
        ?>
                                                                           <!--<table style="width: 75%; border: 1px solid black; border-collapse: collapse; margin-left: 135px; margin-top: 10px; background-color: #ccccb3">-->
        <table style="width: 75%; border: 1px solid black; border-collapse: collapse; margin-left: 159px; margin-top: 30px; background-color: #ccccb3">
            <tr style="width: 20%">
                <td>
            <font size="1"><center><b>BUKTI PEMUNGUTAN PPh PASAL 4 AYAT (2)</b></center>
        </td>
        </tr>
        <tr>
            <td>
        <font size="1"><center><b>ATAS PENGHASILAN DARI USAHA JASA KONSTRUKSI</b></center>
        </td>
        </tr>
        <tr>        
            <td style="border-top: 1px solid black;">
        <font size="1"><center><b>NOMOR : <?php echo $nomorBuktiPotong[$n] ?></b></center>
        </td>

        </tr>
        </table>
        <!--<HR><HR>-->
        <!--<hr style="height: 5px" />-->

        <table class="good" border="none" width="100%" style="margin-left: 40px; margin-top: 20px; height: 3px;">
            <tr>
                <td width="280px" margin-right="1000px">
                    <table style=" border-collapse: collapse;">
                        <tr>
                            <td style = "width: 96px; letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][0])) {
                                    echo $npwp[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][1])) {
                                    echo $npwp[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][2])) {
                                    echo $npwp[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][3])) {
                                    echo $npwp[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][4])) {
                                    echo $npwp[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][5])) {
                                    echo $npwp[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][6])) {
                                    echo $npwp[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][7])) {
                                    echo $npwp[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][8])) {
                                    echo $npwp[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][9])) {
                                    echo $npwp[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][10])) {
                                    echo $npwp[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][11])) {
                                    echo $npwp[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][12])) {
                                    echo $npwp[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][13])) {
                                    echo $npwp[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][14])) {
                                    echo $npwp[$n][14];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style = "width: 96px; height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][0])) {
                                    echo $nama[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][1])) {
                                    echo $nama[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][2])) {
                                    echo $nama[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][3])) {
                                    echo $nama[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][4])) {
                                    echo $nama[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][5])) {
                                    echo $nama[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][6])) {
                                    echo $nama[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][7])) {
                                    echo $nama[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][8])) {
                                    echo $nama[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][9])) {
                                    echo $nama[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][10])) {
                                    echo $nama[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][11])) {
                                    echo $nama[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][12])) {
                                    echo $nama[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][13])) {
                                    echo $nama[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][14])) {
                                    echo $nama[$n][14];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][15])) {
                                    echo $nama[$n][15];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][16])) {
                                    echo $nama[$n][16];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][17])) {
                                    echo $nama[$n][17];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][18])) {
                                    echo $nama[$n][18];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][19])) {
                                    echo $nama[$n][19];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][20])) {
                                    echo $nama[$n][20];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][21])) {
                                    echo $nama[$n][21];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][22])) {
                                    echo $nama[$n][22];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][23])) {
                                    echo $nama[$n][23];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][24])) {
                                    echo $nama[$n][24];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][25])) {
                                    echo $nama[$n][25];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][26])) {
                                    echo $nama[$n][26];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][27])) {
                                    echo $nama[$n][27];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][28])) {
                                    echo $nama[$n][28];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style = "width: 96px;height: 12px"><b><font align="left" size="2">Alamat</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][0])) {
                                    echo $alamat[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][1])) {
                                    echo $alamat[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][2])) {
                                    echo $alamat[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][3])) {
                                    echo $alamat[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][4])) {
                                    echo $alamat[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][5])) {
                                    echo $alamat[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][6])) {
                                    echo $alamat[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][7])) {
                                    echo $alamat[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][8])) {
                                    echo $alamat[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][9])) {
                                    echo $alamat[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][10])) {
                                    echo $alamat[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][11])) {
                                    echo $alamat[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][12])) {
                                    echo $alamat[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][13])) {
                                    echo $alamat[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][14])) {
                                    echo $alamat[$n][14];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][15])) {
                                    echo $alamat[$n][15];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][16])) {
                                    echo $alamat[$n][16];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][17])) {
                                    echo $alamat[$n][17];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][18])) {
                                    echo $alamat[$n][18];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][19])) {
                                    echo $alamat[$n][19];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][20])) {
                                    echo $alamat[$n][20];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][21])) {
                                    echo $alamat[$n][21];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][22])) {
                                    echo $alamat[$n][22];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][23])) {
                                    echo $alamat[$n][23];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][24])) {
                                    echo $alamat[$n][24];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][25])) {
                                    echo $alamat[$n][25];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][26])) {
                                    echo $alamat[$n][26];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][27])) {
                                    echo $alamat[$n][27];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][28])) {
                                    echo $alamat[$n][28];
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="border: 1px solid black; font-family: fantasy; border-collapse: collapse; font-size: 10px; margin-top: 40px; margin-left: 25px; width: 95.5%">
            <tr>
                <th style="background-color: #ccccb3; width: 6.6%; text-align: center; border: 1px solid black; padding: 3px 0px 5px 0px; height: 40px">No</th>
                <th style="background-color: #ccccb3; width: 38.7%; text-align: center; border: 1px solid black; padding: 3px 0px 5px 0px; height: 40px; letter-spacing: 3px">Uraian</th>
                <th style="background-color: #ccccb3; width: 23.5%; text-align: center; border: 1px solid black; padding: 3px 19px 5px 19px; height: 40px">Jumlah Nilai Bruto (Rp)</th>
                <th style="background-color: #ccccb3; width: 7.2%; text-align: center; border: 1px solid black; font-size: 10px; padding: -1px 10px 0px 10px; height: 40px">Tarif (%)</th>
                <th style="background-color: #ccccb3; text-align: center; border: 1px solid black; padding: -1px 12px 0px 12px; height: 40px">PPh yang Dipotong/ Dipungut (Rp)</th>
            </tr>
            <tr>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(1)</th>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(2)</th>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(3)</th>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(4)</th>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(5)</th>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                    1.
                </td>
                <td style="height: 22px; padding: 2px 0px 0px 5px;">Jasa pelaksanaan konstruksi oleh penyedia jasa</td>
                <th style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;"></th>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
                <td style="height: 22px; padding: -6px 0px 0px 5px; margin-top: -2px">dengan kualifikasi usaha kecil</td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 1) {
                            if ($arrayStatusIujk[$n] == 1) {
                                if ($arrayLevel[$n] == 1) {
                                    echo number_format($jumlahBruto[$n], '0', '', '.');
                                }
                            }
                        }
                    }
                    ?>
                </td>
                <td style="text-align: center; height: 22px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 1) {
                            if ($arrayStatusIujk[$n] == 1) {
                                if ($arrayLevel[$n] == 1) {
                                    echo number_format($tarif[$n], '2', ',', '');
                                } else {
                                    echo '2%';
                                }
                            } else {
                                echo '2%';
                            }
                        } else {
                            echo '2%';
                        }
                    }
                    ?>
                </td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 1) {
                            if ($arrayStatusIujk[$n] == 1) {
                                if ($arrayLevel[$n] == 1) {
                                    echo number_format($pphDipungut[$n], '0', '', '.');
                                }
                            }
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                    2.
                </td>
                <td style="height: 22px; padding: 2px 0px 0px 5px;">Jasa pelaksanaan konstruksi oleh penyedia jasa</td>
                <th style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;"></th>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
                <td style="height: 22px; padding: -6px 0px 0px 5px; margin-top: -2px">yang tidak memiliki kualifikasi usaha</td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 0) {
                            echo number_format($jumlahBruto[$n], '0', '', '.');
                        } else {
                            if ($arrayStatusIujk[$n] == 0) {
                                echo number_format($jumlahBruto[$n], '0', '', '.');
                            }
                        }
                    }
                    ?>
                </td>
                <td style="text-align: center; height: 22px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 0) {
                            echo number_format($tarif[$n], '2', ',', '');
                        } else {
                            echo '4%';
                        }
                    }
                    ?>
                </td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 0) {
                            echo number_format($pphDipungut[$n], '0', '', '.');
                        } else {
                            if ($arrayStatusIujk[$n] == 0) {
                                echo number_format($pphDipungut[$n], '0', '', '.');
                            }
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                    3.
                </td>
                <td style="height: 22px; padding: 2px 0px 0px 5px;">Jasa pelaksanaan konstruksi oleh penyedia jasa</td>
                <th style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;"></th>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
                <td style="height: 22px; padding: -6px 0px 0px 5px; margin-top: -2px">selain angka 1 dan angka 2 di atas</td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 1) {
                            if ($arrayStatusIujk[$n] == 1) {
                                if ($arrayLevel[$n] == 2) {
                                    echo number_format($jumlahBruto[$n], '0', '', '.');
                                }
                            }
                        }
                    }
                    ?>
                </td>
                <td style="text-align: center; height: 22px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 1) {
                            if ($arrayStatusIujk[$n] == 1) {
                                if ($arrayLevel[$n] == 2) {
                                    echo number_format($tarif[$n], '2', ',', '');
                                } else {
                                    echo '3%';
                                }
                            } else {
                                echo '3%';
                            }
                        } else {
                            echo '3%';
                        }
                    }
                    ?>
                </td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    if ($jenisBp[$n] == 3) {
                        if ($arrayIujkExist[$n] == 1) {
                            if ($arrayStatusIujk[$n] == 1) {
                                if ($arrayLevel[$n] == 2) {
                                    echo number_format($pphDipungut[$n], '0', '', '.');
                                }
                            }
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                    4.
                </td>
                <td style="height: 22px; padding: 2px 0px 0px 5px;">Jasa perencanaan atau pengawasan konstruksi</td>
                <th style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;"></th>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
                <td style="height: 22px; padding: -6px 0px 0px 5px; margin-top: -2px">oleh penyedia jasa yang memiliki kualifikasi usaha</td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;">
                    <?php
                    if ($jenisBp[$n] == 4) {
                        if ($arrayBujpkExist[$n] == 1) {
                            if ($arrayStatusBujpk[$n] == 1) {
                                echo number_format($jumlahBruto[$n], '0', '', '.');
                            }
                        }
                    }
                    ?>
                </td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;">
                    <?php
                    if ($jenisBp[$n] == 4) {
                        if ($arrayBujpkExist[$n] == 1) {
                            if ($arrayStatusBujpk[$n] == 1) {
                                echo number_format($tarif[$n], '2', ',', '');
                            } else {
                                echo '4%';
                            }
                        }
                    }
                    ?>
                </td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;">
                    <?php
                    if ($jenisBp[$n] == 4) {
                        if ($arrayBujpkExist[$n] == 1) {
                            if ($arrayStatusBujpk[$n] == 1) {
                                echo number_format($pphDipungut[$n], '0', '', '.');
                            }
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black;  padding: 2px 0px 0px 0px;">
                    5.
                </td>
                <td style="height: 22px; padding: 2px 0px 0px 5px;">Jasa perencanaan atau pengawasan konstruksi</td>
                <th style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;"></th>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: center; height: 22px; border-right: 1px solid black; border-bottom: 1px solid black; padding: 2px 0px 0px 0px;"></td>
                <td style="height: 22px; padding: -6px 0px 0px 5px; margin-top: -2px; border-bottom: 1px solid black;">oleh penyedia jasa yang tidak memiliki kualifikasi usaha</td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 5px;">
                    <?php
                     if ($jenisBp[$n] == 4) {
                        if ($arrayBujpkExist[$n] == 1) {
                            if ($arrayStatusBujpk[$n] == 0) {
                                echo number_format($jumlahBruto[$n], '0', '', '.');
                            }
                        } else {
                            echo number_format($jumlahBruto[$n], '0', '', '.');
                        }
                    }
                    ?>
                </td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;">
                    <?php
                    if ($jenisBp[$n] == 4) {
                        if ($arrayBujpkExist[$n] == 1) {
                            if ($arrayStatusBujpk[$n] == 0) {
                                echo number_format($tarif[$n], '2', ',', '');
                            } else {
                                echo '6%';
                            }
                        } else {
                            echo number_format($tarif[$n], '2', ',', '');
                        }
                    }
                    ?>
                </td>
                <td style="background-color: #ccccb3; text-align: left; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;">
                    <?php
                    if ($jenisBp[$n] == 4) {
                        if ($arrayBujpkExist[$n] == 1) {
                            if ($arrayStatusBujpk[$n] == 0) {
                                echo number_format($pphDipungut[$n], '0', '', '.');
                            }
                        } else {
                            echo number_format($pphDipungut[$n], '0', '', '.');
                        }
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center; height: 22px; border-right: 1px solid black; padding: 2px 0px 0px 0px; letter-spacing: 2px; font-weight: bold">JUMLAH</td>
                <td style="height: 22px; padding: 2px 4px 0px 5px; text-align: right;">
                    <?php
                    echo number_format($jumlahBruto[$n], '0', '', '.');
                    ?>            
                </td>
                <td style="text-align: left; background-color: #ccccb3; height: 22px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
                <td style="height: 22px; padding: 2px 4px 0px 5px; text-align: right;">
                    <?php
                    echo number_format($pphDipungut[$n], '0', '', '.');
                    ?>        
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left; height: 22px; border-right: 1px solid black; border-top: 1px solid black; padding: 0px 0px 0px 4px;">
                    Terbilang :  ==  <?php echo terbilang($pphDipungut[$n], $style = 1) ?> RUPIAH ==
                </td>
            </tr>
        </table>

        <div style="overflow: hidden; margin: 2px 0px 0px 6px; padding: 0px 0px 2px 0px">
            <div class="floatLeft" style="width: 36.5%; float: left;">
                <table width="100%" style="font-size: 12px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left">
                </table>
            </div>
            <div class="floatRight" style="width: 63.5%; float: right; margin: 40px 0px 0px 0px">
                <table width="89.4%" style="font-size: 12px; padding: 8px 4px 2px 4px; float: left; text-align: center; margin-left: 15px">
                    <tr>
                        <td style="font-weight: bold"><?php echo $kota[$n] ?>, <?php
                            $fixDate = date("d F Y", strtotime($tanggal[$n]));
                            $month = preg_replace("/[^A-Za-z?!]/", '', $fixDate);
                            $transMonth = \Yii::t('app', $month);
                            $fixDate = str_replace($month, $transMonth, $fixDate);
                            echo $fixDate;
                            ?></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; padding-top: 10px">Pemotong/Pemungut Pajak,</td>
                    </tr>
                </table>
            </div>
        </div>
        <table style="margin: 10px 0px 0px 245px; border-collapse: collapse; width: 40%">
            <tr>
                <td style = "letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
                <td width="17px" style="height: 12px; padding-right: 10px">:</td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][0])) {
                        echo $npwpPemungutPajak[$n][0];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][1])) {
                        echo $npwpPemungutPajak[$n][1];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][2])) {
                        echo $npwpPemungutPajak[$n][2];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][3])) {
                        echo $npwpPemungutPajak[$n][3];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][4])) {
                        echo $npwpPemungutPajak[$n][4];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][5])) {
                        echo $npwpPemungutPajak[$n][5];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][6])) {
                        echo $npwpPemungutPajak[$n][6];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][7])) {
                        echo $npwpPemungutPajak[$n][7];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][8])) {
                        echo $npwpPemungutPajak[$n][8];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][9])) {
                        echo $npwpPemungutPajak[$n][9];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][10])) {
                        echo $npwpPemungutPajak[$n][10];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][11])) {
                        echo $npwpPemungutPajak[$n][11];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][12])) {
                        echo $npwpPemungutPajak[$n][12];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][13])) {
                        echo $npwpPemungutPajak[$n][13];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][14])) {
                        echo $npwpPemungutPajak[$n][14];
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style = "height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
                <td width="17px" style="height: 12px; padding-right: 10px">:</td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[0])) {
                        echo $namaPemungutPajak[0];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[1])) {
                        echo $namaPemungutPajak[1];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[2])) {
                        echo $namaPemungutPajak[2];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[3])) {
                        echo $namaPemungutPajak[3];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[4])) {
                        echo $namaPemungutPajak[4];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[5])) {
                        echo $namaPemungutPajak[5];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[6])) {
                        echo $namaPemungutPajak[6];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[7])) {
                        echo $namaPemungutPajak[7];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[8])) {
                        echo $namaPemungutPajak[8];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[9])) {
                        echo $namaPemungutPajak[9];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[10])) {
                        echo $namaPemungutPajak[10];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[11])) {
                        echo $namaPemungutPajak[11];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[12])) {
                        echo $namaPemungutPajak[12];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[13])) {
                        echo $namaPemungutPajak[13];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[14])) {
                        echo $namaPemungutPajak[14];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[15])) {
                        echo $namaPemungutPajak[15];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[16])) {
                        echo $namaPemungutPajak[16];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[17])) {
                        echo $namaPemungutPajak[17];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                    <?php
                    if (isset($namaPemungutPajak[18])) {
                        echo $namaPemungutPajak[18];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                    <?php
                    if (isset($namaPemungutPajak[19])) {
                        echo $namaPemungutPajak[19];
                    }
                    ?>
                </td>
            </tr>
        </table>
        <div style="overflow: hidden; margin: 50px 0px 0px 25px">
            <div class="floatLeft" style="width: 34.2%; float: left; margin-left: 15px">
                <table width="100%" style="font-size: 11px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left; margin-top: 40px">
                    <tr>
                        <td></td>
                        <td style="padding-left: -10px; font-style: italic">Perhatian:</td>
                    </tr>
                    <tr>
                        <td style="width: 4px; padding: -3px 0px 0px 2px">1. </td>
                        <td rowspan="6" style="padding: 0px 0px 0px 6px">Jumlah Pajak Penghasilan dari Jasa Konstruksi yang dipotong/dipungut di atas bukan merupakan kredit pajak dalam Surat Pemberitahuan (SPT) Tahunan PPh</td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"> </td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px; padding: -10px 0px 0px 2px">2. </td>
                        <td style="padding: 0px 0px 0px 6px">Bukti Pemotongan/Pemungutan ini dianggap sah apabila diisi dengan lengkap dan benar</td>
                    </tr>

                </table>
            </div>
            <div style="width: 50%;"> 
                <table width="100%" style="font-size: 12px; padding: 8px 4px 2px 4px; text-align: center; margin-left: 86px">
                    <tr>
                        <td style="font-weight: bold">Tanda Tangan, Nama dan Cap</td>
                    </tr>
                    <tr>
                        <td style="padding-top: 98px"><?php
                            echo $namaPenanggungJawab[$n];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php
                            echo $jabatan[$n];
                            ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <!--    <div class="footer-main">-->
        <!--<div class="floatLeft" style="width: 34.2%; float: left; font-weight: bold; font-size: 9.7px; margin: 0px 0px 0px 26px">-->

        <!--        <div style=" float: right; font-weight: bold; font-size: 8.4px;">
                <div class="floatRight" style="float: right; text-align: right; font-weight: bold; font-size: 8.4px; margin: 0px 0px 0px 0px; padding-right: 29px">
                    Lampiran III.3 Peraturan Direktur Jenderal Pajak Nomor PER-53/PJ/2009
                </div>-->
        <!--</div>-->
        <div class="footer-m">
            F.1.1.33.16
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div class="footer-f"></div>
        <div class="footer-s"></div>

        <?php
    } else {
        ?>
        <table style="width: 75%; border: 1px solid black; border-collapse: collapse; margin-left: 159px; margin-top: 30px; background-color: #ccccb3">
            <tr style="width: 20%">
                <td>
            <font size="1"><center><b>BUKTI PEMUNGUTAN PPh PASAL 4 AYAT (2)</b></center>
        </td>
        </tr>
        <tr>
            <td>
        <font size="1"><center><b>ATAS PENGHASILAN DARI PERSEWAAN TANAH</b></center>
        </td>
        </tr>
        <tr>
            <td>
        <font size="1"><center><b>DAN/ATAU BANGUNAN</b></center>
        </td>
        </tr>
        <tr>        
            <td style="border-top: 1px solid black;">
        <font size="1"><center><b>NOMOR : <?php echo $nomorBuktiPotong[$n] ?></b></center>
        </td>

        </tr>
        </table>
        <table class="good" border="none" width="100%" style="margin-left: 40px; margin-top: 20px; height: 3px;">
            <tr>
                <td width="280px" margin-right="1000px">
                    <table style=" border-collapse: collapse;">
                        <tr>
                            <td style = "width: 96px; letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][0])) {
                                    echo $npwp[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][1])) {
                                    echo $npwp[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][2])) {
                                    echo $npwp[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][3])) {
                                    echo $npwp[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][4])) {
                                    echo $npwp[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][5])) {
                                    echo $npwp[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][6])) {
                                    echo $npwp[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][7])) {
                                    echo $npwp[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][8])) {
                                    echo $npwp[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][9])) {
                                    echo $npwp[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][10])) {
                                    echo $npwp[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][11])) {
                                    echo $npwp[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; width: 15px"></td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][12])) {
                                    echo $npwp[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][13])) {
                                    echo $npwp[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($npwp[$n][14])) {
                                    echo $npwp[$n][14];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style = "width: 96px; height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][0])) {
                                    echo $nama[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][1])) {
                                    echo $nama[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][2])) {
                                    echo $nama[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][3])) {
                                    echo $nama[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][4])) {
                                    echo $nama[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][5])) {
                                    echo $nama[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][6])) {
                                    echo $nama[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][7])) {
                                    echo $nama[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][8])) {
                                    echo $nama[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][9])) {
                                    echo $nama[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][10])) {
                                    echo $nama[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][11])) {
                                    echo $nama[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][12])) {
                                    echo $nama[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][13])) {
                                    echo $nama[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][14])) {
                                    echo $nama[$n][14];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][15])) {
                                    echo $nama[$n][15];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][16])) {
                                    echo $nama[$n][16];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][17])) {
                                    echo $nama[$n][17];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][18])) {
                                    echo $nama[$n][18];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][19])) {
                                    echo $nama[$n][19];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][20])) {
                                    echo $nama[$n][20];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][21])) {
                                    echo $nama[$n][21];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][22])) {
                                    echo $nama[$n][22];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($nama[$n][23])) {
                                    echo $nama[$n][23];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][24])) {
                                    echo $nama[$n][24];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][25])) {
                                    echo $nama[$n][25];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][26])) {
                                    echo $nama[$n][26];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][27])) {
                                    echo $nama[$n][27];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($nama[$n][28])) {
                                    echo $nama[$n][28];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style = "width: 96px;height: 12px"><b><font align="left" size="2">Alamat</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][0])) {
                                    echo $alamat[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][1])) {
                                    echo $alamat[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][2])) {
                                    echo $alamat[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][3])) {
                                    echo $alamat[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][4])) {
                                    echo $alamat[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][5])) {
                                    echo $alamat[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][6])) {
                                    echo $alamat[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][7])) {
                                    echo $alamat[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][8])) {
                                    echo $alamat[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][9])) {
                                    echo $alamat[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][10])) {
                                    echo $alamat[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][11])) {
                                    echo $alamat[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][12])) {
                                    echo $alamat[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][13])) {
                                    echo $alamat[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][14])) {
                                    echo $alamat[$n][14];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][15])) {
                                    echo $alamat[$n][15];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][16])) {
                                    echo $alamat[$n][16];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][17])) {
                                    echo $alamat[$n][17];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][18])) {
                                    echo $alamat[$n][18];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][19])) {
                                    echo $alamat[$n][19];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][20])) {
                                    echo $alamat[$n][20];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][21])) {
                                    echo $alamat[$n][21];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][22])) {
                                    echo $alamat[$n][22];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($alamat[$n][23])) {
                                    echo $alamat[$n][23];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][24])) {
                                    echo $alamat[$n][24];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][25])) {
                                    echo $alamat[$n][25];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][26])) {
                                    echo $alamat[$n][26];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][27])) {
                                    echo $alamat[$n][27];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($alamat[$n][28])) {
                                    echo $alamat[$n][28];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style = "width: 96px;height: 12px"><b><font align="left" size="2">Lokasi Tanah dan</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][0])) {
                                    echo $lokasiTanah[$n][0];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][1])) {
                                    echo $lokasiTanah[$n][1];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][2])) {
                                    echo $lokasiTanah[$n][2];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][3])) {
                                    echo $lokasiTanah[$n][3];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][4])) {
                                    echo $lokasiTanah[$n][4];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][5])) {
                                    echo $lokasiTanah[$n][5];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][6])) {
                                    echo $lokasiTanah[$n][6];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][7])) {
                                    echo $lokasiTanah[$n][7];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][8])) {
                                    echo $lokasiTanah[$n][8];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][9])) {
                                    echo $lokasiTanah[$n][9];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][10])) {
                                    echo $lokasiTanah[$n][10];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][11])) {
                                    echo $lokasiTanah[$n][11];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][12])) {
                                    echo $lokasiTanah[$n][12];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][13])) {
                                    echo $lokasiTanah[$n][13];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][14])) {
                                    echo $lokasiTanah[$n][14];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][15])) {
                                    echo $lokasiTanah[$n][15];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][16])) {
                                    echo $lokasiTanah[$n][16];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][17])) {
                                    echo $lokasiTanah[$n][17];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][18])) {
                                    echo $lokasiTanah[$n][18];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][19])) {
                                    echo $lokasiTanah[$n][19];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][20])) {
                                    echo $lokasiTanah[$n][20];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][21])) {
                                    echo $lokasiTanah[$n][21];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][22])) {
                                    echo $lokasiTanah[$n][22];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][23])) {
                                    echo $lokasiTanah[$n][23];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][24])) {
                                    echo $lokasiTanah[$n][24];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][25])) {
                                    echo $lokasiTanah[$n][25];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][26])) {
                                    echo $lokasiTanah[$n][26];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][27])) {
                                    echo $lokasiTanah[$n][27];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][28])) {
                                    echo $lokasiTanah[$n][28];
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style = "width: 96px;height: 12px"><b><font align="left" size="2">atau Bangunan</font></b></td>
                            <td width="17px" style="height: 12px">:</td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][29])) {
                                    echo $lokasiTanah[$n][29];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][30])) {
                                    echo $lokasiTanah[$n][30];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][31])) {
                                    echo $lokasiTanah[$n][31];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][32])) {
                                    echo $lokasiTanah[$n][32];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][33])) {
                                    echo $lokasiTanah[$n][33];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][34])) {
                                    echo $lokasiTanah[$n][34];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][35])) {
                                    echo $lokasiTanah[$n][35];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][36])) {
                                    echo $lokasiTanah[$n][36];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][37])) {
                                    echo $lokasiTanah[$n][37];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][38])) {
                                    echo $lokasiTanah[$n][38];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][39])) {
                                    echo $lokasiTanah[$n][39];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][40])) {
                                    echo $lokasiTanah[$n][40];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][41])) {
                                    echo $lokasiTanah[$n][41];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][42])) {
                                    echo $lokasiTanah[$n][42];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][43])) {
                                    echo $lokasiTanah[$n][43];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][44])) {
                                    echo $lokasiTanah[$n][44];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][45])) {
                                    echo $lokasiTanah[$n][45];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][46])) {
                                    echo $lokasiTanah[$n][46];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][47])) {
                                    echo $lokasiTanah[$n][47];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][48])) {
                                    echo $lokasiTanah[$n][48];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][49])) {
                                    echo $lokasiTanah[$n][49];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][50])) {
                                    echo $lokasiTanah[$n][50];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][51])) {
                                    echo $lokasiTanah[$n][51];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                                <?php
                                if (isset($lokasiTanah[$n][52])) {
                                    echo $lokasiTanah[$n][52];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][53])) {
                                    echo $lokasiTanah[$n][53];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][54])) {
                                    echo $lokasiTanah[$n][54];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][55])) {
                                    echo $lokasiTanah[$n][55];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][56])) {
                                    echo $lokasiTanah[$n][56];
                                }
                                ?>
                            </td>
                            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                                if (isset($lokasiTanah[$n][57])) {
                                    echo $lokasiTanah[$n][57];
                                }
                                ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table style="border: 1px solid black; font-family: fantasy; border-collapse: collapse; font-size: 10px; margin-top: 40px; margin-left: 25px; width: 95.5%">
            <tr>
                <th style="background-color: #ccccb3; width: 46%; text-align: center; border: 1px solid black; padding: 3px 19px 5px 19px; height: 40px">Jumlah Nilai Bruto (Rp)</th>
                <th style="background-color: #ccccb3; width: 8%; text-align: center; border: 1px solid black; font-size: 10px; padding: -1px 10px 0px 10px; height: 40px">Tarif (%)</th>
                <th style="background-color: #ccccb3; width: 46%; text-align: center; border: 1px solid black; padding: -1px 12px 0px 12px; height: 40px">PPh yang Dipotong/ Dipungut (Rp)</th>
            </tr>
            <tr>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(1)</th>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(2)</th>
                <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(3)</th>
            </tr>   
            <tr>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    echo number_format($jumlahBruto[$n], '0', '', '.');
                    ?>
                </td>
                <td style="text-align: center; height: 22px; border: 1px solid black; padding: 0px 0px 0px 0px;">10,00%</td>
                <td style="text-align: right; height: 22px; border: 1px solid black; padding: 0px 5px 0px 0px;">
                    <?php
                    echo number_format($pphDipungut[$n], '0', '', '.');
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left; height: 22px; border-right: 1px solid black; border-top: 1px solid black; padding: 0px 0px 0px 4px;">
                    Terbilang :  ==  <?php echo terbilang($pphDipungut[$n], $style = 1) ?> RUPIAH ==
                </td>
            </tr>
        </table>

        <div style="overflow: hidden; margin: 2px 0px 0px 6px; padding: 0px 0px 2px 0px">
            <div class="floatLeft" style="width: 36.5%; float: left;">
                <table width="100%" style="font-size: 12px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left">
                </table>
            </div>
            <div class="floatRight" style="width: 63.5%; float: right; margin: 40px 0px 0px 0px">
                <table width="89.4%" style="font-size: 12px; padding: 8px 4px 2px 4px; float: left; text-align: center; margin-left: 15px">
                    <tr>
                        <td style="font-weight: bold"><?php echo $kota[$n] ?>, <?php
                            $fixDate = date("d F Y", strtotime($tanggal[$n]));
                            $month = preg_replace("/[^A-Za-z?!]/", '', $fixDate);
                            $transMonth = \Yii::t('app', $month);
                            $fixDate = str_replace($month, $transMonth, $fixDate);
                            echo $fixDate;
                            ?></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; padding-top: 10px">Pemotong/Pemungut Pajak,</td>
                    </tr>
                </table>
            </div>
        </div>
        <table style="margin: 10px 0px 0px 245px; border-collapse: collapse; width: 40%">
            <tr>
                <td style = "letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
                <td width="17px" style="height: 12px; padding-right: 10px">:</td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][0])) {
                        echo $npwpPemungutPajak[$n][0];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][1])) {
                        echo $npwpPemungutPajak[$n][1];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][2])) {
                        echo $npwpPemungutPajak[$n][2];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][3])) {
                        echo $npwpPemungutPajak[$n][3];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][4])) {
                        echo $npwpPemungutPajak[$n][4];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][5])) {
                        echo $npwpPemungutPajak[$n][5];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][6])) {
                        echo $npwpPemungutPajak[$n][6];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][7])) {
                        echo $npwpPemungutPajak[$n][7];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][8])) {
                        echo $npwpPemungutPajak[$n][8];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][9])) {
                        echo $npwpPemungutPajak[$n][9];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][10])) {
                        echo $npwpPemungutPajak[$n][10];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][11])) {
                        echo $npwpPemungutPajak[$n][11];
                    }
                    ?>
                </td>
                <td style="height: 12px; width: 15px"></td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][12])) {
                        echo $npwpPemungutPajak[$n][12];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][13])) {
                        echo $npwpPemungutPajak[$n][13];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($npwpPemungutPajak[$n][14])) {
                        echo $npwpPemungutPajak[$n][14];
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td style = "height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
                <td width="17px" style="height: 12px; padding-right: 10px">:</td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[0])) {
                        echo $namaPemungutPajak[0];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[1])) {
                        echo $namaPemungutPajak[1];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[2])) {
                        echo $namaPemungutPajak[2];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[3])) {
                        echo $namaPemungutPajak[3];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[4])) {
                        echo $namaPemungutPajak[4];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[5])) {
                        echo $namaPemungutPajak[5];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[6])) {
                        echo $namaPemungutPajak[6];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[7])) {
                        echo $namaPemungutPajak[7];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[8])) {
                        echo $namaPemungutPajak[8];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[9])) {
                        echo $namaPemungutPajak[9];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[10])) {
                        echo $namaPemungutPajak[10];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[11])) {
                        echo $namaPemungutPajak[11];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[12])) {
                        echo $namaPemungutPajak[12];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[13])) {
                        echo $namaPemungutPajak[13];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[14])) {
                        echo $namaPemungutPajak[14];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[15])) {
                        echo $namaPemungutPajak[15];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[16])) {
                        echo $namaPemungutPajak[16];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px"><?php
                    if (isset($namaPemungutPajak[17])) {
                        echo $namaPemungutPajak[17];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                    <?php
                    if (isset($namaPemungutPajak[18])) {
                        echo $namaPemungutPajak[18];
                    }
                    ?>
                </td>
                <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 12px">
                    <?php
                    if (isset($namaPemungutPajak[19])) {
                        echo $namaPemungutPajak[19];
                    }
                    ?>
                </td>
            </tr>
        </table>
        <div style="overflow: hidden; margin: 50px 0px 0px 25px">
            <div class="floatLeft" style="width: 34.2%; float: left; margin-left: 15px">
                <table width="100%" style="font-size: 11px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left; margin-top: 40px">
                    <tr>
                        <td></td>
                        <td style="padding-left: -10px; font-style: italic">Perhatian:</td>
                    </tr>
                    <tr>
                        <td style="width: 4px; padding: -3px 0px 0px 2px">1. </td>
                        <td rowspan="6" style="padding: 0px 0px 0px 6px">Jumlah Pajak Penghasilan atas Persewaan Tanah dan/ atau Bangunan yang dipotong di atas bukan merupakan kredit pajak dalam Surat Pemberitahuan (SPT) Tahunan PPh</td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"> </td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px;"></td>
                    </tr>
                    <tr>
                        <td style="width: 4px; padding: -10px 0px 0px 2px">2. </td>
                        <td style="padding: 0px 0px 0px 6px">Bukti Pemotongan/Pemungutan ini dianggap sah apabila diisi dengan lengkap dan benar</td>
                    </tr>

                </table>
            </div>
            <div style="width: 50%;"> 
                <table width="100%" style="font-size: 12px; padding: 8px 4px 2px 4px; text-align: center; margin-left: 86px">
                    <tr>
                        <td style="font-weight: bold">Tanda Tangan, Nama dan Cap</td>
                    </tr>
                    <tr>
                        <td style="padding-top: 98px"><?php
                            echo $namaPenanggungJawab[$n];
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td><?php
                            echo $jabatan[$n];
                            ?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="footer-m"></div>
        <div class="footer-f"></div>
        <div class="footer-s"></div>
        <?php
    }
}
?>
</body>
</html>

<?php

//use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formatter = Yii::$app->formatter;
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];

function kekata($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = kekata($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x < 1000) {
        $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x < 1000000) {
        $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
    } else if ($x < 1000000000) {
        $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
    }
    return $temp;
}

function terbilang($x, $style = 4) {
    if ($x < 0) {
        $hasil = "minus " . trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}
?>
<style>
    .header-notices{
        letter-spacing: 3px;
        position:fixed;
        top:-39;
        left: 21;
        margin: -3px 0px 0px -15px;  
        color: #d6d6c2;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;        
    }

    .header-f{
        width:4.9%;
        height:12px;
        background:#000;
        position:fixed;
        top:-21;
        left: 11;
        margin: -3px 0px 0px -15px;        
    }
    .header-s{
        width:2.6%;
        height:12px;
        background:#000;
        position:fixed;
        top:-21;
        left:662;
        margin: -3px 0px 0px 0px;
    }

    .footer-f{
        width:4.9%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:20;
        left: 9;
        margin: -52px 0px -52px -12px;
    }
    .footer-s{
        width:3%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:20;
        left:659;
        margin: -52px 0px -52px 0px;
    }

    .box-f{
        width:1.1%;
        height:0.8px;
        background:#000;
        position:fixed;
        top:-18;
        left:637;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .box-s{
        width:1.1%;
        height:0.8px;
        background:#fff;
        position:fixed;
        top:-18;
        left:648;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .box-t{
        width:1.1%;
        height:0.8px;
        background:#000;
        position:fixed;
        top:-18;
        left:659;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .box-fo{
        width:1.1%;
        height:0.8px;
        background:#fff;
        position:fixed;
        top:-18;
        left:670;
        margin: 7px 0px 0px 0px;
        border: 1px solid black;
    }

    .header{        
        position:fixed;
        width: 120px;
        /*top:-20;*/
        left: 425;
        font-family: fantasy;
        font-size: 8px;
        text-align: left;
        margin: -2px 0px 0px 64px;
    }
    .footer-m{
        font-weight: bold; 
        font-size: 9.7px;
        padding-bottom: -29px;
        position:fixed;
        bottom:-22;
        left:3;
        margin: 0px -50px 0px 2px;
    }

    .nomor-surat{
        width: 60%; 
        font-size: 11;
        font-weight: bold;
        padding: 0px 0px 0px 14px;
        letter-spacing: 2px;
    }

    .header-box{
        border-collapse: collapse;
        font-family: "Arial", Arial, sans-serif;
        margin-left: -4px; 
        margin-top: -50px;
        border-top-style: dotted;
        border-bottom-style: solid;
        border-width: 1.5px;
        padding: 4px 0px 4px 0px;

    }

    .logo-header-box{        
        width: 22%; 
        font-size: 10.8px;
        border-right-style: solid;
        border-width: 1.5px;
        padding: 6px 0px 0px 78px;
    }

    .judul-box{
        font-size: 12.7px;         
        height: 78px;
        padding: -10px 0px 10px 0px;
        border-right-style: solid;
        border-bottom-style: solid;
        border-width: 1.5px;
    }

    .kode-form-header{
        width: 20%; 
        padding: -14px -60px 0px 0px;
        border-bottom-style: solid;
        border-width: 1.5px;
        font-size: 13px;
    }

    .sub-a{
        font-family: "Arial", Arial, sans-serif;
        font-size: 11px;
        font-weight: bold;
        padding: 12px 0px 0px -4px;
    }

    .table-f{
        border-style: solid;
        border-width: 1.5px;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;
        margin: 4px 0px 0px -4px; 
        padding: 8px 4px 10px 4px;
    }

    .table-s{
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;
        margin: 4px 0px 0px -4px;  
        padding: 16px 0px 0px 4px;
    }

    tr.c-1 td.c-1-r-1{
        width: 11%;
    }

    tr.c-1 td.c-1-r-2{
        width: 1%;
    }

    tr.c-1 td.c-1-r-3{
        width: 3%;
    }

    tr.c-1 td.c-1-r-4{
        letter-spacing: 2px; 
        border-bottom-style: solid; 
        border-width: 0.5px;
        width: 32%;
        /*        background-color: red;*/
    }    

    tr.c-1 td.c-1-r-5{
        width: 22%;
    }

    tr.c-1 td.c-1-r-7{
        width: 3%;
    }

    tr.c-1 td.c-1-r-8{
        padding-left: 0px;
        width: 26%;
        letter-spacing: 2px;
    }

    tr.c-5 td.c-5-r-2{
        width: 18%;
    }

    tr.c-5 td.c-5-r-5{
        padding-left: -130px;
        width: 3%;
    }

    tr.c-5 td.c-5-r-6{

    }

    tr.c3-1 td.c3-1-r-1{
        width: 8%;
    }

    tr.c3-1 td.c3-1-r-2{
        width: 1%;
    }

    tr.c3-1 td.c3-1-r-3{
        width: 3%;
    }

    tr.c3-2 td.c3-2-r-5{
        width: 3%;
    }
    tr.c3-2 td.c3-2-r-6{
        width: 24%;
        font-size: 10px;
        text-decoration: underline;
    }

    .c3-1{
        font-size: 10px;   
    }

    .djp{
        padding: 4px 0px 3px 0px;
        font-size: 11px;
        border-right-style: solid;
        border-width: 1.5px;
    }
</style>
<html>
    <?php for ($n = 0; $n < $countPdf; $n++) { ?>

            <!--<style font-size="8px"></style>-->
        <body>
            <!--<div class="header-notices">area staples</div>-->
            <div style="height: 25px"></div>
            <div class="header-f"></div>
            <div class="header-s"></div>
            <div class="box-f"></div>
            <div class="box-s"></div>
            <div class="box-t"></div>
            <div class="box-fo"></div>
            <table class="header-box">
                <tr>
                    <td class="logo-header-box">
                        <?= Html::img("@web/img/djp.png", ['width' => '68px', 'height' => '70px']) ?>                        
                    </td>                                                        
                    <td class="judul-box">
                <font align="left"><center><b>BUKTI PEMOTONGAN PAJAK</b></center>
                <font align="left"><center><b>PENGHASILAN PASAL 21 (TIDAK FINAL)</b></center>
                <font align="left"><center><b>ATAU PASAL 26</b></center>
            </td>                                                        
            <td class="kode-form-header">
            <font><center><b>FORMULIR 1721 - VI</b></center>
            <div class="header" style="padding: -4px 0px 0px 29px;">
                <p>Lembar ke-1 : untuk Penerima Penghasilan</p>
                <p>Lembar ke-2 : untuk Pemotong</p>
            </div>
        </td>                                                                            
    </tr>
    <tr>                    
        <td class="djp">
    <font align="left"><center><b>KEMENTERIAN KEUANGAN RI</b></center>
    <font align="left"><center><b>DIREKTORAT JENDERAL PAJAK</b></center>
    </td>
    <td class="nomor-surat" colspan="2"><b>NOMOR:</b> &nbsp;<sub>H-01</sub>&nbsp;&nbsp;&nbsp;
        1 . 3 - 
        <?php
            echo $masapph = $curMonth . '.' . $curYear . '-' . $nomorBuktiPotong[$n];
        ?>
    </td>    
    </tr>
    </table>

    <div class="sub-a">A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</div>

    <table class="table-f" height="100%" width="100%">
        <tr class="c-1">
            <td class="c-1-r-1">1. NPWP</td>
            <td class="c-1-r-2">:</td>
            <td class="c-1-r-3"><sub>A. 01</sub></td>            
            <td class="c-1-r-4" colspan="2">
                <?php
                if (isset($npwp[$n])) {
                    echo substr($npwp[$n], 0, 2) . '.' . substr($npwp[$n], 2, 3) . '.' . substr($npwp[$n], 5, 3) . '.' . substr($npwp[$n], 8, 1) . '-' . substr($npwp[$n], 9, 3) . '.' . substr($npwp[$n], 12, 3);
                }
                ?>
            </td>
            <td class="c-1-r-5">2. NIK/NO.PASPOR:</td>
            <td class="c-1-r-7"><sub>A. 02</sub></td>
            <td class="c-1-r-8" style="border-bottom-style: solid; border-width: 0.5px">
                <?php
                if (isset($nik[$n])) {
                    echo substr($nik[$n], 0, 2) . '.' . substr($nik[$n], 2, 3) . '.' . substr($nik[$n], 5, 3) . '.' . substr($nik[$n], 8, 1) . '-' . substr($nik[$n], 9, 3) . '.' . substr($nik[$n], 12, 3);
                }
                ?>
            </td>
        </tr>
        <tr class="c-2">
            <td>3. NAMA</td>
            <td>:</td>
            <td><sub>A. 03</sub></td>            
            <td colspan="5" style="border-bottom-style: solid; border-width: 0.5px">
                <?php
                if (isset($nama[$n])) {
                    echo $nama[$n];
                }
                ?>
            </td>
        </tr>
        <tr class="c-3">
            <td>4. ALAMAT</td>
            <td>:</td>
            <td><sub>A. 04</sub></td>
            <td colspan="5" rowspan="2" style="border-bottom-style: solid; border-width: 0.5px"><?php
                if (isset($alamat[$n])) {
                    echo $alamat[$n];
                }
                ?></td>
        </tr>
        <tr class="c-4">
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="c-5">
            <td class="c-5-r-1" colspan="4">5. WAJIB PAJAK LUAR NEGERI:&nbsp;<sub>A. 05</sub> </td>
            <td class="c-5-r-5">            
                <?php
                    $a = Html::img("@web/img/square.png", ['width' => '18px', 'height' => '18px']);
                    echo $a;  
                ?> YA</td>            
            <td class="c-5-r-6">
                6. KODE NEGARA DOMISILI:
            </td>
            <td><sub>A. 06</sub></td>
            <td style="border-bottom-style: solid; border-width: 0.5px;"></td>
        </tr>
    </table>

    <div class="sub-a">B. PPh 21 DAN/ATAU PASAL 26 YANG DIPOTONG</div>
    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse;">
        <tr>
            <th style="border: 0.5px solid black; width: 26%; font-size: 9px;">KODE OBJEK PAJAK</th>
            <th style="border: 0.5px solid black; width: 19%; font-size: 9px;">JUMLAH PENGHASILAN BRUTO (Rp)</th>
            <th style="border: 0.5px solid black; width: 19%; font-size: 9px;">DASAR PENGENAAN PAJAK (Rp)</th>
            <th style="border: 0.5px solid black; width: 10%; font-size: 6.5px">TARIF LEBIH TINGGI 20% (TIDAK BERNPWP)</th>
            <th style="border: 0.5px solid black; font-size: 9px;">TARIF (%)</th>
            <th style="border: 0.5px solid black; width: 19%; font-size: 9px;">PPh DIPOTONG (Rp)</th>
        </tr>
        <tr>
            <td style="background-color: #ccccb3; border: 0.5px solid black;">(1)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black;">(2)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black;">(3)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black;">(4)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black;">(5)</td>
            <td style="background-color: #ccccb3; border: 0.5px solid black;">(6)</td>
        </tr>
        <tr>
            <td style="border: 0.5px solid black; height: 19px; letter-spacing: 2px">                
                <?php
                if (isset($statusKerja[$n])) {
                    if ($statusKerja[$n] == 2) {
                        echo '21 - 100 - 03';
                    } else {
                        echo '21 - 100 - 09';
                    }
                }
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 19px;">
                <?php
                if (isset($jumlahBruto[$n])) {
                    $bruto = $jumlahBruto[$n];
                    echo number_format($bruto, '0', '', '.');
                } else {
                    echo $bruto = '';
                }
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 19px;">
                <?php
                if (isset($dpp[$n])) {
                    $dpp = $dpp[$n];
                    echo number_format($dpp, '0', '', '.');
                } else {
                    echo $dpp = '';
                }
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 19px;">
                <?php
                if ($statusNpwpMandor[$n] == 1) {
                    $a = Html::img("@web/img/square.png", ['width' => '15px', 'height' => '15px']);
                    echo $a;
                } else {
                    $b = Html::img("@web/img/square - selected.png", ['width' => '15px', 'height' => '15px']);
                    echo $b;
                }
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 19px;">
                <?php
                echo number_format($tarif[$n], '2', ',', '');
                ?>
            </td>
            <td style="border: 0.5px solid black; height: 19px;">
                <?php
                if (isset($pphDipungut[$n])) {
                    $pph = $pphDipungut[$n];
                    echo number_format($pph, '0', '', '.');
                } else {
                    echo $pph = '';
                }
                ?>
            </td>
        </tr>
    </table>

    <div class="sub-a">C. IDENTITAS PEMOTONG</div>    
    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse; font-size: 10px">
        <tr class="c3-1">   
            <td class="c3-1-r-1" style="height: 25px;">1. NPWP</td>
            <td class="c3-1-r-2">:</td>
            <td class="c3-1-r-3"><sub>C-01</sub></td>
            <td colspan="2" style="text-align: left; letter-spacing: 2px; font-size: 10px">
                <?php
                if (isset($npwpPemungutPajak)) {
                    echo substr($npwpPemungutPajak, 0, 2) . ' . ' . substr($npwpPemungutPajak, 2, 3) . ' . ' . substr($npwpPemungutPajak, 5, 3) . ' . ' . substr($npwpPemungutPajak, 8, 1) . ' - ' . substr($npwpPemungutPajak, 9, 3) . ' . ' . substr($npwpPemungutPajak, 12, 3);
                }
                ?>
            </td>
            <td>3. TANGGAL & TANDA TANGAN</td>
            <td rowspan="2" style="border: 0.5px solid black; width: 130px;"></td>
        </tr>        
        <tr class="c3-2"> 
            <td class="c3-2-r-1" style="height: 25px">2. NAMA</td>
            <td class="c3-2-r-2">:</td>
            <td class="c3-2-r-3"><sub>C-02</sub></td>
            <td class="c3-2-r-4" style="text-align: left; letter-spacing: 1px; font-size: 10px"><?php
                if (isset($namaPemungutPajak)) {
                    echo $namaPemungutPajak;
                }
                ?></td>
            <td class="c3-2-r-5"><sub>C-03</sub></td>
            <td class="c3-2-r-6"><?php
                if (isset($tanggal[$n])) {
                    $fixDate = date("d - m - Y", strtotime($tanggal[$n]));
                    echo $fixDate;
                }
                ?>
            </td>            
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td style="font-size:8px; font-weight: bold; padding-top: -4px">[dd-mm-yyyy]</td>
            <td></td>
        </tr>
    </table>

    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid black; border-collapse: collapse; margin-top: 18px;">
        <tr>
            <td colspan="3" style="background-color: #ccccb3; height: 24px; border-bottom: 1.5px solid black; font-size: 10px"><b>KODE OBJEK PAJAK PENGHASILAN PASAL 21 (TIDAK FINAL) ATAU PASAL 26</b></td>
        </tr>
        <tr>
            <td style="width: 3%;"></td>
            <td colspan="2" style="text-align: left; padding-top: 6px; font-size: 9.6px"><b>PPh PASAL 21 TIDAK FINAL</b></td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">1.</td>
            <td style="width: 8%; text-align: left">21-100-03</td>
            <td style="text-align: left">Upah Pegawai Tidak Tetap atau Tenaga Kerja Lepas</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">2.</td>
            <td style="width: 8%; text-align: left">21-100-04</td>
            <td style="text-align: left">Imbalan Kepada Distributor <i>Multi Level Marketing</i> (MLM)</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">3.</td>
            <td style="width: 8%; text-align: left">21-100-05</td>
            <td style="text-align: left">Imbalan Kepada Petugas Dinas Luar Asuransi</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">4.</td>
            <td style="width: 8%; text-align: left">21-100-06</td>
            <td style="text-align: left">Imbalan Kepada Penjaja Barang Dagangan</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">5.</td>
            <td style="width: 8%; text-align: left">21-100-07</td>
            <td style="text-align: left">Imbalan Kepada Tenaga Ahli</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">6.</td>
            <td style="width: 8%; text-align: left">21-100-08</td>
            <td style="text-align: left">Imbalan Keapda Bukan Pegawai yang Menerima Penghasilan yang Bersifat Berkesinambungan</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">7.</td>
            <td style="width: 8%; text-align: left">21-100-09</td>
            <td style="text-align: left">Imbalan Keapda Bukan Pegawai yang Menerima Penghasilan yang Tidak Bersifat Berkesinambungan</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">8.</td>
            <td style="width: 8%; text-align: left">21-100-10</td>
            <td style="text-align: left">Honorarium atau Imbalan Kepada Anggota Dewan Komisaris atau Dewan Pengawas yang tidak Merangkap sebagai Pegawai Tetap</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">9.</td>
            <td style="width: 8%; text-align: left">21-100-11</td>
            <td style="text-align: left">Jasa, Produksi, Tantiem, Bonus atau Imbalan Kepada Mantan Pegawai</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">10.</td>
            <td style="width: 8%; text-align: left">21-100-12</td>
            <td style="text-align: left">Penarikan Dana Pensiun oleh Pegawai</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">11.</td>
            <td style="width: 8%; text-align: left">21-100-13</td>
            <td style="text-align: left">Imbalan Kepada Peserta Kegiatan</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">12.</td>
            <td style="width: 8%; text-align: left">21-100-99</td>
            <td style="text-align: left">Objek PPh Pasal 21 Tidak Final Lainnya</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="width: 3%;"></td>
            <td colspan="2" style="text-align: left; padding-top: 6px; font-size: 9.6px"><b>PPh PASAL 26</b></td>
        </tr>
        <tr style="font-size: 9.2px">
            <td style="text-align: left">1.</td>
            <td style="text-align: left">27-100-99</td>
            <td style="text-align: left">Imbalan sehubungan dengan jasa, pekerja dan kegiatan, hadiah dan penghargaan, pensiun dan pembayaran berkala lainnya yang</td>
        </tr>
        <tr style="font-size: 9.2px">
            <td></td>
            <td></td>
            <td style="text-align: left">dipotong PPh Pasal 26</td>
        </tr>
    </table>

    <table class="table-s" height="90%" width="100%" style="text-align: center; border: 1.5px solid white; border-collapse: collapse; font-weight: bold; margin-top: 18px">
        <tr>
            <td style="height: 360px; letter-spacing: 2px"> 
        </tr>
    </table>

    <div class="footer-f"></div>
    <div class="footer-s"></div>

<?php } ?>
</body>
</html>

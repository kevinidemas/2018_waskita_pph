<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PphJenisBisnis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-jenis-bisnis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenisBisnis')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'parentId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

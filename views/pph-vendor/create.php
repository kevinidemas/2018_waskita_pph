<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */

$this->title = 'Tambah Perusahaan Baru';
$this->params['breadcrumbs'][] = ['label' => 'PPH Perusahaan', 'url' => ['index']];
$this->params[] = $this->title;
?>
<div class="pph-vendor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

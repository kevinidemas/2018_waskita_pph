<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\PphSsp */

$this->title = $model->ntpn;
$this->params['breadcrumbs'][] = ['label' => 'SSP', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-ssp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('<i class=" glyphicon glyphicon-edit"></i> Ubah', ['update', 'id' => $model->sspId], ['class' => 'btn btn-update']) ?>
        <?=
        Html::a('<i class="glyphicon glyphicon-trash"></i> Hapus', ['delete', 'id' => $model->sspId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ntpn',            
            [
                'attribute' => 'tanggalSetor',
                'value' => $model->tanggalSetor,
                'format' => ['date', 'php:d - m - Y']
            ],            
            [
                'attribute' => 'nilai',
                'value' => $model->nilai,
                'format'=>['decimal',0]
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $userId = $modelUser->username;
                    return ucfirst($userId);
                },
            ],
            'created_at',
            'bukti',
        ],
    ]) ?>

</div>

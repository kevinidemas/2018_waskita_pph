<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphSsp */

$this->title = 'SSP Baru';
$this->params['breadcrumbs'][] = ['label' => 'SSP', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-ssp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

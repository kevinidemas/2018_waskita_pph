<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\builder\FormGrid;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use yii\helpers\Url;
use kartik\alert\Alert;
use kartik\builder\Form;
use kartik\file\FileInput;
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\PphSsp */
/* @var $form yii\widgets\ActiveForm */

$session = Yii::$app->session;

$tanggalValue = null;
$tahun = date('Y');
$bulan = date('m');

if (isset($session['masaId'])) {
    $m = $session['masaId'];
    if (strlen($m) < 2) {
        $m = '0' . $m;
    }
    $Y = date('Y');
    $countDate = date('t', strtotime($Y . '-' . $m . '-01'));
    if ($tahun == $session['masaTahun']) {
        if ($bulan == $m) {
            $tanggalValue = date('d');
        } else {
            $tanggalValue = date('t', strtotime($tahun . '-' . $m));
        }
    }
}
?>


<?php
    $form = ActiveForm::begin([]);
?>

<div class="pph-ssp-form" id="form">      
    <div class="col-sm-4" style="margin-left: -15px" id="date-picker">
            <?php
            echo '<label class="control-label">Tanggal</label>';
            echo DatePicker::widget([
                'model' => $model,
                'attribute' => 'tanggalSetor',
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'options' => ['value' => date('d-' . $m . '-Y', strtotime(date('Y-m-' . $tanggalValue)))],
                'pluginOptions' => [
                    'autoclose' => true,
                    'startDate' => date('d-' . $m . '-Y', strtotime(date('Y-m-1'))),
                    'endDate' => $countDate . '-' . $m . '-' . $Y,
                    'format' => 'dd-mm-yyyy',
                ]
            ]);
            ?>
        </div>
    <?php 
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'ntpn' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor NTPN / PBK',
                            'id' => 'ssp-ntpn'
                        ]
                    ],                    
                ],
            ],
            [
                'attributes' => [
                    'nilai' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nilai',
                            'id' => 'ssp-nilai'
                        ],
                    ],
                ]
            ],
        ],
    ]);
    ?>
    <?=
    $form->field($model, 'bukti')->widget(FileInput::classname(), [  
        'options' => [
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',            
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg', 'pdf'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
        ]
    ]);
    ?>
<div class="form-group">
        <?php echo '<div class="text-right" style="margin-right: 18px">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
        ?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphSsp */

$this->title = 'Ubah SSP ' . $model->ntpn;
//$this->params['breadcrumbs'][] = ['label' => 'Pph Ssps', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->sspId, 'url' => ['view', 'id' => $model->sspId]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-ssp-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\PphSsp;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphSspSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar SSP';
//$this->params['breadcrumbs'][] = $this->title;

$session = Yii::$app->session;
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}
$fieldValue = '';
$arrayRows = [];
if (isset($_GET['sel-rows'])) {
    if (!$_GET['sel-rows'] == null) {
        $session = Yii::$app->session;
        $rows = base64_decode($_GET['sel-rows']);
        $arrayRows = explode(",", $rows);
        $jsonArray = json_encode($rows);
        $session['sel-rows'] = $jsonArray;
    }
    $fieldValue = 'ada';
}

$jenisWp = 6;
$session['jenis-wajib-pajak'] = $jenisWp;
?>
<div class="pph-ssp-index">

    <h2><?= Html::encode($this->title) ?></h2>
    
    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> SSP Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div style="visibility: hidden;">
        <input type="hidden" id="choosen-id" name="choosen-id" value=<?php echo $fieldValue ?>> 
    </div>
    <p style="margin-bottom: -38px">
        <button type="button" onclick="cetakBerkasSsp()" class="btn btn-choosen" id="btn-print-file"><i class=" glyphicon glyphicon-cloud-download"></i> Download</button>
        <?= Html::button('<i class="glyphicon glyphicon-print"></i> Download', ['value' => Url::to('../pph-berkas/pilih-data'), 'class' => 'btn btn-pilih-berkas', 'id' => 'modalButton']) ?>
    </p>
    <?php
    Modal::begin([
        'header' => '<h4>Pilih Masa Pajak dan Jenis Berkas</h4>',
        'id' => 'modal',
        'size' => 'modal-lg'
    ]);

    echo "<div id='modalContent'></div>";

    Modal::end();
    ?>
    
   <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) use ($arrayRows) {
                    if (isset($_GET['sel-rows'])) {
                        $bool = in_array($model->sspId, $arrayRows);
                        return ['checked' => $bool];
                    }
                }
                    ],
                    ['class' => 'kartik\grid\SerialColumn',
                        'header' => 'No',
                    ],
                    [
                        'attribute' => 'ntpn',
                        'options' => [
                            'width' => 210
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'tanggalSetor',
                        'options' => [
                            'width' => 110
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'nilai',
                        'format' => ['decimal', 0],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                        'mergeHeader' => true,
                        'options' => [
                            'width' => 260
                        ],
                    ],
                    [
                        'attribute' => 'sspId',
                        'format' => 'raw',
                        'options' => [
                            'width' => 200
                        ],
                        'value' => function ($data) {
                            $ssp = $data->sspId;
                            if ($ssp == null) {
                                $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                            } else {
                                $result = $data->bukti;
                            }

                            $url = Url::toRoute([
                                        'pph-ssp/download', 'id' => $ssp// $model->sspId
                                            ], ['data-method' => 'post',]);
                            return Html::a($result, $url, [
                                        'title' => Yii::t('app', 'Download IUJK'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                                'vAlign' => 'middle',
                                'hAlign' => 'center',
                                'mergeHeader' => true
                            ],
                                    [
                                        'attribute' => 'created_by',
                                        'header' => 'Created By',
                                        'value' => function ($data) {
                                            $modelUser = User::find()->where(['id' => $data->created_by])->one();
                                            $nama = $modelUser->username;
                                            return ucfirst($nama);
                                        },
                                                'options' => [
                                                    'width' => 80
                                                ],
                                                'vAlign' => 'middle',
                                                'hAlign' => 'center',
                                            ],
                                            ['class' => 'yii\grid\ActionColumn',
                                                'options' => [
                                                    'width' => 53
                                                ],
                                            ],
                                        ],
                                        'toolbar' => [
                                            ['content' =>
//                $removeBtn . ' ' .
                                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                                            ],
                                            '{toggleData}',
                                        ],
                                        'panel' => [
                                            'type' => GridView::TYPE_SUCCESS,
                                            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>'                                            
                                        ],
                                        'persistResize' => false,
                                        'toggleDataOptions' => ['minCount' => 10]
                                    ]);
                                    ?>
</div>


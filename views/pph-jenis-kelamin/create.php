<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphJenisKelamin */

$this->title = 'Create Pph Jenis Kelamin';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Kelamins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jenis-kelamin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphUnitBisnis */

$this->title = 'Create Pph Unit Bisnis';
$this->params['breadcrumbs'][] = ['label' => 'Pph Unit Bisnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-unit-bisnis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

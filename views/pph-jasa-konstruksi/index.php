<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphJasaKonstruksiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Jasa Konstruksis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jasa-konstruksi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Jasa Konstruksi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'jasaKonstruksiId',
            'status',
            'tarifId',
            'userId',
            'parentId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */

$this->title = 'Ubah Data Penghasilan PT Pasal 21';
//$this->params['breadcrumbs'][] = ['label' => 'Master Penghasilans', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->penghasilanId, 'url' => ['view', 'id' => $model->penghasilanId]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-penghasilan-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

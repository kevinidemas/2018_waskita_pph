<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PphNomorBuktiPotong */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-nomor-bukti-potong-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>
    
    <?php echo $form->field($model, 'bulan[]')->dropDownList([
    '1' => 'Januari', 
    '2' => 'Februari', 
    '3' => 'Maret', 
    '4' => 'April', 
    '5' => 'Mei', 
    '6' => 'Juni', 
    '7' => 'Juli', 
    '8' => 'Agustus', 
    '9' => 'September', 
    '10' => 'Oktober', 
    '11' => 'November', 
    '12' => 'Desember', 
    ]); 
?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

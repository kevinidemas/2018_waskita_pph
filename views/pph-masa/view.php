<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphMasa */

$this->title = $model->masaId;
$this->params['breadcrumbs'][] = ['label' => 'Pph Masas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-masa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->masaId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->masaId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'masaId',
            'nama',
        ],
    ]) ?>

</div>

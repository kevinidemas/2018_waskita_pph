<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\PphIujk */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#iujk-npwp").mask("99.999.999.9-999.999");
    });
</script>
<div class="pph-iujk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomorIujk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'namaPerusahaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamatPerusahaan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'penanggungJawab')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kemampuanKeuangan')->textInput(['maxlength' => true, 'id' => 'keuangan']) ?>

    <?= $form->field($model, 'npwp')->textInput(['maxlength' => true, 'id' => 'iujk-npwp']) ?>

    <?php
    echo '<label class="control-label">Expired</label>';
    echo DatePicker::widget([
        'model' => $model,
        'attribute' => 'expired_at',
        'type' => DatePicker::TYPE_COMPONENT_PREPEND,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd-mm-yyyy',
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'bukti')->widget(FileInput::classname(), [
        'options' => [
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'doc', 'docx', 'pdf', 'txt', 'pptx', 'ppt', 'xlsx', 'xls'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (max 2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ]);
    ?>

    <?= $form->field($model, 'keterangan')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
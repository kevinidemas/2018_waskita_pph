<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use app\models\PphJenisWajibPajak;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
// echo '<pre>';
//                print_r($model);
//                die();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#wp-npwp").mask("99.999.999.9-999.999");
    });
</script>
<?php
//if(isset($id)){
//    if($id != null){
//        $modelNPWP = $model->buktiNpwp;
//        $modelSppkp = $model->buktiSppkp;
//    } else {
//        $modelNPWP = '';
//        $modelSppkp = '';
//    }
//}

$iujkLabel = '';
$iujkId = null;
$id = null;
if (isset($_GET['id'])) {
    $statusRead = true;
    $session = Yii::$app->session;
    $id = $_GET['id'];
    $iujkId = '2';
    $session['idIujk'] = $id;
    $iujkLabel = 'IUJK';
}
?>
<?php
Modal::begin([
    'header' => '<h4>Tambah IUJK Baru</h4>',
    'id' => 'modal',
    'size' => 'modal-lg'
]);

echo "<div id='modalContent'></div>";

Modal::end();
?>

<div class="pph-vendor-form">
    <p><!-- Html::button('<i class="glyphicon glyphicon-plus"></i> IUJK Baru', ['value' => Url::to('../pph-iujk/create-modal'), 'class' => 'btn btn-add-iujk', 'id' => 'modalButton']) --></p>
    <?php
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
//            [
//                'attributes' => [
//                    'jenisWp' => [
//                        'type' => Form::INPUT_DROPDOWN_LIST,
//                        'items' => $jenisWpOptions,
//                        'options' => [
//                            'value' => 2,
//                        ],
//                        'hint' => 'Pilih Jenis Wajib Pajak',
//                    ],
//                ]
//            ],            
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Perusahaan',
                            'id' => 'wp-nama'
                        ]
                    ],
                    'npwp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor NPWP Perusahaan',
                            'id' => 'wp-npwp'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'telepon' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor Telepon Perusahaan',
                            'id' => 'wp-telepon'
                        ]
                    ],
                    'fax' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor Fax. Perusahaan',
                            'id' => 'wp-fax'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'penanggungJawab' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Penanggungjawab',
                            'id' => 'wp-penanggungJawab'
                        ]
                    ],
                    'hp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor HP',
                            'id' => 'wp-hp'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'website' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan alamat Website Perusahaan',
                            'id' => 'wp-website'
                        ]
                    ],
                    'email' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan alamat E-mail Perusahaan',
                            'id' => 'wp-email'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
        ],
    ]);
    ?>
     <?=
    $form->field($model, 'buktiNpwp')->widget(FileInput::classname(), [  
        'options' => [
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',            
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false,
        ]
    ]);
    ?>
    <?=
    $form->field($model, 'buktiSppkp')->widget(FileInput::classname(), [
//        'value' => function ($data) {
//            return 'as';
//        },
        'options' => [            
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'jpeg'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ]);
    ?>

    <div class="form-group">
        <?php echo '<div class="text-right" style="margin-right: 18px">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
        ?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>

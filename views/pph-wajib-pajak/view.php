<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\PphTarif;
use app\models\PphStatusVendor;
use app\models\PphJenisWajibPajak;

/* @var $this yii\web\View */
/* @var $model app\models\PphWajibPajak */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Wajib Pajak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-wajib-pajak-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('<i class=" glyphicon glyphicon-edit"></i> Ubah', ['update', 'id' => $model->wajibPajakId], ['class' => 'btn btn-update']) ?>
        <?=
        Html::a('<i class="glyphicon glyphicon-trash"></i> Hapus', ['delete', 'id' => $model->wajibPajakId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i> Lihat IUJK', ['/pph-iujk/index-iujk-wp', 'id' => $model->wajibPajakId], ['class' => 'btn btn-create-new']) ?>
    <?= Html::a('<i class="glyphicon glyphicon-eye-open"></i> Lihat BUJPK', ['/pph-bujpk/index-bujpk-wp', 'id' => $model->wajibPajakId], ['class' => 'btn btn-create-new']) ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'wajibPajakId',
            'nama',
            [
                'attribute' => 'npwp',
                'value' => function ($model) {
                    $nilai = $model->npwp;
                    $npwp = substr($nilai, 0, 2) . '.' . substr($nilai, 2, 3) . '.' . substr($nilai, 5, 3) . '.' . substr($nilai, 8, 1) . '-' . substr($nilai, 9, 3) . '.' . substr($nilai, 12, 3);
                    return $npwp;
                },
            ],
            'alamat',
//            [
//                'attribute' => 'iujkId',
//                'value' => function ($model) {
//                    if ($model->iujkId == null) {
//                        return $iujk = 'Tidak memiliki IUJK';
//                    } else {
//                        $modelIujk = app\models\PphIujk::find()->where(['iujkId' => $model->iujkId])->one();
//                        $iujk = $modelIujk->nomorIujk;
//                        return $iujk;
//                    }
//                },
//            ],
//            [
//                'attribute' => 'statusWpId',
//                'value' => function ($model) {
//                    $modelStatusWp = PphStatusVendor::find()->where(['statusVendorId' => $model->statusWpId])->one();
//                    $modelTarif = PphTarif::find()->where(['tarifId' => $modelStatusWp->tarifId])->one();
//                    return $modelTarif->status;
//                },
//            ],
            [
                'attribute' => 'jenisWp',
                'value' => function ($model) {
                    $modelJenisWp = PphJenisWajibPajak::find()->where(['jenisWajibPajakId' => $model->jenisWp])->one();
                    $jenisWpId = $modelJenisWp->nama;
                    return $jenisWpId;
                },
            ],
            [
                'attribute' => 'user_id',
                'label' => 'User',
                'value' => function ($model) {
                    $modelUser = User::find()->where(['id' => $model->user_id])->one();
                    $userId = $modelUser->username;
                    return ucfirst($userId);
                },
            ],
                        'penanggungJawab',
                        'hp',
                        'telepon',
                        'fax',
                        'website',
                        'email',
                        'buktiNpwp',
                        'buktiSppkp'
        ],
    ])
    ?>
</div>

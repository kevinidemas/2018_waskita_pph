<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use app\models\PphJenisWajibPajak;
use app\models\PphStatusKerja;
use app\models\PphTarif;
use yii\bootstrap\Modal;
use app\models\PphIujk;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#wp-npwp").mask("99.999.999.9-999.999");
    });
</script>
<?php
$dataJenis = PphJenisWajibPajak::find()->asArray()->all();
$arrayJenisWajibPajak = ArrayHelper::getColumn($dataJenis, 'jenisWajibPajakId');

$dataIujk = PphIujk::find()->where(['created_by' => Yii::$app->user->id, 'wajibPajakId' => null])->asArray()->all();

$arrayIujkId = ArrayHelper::getColumn($dataIujk, 'iujkId');
if ($arrayIujkId != null) {
    for ($n = 0; $n < count($arrayIujkId); $n++) {
        $mn = $arrayIujkId[$n];
        if (isset($arrayIujkId[$n])) {
            $modelIujk = PphIujk::find()->where(['iujkId' => $arrayIujkId[$n]])->one();
            $iujkOptions[$mn] = $modelIujk->nomorIujk;
        }
    }
} else {
    $iujkOptions[] = 'KOSONG';
}
$countArray = count($arrayJenisWajibPajak);

$jenisWpOptions = [];
$IndividuOptions = [];
$statusRead = false;
if (isset($arrayJenisWajibPajak[1])) {
    $modelTarif = PphJenisWajibPajak::find()->where(['jenisWajibPajakId' => $arrayJenisWajibPajak[1]])->one();
    $jenisWpOptions[] = $modelTarif->nama;
}
?>

<div class="pph-vendor-form">
    <p><!-- Html::button('<i class="glyphicon glyphicon-plus"></i> IUJK Baru', ['value' => Url::to('../pph-iujk/create-modal'), 'class' => 'btn btn-add-iujk', 'id' => 'modalButton']) --></p>
    <?php
    $form = ActiveForm::begin([
                'enableAjaxValidation' => true
    ]);
    ?>
    
    <?php
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'jenisWp' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $jenisWpOptions,
                        'options' => [
                            'value' => $iujkId,
                        ],
                        'hint' => 'Pilih Jenis Wajib Pajak',
                    ],
                ]
            ],
            [
                'attributes' => [
                    'iujkId' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $iujkOptions,
                        'options' => [
                            'id' => 'wp-iujk',
                            'value' => $id,
                        ],
                    ],
                ]
            ],
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Individu / Perusahaan',
                            'id' => 'wp-nama'
                        ]
                    ],
                    'npwp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor NPWP Individu / Perusahaan',
                            'id' => 'wp-npwp'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
                            'class' => 'btn btn-default',
                        ]) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
                            'class' => 'btn btn-primary',
//                            'disabled' => true
                        ]) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>
</div>

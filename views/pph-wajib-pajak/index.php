<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\PphIujk;
use yii\helpers\Url;
use app\models\PphBujpk;
use yii\bootstrap\Modal;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphWajibPajakSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pusat Data Wajib Pajak';
//$this->params['breadcrumbs'][] = $this->title;

$session = Yii::$app->session;
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}
$fieldValue = '';
$arrayRows = [];
if (isset($_GET['sel-rows'])) {
    if (!$_GET['sel-rows'] == null) {
        $session = Yii::$app->session;
        $rows = base64_decode($_GET['sel-rows']);
        $arrayRows = explode(",", $rows);
        $jsonArray = json_encode($rows);
        $session['sel-rows'] = $jsonArray;
    }
    $fieldValue = 'ada';
}

$jenisWp = 1;
$session['jenis-wajib-pajak'] = $jenisWp;
?>
<div class="pph-wajib-pajak-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Wajib Pajak Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <div style="visibility: hidden;">
        <input type="hidden" id="choosen-id" name="choosen-id" value=<?php echo $fieldValue ?>> 
    </div>
    <p style="margin-bottom: -38px">
        <button type="button" onclick="cetakBerkasWp()" class="btn btn-choosen" id="btn-print-file"><i class=" glyphicon glyphicon-cloud-download"></i> Download</button>
        <?= Html::button('<i class="glyphicon glyphicon-print"></i> Download', ['value' => Url::to('../pph-berkas/pilih-data'), 'class' => 'btn btn-pilih-berkas', 'id' => 'modalButton']) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Pilih Masa Pajak dan Jenis Berkas</h4>',
        'id' => 'modal',
        'size' => 'modal-lg'
    ]);

    echo "<div id='modalContent'></div>";

    Modal::end();
    ?>
    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) use ($arrayRows) {
                    if (isset($_GET['sel-rows'])) {
                        $bool = in_array($model->wajibPajakId, $arrayRows);
                        return ['checked' => $bool];
                    }
                }
                    ],
                    ['class' => 'kartik\grid\SerialColumn',
                        'header' => 'No',
                    ],
                    [
                        'attribute' => 'nama',
                        'options' => [
                            'width' => 275
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'npwp',
                        'value' => function ($model) {
                            $npwp = $model->npwp;
                            $result = substr($npwp, 0, 2) . '.' . substr($npwp, 2, 3) . '.' . substr($npwp, 5, 3) . '.' . substr($npwp, 8, 1) . '-' . substr($npwp, 9, 3) . '.' . substr($npwp, 12, 3);
                            return $result;
                        },
                        'options' => [
                            'width' => 10
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'alamat',
                        'options' => [
                            'width' => 240
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'iujkId',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $iujk = $data->iujkId;
                            if ($iujk == null) {
                                $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                            } else {
                                $modelIujk = PphIujk::find()->where(['iujkId' => $iujk])->one();
                                if ($modelIujk->is_approve == 0) {
                                    $result = '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                                } else if ($modelIujk->is_approve == 1) {
                                    $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                                } else {
                                    $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                                }
                            }

                            $url = Url::toRoute([
                                        'pph-iujk/download', 'id' => $iujk// $model->iujkId
                                            ], ['data-method' => 'post',]);
                            return Html::a($result, $url, [
                                        'title' => Yii::t('app', 'Download IUJK'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                                'vAlign' => 'middle',
                                'hAlign' => 'center',
                                'options' => [
                                    'width' => 5
                                ],
                                'mergeHeader' => true
                            ],
                            [
                                'attribute' => 'bujpkId',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    // echo '<pre>'; print_r($data); echo '</pre>'; die();
                                    $bujpk = $data->bujpkId;
                                    if ($bujpk == null) {
                                        $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                                    } else {
                                        $modelBujpk = PphBujpk::find()->where(['bujpkId' => $bujpk])->one();
                                        // echo '<pre>'; print_r($modelBujpk); echo '</pre>'; die();
                                        if ($modelBujpk->is_approve == 0) {
                                            $result = '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                                        } else if ($modelBujpk->is_approve == 1) {
                                            $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                                        } else {
                                            $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                                        }
                                    }

                                    $url = Url::toRoute([
                                                'pph-bujpk/download', 'id' => $bujpk
                                                    ], ['data-method' => 'post',]);
                                    return Html::a($result, $url, [
                                                'title' => Yii::t('app', 'Download BUJPK'),
                                                'data-toggle' => "modal",
                                                'data-target' => "#myModal",
                                                'data-method' => 'post',
                                    ]);
                                },
                                        'vAlign' => 'middle',
                                        'hAlign' => 'center',
                                        'options' => [
                                            'width' => 5
                                        ],
                                        'mergeHeader' => true
                                    ],
//            [
//                'attribute' => 'statusNpwpId',
//                'value' => function ($model) {
//                    if ($model->statusNpwpId == 1) {
//                        return $npwp = 'Memiliki NPWP';
//                    } else {
//                        return $npwp = 'Tidak Memiliki NPWP';
//                    }
//                },
//                'options' => [
//                    'width' => 180
//                ]
//            ],
                                    [
                                        'attribute' => 'user_id',
                                        'header' => 'Created By',
                                        'value' => function ($data) {
                                            $modelUser = User::find()->where(['id' => $data->user_id])->one();
                                            $nama = $modelUser->username;
                                            return ucfirst($nama);
                                        },
                                                'options' => [
                                                    'width' => 80
                                                ],
                                                'vAlign' => 'middle',
                                                'hAlign' => 'center',
                                            ],
                                            ['class' => 'yii\grid\ActionColumn',
                                                'options' => [
                                                    'width' => 53
                                                ],
                                            ],
                                        ],
                                        'toolbar' => [
                                            ['content' =>
//                $removeBtn . ' ' .
                                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                                            ],
                                            '{toggleData}',
                                        ],
                                        'panel' => [
                                            'type' => GridView::TYPE_SUCCESS,
                                            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>'
                                            . '<br>' . '<b>Keterangan</b>' . ' :'
                                            . '<br>' . '<i class="glyphicon glyphicon-minus" style="color: #000000"></i> Tidak Memiliki <b>IUJK</b> / <b>IUJK</b> belum diproses oleh Admin'
                                            . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i> <b>IUJK</b> telah disetujui Admin'
                                            . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i> <b>IUJK</b> ditolak oleh Admin'
//            .'<br>' . '<i class="glyphicon glyphicon-minus" style="color: #000000"></i> Tidak Memiliki IUJK'
//            . '<br>' . '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i> IUJK telah disetujui Admin'
//            . '<br>' . '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i> IUJK belum/tidak disetujui Admin'
//            'heading' => $headerText,
//            'before' => $tombol,
                                        ],
                                        'persistResize' => false,
                                        'toggleDataOptions' => ['minCount' => 10]
                                    ]);
                                    ?>
</div>

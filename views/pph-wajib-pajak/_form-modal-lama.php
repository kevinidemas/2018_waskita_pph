<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use app\models\PphJenisWajibPajak;
use app\models\PphStatusVendor;
use app\models\PphTarif;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
    jQuery(function ($) {
        $("#wp-npwp-modal").mask("99.999.999.9-999.999");
    });
</script>
<?php
$dataJenis = PphJenisWajibPajak::find()->asArray()->all();
$arrayJenisWajibPajak = ArrayHelper::getColumn($dataJenis, 'jenisWajibPajakId');

$dataVendor = PphStatusVendor::find()->where(['pasalId' => '2'])->asArray()->all();
$arrayVendorId = ArrayHelper::getColumn($dataVendor, 'tarifId');

$countArray = null;
if (count($arrayVendorId) < $arrayJenisWajibPajak) {
    $countArray = count($arrayJenisWajibPajak);
} else {
    $countArray = count($arrayVendorId);
}

$jenisWpOptions = [];
$vendorOptions = [];
$statusRead = false;
for ($n = 0; $n < $countArray; $n++) {
    $m = $n + 1;
    if (isset($arrayJenisWajibPajak[$n])) {
        $modelTarif = PphJenisWajibPajak::find()->where(['jenisWajibPajakId' => $arrayJenisWajibPajak[$n]])->one();
        $jenisWpOptions[$m] = $modelTarif->nama;
    }
    if (isset($arrayVendorId[$n])) {
        $modelVendor = PphTarif::find()->where(['tarifId' => $arrayVendorId[$n]])->one();
        $vendorOptions[] = $modelVendor->status;
//        $statusRead = false;
    } else {
//        $statusRead = true;
    }
}
?>

<div class="pph-vendor-form">
    <?php
    $form = ActiveForm::begin();
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nama' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nama Perusahaan']],
                    'npwp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor NPWP Perusahaan',
                            'id' => 'wp-npwp-modal'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'jenisWp' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => $jenisWpOptions, 'hint' => 'Pilih Jenis Wajib Pajak'],
                    'statusWpId' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => $vendorOptions, 'options' => ['readOnly' => $statusRead], 'hint' => 'Pilih Status Perusahaan'],
                ]
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary']) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>
</div>
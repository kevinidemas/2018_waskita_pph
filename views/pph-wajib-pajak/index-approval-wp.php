<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\alert\Alert;
use app\models\PphIujk;
use app\models\PphBujpk;
use yii\helpers\Url;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphWajibPajakSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Wajib Pajak Baru';

$session = Yii::$app->session;
if (isset($session['notification-not-found'])) {
    $msgUpdateFailed = $session['notification-not-found'];
    unset($session['notification-not-found']);
    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'title' => 'Tidak Ditemukan!',
        'icon' => 'glyphicon glyphicon-remove-sign',
        'body' => $msgUpdateFailed,
        'showSeparator' => true,
        'delay' => 2000
    ]);
} else if (isset($session['notification-found'])) {
    $msgGenerateFailed = $session['notification-found'];
    unset($session['notification-found']);
    echo Alert::widget([
        'type' => Alert::TYPE_INFO,
        'title' => 'Ditemukan!',
        'icon' => 'glyphicon glyphicon-info-sign',
        'body' => $msgGenerateFailed,
        'showSeparator' => true,
        'delay' => 2000
    ]);
}

?>
<div class="pph-wajib-pajak-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
            ],
            [
                'attribute' => 'nama',
                'options' => [
                    'width' => 275
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'npwp',
                'value' => function ($model) {
                    $npwp = $model->npwp;
                    $result = substr($npwp, 0, 2) . '.' . substr($npwp, 2, 3) . '.' . substr($npwp, 5, 3) . '.' . substr($npwp, 8, 1) . '-' . substr($npwp, 9, 3) . '.' . substr($npwp, 12, 3);
                    return $result;
                },
                'options' => [
                    'width' => 10
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'alamat',
                'options' => [
                    'width' => 240
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'buktiNpwp',
                'label' => 'Unduh NPWP',
                'format' => 'raw',
                'value' => function ($data) {
                    $npwp = $data->npwp;
                    if ($npwp == null) {
                        $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                    } else {
                        $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }

                    $url = Url::toRoute([
                                'pph-wajib-pajak/download-npwp', 'id' => $data->wajibPajakId// $model->iujkId
                                    ], ['data-method' => 'post',]);
                    return Html::a($result, $url, [
                                'title' => Yii::t('app', 'Download NPWP'),
                                'data-toggle' => "modal",
                                'data-target' => "#myModal",
                                'data-method' => 'post',
                    ]);
                },
                        'options' => [
                            'width' => 10
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'buktiSppkp',
                        'label' => 'Unduh SPPKP',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $sppkp = $data->buktiSppkp;
                            if ($sppkp == null) {
                                $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                            } else {
                                $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                            }

                            $url = Url::toRoute([
                                        'pph-wajib-pajak/download-sppkp', 'id' => $data->wajibPajakId// $model->iujkId
                                            ], ['data-method' => 'post',]);
                            return Html::a($result, $url, [
                                        'title' => Yii::t('app', 'Download SPPKP'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                                'options' => [
                                    'width' => 10
                                ],
                                'vAlign' => 'middle',
                                'hAlign' => 'center',
                            ],
                            [
                                'attribute' => 'user_id',
                                'header' => 'Created By',
                                'value' => function ($data) {
                                    $modelUser = User::find()->where(['id' => $data->user_id])->one();
                                    $nama = $modelUser->username;
                                    return ucfirst($nama);
                                },
                                        'options' => [
                                            'width' => 80
                                        ],
                                        'vAlign' => 'middle',
                                        'hAlign' => 'center',
                                    ],
                                    [
                                        'header' => 'Approve',
                                        'class' => 'yii\grid\ActionColumn',
                                        'headerOptions' => ['width' => '4', 'align' => 'center'],
                                        'template' => '{active}',
                                        'buttons' => [
                                            'active' => function ($url, $model) {
                                                return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
                                                            'title' => Yii::t('app', 'Approve'),
                                                            'data-toggle' => "modal",
                                                            'data-target' => "#myModal",
                                                            'data-method' => 'post',
                                                ]);
                                            },
                                                ],
                                                'urlCreator' => function ($action, $model, $key, $index) {
                                            $url = Url::toRoute(['pph-wajib-pajak/approve-wp', 'id' => $model->wajibPajakId]);
                                            return $url;
                                        },
                                            ],
                                            [
                                                'header' => 'Reject',
                                                'class' => 'yii\grid\ActionColumn',
                                                'headerOptions' => ['width' => '4', 'align' => 'center'],
                                                'template' => '{active}',
                                                'buttons' => [
                                                    'active' => function ($url, $model) {
                                                        return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-remove-sign"></i></button>', $url, [
                                                                    'title' => Yii::t('app', 'Reject'),
                                                                    'data-toggle' => "modal",
                                                                    'data-target' => "#myModal",
                                                                    'data-method' => 'post',
                                                        ]);
                                                    },
                                                        ],
                                                        'urlCreator' => function ($action, $model, $key, $index) {
                                                    $url = Url::toRoute(['pph-wajib-pajak/reject-wp', 'id' => $model->wajibPajakId]);
                                                    return $url;
                                                },
                                                    ],
                                                    ['class' => 'yii\grid\ActionColumn',
                                                        'options' => [
                                                            'width' => 53
                                                        ],
                                                    ],
                                                ],
                                                'toolbar' => [
                                                    ['content' =>
//                $removeBtn . ' ' .
                                                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                                                    ],
                                                    '{toggleData}',
                                                ],
//                        'panel' => [
//                            'type' => GridView::TYPE_SUCCESS,
//                            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>'
//                            . '<br>' . '<b>Keterangan</b>' . ' :'
//                            . '<br>' . '<i class="glyphicon glyphicon-minus" style="color: #000000"></i> Tidak Memiliki <b>IUJK</b> / <b>IUJK</b> belum diproses oleh Admin'
//                            . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i> <b>IUJK</b> telah disetujui Admin'
//                            . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '&nbsp;' . '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i> <b>IUJK</b> ditolak oleh Admin'
////            .'<br>' . '<i class="glyphicon glyphicon-minus" style="color: #000000"></i> Tidak Memiliki IUJK'
////            . '<br>' . '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i> IUJK telah disetujui Admin'
////            . '<br>' . '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i> IUJK belum/tidak disetujui Admin'
////            'heading' => $headerText,
////            'before' => $tombol,
//                        ],
                                                'persistResize' => false,
                                                'toggleDataOptions' => ['minCount' => 10]
                                            ]);
                                            ?>
</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterIujkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Iujks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-iujk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Master Iujk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iujkId',
            'nomorIujk',
            'validDate',
            'userId',
            'parentId',
            // 'tarifId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

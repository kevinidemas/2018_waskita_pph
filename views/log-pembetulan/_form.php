<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\LogPembetulan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-pembetulan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'masa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tahun')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pasalId')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'prevId')->textInput() ?>

    <?= $form->field($model, 'currId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\User;
use app\models\PphPasal;

/* @var $this yii\web\View */
/* @var $searchModel app\search\LogPembetulanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Pembetulan';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-pembetulan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'masa',
                'value' => function ($data) {
                    $bln = \Yii::t('app', $data->masa);
                    return $bln;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],            
            [
                'attribute' => 'tahun',            
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
//                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],
            [
                'attribute' => 'pembetulan',
                'format'=>'raw',
                'value' => function($data)
                {
                    return
                    Html::a($data->pembetulan, ['pph-history-update/index','id'=>$data->pembetulanId], ['title' => 'Index','class'=>'no-pjax']);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],
            [
                'attribute' => 'pasalId',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'value' => function ($data) {
                    $modelPasal = PphPasal::find()->where(['pasalId' => $data->pasalId])->one();
                    $pasal = $modelPasal->nama;
                    return ucfirst($pasal);
                },
                'options' => [
                    'width' => '10px',
                ],
                'mergeHeader' => true
            ],
            [
                'attribute' => 'userId',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = User::find()->where(['id' => $data->userId])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
    //                        'width' => '100px',
                    'mergeHeader' => true
            ],
            [
                'attribute' => 'created_at',
                'format' => 'raw',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                //                        'width' => '100px',
                    'mergeHeader' => true
            ],
//                    ['class' => 'yii\grid\ActionColumn'],
                ],
                'toolbar' => [
                    ['content' =>
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                    ],
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_SUCCESS,
                    'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
                    'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>LOG</b>',
                ],
                'persistResize' => false,
                'toggleDataOptions' => ['minCount' => 10]
            ]);
            ?>
</div>

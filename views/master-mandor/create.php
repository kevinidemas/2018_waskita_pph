<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterMandor */

$this->title = 'Create Master Mandor';
$this->params['breadcrumbs'][] = ['label' => 'Master Mandors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-mandor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

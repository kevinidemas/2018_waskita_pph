<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterMandor */

$this->title = 'Update Master Mandor: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Master Mandors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mandorId, 'url' => ['view', 'id' => $model->mandorId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-mandor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

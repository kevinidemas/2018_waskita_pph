<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\User;
/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterHonorerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Honorers';
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    if (!is_array($msgErrors)) {
        $alertBootstrap = '<div class="alert-single alert-danger">';
    } else {
        $alertBootstrap = '<div class="alert alert-danger">';
    }
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
        echo $alertBootstrap;
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-honorer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Honorer Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nama',
            'alamat',
            'posisi',
            'nik',
            'npwp',
            [
                'attribute' => 'created_by',
                'header' => 'Created By',
                'value' => function ($data) {
                    $modelUser = User::find()->where(['id' => $data->created_by])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                        'options' => [
                            'width' => 80
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
            ],
            //'jenisKelamin',
            //'statusKerjaId',
            //'userId',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            //'hp',
            //'email:email',
            //'approvalStatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\PphJenisKelamin;

/* @var $this yii\web\View */
/* @var $model app\models\MasterHonorer */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#honorer-nik").mask("9999999999999999");
        $("#honorer-npwp").mask("99.999.999.9-999.999");
    });
</script>

<div class="master-honorer-form">
    <?php
$dataJenisKelamin = PphJenisKelamin::find()->asArray()->all();
    $arrayJenisKelamin = ArrayHelper::getColumn($dataJenisKelamin, 'jenisKelaminId');
    $countArrayJK = count($arrayJenisKelamin);
    $jenKelOptions = [];
    for ($n = 0; $n < $countArrayJK; $n++) {
        $m = $n + 1;
        if (isset($arrayJenisKelamin[$n]) && $n != 2) {
            $modelJK = PphJenisKelamin::find()->where(['jenisKelaminId' => $arrayJenisKelamin[$n]])->one();
            $jenKelOptions[$m] = $modelJK->jenis;
        }
    }
    
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Pegawai Honorer',
                            'id' => 'honorer-nama'
                        ]
                    ],
                ],
            ],
            [
                'attributes' => [
                    'jenisKelamin' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $jenKelOptions,
                        'hint' => 'Pilih Jenis Kelamin',
                    ],
                    'posisi' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Posisi Kerja Pegawai Honorer',
                        ]
                    ],
                ]
            ],
            [
                'attributes' => [
                    'nik' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan NIK Pegawai Honorer',
                            'id' => 'honorer-nik'
                        ]
                    ],
                    'npwp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan NPWP Pegawai Honorer',
                            'id' => 'honorer-npwp'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'hp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor Telepon Pegawai Honorer',
                            'id' => 'honorer-hp'
                        ]
                    ],
                    'email' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan alamat E-mail Pegawai Honorer',
                            'id' => 'honorer-email'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
        ],
    ]);
    ?>    

    <div class="form-group">
        <?php echo '<div class="text-right" style="margin-right: 18px">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
        ?>
    </div>
    <?php 
    ActiveForm::end();
    ?>

</div>

<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\alert\Alert;
use app\models\MasterPegawai;
use app\models\PphJenisKelamin;
use app\models\PphStatusKawin;
use app\models\PphStatusKerja;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterPegawaiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pusat Data Pegawai';

$session = Yii::$app->session;
$fieldValue = '';
$arrayRows = [];
if (isset($_GET['sel-rows'])) {
    if (!$_GET['sel-rows'] == null) {
        $session = Yii::$app->session;
        $rows = base64_decode($_GET['sel-rows']);
        $arrayRows = explode(",", $rows);
        $jsonArray = json_encode($rows);
        $session['sel-rows'] = $jsonArray;
    }
    $fieldValue = 'ada';
}

if (Yii::$app->session->hasFlash('success')) {
    $msgSuccess = Yii::$app->session->getFlash('success');
    echo Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'title' => 'Berhasil!',
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => $msgSuccess,
        'showSeparator' => true,
        'delay' => 10000
    ]);
}
if (Yii::$app->session->hasFlash('info')) {
    $msgInfo = Yii::$app->session->getFlash('info');
    echo Alert::widget([
        'type' => Alert::TYPE_INFO,
        'title' => 'Pemberitahuan!',
        'icon' => 'glyphicon glyphicon-info-sign',
        'body' => $msgInfo,
        'showSeparator' => true,
        'delay' => 10000
    ]);
}
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    if (!is_array($msgErrors)) {
        $alertBootstrap = '<div class="alert-single alert-danger">';
    } else {
        $alertBootstrap = '<div class="alert alert-danger">';
    }
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
        echo $alertBootstrap;
}
?>
<div class="master-pegawai-index">

    <h2><?= Html::encode($this->title) ?></h2>
    
    <?php $userId = Yii::$app->user->identity->id; if($userId == 1) {?>
    <?= Html::beginForm(['master-pegawai/remove-selected'], 'post'); ?>
    <?php $removeBtn = '<button type="button" onclick="removeSelectedPegawai()" id="removeBtn" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>'; ?>
    <div style="visibility: hidden;">
        <button type="submit" class="btn btn-danger" id="remove-selected-pegawai"><i class="glyphicon glyphicon-trash"></i></button>
    </div>
    
    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Upload', ['create-upload'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Manual', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?php } else {
            $removeBtn = '';
          }
    ?>

    <?php
    echo GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) use ($arrayRows) {
                    if (isset($_GET['sel-rows'])) {
                        $bool = in_array($model->wajibPajakId, $arrayRows);
                        return ['checked' => $bool];
                    }
                }
                    ],
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'nama',
                'header' => 'Nama',
                'options' => [
                    'width' => '190px'
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'jabatan',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '100px',
                ],
            ],
            [
                'attribute' => 'nip',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'npwp',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'alamat',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '229px',
                ],
            ],
            [
                'attribute' => 'jenisKelamin',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelJK = PphJenisKelamin::find()->where(['jenisKelaminId' => $data->jenisKelamin])->one();
                    $jk = $modelJK->jenis;
                    return ucfirst($jk);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'statusPerkawinan',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelJP = PphStatusKawin::find()->where(['statusKawinId' => $data->statusPerkawinan])->one();
                    $jp = $modelJP->status;
                    return ucfirst($jp);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '70px',
                ],
            ],
            [
                'attribute' => 'statusKerjaId',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelSK = PphStatusKerja::find()->where(['statusKerjaId' => $data->statusKerjaId])->one();
                    $sk = $modelSK->statusKerja;
                    return ucfirst($sk);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'attribute' => 'created_by',
                'header' => 'Dibuat Oleh',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = User::find()->where(['id' => $data->created_by])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'options' => [
                    'width' => '80px',
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
            ],
        ],
        'toolbar' => [
                ['content' =>
                $removeBtn . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
            ],
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
            'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR PEGAWAI TETAP</b>',
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]                        
    ]);
    ?>
</div>

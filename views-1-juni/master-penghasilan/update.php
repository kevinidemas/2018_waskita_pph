<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */

$this->title = 'Update Master Penghasilan: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Master Penghasilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->penghasilanId, 'url' => ['view', 'id' => $model->penghasilanId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-penghasilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

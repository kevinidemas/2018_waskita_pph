<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\User;
use kartik\alert\Alert;
use yii\bootstrap\Modal;
use app\models\PphJenisPenghasilan;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;

//use yii\bootstrap\Modal;
//use yii\bootstrap\ActiveForm;


/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterPenghasilanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Penghasilan Pegawai';
//$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
if (isset($session['notification-success-download-pasal-21'])) {
    $msgSuccess = $session['notification-success-download-pasal-21'];
    unset($session['notification-success-download-pasal-21']);
    Alert::widget([
        'type' => Alert::TYPE_SUCCESS,
        'title' => 'Berhasil!',
        'icon' => 'glyphicon glyphicon-ok-sign',
        'body' => $msgSuccess,
        'showSeparator' => true,
        'delay' => 3500
    ]);
}

if (Yii::$app->session->hasFlash('success')) {
    $msgErrors = Yii::$app->session->getFlash('success');
    $alertBootstrap = '<div class="alert alert-info">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }

        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}

if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }

        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}
?>
<div class="master-penghasilan-index">
    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    
    <?php
    $fieldValue = '';
    $actStatus = null;
    if ($session['expiredDate'] == 1) {
        $actStatus = 'not-active';
    }
    $arrayRows = [];
    if (isset($_GET['sel-rows'])) {
        if (!$_GET['sel-rows'] == null) {
            $session = Yii::$app->session;
            $rows = base64_decode($_GET['sel-rows']);
            $arrayRows = explode(",", $rows);
            $jsonArray = json_encode($rows);
            $session['sel-rows'] = $jsonArray;
        }
        $fieldValue = 'ada';
    }
    
    if (isset($session['dataCount-pasal-21'])) {
        unset($session['dataCount-pasal-21']);
        $dataCountStatus = 'Ada';
    } else {
        $dataCountStatus = null;
    }

    ?>

    <?php
    if (Yii::$app->user->id != 1) {
        ?>
        <style type="text/css">
            #btn-print-file{
                display:none;
            };
        </style>
        <?php
    }
    ?>


    <div style="visibility: hidden;">
        <input type="hidden" id="choosen-id" name="choosen-id" value=<?php echo $fieldValue ?>> 
        <input type="hidden" id="act-status" name="act-status" value=<?php echo $actStatus ?>>
        <input type="hidden" id="data-count-status" name="data-count-status" value=<?php echo $dataCountStatus ?>>
    </div>
    <p style="margin-bottom: -38px">
        <button type="button" onclick="cetakBerkas21()" class="btn btn-choosen" id="btn-print-file"><i class="glyphicon glyphicon-print"></i> Cetak Berkas</button>
        <?= Html::button('<i class="glyphicon glyphicon-print"></i> Cetak Berkas', ['value' => Url::to('../pph-berkas/pilih-bulan'), 'class' => 'btn btn-pilih-berkas', 'id' => 'modalButton']) ?>
    </p>
    <?php
    Modal::begin([
        'header' => '<h4>Pilih Jenis Berkas</h4>',
        'id' => 'modal',
        'size' => 'modal-lg'
    ]);

    echo "<div id='modalContent'></div>";

    Modal::end();
    ?>

    <?= Html::beginForm(['master-penghasilan/remove-data'], 'post'); ?>
    <?php $removeBtn = '<button type="button" disabled onclick="removeData()" id="removeBtn" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>'; ?>
    <div style="visibility: hidden;">
        <button type="submit" class="btn btn-danger" id="remove-data"><i class="glyphicon glyphicon-trash"></i></button>
    </div>
    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            [
                'class' => 'kartik\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) use ($arrayRows) {
                    if (isset($_GET['sel-rows'])) {
                        $bool = in_array($model->penghasilanId, $arrayRows);
                        return ['checked' => $bool];
                    }
                }
            ],
            ['class' => 'yii\grid\SerialColumn',
                'header' => 'No',
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'pegawaiId',
                'label' => 'NPWP',
                'value' => function ($model) {
                    $npwp = $model->getNpwp($model->pegawaiId);
                    $result = substr($npwp, 0, 2) . '.' . substr($npwp, 2, 3) . '.' . substr($npwp, 5, 3) . '.' . substr($npwp, 8, 1) . '-' . substr($npwp, 9, 3) . '.' . substr($npwp, 12, 3);
                    return $result;
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '90px',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'pegawaiId',
                'label' => 'Nama',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getNama($model->pegawaiId);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '320px',
//                'mergeHeader' => true
            ],
            [
                'attribute' => 'bruto',
                'format' => ['decimal', 0],
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'pph',
                'label' => 'Jumlah PPh',
                'format' => ['decimal', 0],
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true
            ],
            [
                'attribute' => 'masaPajak',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true,
                'width' => '30px',
            ],
            [
                'attribute' => 'tahunPajak',
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'mergeHeader' => true,
                'width' => '60px',
            ],
            [
                'attribute' => 'created_by',
//                'label' => 'Created By',
                'format' => 'raw',
                'value' => function ($data) {
                    $modelUser = User::find()->where(['id' => $data->created_by])->one();
                    $nama = $modelUser->username;
                    return ucfirst($nama);
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
                'width' => '100px',
//                'mergeHeader' => true
            ],
//            [
//                'attribute' => 'jenisPenghasilanId',
//                'format' => 'raw',
//                'value' => function ($data) {
//                    $modelJP = PphJenisPenghasilan::find()->where(['jenisPenghasilanId' => $data->jenisPenghasilanId])->one();
//                    $namaJp = $modelJP->nama;
//                    return ucfirst($namaJp);
//                },
//                'vAlign' => 'middle',
//                'hAlign' => 'center',
//                'width' => '100px',
////                'mergeHeader' => true
//            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '10', 'align' => 'center'],
                'template' => '{edit}',
                'buttons' => [
                    'edit' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-edit-npwp" id="edit-bp" style="display: none"><i class="glyphicon glyphicon-edit"></i></button>', $url, [
                                    'title' => Yii::t('app', 'Edit'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                                    'id' => 'editBtn'
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    $url = Url::toRoute(['update', 'id' => $model->penghasilanId]);
                    return $url;
                }
            ],
        ],
        'toolbar' => [
            ['content' =>
                $removeBtn . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
            ],
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
            'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR DATA PENGHASILAN PEGAWAI</b>',
//            'before' => $tombol,
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]
    ]);
    ?>
    <?= Html::endForm(); ?> 
</div>

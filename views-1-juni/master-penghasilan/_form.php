<?php

use yii\helpers\Html;
use app\models\MasterPegawai;
use app\models\PphJenisPenghasilan;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-penghasilan-form">

    <?php
    //ambil options untuk pegawai
    $dataPegawai = MasterPegawai::find()->asArray()->all();
    $arrayPegawai = ArrayHelper::getColumn($dataPegawai, 'pegawaiId');
    $countArrayPegawai = count($arrayPegawai);
    $pegawaiOptions = [];
    if ($arrayPegawai != NULL) {
        for ($n = 0; $n < $countArrayPegawai; $n++) {
            $m = $n + 1;
            if (isset($arrayPegawai[$n]) && $n != 2) {
                $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $arrayPegawai[$n]])->one();
                $pegawaiOptions[$m] = '(' . $modelPegawai->nip . ') ' . $modelPegawai->nama;
            }
        }
    } else {
        $pegawaiOptions[] = 'KOSONG';
    }

    $dataJenisPenghasilan = PphJenisPenghasilan::find()->asArray()->all();
    $arrayJenPeng = ArrayHelper::getColumn($dataJenisPenghasilan, 'jenisPenghasilanId');

    $penghasilanOptions = [];
    for ($z = 0; $z < count($arrayJenPeng); $z++) {
        $y = $z + 1;
        if (isset($arrayJenPeng[$z]) && $z != 2) {
            $modelPenghasilan = PphJenisPenghasilan::find()->where(['jenisPenghasilanId' => $arrayJenPeng[$z]])->one();
            $penghasilanOptions[$y] = $modelPenghasilan->nama;
        }
    }
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'pegawaiId' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'label' => 'Pegawai',
                        'items' => $pegawaiOptions,
                        'options' => [
//                            'value' => $iujkId,
                            'id' => 'pilih-pegawai',
                        ],
                        'hint' => 'Pilih Wajib Pajak / Pegawai',
                    ],
                    'jenisPenghasilanId' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'label' => 'Jenis Penghasilan',
                        'items' => $penghasilanOptions,
                        'hint' => 'Pilih Wajib Pajak / Pegawai',
                    ],
                ],
            ],
            [
                'attributes' => [
                    'bruto' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nilai Penghasilan',
                            'id' => 'penghasilan-bruto'
                        ]
                    ],
                    'pph' => [
                        'type' => Form::INPUT_TEXT,
                        'label' => 'PPh',
                        'options' => [
                            'placeholder' => 'Masukkan Nilai di Potong PPh 21',
                            'id' => 'penghasilan-pph21'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
                            'class' => 'btn btn-default',
                        ]) . ' ' .
                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
                            'class' => 'btn btn-primary',
                            'id' => 'btn-penghasilan-submit'
//                            'disabled' => true
                        ]) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>
</div>

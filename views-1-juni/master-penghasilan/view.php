<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MasterPegawai;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */

$modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $model->pegawaiId])->one();
$nama = $modelPegawai->nama;
$this->title = $nama;
$this->params['breadcrumbs'][] = ['label' => 'Master Penghasilan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-penghasilan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->penghasilanId], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a('Delete', ['delete', 'id' => $model->penghasilanId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'pegawaiId',
                'label' => 'Nama Pegawai / Wajib Pajak',
                'value' => function ($model) {
                    $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $model->pegawaiId])->one();
                    $nama = $modelPegawai->nama;
                    return $nama;
                },
            ],
            [
                'attribute' => 'pegawaiId',
                'label' => 'NPWP',
                'value' => function ($model) {
                    $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $model->pegawaiId])->one();
                    $npwp = $modelPegawai->npwp;
                    if($npwp != '000000000000000'){
                        return $npwp;
                    } else {
                        return 'Tanpa NPWP';
                    }
                },
            ],
            [
                'attribute' => 'pegawaiId',
                'label' => 'NIP',
                'value' => function ($model) {
                    $modelPegawai = MasterPegawai::find()->where(['pegawaiId' => $model->pegawaiId])->one();
                    $npwp = $modelPegawai->nip;
                    return $npwp;
                },
            ],
            'masaPajak',
            'tahunPajak',
            'kodePajak',
            [
                'attribute' => 'bruto',
                'value' => $model->bruto,
                'format' => ['decimal', 0]
            ],
            [
                'attribute' => 'pph',
                'value' => $model->pph,
                'format' => ['decimal', 0]
            ],
            [
                'attribute' => 'created_at',
                'value' => $model->created_at,
                'format' => ['date', 'php:d F Y']
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    $modelUser = User::find()->where(['id' => $model->created_by])->one();
                    $userId = $modelUser->username;
                    return ucfirst($userId);
                },
            ],
        ],
    ])
    ?>

</div>

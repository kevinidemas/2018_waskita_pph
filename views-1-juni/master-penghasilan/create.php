<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterPenghasilan */

$this->title = 'Tambah(Manual) Penghasilan Pegawai';
//$this->params['breadcrumbs'][] = ['label' => 'Master Penghasilans', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-penghasilan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

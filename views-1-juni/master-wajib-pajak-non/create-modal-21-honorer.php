<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphWajibPajak */

$this->title = 'Tanpa NPWP';
$this->params['breadcrumbs'][] = ['label' => 'Wajib Pajak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-wajib-pajak-create">

    <h5><?= Html::encode($this->title) ?></h5>

    <?= $this->renderAjax('_form-modal-21-honorer', [
        'model' => $model,  
    ]) ?>

</div>

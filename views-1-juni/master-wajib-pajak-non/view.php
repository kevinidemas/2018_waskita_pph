<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MasterWajibPajakNon */

$this->title = $model->wajibPajakNonId;
$this->params['breadcrumbs'][] = ['label' => 'Master Wajib Pajak Nons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-wajib-pajak-non-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->wajibPajakNonId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->wajibPajakNonId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'wajibPajakNonId',
            'nama',
            'alamat',
            'created_by',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MasterWajibPajakNon */

$this->title = 'Update Master Wajib Pajak Non: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Master Wajib Pajak Nons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wajibPajakNonId, 'url' => ['view', 'id' => $model->wajibPajakNonId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-wajib-pajak-non-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

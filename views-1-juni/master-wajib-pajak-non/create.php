<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterWajibPajakNon */

$this->title = 'Wajib Pajak Baru';
$this->params['breadcrumbs'][] = ['label' => 'Master Wajib Pajak Nons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-wajib-pajak-non-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $modelnon,
    ]) ?>

</div>

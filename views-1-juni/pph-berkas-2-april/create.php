<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphBerkas */

$this->title = 'Create Pph Berkas';
$this->params['breadcrumbs'][] = ['label' => 'Pph Berkas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-berkas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

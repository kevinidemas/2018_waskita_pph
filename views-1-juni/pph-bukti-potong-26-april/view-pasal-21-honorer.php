<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\PphWajibPajak;
use app\models\PphPasal;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$this->title = $model->buktiPotongId;
$session = Yii::$app->session;
if(isset($session['ad']) && isset($session['at']) && isset($session['atp'])){    
    $jsonAD = json_decode($session['ad']);
    $jsonAT = json_decode($session['at']);
    $jsonATP = json_decode($session['atp']);
    $arrayPPh = explode('.', $jsonAD);
    $arrayTarif = explode(',', $jsonAT);
    $arrayTarifProgressif = explode(',', $jsonATP);
    $totalPph = $session['total-pph'];
}
?>
<div class="pph-bukti-potong-view">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Baru', ['create-pasal-21-honorer'], ['class' => 'btn btn-create-new']) ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [                                              
                'label' => $arrayTarif[0] . '% x ' . $arrayPPh[0],
                'value' => $arrayTarifProgressif[0],
                'contentOptions' => ['class' => 'bg-red'], // HTML attributes to customize value tag
                'captionOptions' => ['tooltip' => 'Tooltip'], // HTML attributes to customize label tag
                'format' => ['decimal', 0]
            ],
            [                                              
                'label' => $arrayTarif[1] . '% x ' . $arrayPPh[1],
                'value' => $arrayTarifProgressif[1],
                'contentOptions' => ['class' => 'bg-red'], // HTML attributes to customize value tag
                'captionOptions' => ['tooltip' => 'Tooltip'], // HTML attributes to customize label tag
                'format' => ['decimal', 0]
            ],
            [                                              
                'label' => $arrayTarif[2] . '% x ' . $arrayPPh[2],
                'value' => $arrayTarifProgressif[2],
                'contentOptions' => ['class' => 'bg-red'], // HTML attributes to customize value tag
                'captionOptions' => ['tooltip' => 'Tooltip'], // HTML attributes to customize label tag
                'format' => ['decimal', 0]
            ],
            [                                              
                'label' => $arrayTarif[3] . '% x ' . $arrayPPh[3],
                'value' => $arrayTarifProgressif[3],
                'contentOptions' => ['class' => 'bg-red'], // HTML attributes to customize value tag
                'captionOptions' => ['tooltip' => 'Tooltip'], // HTML attributes to customize label tag
                'format' => ['decimal', 0]
            ],   
            [                                              
                'label' => 'PPh Terutang',
                'value' => $totalPph,
                'contentOptions' => ['class' => 'bg-red'], // HTML attributes to customize value tag
                'captionOptions' => ['tooltip' => 'Tooltip'], // HTML attributes to customize label tag
                'format' => ['decimal', 0]
            ],   
        ],
    ])
    ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\PphWajibPajak;
use app\models\PphPasal;
use app\models\PphBuktiPotong;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$buktiPotongId = $model->buktiPotongId;
$this->params['breadcrumbs'][] = ['label' => 'Bukti Potong Pasal 23 Jasa', 'url' => ['index-pasal-23']];

$modelBp = PphBuktiPotong::find()->where(['buktiPotongId' => $buktiPotongId])->one();
$this->params['breadcrumbs'][] = $modelBp->nomorPembukuan;
$this->title = 'Pasal 23 - '.$modelBp->nomorPembukuan;
?>
<div class="pph-bukti-potong-view">

    <!--<h4><?= Html::encode($modelBp->nomorPembukuan) ?></h4>-->

    <p>
    <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Tambah Baru', ['create-pasal-23-jasa'], ['class' => 'btn btn-create-new']) ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomorPembukuan',           
            [
                'attribute' => 'tanggal',
                'value' => $model->tanggal,
                'format' => ['date', 'php:d - m - Y']
            ],
            [
                'attribute' => 'jumlahBruto',
                'value' => $model->jumlahBruto,
                'format'=>['decimal',0]
            ],
            [
                'attribute' => 'jumlahPphDiPotong',
                'value' => $model->jumlahPphDiPotong,
                'format'=>['decimal',0]
            ],
            [
                'attribute' => 'wajibPajakId',
                'value' => function ($model) {
                    if ($model->wajibPajakId != null) {
                        $modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $model->wajibPajakId])->one();
                        $nama = $modelWp->nama;
                        return $nama;
                    } else {
                        return $npwp = 'Tanpa NPWP';
                    }
                },
            ],
            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                        $modelUser = User::find()->where(['id' => $model->created_by])->one();
                        $userId = $modelUser->username;
                        return ucfirst($userId);
                },
            ],
            [
                'attribute' => 'pasalId',
                'value' => function ($model) {
                        $modelPasal = PphPasal::find()->where(['pasalId' => $model->pasalId])->one();
                        $pasalId = $modelPasal->nama;
                        return $pasalId . ' - Jasa';
                },
            ],
        ],
    ])
    ?>

</div>

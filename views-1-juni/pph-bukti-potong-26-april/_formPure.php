<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\builder\FormGrid;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use app\models\PphVendor;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$dataVendor = PphVendor::find()->where(['userId' => Yii::$app->user->id])->asArray()->all();
$arrayVendorId = ArrayHelper::getColumn($dataVendor, 'wajibPajakId');

for ($n = 0; $n < count($arrayVendorId); $n++) {
    if (isset($arrayVendorId[$n])) {
        $modelVendor = PphVendor::find()->where(['wajibPajakId' => $arrayVendorId[$n]])->one();
        $vendorOptions[] = $modelVendor->nama;
    }
}
?>
<div class="pph-bukti-potong-form">

    <?php
    $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL, 'formConfig' => ['labelSpan' => 4]]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nomorPembukuan' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor Pembukuan'], 'label' => 'Nomor Pembukuan'],
                    'jumlahPphDiPotong' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Nilai Total PPH yang di Potong...'], 'label' => 'Jumlah'],
                ],
            ],
            [
                'attributes' => [
                    'nomorBuktiPotong' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor Bukti Potong']],
                    'jumlahBruto' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Nilai Total Bruto...'], 'label' => ''],
                ]
            ],
            [
                'attributes' => [
                    'wajibPajakId' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => '\kartik\select2\Select2',
                        'options' => ['data' => $vendorOptions],
                        'hint' => 'Pilih Perusahaan atau Vendor',
                        'label' => 'Perusahaan'
                    ],
                    'tanggal' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => '\kartik\date\DatePicker',
                        'hint' => 'Tanggal (dd/mm/yyyy)',
                        'widgetOptions' => [
                            'autoclose' => true,
                            'format' => 'dd-mm-yyyy'
                        ]
                    ],
//                    'wajibPajakId' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => $vendorOptions, 'hint' => 'Pilih Perusahaan atau Vendor', 'label' => 'Perusahaan'],
                ]
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('Reset', ['class' => 'btn btn-default']) . ' ' .
                        Html::submitButton('Submit', ['class' => 'btn btn-primary']) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>

</div>

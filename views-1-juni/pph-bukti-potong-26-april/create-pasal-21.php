<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$this->title = 'Bukti Potong Pasal 21 ';
$this->params['breadcrumbs'][] = ['label' => 'Bukti Potong', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-bukti-potong-create" id="form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form-pasal-21', [
        'model' => $model,
        'modelWp' => $modelWp,
    ])
    ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphBuktiPotong */

$this->title = 'Bukti Potong Pasal 23 - Jasa';
//$this->params['breadcrumbs'][] = ['label' => 'Bukti Potong', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');    
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    
    echo $alertBootstrap;
}
?>
<div class="pph-bukti-potong-create" id="form">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form-pasal-23-jasa', [
        'model' => $model,
        'modelWp' => $modelWp,
    ])
    ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphWpInduk */

$this->title = 'Update Pph Wp Induk: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Wp Induks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->wpInduk, 'url' => ['view', 'id' => $model->wpInduk]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-wp-induk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

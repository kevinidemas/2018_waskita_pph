<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */

$this->title = $model->wajibPajakId;
$this->params['breadcrumbs'][] = ['label' => 'Vendors List', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-vendor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->wajibPajakId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->wajibPajakId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'wajibPajakId',
            'nama',
            'npwp',
            'alamat',
            'statusNpwpId',
            'iujkId',
            'statusVendorId',
        ],
    ]) ?>

</div>

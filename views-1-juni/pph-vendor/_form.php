<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\widgets\ActiveForm;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use app\models\PphStatusNpwp;
use app\models\PphStatusVendor;
use app\models\PphTarif;

/* @var $this yii\web\View */
/* @var $model app\models\PphVendor */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$dataVendor = PphStatusVendor::find()->where(['userId' => Yii::$app->user->id, 'pasalId' => '2'])->asArray()->all();
$arrayVendorId = ArrayHelper::getColumn($dataVendor, 'tarifId');

$countArray = null;
    $countArray = count($arrayVendorId);

$vendorOptions = [];
for ($n = 0; $n < $countArray; $n++) {
    if (isset($arrayVendorId[$n])) {
        $modelVendor = PphTarif::find()->where(['tarifId' => $arrayVendorId[$n]])->one();
        $vendorOptions[] = $modelVendor->status;
    }
}
?>
<div class="pph-vendor-form">
    <?php
    $form = ActiveForm::begin();
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nama' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nama Perusahaan']],
                ],
            ],
            [
                'attributes' => [
                    'npwp' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => 'Masukkan Nomor NPWP Perusahaan']],
                    'statusVendorId' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => $vendorOptions, 'hint' => 'Pilih Status Perusahaan'],
                ]
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
            [
                'attributes' => [
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => '<div style="text-align: right; margin-top: 20px">' .
                        Html::resetButton('Reset', ['class' => 'btn btn-default']) . ' ' .
                        Html::submitButton('Submit', ['class' => 'btn btn-primary']) .
                        '</div>'
                    ],
                ],
            ],
        ],
    ]);
    ActiveForm::end();
    ?>
</div>

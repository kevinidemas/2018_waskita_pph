<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.ico" type="image/x-icon" />
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => Html::img('@web/img/logo.png', ['alt' => Yii::$app->name]),
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/user/login']];
            } else {
                if (isset($_SESSION['masaPajak'])) {
                    $session = Yii::$app->session;
                    $masa = $session['masaPajak'];
                    if (Yii::$app->user->id == 1) {
                        ?>
                        <div class="masa-label">
                            <p>Masa Pajak ( <?php echo $masa ?>)</p>
                        </div>
                        <?php
                        $menuItems = [
                            ['label' => 'Tambah Data Baru', 'items' => [
                                    ['label' => 'Pasal 21', 'items' => [
                                            ['label' => 'Pegawai', 'items' => [
                                                    ['label' => 'Input Manual', 'url' => ['/master-penghasilan/create']],
                                                    ['label' => 'Upload File', 'url' => ['/master-penghasilan/create-upload']],
                                                ]
                                            ],
                                            ['label' => 'Mandor', 'items' => [
                                                    ['label' => 'Input Manual', 'url' => ['/pph-bukti-potong/create-pasal-21-mandor']]
                                                ]],
                                            ['label' => 'Honorer', 'items' => [
                                                    ['label' => 'Input Manual', 'url' => ['/pph-bukti-potong/create-pasal-21-honorer']]
                                                ]
                                            ]
                                        ]
                                    ],
                                    ['label' => 'Bukti Potong Pasal 22', 'url' => ['/pph-bukti-potong/create']],
                                    ['label' => 'Bukti Potong Pasal 23', 'items' => [
                                            ['label' => 'Jasa', 'url' => ['/pph-bukti-potong/create-pasal-23-jasa']],
                                            ['label' => 'Sewa', 'url' => ['/pph-bukti-potong/create-pasal-23-sewa']],
                                        ]
                                    ],
                                    ['label' => 'Bukti Potong Pasal 4 (2)', 'items' => [
                                            ['label' => 'Jasa', 'url' => ['/pph-bukti-potong/create-pasal-4-jasa']],
                                            ['label' => 'Sewa', 'url' => ['/pph-bukti-potong/create-pasal-4-sewa']],
                                            ['label' => 'Perencana', 'url' => ['/pph-bukti-potong/create-pasal-4-perencana']],
                                        ]
                                    ],
                                ]
                            ],
                            ['label' => 'Daftar Data', 'items' => [
                                    ['label' => 'Pasal 21', 'items' => [
                                        ['label' => 'Pegawai Tetap', 'url' => ['/master-penghasilan/index']],
                                        ['label' => 'Pegawai Tidak Tetap', 'url' => ['/pph-bukti-potong/index-pasal-21']],
                                        ]],
                                    ['label' => 'Bukti Potong Pasal 22', 'url' => ['/pph-bukti-potong/index']],
                                    ['label' => 'Bukti Potong Pasal 23', 'url' => ['/pph-bukti-potong/index-pasal-23']],
                                    ['label' => 'Bukti Potong Pasal 4 (2)', 'url' => ['/pph-bukti-potong/index-pasal-4']],
                                ]
                            ],
                            ['label' => 'Master Data', 'items' => [
                                    ['label' => 'Wajib Pajak', 'items' => [
                                        ['label' => 'Approval WP', 'url' => ['/pph-wajib-pajak/index-approval-wp']],
                                        ['label' => 'List Wajib Pajak', 'url' => ['/pph-wajib-pajak/index']],
                                    ]],
                                    ['label' => 'Pegawai', 'url' => ['/master-pegawai/index']],
                                    ['label' => 'Pegawai Tidak Tetap', 'items' => [
                                            ['label' => 'Mandor', 'items' => [
                                                ['label' => 'Approval Mandor', 'url' => ['/master-mandor/index-approval-mandor']],
                                                ['label' => 'List Mandor', 'url' => ['/master-mandor/index']],
                                            ]],
                                            ['label' => 'Honorer', 'url' => ['/master-honorer/index']],
                                        ]],
                                    ['label' => 'Sertifikat', 'items' => [
                                            ['label' => 'IUJK', 'url' => ['/pph-iujk/index']],
                                            ['label' => 'BUJPK', 'url' => ['/pph-bujpk/index']],
                                        ]
                                    ]
                                ]
                            ],
                            ['label' => 'User', 'items' => [
                                    ['label' => 'Account', 'url' => ['/user/admin']],
//                                    ['label' => 'Account', 'url' => ['/user/admin/update', 'id' => Yii::$app->user->id]],
                                    ['label' => 'Options', 'url' => ['/options/update', 'id' => 1]],
                                    ['label' => 'Calendar', 'url' => ['/pph-calendar/index']],
                                    Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                            'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                                    )
                                    . Html::endForm()
                                ]
                            ],
                        ];
                    } else {
                        ?>
                        <div class="masa-label">
                            <p>Masa Pajak ( <?php echo $masa ?>)</p>
                        </div>
                        <?php
                        $menuItems = [
                            ['label' => 'Tambah Data Baru', 'items' => [
                                    ['label' => 'Pasal 21', 'items' => [
                                            ['label' => 'Pegawai', 'items' => [
                                                    ['label' => 'Input Manual', 'url' => ['/master-penghasilan/create']],
                                                    ['label' => 'Upload File', 'url' => ['/master-penghasilan/create-upload']],
                                                ]
                                            ],
                                            ['label' => 'Mandor', 'items' => [
                                                    ['label' => 'Input Manual', 'url' => ['/pph-bukti-potong/create-pasal-21-mandor']]
                                                ]],
                                            ['label' => 'Honorer', 'items' => [
                                                    ['label' => 'Input Manual', 'url' => ['/pph-bukti-potong/create-pasal-21-honorer']]
                                                ]
                                            ]
                                        ]
                                    ],
                                    ['label' => 'Bukti Potong Pasal 22', 'url' => ['/pph-bukti-potong/create']],
                                    ['label' => 'Bukti Potong Pasal 23', 'items' => [
                                            ['label' => 'Jasa', 'url' => ['/pph-bukti-potong/create-pasal-23-jasa']],
                                            ['label' => 'Sewa', 'url' => ['/pph-bukti-potong/create-pasal-23-sewa']],
                                        ]
                                    ],
                                    ['label' => 'Bukti Potong Pasal 4 (2)', 'items' => [
                                            ['label' => 'Jasa', 'url' => ['/pph-bukti-potong/create-pasal-4-jasa']],
                                            ['label' => 'Sewa', 'url' => ['/pph-bukti-potong/create-pasal-4-sewa']],
                                            ['label' => 'Perencana', 'url' => ['/pph-bukti-potong/create-pasal-4-perencana']],
                                        ]
                                    ],
                                ]
                            ],
                            ['label' => 'Daftar Data', 'items' => [
                                    ['label' => 'Pasal 21', 'items' => [
                                        ['label' => 'Pegawai Tetap', 'url' => ['/master-penghasilan/index']],
                                        ['label' => 'Pegawai Tidak Tetap', 'url' => ['/pph-bukti-potong/index-pasal-21']],
                                        ]],
                                    ['label' => 'Bukti Potong Pasal 22', 'url' => ['/pph-bukti-potong/index']],
                                    ['label' => 'Bukti Potong Pasal 23', 'url' => ['/pph-bukti-potong/index-pasal-23']],
                                    ['label' => 'Bukti Potong Pasal 4 (2)', 'url' => ['/pph-bukti-potong/index-pasal-4']],
                                ]
                            ],
                            ['label' => 'Master Data', 'items' => [
                                    ['label' => 'Wajib Pajak', 'items' => [
                                        ['label' => 'List Wajib Pajak', 'url' => ['/pph-wajib-pajak/index']],
                                    ]],
                                    ['label' => 'Pegawai', 'url' => ['/master-pegawai/index']],
                                    ['label' => 'Pegawai Tidak Tetap', 'items' => [
                                            ['label' => 'Mandor', 'url' => ['/master-mandor/index']],
                                            ['label' => 'Honorer', 'url' => ['/master-honorer/index']],
                                        ]],
                                    ['label' => 'Sertifikat', 'items' => [
                                            ['label' => 'IUJK', 'url' => ['/pph-iujk/index']],
                                            ['label' => 'BUJPK', 'url' => ['/pph-bujpk/index']],
                                        ]
                                    ]
                                ]
                            ],
                            ['label' => 'User', 'items' => [
                                    ['label' => 'Change Password', 'url' => ['/setting/change']],
                                    Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                            'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                                    )
                                    . Html::endForm()
                                ]
                            ],
                        ];
                    }
                } else {
                    $menuItems = [
                        ['label' => 'User', 'items' => [
                                Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                        'Logout (' . Yii::$app->user->identity->username . ')', ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                            ]
                        ],
                    ];
                }
            }

            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems
//        'items' => [
////            ['label' => 'Home', 'url' => ['/site/index']],
////            ['label' => 'About', 'url' => ['/site/about']],
////            ['label' => 'Contact', 'url' => ['/site/contact']],
//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/admin/user/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
//        ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'homeLink' => false
                ])
                ?>

                <?php
//echo "asdasd";
// print_r(Yii::$app->authManager->getRole("admin"));
                ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; WASKITA KARYA <?= date('Y') ?></p>

                <p class="pull-right"> Powered by  <a href="https://scan.barcodefaktur.com/web/user/login">PT. IDEMAS SOLUSINDO SENTOSA</a></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

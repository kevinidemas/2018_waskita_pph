<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Options */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="options-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- $form->field($model, 'name')->textInput(['maxlength' => true, 'readOnly' => true]) -->

    <div id="options-title">
        <div id='options-form'>
            <p>Validasi Aturan Nomor Bukti Potong</p>
        </div>
    </div>
    <div id="options-value-1">
        <?=
//    $form->field($model, 'value')->textInput()
        $form->field($model, 'value')->dropDownList([
            '1' => 'Ya',
            '2' => 'Tidak',
        ]);
        ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphJenisKelamin */

$this->title = 'Update Pph Jenis Kelamin: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Kelamins', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenisKelaminId, 'url' => ['view', 'id' => $model->jenisKelaminId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-jenis-kelamin-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

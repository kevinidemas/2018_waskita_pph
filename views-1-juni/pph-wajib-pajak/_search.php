<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\PphWajibPajakSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-wajib-pajak-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'wajibPajakId') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'npwp') ?>

    <?= $form->field($model, 'alamat') ?>

    <?= $form->field($model, 'statusNpwpId') ?>

    <?php // echo $form->field($model, 'iujkId') ?>

    <?php // echo $form->field($model, 'statusWpId') ?>

    <?php // echo $form->field($model, 'jenisWp') ?>

    <?php // echo $form->field($model, 'userId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

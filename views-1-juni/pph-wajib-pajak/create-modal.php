<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphWajibPajak */

$this->title = 'Wajib Pajak Baru';
$this->params['breadcrumbs'][] = ['label' => 'Wajib Pajak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-wajib-pajak-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-modal', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphWajibPajak */

$this->title = 'Dengan NPWP';
$this->params['breadcrumbs'][] = ['label' => 'Wajib Pajak', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-wajib-pajak-create">

    <h5><?= Html::encode($this->title) ?></h5>

    <?= $this->render('_form-modal-23-sewa', [
        'model' => $model,
    ]) ?>

</div>

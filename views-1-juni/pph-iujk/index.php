<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\models\PphWajibPajak;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphIujkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar IUJK';
//$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('modalJs');
$this->registerCssFile('modalCss');

if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert-single alert-danger">';
    $alertBootstrap .= $msgErrors;
    $alertBootstrap .= '</div>';

    echo $alertBootstrap;
}    

?>

<!--<link rel="stylesheet" href="@webroot/css/style/magnific-popup.css">-->
<!--<script src="@webroot/js/jquery.magnific-popup.js"></script>-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<div class="pph-iujk-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- Html::a('Create Pph Iujk', ['create'], ['class' => 'btn btn-success']) -->
    </p>

    <?php
    if (Yii::$app->user->id == 1) {
        echo GridView::widget([
            'filterModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'font-size:12px;'],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => '-pjax',
                    'enableReplaceState' => false,
                    'enablePushState' => false,
                ],
            ],
            'striped' => true,
            'hover' => true,
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'tableOptions' => ['class' => 'table table-hover'],
            'resizableColumns' => true,
            'rowOptions' => function ($model, $key, $index, $grid) {
                if ($model['is_expired_warning'] == 1) {
                    return ['style' => "background-color: #ebcccc;"];
                }
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'options' => [
                        'width' => '10px',
                    ],
                ],
                [
                    'attribute' => 'nomorIujk',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '150px',
                    ],
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'published_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->published_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'expired_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->expired_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '10px',
                    ],
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'penanggungJawab',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'options' => [
                        'width' => '200px',
                    ],
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'wajibPajakId',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->wajibPajakId == null) {
                            return '<i style="color: #ff0000">(Kosong)</i>';
                        } else {
                            $modelWp = PphWajibPajak::find()->where(['wajibPajakId' => $data->wajibPajakId])->one();
                            $nama = $modelWp->nama;
                            return $nama;
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'kemampuanKeuangan',
                    'format' => ['decimal', 0],
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'mergeHeader' => true,
                    'options' => [
                        'width' => '150px',
                    ],
                ],
                [
                    'attribute' => 'is_approve',
                    'header' => 'Status Verifikasi',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->is_approve == 0) {
                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else if ($data->is_approve == 1) {
                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        } else {
                            return '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '20px',
                    'mergeHeader' => true
                ],
                [
                    'header' => 'Aksi Verifikasi',
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '110', 'align' => 'center'],
                    'template' => '{approve} {reject}',
                    'buttons' => [
                        'approve' => function ($url, $model) {
                            return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
                                        'title' => Yii::t('app', 'Approve'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                        'reject' => function ($url, $model) {
                            return Html::a('<button type="button" class="btn btn-primary-red"><i class="glyphicon glyphicon-remove-sign"></i></button>', $url, [
                                        'title' => Yii::t('app', 'Reject'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action == 'approve') {
                            $url = Url::toRoute(['pph-iujk/approve', 'id' => $model->iujkId]);
                            return $url;
                        } else {
                            $url = Url::toRoute(['pph-iujk/reject', 'id' => $model->iujkId]);
                            return $url;
                        }
                    },
                ],
//                [
//                    'attribute' => 'is_expired_warning',
//                    'format' => 'raw',
//                    'value' => function ($data) {
//                        if ($data->is_expired_warning == 1) {
//                            return 'Masa Berlaku';
//                        } else {
//                            return '-';
//                        }
//                    },
//                    'vAlign' => 'middle',
//                    'hAlign' => 'center',
//                    'options' => [
//                        'width' => '10px',
//                    ],
//                ],
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'options' => [
//                        'width' => '60px',
//                    ],
//                ],
                [
                    'header' => 'Action',
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => ['width' => '85', 'align' => 'center'],
                    'template' => '{download}&nbsp;&nbsp;{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url, [
                                        'title' => Yii::t('app', 'view'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                        'download' => function ($url, $model) {
                            return Html::a('<i class="glyphicon glyphicon-download-alt"></i>', $url, [
                                        'title' => Yii::t('app', 'download'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<i class="glyphicon glyphicon-trash"></i>', $url, [
                                        'title' => Yii::t('app', 'add'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="glyphicon glyphicon-edit"></i>', $url, [
                                        'title' => Yii::t('app', 'update'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                    ],
                    'urlCreator' => function ($action, $model, $key, $index) {
                        if ($action === 'view') {
                            $url = Url::toRoute(['pph-iujk/view', 'id' => $model->iujkId], ['data-method' => 'post',]);
                            return $url;
                        } else if ($action === 'delete') {
                            $url = Url::toRoute(['pph-iujk/delete', 'id' => $model->iujkId], ['data-method' => 'post',]);
                            return $url;
                        } else if ($action === 'download') {
                            $url = Url::toRoute(['pph-iujk/download', 'id' => $model->iujkId], ['data-method' => 'post',]);
                            return $url;
                        } else if ($action === 'update') {
                            $url = Url::toRoute(['pph-iujk/update', 'id' => $model->iujkId], ['data-method' => 'post',]);
                            return $url;
                        }
                    }
                ],
            ],
            'toolbar' => [
                ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
                'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR IUJK</b>',
            ],
            'persistResize' => false,
            'toggleDataOptions' => ['minCount' => 10]
        ]);
    } else {
        echo GridView::widget([
            'filterModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'font-size:12px;'],
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
                'options' => [
                    'id' => '-pjax',
                    'enableReplaceState' => false,
                    'enablePushState' => false,
                ],
            ],
            'striped' => true,
            'hover' => true,
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'tableOptions' => ['class' => 'table table-hover'],
            'resizableColumns' => true,
            'rowOptions' => function ($model, $key, $index, $grid) {
                if ($model['is_expired_warning'] == 1) {
                    return ['style' => "background-color: #ebcccc;"];
                }
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'options' => [
                        'width' => '10px',
                    ],
                ],
                [
                    'attribute' => 'nomorIujk',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '200px',
                ],
                [
                    'attribute' => 'published_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->published_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '146px',
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'expired_at',
                    'value' => function ($data) {
                        return date('d-m-Y', strtotime($data->expired_at));
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '146px',
                    'mergeHeader' => true
                ],
                [
                    'attribute' => 'penanggungJawab',
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '230px',
                ],
                [
                    'attribute' => 'wajibPajakId',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->wajibPajakId == null) {
                            return '<i style="color: #ff0000">(Kosong)<i>';
                        } else {
                            $wpId = $data->wajibPajakId;
                            $modelWajibPajak = PphWajibPajak::find()->where(['wajibPajakId' => $wpId])->one();
//                            print_r($modelWajibPajak);
//                            die();
                            $namaWp = $modelWajibPajak->nama;
                            return $namaWp;
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'width' => '320px',
                    'mergeHeader' => true
                ],
//                [
//                    'attribute' => 'is_active',
//                    'format' => 'raw',
//                    'value' => function ($data) {
//                        if ($data->is_active == 0) {
//                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
//                        } else {
//                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
//                        }
//                    },
//                    'vAlign' => 'middle',
//                    'hAlign' => 'center',
//                ],
                [
                    'attribute' => 'is_approve',
                    'header' => 'Status Disetujui',
                    'format' => 'raw',
                    'value' => function ($data) {
                        if ($data->is_approve == 0) {
                            return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                        } else if ($data->is_approve == 1) {
                            return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                        } else {
                            return '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                        }
                    },
                    'vAlign' => 'middle',
                    'hAlign' => 'center',
                    'mergeHeader' => true
                ],
//                [
//                    'header' => 'Active Status',
//                    'class' => 'yii\grid\ActionColumn',
//                    'headerOptions' => ['width' => '15', 'align' => 'center'],
//                    'template' => '{active}',
//                    'buttons' => [
//                        'active' => function ($url, $model) {
//                            return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
//                                        'title' => Yii::t('app', 'Active'),
//                                        'data-toggle' => "modal",
//                                        'data-target' => "#myModal",
//                                        'data-method' => 'post',
//                            ]);
//                        },
//                    ],
//                    'urlCreator' => function ($action, $model, $key, $index) {
//                        $url = Url::toRoute(['pph-iujk/active', 'id' => $model->iujkId]);
//                        return $url;
//                    },
//                ],
//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'options' => [
//                        'width' => '60px',
//                    ],
//                ],
            ],
            'toolbar' => [
                ['content' =>
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                ],
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
                'heading' => '<i class="glyphicon glyphicon-align-left"></i>&nbsp;&nbsp;<b>DAFTAR IUJK</b>',
            ],
            'persistResize' => false,
            'toggleDataOptions' => ['minCount' => 10]
        ]);
    }
    ?>
</div>
<div class="col-md-12" style="background-color:#FFE4C4; border: 2px solid #FFE4C4; border-radius: 3px;">
    <b>Catatan :</b>
    <p style="margin-bottom: -3px"><i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>&emsp;&#x261B; <strong> Diterima</strong></p>    
    <p style="margin-bottom: -3px"><i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>&emsp;&#x261B; <strong> Ditolak</strong></p>    
    <p><i class="glyphicon glyphicon-minus" style="color: #000000"></i>&emsp;&#x261B; <strong> Belum diberikan keputusan</strong></p>    
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphBujpk */

$this->title = 'Update Pph Bujpk: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Bujpks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bujpkId, 'url' => ['view', 'id' => $model->bujpkId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-bujpk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

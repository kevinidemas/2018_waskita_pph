<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MasterIujk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-iujk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nomorIujk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'validDate')->textInput() ?>

    <?= $form->field($model, 'userId')->textInput() ?>

    <?= $form->field($model, 'parentId')->textInput() ?>

    <?= $form->field($model, 'tarifId')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

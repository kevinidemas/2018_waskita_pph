<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\search\MasterIujkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-iujk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'iujkId') ?>

    <?= $form->field($model, 'nomorIujk') ?>

    <?= $form->field($model, 'validDate') ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'parentId') ?>

    <?php // echo $form->field($model, 'tarifId') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

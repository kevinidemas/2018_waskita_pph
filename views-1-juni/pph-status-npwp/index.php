<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphStatusNpwpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Status Npwps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-status-npwp-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Status Npwp', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'npwpId',
            'tarifId',
            'userId',
            'parentId',
            'pasalId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

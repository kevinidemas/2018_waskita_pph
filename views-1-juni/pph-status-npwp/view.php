<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusNpwp */

$this->title = $model->npwpId;
$this->params['breadcrumbs'][] = ['label' => 'Pph Status Npwps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-status-npwp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->npwpId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->npwpId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'npwpId',
            'tarifId',
            'userId',
            'parentId',
            'pasalId',
        ],
    ]) ?>

</div>

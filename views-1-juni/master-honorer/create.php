<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MasterHonorer */

$this->title = 'Create Master Honorer';
$this->params['breadcrumbs'][] = ['label' => 'Master Honorers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-honorer-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterHonorerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Honorers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-honorer-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Honorer Baru', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'honorerId',
            'nama',
            'posisi',
            'nik',
            'npwp',
            //'alamat',
            //'jenisKelamin',
            //'statusKerjaId',
            //'userId',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            //'hp',
            //'email:email',
            //'approvalStatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

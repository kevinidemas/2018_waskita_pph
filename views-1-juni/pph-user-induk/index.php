<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphUserIndukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph User Induks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-user-induk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph User Induk', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'userIndukId',
            'userId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphJenisPenghasilan */

$this->title = 'Update Pph Jenis Penghasilan: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Penghasilans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->jenisPenghasilanId, 'url' => ['view', 'id' => $model->jenisPenghasilanId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-jenis-penghasilan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

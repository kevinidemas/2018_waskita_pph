<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphNomorBuktiPotong */

$this->title = 'Create Pph Nomor Bukti Potong';
$this->params['breadcrumbs'][] = ['label' => 'Pph Nomor Bukti Potongs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-nomor-bukti-potong-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphNomorBuktiPotong */

$this->title = 'Update Pph Nomor Bukti Potong: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Nomor Bukti Potongs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nomorBuktiPotongId, 'url' => ['view', 'id' => $model->nomorBuktiPotongId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-nomor-bukti-potong-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

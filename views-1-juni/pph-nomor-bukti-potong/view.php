<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphNomorBuktiPotong */

$this->title = $model->nomorBuktiPotongId;
$this->params['breadcrumbs'][] = ['label' => 'Pph Nomor Bukti Potongs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-nomor-bukti-potong-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nomorBuktiPotongId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nomorBuktiPotongId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nomorBuktiPotongId',
            'tahun',
            'bulan',
            'lastNomorBP',
            'prevLastNomorBP',
            'userId',
            'created_at',
        ],
    ]) ?>

</div>

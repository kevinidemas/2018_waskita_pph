<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$modelUser = User::find()->where(['id' => $model->userId])->one();
$name = $modelUser->username;
$this->title = ucfirst($name);
$this->params['breadcrumbs'][] = ['label' => 'Pengguna', 'url' => ['user/admin/index']];
$this->params['breadcrumbs'][] = $this->title;
$session = Yii::$app->session;
$session['user-id-setting'] = $_GET['id'];
?>
<div class="setting-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-edit"></i> Ubah', ['update', 'id' => $model->settingId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Kembali', ['/user/admin/index'], ['class' => 'btn btn-danger']) ?>
    </p>    

    <?php
    if ($model->settingId == 1) {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                 'attribute' => 'userIndukId',
                    'value' => function ($model) {
                        if($model->userIndukId != null){
                            $modelUser = User::find()->where(['id' => $model->userIndukId])->one();
                            $userIndukId = $modelUser->username;
                            return ucfirst($userIndukId);
                        }
                    },
                ],
                'namaPemungutPajak',
                'npwpPemungutPajak',                
                'kppTerdaftar',
                'nama',
                'jabatan',
                'kota',
                'prefixNomorBP_21',
                'startNomorBP_21',
                'endNomorBP_21',
                'prefixNomorBP',
                'digitNomorBP',
                'startNomorBP',
                'prefixNomorBP_23',
                'digitNomorBP_23',
                'startNomorBP_23',
                'prefixNomorBP_4',
                'digitNomorBP_4',
                'startNomorBP_4',
                [
                    'attribute' => 'userId',
                    'label' => 'User',
                    'value' => function ($model) {
                        $modelUser = User::find()->where(['id' => $model->userId])->one();
                        $userId = $modelUser->username;
                        return ucfirst($userId);
                    },
                        ],
                    ],
                ]);
            } else {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'userIndukId',
                            'value' => function ($model) {
                               if($model->userIndukId != null){
                                   $modelUser = User::find()->where(['id' => $model->userIndukId])->one();
                                   $userIndukId = $modelUser->username;
                                   return ucfirst($userIndukId);
                               }
                           },
                        ],
                        'npwpPemungutPajakCabang',
                        'nama',
                        'jabatan',
                        'kota',
                        'kppTerdaftar',
                        'prefixNomorBP_21',
                        'startNomorBP_21',
                        'endNomorBP_21',                        
                        'prefixNomorBP',
                        'digitNomorBP',
                        'startNomorBP',
                        'prefixNomorBP_23',
                        'digitNomorBP_23',
                        'startNomorBP_23',
                        'prefixNomorBP_4',
                        'digitNomorBP_4',
                        'startNomorBP_4',
                        [
                            'attribute' => 'pasal21_pt',
                            'label' => 'Multi NPWP Pasal 21 PT',
                            'value' => function ($model) {
                                if ($model->pasal21_pt == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal21_ptt',
                            'label' => 'Multi NPWP Pasal 21 PTT',
                            'value' => function ($model) {
                                if ($model->pasal21_ptt == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal22',
                            'label' => 'Multi NPWP Pasal 22',
                            'value' => function ($model) {
                                if ($model->pasal22 == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal23_jasa',
                            'label' => 'Multi NPWP Pasal 23 - Jasa',
                            'value' => function ($model) {
                                if ($model->pasal23_jasa == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal23_sewa',
                            'label' => 'Multi NPWP Pasal 23 - Sewa',
                            'value' => function ($model) {
                                if ($model->pasal23_sewa == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal4_jasa',
                            'label' => 'Multi NPWP Pasal 4 Ayat (2) - Jasa',
                            'value' => function ($model) {
                                if ($model->pasal4_jasa == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal4_sewa',
                            'label' => 'Multi NPWP Pasal 4 Ayat (2) - Sewa',
                            'value' => function ($model) {
                                if ($model->pasal4_sewa == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'pasal4_perencana',
                            'label' => 'Multi NPWP Pasal 4 Ayat (2) - Perencana',
                            'value' => function ($model) {
                                if ($model->pasal4_perencana == 1) {
                                    return ucfirst('Ya');
                                } else {
                                    return ucfirst('Tidak');
                                }
                            },
                        ],
                        [
                            'attribute' => 'userId',
                            'label' => 'User',
                            'value' => function ($model) {
                                $modelUser = User::find()->where(['id' => $model->userId])->one();
                                $userId = $modelUser->username;
                                return ucfirst($userId);
                            },
                                ],
                            ],
                        ]);
                    }
                    ?>

</div>

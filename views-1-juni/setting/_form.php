<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\PphUserInduk;
use app\models\Setting;
use app\models\User;
use yii\helpers\ArrayHelper;

//use app\assets\AppAsset;
//
//AppAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Setting */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#wp-npwp").mask("99.999.999.9-999.999");
        $("#wp-npwp-cabang").mask("99.999.999.9-999.999");
    });
</script>

<?php
$session = Yii::$app->session;
$userId = Yii::$app->user->id;

$dataUserInduk = PphUserInduk::find()->asArray()->all();
$arrayUserIndukId = ArrayHelper::getColumn($dataUserInduk, 'userId');
// echo '<pre>';
// print_r($arrayUserIndukId);
// die();
if (isset($session['user-id-setting'])) {
    $id = $session['user-id-setting'];
}
if ($arrayUserIndukId != null) {
    for ($n = 0; $n < count($arrayUserIndukId); $n++) {
        $mn = $arrayUserIndukId[$n];
        if($mn != $id){
            if (isset($arrayUserIndukId[$n])) {
                if($arrayUserIndukId[$n] == $model->userId){                
                    if(count($arrayUserIndukId) == 1){                    
                        $userIndukOptions[] = 'KOSONG';
                    }
                }  else {                    
                        $modelSetting = Setting::find()->where(['userId' => $arrayUserIndukId[$n]])->one();
                        $modelUser = User::find()->where(['id' => $arrayUserIndukId[$n]])->one();
                        $userIndukOptions[$mn] = $modelSetting->npwpPemungutPajakCabang . ' (' . ucfirst($modelUser->username) . ')';
                }
            }
        } else if($mn == $id && count($arrayUserIndukId) <= 1){
            $userIndukOptions[] = 'KOSONG';
        }
    }
} else {
    $userIndukOptions[] = 'KOSONG';
}


if (isset($session['user-id-setting'])) {
    $id = $session['user-id-setting'];
    $setValA1 = 0;
    $setValA2 = 0;
    $setValB = 0;
    $setValC1 = 0;
    $setValC2 = 0;
    $setValD1 = 0;
    $setValD2 = 0;
    $setValD3 = 0;
    $value = Yii::$app->db->CreateCommand("SELECT * FROM setting WHERE userId = '$id'")->queryAll();
    
    if ($value != null) {
        $a1 = $value[0]['pasal21_pt'];
        $a2 = $value[0]['pasal21_ptt'];
        $b = $value[0]['pasal22'];
        $c1 = $value[0]['pasal23_jasa'];
        $c2 = $value[0]['pasal23_sewa'];
        $d1 = $value[0]['pasal4_jasa'];
        $d2 = $value[0]['pasal4_sewa'];
        $d3 = $value[0]['pasal4_perencana'];
        if ($a1 == 1) {
            $setValA1 = 1;
        }
        if ($a2 == 1) {
            $setValA2 = 1;
        }
        if ($b == 1) {
            $setValB = 1;
        }
        if ($c1 == 1) {
            $setValC1 = 1;
        }
        if ($c2 == 1) {
            $setValC2 = 1;
        }
        if ($d1 == 1) {
            $setValD1 = 1;
        }
        if ($d2 == 1) {
            $setValD2 = 1;
        }
        if ($d3 == 1) {
            $setValD3 = 1;
        }
    }
} else {
    $setVal = 0;
}

?>
<div class="setting-form">
    <div style="padding-bottom: 20px">
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <?php
    if ($model->settingId == 1) {
        echo $form->field($model, 'npwpPemungutPajak')->textInput(['maxlength' => true, 'id' => 'wp-npwp']);

        echo $form->field($model, 'namaPemungutPajak')->textInput(['maxlength' => true]);
    }
    ?>
    <input type="hidden" id="status-npwp-21-pt" name="status-npwp" value=<?php echo $setValA1 ?>> 
    <input type="hidden" id="status-npwp-21-ptt" name="status-npwp" value=<?php echo $setValA2 ?>> 
    <input type="hidden" id="status-npwp-22" name="status-npwp" value=<?php echo $setValB ?>> 
    <input type="hidden" id="status-npwp-23-jasa" name="status-npwp" value=<?php echo $setValC1 ?>> 
    <input type="hidden" id="status-npwp-23-sewa" name="status-npwp" value=<?php echo $setValC2 ?>> 
    <input type="hidden" id="status-npwp-4-jasa" name="status-npwp" value=<?php echo $setValD1 ?>> 
    <input type="hidden" id="status-npwp-4-sewa" name="status-npwp" value=<?php echo $setValD2 ?>> 
    <input type="hidden" id="status-npwp-4-perencana" name="status-npwp" value=<?php echo $setValD3 ?>> 
    
    <?php if ($model->settingId != 1) { 
        echo $form->field($model, 'npwpPemungutPajakCabang')->textInput(['maxlength' => true, 'id' => 'wp-npwp-cabang']);
        ?>
    <div style="width: 100%; margin-left: 0px" id="checkbox-npwp">
        <input type="checkbox" class="checkbox-pasal" id="pasal-21-pt" name="pasal-21-pt" value="1"> Pasal 21 PT
        <input type="checkbox" class="checkbox-pasal" id="pasal-21-ptt" name="pasal-21-ptt" value="1"> Pasal 21 PTT
        <input type="checkbox" class="checkbox-pasal" id="pasal-22" name="pasal-22" value="1"> Pasal 22
        <input type="checkbox" class="checkbox-pasal" id="pasal-23-jasa" name="pasal-23-jasa" value="1"> Pasal 23 - Jasa
        <input type="checkbox" class="checkbox-pasal" id="pasal-23-sewa" name="pasal-23-sewa" value="1"> Pasal 23 - Sewa
        <input type="checkbox" class="checkbox-pasal" id="pasal-4-jasa" name="pasal-4-jasa" value="1"> Pasal 4 - Jasa
        <input type="checkbox" class="checkbox-pasal" id="pasal-4-sewa" name="pasal-4-sewa" value="1"> Pasal 4 - Sewa
        <input type="checkbox" class="checkbox-pasal" id="pasal-4-perencana" name="pasal-4-perencana" value="1"> Pasal 4 - Perencana
    </div>
    <?php } ?>
    
    <?php
        echo '<label class="control-label">Unit Bisnis Induk Pasal 21 - PTT</label>';
        echo Select2::widget([
            'model' => $model,
            'attribute' => 'userIndukId',
            'data' => $userIndukOptions,
            'options' => [
                'placeholder' => Yii::t('app', 'Pilih Unit Bisnis Induk'),
                'id' => 'pph-user-induk'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        echo '<br>';
    ?>
    
    <?= $form->field($model, 'kppTerdaftar')->textInput(['maxlength' => true]); ?>

    <?= $form->field($model, 'kota')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

    <div id="setting-container">
        <div id="setting-title">
            <div id='setting-form'>
                <p>Pasal 21</p>
            </div>
        </div>
        <?= $form->field($model, 'prefixNomorBP_21')->textInput([
            'maxlength' => true, 
            'value' => '1.3', 
            'readOnly' => true,            
            ]) 
        ?>
        
        <?= $form->field($model, 'startNomorBP_21')->textInput([
            'maxlength' => true,
            'options' => [
              'id' => 'bp21_s1'
            ],
            ]) ?>
        
        <?= $form->field($model, 'endNomorBP_21')->textInput(['maxlength' => true]) ?>
    </div>
    
    <div id="setting-container">
        <div id="setting-title">
            <div id='setting-form'>
                <p>Pasal 22</p>
            </div>
        </div>
        <?= $form->field($model, 'prefixNomorBP')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'digitNomorBP')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'startNomorBP')->textInput(['maxlength' => true]) ?>
    </div>

    <div id="setting-container">
        <div id="setting-title">
            <div id='setting-form'>
                <p>Pasal 23</p>
            </div>
        </div>
        <?= $form->field($model, 'prefixNomorBP_23')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'digitNomorBP_23')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'startNomorBP_23')->textInput(['maxlength' => true]) ?>
    </div>

    <div id="setting-container">
        <div id="setting-title">
            <div id='setting-form-4'>
                <p>Pasal 4 Ayat (2)</p>
            </div>
        </div>
        <?= $form->field($model, 'prefixNomorBP_4')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'digitNomorBP_4')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'startNomorBP_4')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

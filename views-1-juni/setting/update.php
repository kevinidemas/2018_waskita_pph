<?php

use yii\helpers\Html;
use app\models\user;
/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$this->title = 'Update Setting';
//$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$modelUser = User::find()->where(['id' => $model->userId])->one();
$user = ucfirst($modelUser->username);
$this->params['breadcrumbs'][] = ['label' => $user, 'url' => ['view', 'id' => $model->settingId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

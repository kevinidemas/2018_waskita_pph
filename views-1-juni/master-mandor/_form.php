<?php

use yii\helpers\Html;
use kartik\builder\FormGrid;
use kartik\builder\Form;
use kartik\form\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use app\models\PphJenisKelamin;

/* @var $this yii\web\View */
/* @var $model app\models\MasterMandor */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script>
    jQuery(function ($) {
        $("#mandor-nik").mask("9999999999999999");
        $("#mandor-npwp").mask("99.999.999.9-999.999");
    });
</script>

<div class="master-mandor-form">

    <?php 
    //ambil options untuk jenis kelamin
    $dataJenisKelamin = PphJenisKelamin::find()->asArray()->all();
    $arrayJenisKelamin = ArrayHelper::getColumn($dataJenisKelamin, 'jenisKelaminId');
    $countArrayJK = count($arrayJenisKelamin);
    $jenKelOptions = [];
    for ($n = 0; $n < $countArrayJK; $n++) {
        $m = $n + 1;
        if (isset($arrayJenisKelamin[$n]) && $n != 2) {
            $modelJK = PphJenisKelamin::find()->where(['jenisKelaminId' => $arrayJenisKelamin[$n]])->one();
            $jenKelOptions[$m] = $modelJK->jenis;
        }
    }
    
    $form = ActiveForm::begin([
                'enableAjaxValidation' => false
    ]);
    echo FormGrid::widget([
        'model' => $model,
        'form' => $form,
        'autoGenerateColumns' => true,
        'rows' => [
            [
                'attributes' => [
                    'nama' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nama Mandor',
                            'id' => 'mandor-nama'
                        ]
                    ],
                ],
            ],
            [
                'attributes' => [
                    'jenisKelamin' => [
                        'type' => Form::INPUT_DROPDOWN_LIST,
                        'items' => $jenKelOptions,
                        'hint' => 'Pilih Jenis Kelamin',
                    ],
                    'posisi' => [
                        'type' => Form::INPUT_TEXT,
                        'placeholder' => 'Masukkan Posisi Kerja Mandor',
                    ],
                ]
            ],
            [
                'attributes' => [
                    'nik' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan NIK Mandor',
                            'id' => 'mandor-nik'
                        ]
                    ],
                    'npwp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan NPWP Mandor',
                            'id' => 'mandor-npwp'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'hp' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan Nomor Telepon Mandor',
                            'id' => 'mandor-hp'
                        ]
                    ],
                    'email' => [
                        'type' => Form::INPUT_TEXT,
                        'options' => [
                            'placeholder' => 'Masukkan alamat E-mail Mandor',
                            'id' => 'mandor-email'
                        ],
                    ],
                ],
            ],
            [
                'attributes' => [
                    'alamat' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => 'Masukkan Alamat Lengkap...']],
                ]
            ],
//            [
//                'attributes' => [
//                    'actions' => [
//                        'type' => Form::INPUT_RAW,
//                        'value' => '<div style="text-align: right; margin-top: 20px">' .
//                        Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', [
//                            'class' => 'btn btn-default',
//                        ]) . ' ' .
//                        Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', [
//                            'class' => 'btn btn-primary',
////                            'disabled' => true
//                        ]) .
//                        '</div>'
//                    ],
//                ],
//            ],
        ],
    ]);
    ?>
    
    <?=
    $form->field($model, 'buktiNpwp')->widget(FileInput::classname(), [
        'options' => [
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'doc', 'docx', 'pdf', 'txt', 'pptx', 'ppt', 'xlsx', 'xls'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ]);
    ?>
    <?=
    $form->field($model, 'buktiNik')->widget(FileInput::classname(), [
        'options' => [
            'multiple' => false,
            'accept' => 'img/*', 'doc/*', 'file/*',
            'class' => 'optionvalue-img',
            'placeholder' => 'maximum size is 2 MB',
        ],
        'pluginOptions' => [
            'allowedFileExtensions' => ['jpg', 'gif', 'png', 'doc', 'docx', 'pdf', 'txt', 'pptx', 'ppt', 'xlsx', 'xls'],
            'maxFileSize' => 2048, //membatasi size file upload
            'layoutTemplates' => ['footer' => 'Maximum size is 2 MB'],
            'browseLabel' => 'Browse (2MB)',
            'showPreview' => false,
            'showCaption' => true,
            'showRemove' => true,
            'showUpload' => false
        ]
    ]);
    ?>

    <div class="form-group">
        <?php echo '<div class="text-right" style="margin-right: 18px">' . Html::resetButton('<i class="glyphicon glyphicon-refresh"></i> Reset', ['class' => 'btn btn-default']) . ' ' . Html::submitButton('<i class="glyphicon glyphicon-save"></i> Submit', ['class' => 'btn btn-primary', 'id' => 'btn-create-bp']) . '</div>';
        ?>
    </div>
    <?php 
    ActiveForm::end();
    ?>
</div>

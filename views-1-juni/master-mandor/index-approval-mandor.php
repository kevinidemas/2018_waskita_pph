<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\alert\Alert;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\search\MasterMandorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Mandors';

$session = Yii::$app->session;
if (isset($session['notification-not-found'])) {
    $msgUpdateFailed = $session['notification-not-found'];
    unset($session['notification-not-found']);
    echo Alert::widget([
        'type' => Alert::TYPE_DANGER,
        'title' => 'Tidak Ditemukan!',
        'icon' => 'glyphicon glyphicon-remove-sign',
        'body' => $msgUpdateFailed,
        'showSeparator' => true,
        'delay' => 2000
    ]);
} else if (isset($session['notification-found'])) {
    $msgGenerateFailed = $session['notification-found'];
    unset($session['notification-found']);
    echo Alert::widget([
        'type' => Alert::TYPE_INFO,
        'title' => 'Ditemukan!',
        'icon' => 'glyphicon glyphicon-info-sign',
        'body' => $msgGenerateFailed,
        'showSeparator' => true,
        'delay' => 2000
    ]);
}

?>
<div class="master-mandor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
            ],
            [
                'attribute' => 'nama',
                'options' => [
                    'width' => 200
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'npwp',
                'options' => [
                    'width' => 100
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'nik',
                'options' => [
                    'width' => 100
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'alamat',
                'options' => [
                    'width' => 200
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'posisi',
                'options' => [
                    'width' => 130
                ],
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'attribute' => 'buktiNpwp',
                'label' => 'Unduh NPWP',
                'format' => 'raw',
                'value' => function ($data) {
                    $npwp = $data->npwp;
                    if ($npwp == null) {
                        $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                    } else {
                        $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }

                    $url = Url::toRoute([
                                'master-mandor/download-npwp', 'id' => $data->mandorId// $model->iujkId
                                    ], ['data-method' => 'post',]);
                    return Html::a($result, $url, [
                                'title' => Yii::t('app', 'Download NPWP'),
                                'data-toggle' => "modal",
                                'data-target' => "#myModal",
                                'data-method' => 'post',
                    ]);
                },
                        'options' => [
                            'width' => 10
                        ],
                        'vAlign' => 'middle',
                        'hAlign' => 'center',
                    ],
                    [
                        'attribute' => 'buktiNik',
                        'label' => 'Unduh KTP',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $nik = $data->buktiNik;
                            if ($nik == null) {
                                $result = '<i class="glyphicon glyphicon-minus" style="color: #000000"></i>';
                            } else {
                                $result = '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                            }

                            $url = Url::toRoute([
                                        'master-mandor/download-ktp', 'id' => $data->mandorId// $model->iujkId
                                            ], ['data-method' => 'post',]);
                            return Html::a($result, $url, [
                                        'title' => Yii::t('app', 'Download SPPKP'),
                                        'data-toggle' => "modal",
                                        'data-target' => "#myModal",
                                        'data-method' => 'post',
                            ]);
                        },
                                'options' => [
                                    'width' => 10
                                ],
                                'vAlign' => 'middle',
                                'hAlign' => 'center',
                            ],
            [
                'header' => 'Approve',
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '4', 'align' => 'center'],
                'template' => '{active}',
                'buttons' => [
                    'active' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
                                    'title' => Yii::t('app', 'Approve'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                        ]);
                    },
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                    $url = Url::toRoute(['master-mandor/approve-mandor', 'id' => $model->mandorId]);
                    return $url;
                },
                    ],
                    [
                        'header' => 'Reject',
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '4', 'align' => 'center'],
                        'template' => '{active}',
                        'buttons' => [
                            'active' => function ($url, $model) {
                                return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-remove-sign"></i></button>', $url, [
                                            'title' => Yii::t('app', 'Reject'),
                                            'data-toggle' => "modal",
                                            'data-target' => "#myModal",
                                            'data-method' => 'post',
                                ]);
                            },
                                ],
                                'urlCreator' => function ($action, $model, $key, $index) {
                            $url = Url::toRoute(['master-mandor/reject-mandor', 'id' => $model->mandorId]);
                            return $url;
                        },
                            ],
                            ['class' => 'yii\grid\ActionColumn',
                                'options' => [
                                    'width' => 53
                                ],
                            ],
                        ],
                        'toolbar' => [
                            ['content' =>
                                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
                            ],
                            '{toggleData}',
                        ],
                        'persistResize' => false,
                        'toggleDataOptions' => ['minCount' => 10]
                    ]);
                    ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PphStatusKerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pph-status-kerja-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'statusKerja')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomor')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

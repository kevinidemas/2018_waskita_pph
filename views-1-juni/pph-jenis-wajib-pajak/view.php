<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PphJenisWajibPajak */

$this->title = $model->jenisWajibPajakId;
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Wajib Pajaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jenis-wajib-pajak-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->jenisWajibPajakId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->jenisWajibPajakId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'jenisWajibPajakId',
            'nama',
        ],
    ]) ?>

</div>

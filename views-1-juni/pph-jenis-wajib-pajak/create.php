<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphJenisWajibPajak */

$this->title = 'Create Pph Jenis Wajib Pajak';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Wajib Pajaks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jenis-wajib-pajak-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

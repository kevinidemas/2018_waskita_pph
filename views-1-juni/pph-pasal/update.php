<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PphPasal */

$this->title = 'Update Pph Pasal: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Pph Pasals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pasalId, 'url' => ['view', 'id' => $model->pasalId]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pph-pasal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

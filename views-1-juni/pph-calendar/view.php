<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\PphCalendar */

//$userId = $model->created_by;
//$modelUser = User::find()->where(['id' => $userId])->one();
//$name = $modelUser->username;
$this->title = $model->calendarNote;
$this->params['breadcrumbs'][] = ['label' => 'Pph Calendars', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-calendar-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->calendarId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->calendarId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?php
        $var = $model->startDate;
        $start = date("d-m-Y", strtotime($var));
    ?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'calendarId',
            [
                'attribute' => 'startDate',
                'value' => date('d-m-Y', strtotime($model->startDate)),
            ],
            [
                'attribute' => 'endDate',
                'value' => date('d-m-Y', strtotime($model->endDate)),
            ],
            'calendarNote',
            'created_at',
//            'created_by',
        ],
    ]) ?>

</div>

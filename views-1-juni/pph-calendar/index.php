<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\grid\DataColumn;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphCalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendar';
//$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->session->hasFlash('error')) {
    $msgErrors = Yii::$app->session->getFlash('error');
    $alertBootstrap = '<div class="alert alert-danger">';
    if (!is_array($msgErrors)) {
        $alertBootstrap .= $msgErrors;
        $alertBootstrap .= '</div>';
    } else {
        foreach ($msgErrors as $value) {
            $alertBootstrap .= $value . '<br/>';
        }
        $alertBootstrap .= '</div>';
    }
    echo $alertBootstrap;
}
?>
<div class="pph-calendar-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Calendar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'font-size:12px;'],
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
            'options' => [
                'id' => '-pjax',
                'enableReplaceState' => false,
                'enablePushState' => false,
            ],
        ],
        'striped' => true,
        'hover' => true,
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'tableOptions' => ['class' => 'table table-hover'],
        'resizableColumns' => true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn',
                'header' => 'No',
            ],
            [
                'header' => 'Start From',
                'attribute' => 'startDate',
                'value' => function ($data) {
                    return date('d-m-Y', strtotime($data->startDate));
                },
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'header' => 'End At',
                'attribute' => 'endDate',
                'value' => function ($data) {
                    return date('d-m-Y', strtotime($data->endDate));
                },
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'header' => 'Month',
                'attribute' => 'calendarNote',
                'format' => 'raw',
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'header' => 'Active Status',
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->status == 0) {
                        return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                    } else {
                        return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],
            [
                'header' => 'Close Status',
                'attribute' => 'is_permanent_close',
                'format' => 'raw',
                'value' => function ($data) {
                    if ($data->is_permanent_close == 0) {
                        return '<i class="glyphicon glyphicon-remove" style="color: #ff0000"></i>';
                    } else {
                        return '<i class="glyphicon glyphicon-ok" style="color: #00cc00"></i>';
                    }
                },
                'vAlign' => 'middle',
                'hAlign' => 'center',
            ],                        
            [
                'header' => 'Active',
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '15', 'align' => 'center'],
                'template' => '{active}',
                'buttons' => [
                    'active' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
                                    'title' => Yii::t('app', 'Change Active Status'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    $url = Url::toRoute(['pph-calendar/active', 'id' => $model->calendarId]);
                    return $url;
                },
            ],
            [
                'header' => 'Close',
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '15', 'align' => 'center'],
                'template' => '{active}',
                'buttons' => [
                    'active' => function ($url, $model) {
                        return Html::a('<button type="button" class="btn btn-primary"><i class="glyphicon glyphicon-ok-sign"></i></button>', $url, [
                                    'title' => Yii::t('app', 'Close Permanent'),
                                    'data-toggle' => "modal",
                                    'data-target' => "#myModal",
                                    'data-method' => 'post',
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    $url = Url::toRoute(['pph-calendar/close', 'id' => $model->calendarId]);
                    return $url;
                },
            ],
            [
                'header' => 'Action',
                'class' => 'kartik\grid\ActionColumn'
            ],
        ],
        'toolbar' => [
            ['content' =>
//                $removeBtn . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => Yii::t('app', 'Reset Grid')])
            ],
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_SUCCESS,
            'after' => '<em>* Atur lebar kolom dengan cara menarik garis tepi kolom.</em>',
//            'heading' => $headerText,
//            'before' => $tombol,
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10]
    ]);
    ?>
</div>

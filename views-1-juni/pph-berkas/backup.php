<?php

use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use backend\models\PstkSatuanhasil;

$formatter = Yii::$app->formatter;
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];
//echo $countPdf;
//die();
?>

<html>
    <style font-size="20px"></style>
    <body>
        <?php for ($n = 0; $n < $countPdf; $n++) { ?>
            <table style="margin-left: 12px;">
                <tr>
                    <td style="width: 18%">
                        <?= Html::img("@web/img/djp.png", ['width' => '60px', 'height' => '50px', 'margin-top' => '-10px']) ?>
                    </td>
                    <td style="padding-top: 12px; padding-left: 10px;">
                <font size="2" align="left"><center><b>DEPARTEMEN KEUANGAN REPUBLIK INDONESIA</b></center>
                <font size="2" align="left"><center><b>DIREKTORAT JENDERAL PAJAK</b></center>
                <font size="2" align="left"><center><b>KANTOR PELAYANAN PAJAK</b></center>
                <font size="2" align="left"><center><b>WP BESAR EMPAT</b></center>
            </td>
        </tr>
    </table>


    <table style="width: 75%; border: 1px solid black; border-collapse: collapse; margin-left: 135px; margin-top: 10px; background-color: #ccccb3">
        <tr style="width: 20%">
            <td>
        <font size="1"><center><b>BUKTI PEMUNGUTAN PPh PASAL 22</b></center>
    </td>
    </tr>
    <tr>
        <td>
    <font size="1"><center><b>(OLEH BADAN USAHA INDUSTRI/EKSPORTIR TERTENTU)</b></center>
    </td>
    </tr>
    <tr>        
        <td style="border-top: 1px solid black;">
    <font size="1"><center><b>NOMOR : <?php echo $nomorBuktiPotong ?></b></center>
    </td>

    </tr>
    </table>
    <!--<HR><HR>-->
    <!--<hr style="height: 5px" />-->

    <table class="good" border="none" width="100%" style="margin-left: 22px; margin-top: 6px; height: 3px;">
        <tr>
            <td width="280px" margin-right="1000px">
                <table style=" border-collapse: collapse;">
                    <tr>
                        <td style = "width: 96px; letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
                        <td width="17px" style="height: 12px">:</td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[0])) {
                                echo $npwp[0];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[1])) {
                                echo $npwp[1];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[2])) {
                                echo $npwp[2];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[3])) {
                                echo $npwp[3];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[4])) {
                                echo $npwp[4];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[5])) {
                                echo $npwp[5];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[6])) {
                                echo $npwp[6];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[7])) {
                                echo $npwp[7];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[8])) {
                                echo $npwp[8];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[9])) {
                                echo $npwp[9];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[10])) {
                                echo $npwp[10];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[11])) {
                                echo $npwp[11];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; width: 15px"></td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[12])) {
                                echo $npwp[12];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[13])) {
                                echo $npwp[13];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($npwp[14])) {
                                echo $npwp[14];
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style = "width: 96px; height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
                        <td width="17px" style="height: 12px">:</td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[0])) {
                                echo $nama[0];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[1])) {
                                echo $nama[1];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[2])) {
                                echo $nama[2];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[3])) {
                                echo $nama[3];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[4])) {
                                echo $nama[4];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[5])) {
                                echo $nama[5];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[6])) {
                                echo $nama[6];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[7])) {
                                echo $nama[7];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[8])) {
                                echo $nama[8];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[9])) {
                                echo $nama[9];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[10])) {
                                echo $nama[10];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[11])) {
                                echo $nama[11];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[12])) {
                                echo $nama[12];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[13])) {
                                echo $nama[13];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[14])) {
                                echo $nama[14];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[15])) {
                                echo $nama[15];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[16])) {
                                echo $nama[16];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[17])) {
                                echo $nama[17];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($nama[18])) {
                                echo $nama[18];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($nama[19])) {
                                echo $nama[19];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[20])) {
                                echo $nama[20];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[21])) {
                                echo $nama[21];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($nama[22])) {
                                echo $nama[22];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($nama[23])) {
                                echo $nama[23];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[24])) {
                                echo $nama[24];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[25])) {
                                echo $nama[25];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[26])) {
                                echo $nama[26];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[27])) {
                                echo $nama[27];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($nama[28])) {
                                echo $nama[28];
                            }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style = "width: 96px;height: 12px"><b><font align="left" size="2">Alamat</font></b></td>
                        <td width="17px" style="height: 12px">:</td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[0])) {
                                echo $alamat[0];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[1])) {
                                echo $alamat[1];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[2])) {
                                echo $alamat[2];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[3])) {
                                echo $alamat[3];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[4])) {
                                echo $alamat[4];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[5])) {
                                echo $alamat[5];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[6])) {
                                echo $alamat[6];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[7])) {
                                echo $alamat[7];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[8])) {
                                echo $alamat[8];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[9])) {
                                echo $alamat[9];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[10])) {
                                echo $alamat[10];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[11])) {
                                echo $alamat[11];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[12])) {
                                echo $alamat[12];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[13])) {
                                echo $alamat[13];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[14])) {
                                echo $alamat[14];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[15])) {
                                echo $alamat[15];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[16])) {
                                echo $alamat[16];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[17])) {
                                echo $alamat[17];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($alamat[18])) {
                                echo $alamat[18];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($alamat[19])) {
                                echo $alamat[19];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[20])) {
                                echo $alamat[20];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[21])) {
                                echo $alamat[21];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($alamat[22])) {
                                echo $alamat[22];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                            <?php
                            if (isset($alamat[23])) {
                                echo $alamat[23];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[24])) {
                                echo $alamat[24];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[25])) {
                                echo $alamat[25];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[26])) {
                                echo $alamat[26];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[27])) {
                                echo $alamat[27];
                            }
                            ?>
                        </td>
                        <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                            if (isset($alamat[28])) {
                                echo $alamat[28];
                            }
                            ?>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="border: 1px solid black; border-collapse: collapse; font-size: 10px; margin-top: 8px; margin-left: 16px; width: 93.1%">
        <tr>
            <th style="background-color: #ccccb3; width: 5.3%; text-align: center; border: 1px solid black; padding: -2px 0px 5px 0px; height: 80px">No</th>
            <th style="background-color: #ccccb3; width: 34%; text-align: center; border: 1px solid black; padding: -2px 0px 5px 0px; height: 80px; letter-spacing: 3px">Uraian</th>
            <th style="background-color: #ccccb3; width: 21.1%; text-align: center; border: 1px solid black; padding: -2px 0px 5px 0px; height: 80px">Harga(Rp)</th>
            <th style="background-color: #ccccb3; width: 9%; text-align: center; font-size: 8.5px;border: 1px solid black; padding: 13px 5px 15px 5px; height: 80px">Tarif Lebih Tinggi 100% (Tdk ber-NPWP)</th>
            <th style="background-color: #ccccb3; width: 9.3%; text-align: center; border: 1px solid black; padding: 3px 12px 0px 12px; height: 80px">Tarif (%)</th>
            <th style="background-color: #ccccb3; text-align: center; border: 1px solid black; padding: 5px 12px 0px 12px; height: 80px">PPh yang dipungut (Rp)</th>
        </tr>
        <tr>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(1)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(2)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(3)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(4)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(5)</th>
            <th style="background-color: #ccccb3; text-align: center; height: 19px; border: 1px solid black; padding: 1px 0px 4px 0px;">(6)</th>
        </tr>
        <tr>
            <td style="text-align: left; height: 18px; border-right: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <th style="height: 18px; padding: -1px 0px 4px 5px;">Jenis Industri: </th>
            <th style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 5px;">
                Penjualan Bruto:  
            </th>
            <td style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="background-color: #ccccb3; text-align: left; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                1.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">DISINI PRODUK</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                2.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">DISINI PRODUK</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                3.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">DISINI PRODUK</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                4.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">DISINI PRODUK</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                5.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                6.
            </td>
            <td rowspan="2" style="height: 18px; padding: 22px 8px 0px 5px; font-weight: bold; font-size: 10px;">
                Penjualan Barang yang Tergolong Sangat Mewah: 
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
            <!--<td style="height: 18px; padding: 0px 8px 0px 5px; font-weight: bold; font-size: 10px">-->
            <!--Penjualan Barang yang Tergolong Sangat Mewah:--> 
            <!--</td>-->
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 5px; font-weight: bold; font-size: 10.5px">
                Harga Jual:
            </td>
            <td style="text-align: center; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                7.
            </td>
            <td style="height: 20px; padding: 2px 0px 0px 5px; letter-spacing: 2px">    </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
            <td style="height: 18px; padding: 0px 8px 0px 5px; font-weight: bold; font-size: 10px">
                Industri/Eksportir: 
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 5px; font-weight: bold; font-size: 10.5px">
                Pembelian Bruto:
            </td>
            <td style="text-align: center; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                8.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">Sektor</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                9.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; letter-spacing: 2px">Sektor</td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px;"></td>
            <td style="height: 18px; padding: 0px 8px 0px 5px; font-weight: bold; font-size: 10px">
                Badan Tertentu Lainnya: 
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 5px; font-weight: bold; font-size: 10.5px">
            </td>
            <td style="text-align: center; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 0px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; padding: 2px 0px 0px 0px;">
                10.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px;">BUMN Tertentu</td>
            <td style="text-align: right; height: 20px; border: 1px solid black; padding: 0px 4px 0px 0px;"><?php
                if (isset($jumlahBruto)) {
                    $harga[10] = $jumlahBruto;
                    echo number_format($harga[10], '0', '', '.');
                } else {
                    echo $harga[10] = '';
                }
                ?>
            </td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 2px 0px 2px 0px;"><?php
                echo number_format($tarif, '2', ',', '');
                ?></td>
            <td style="text-align: right; height: 20px; border: 1px solid black; padding: 0px 4px 0px 0px;"><?php
                if (isset($pphDipungut)) {
                    $pph[10] = $pphDipungut;
                    echo number_format($pph[10], '0', '', '.');
                } else {
                    echo $pph[10] = '';
                }
                ?>
            </td>
        </tr>
        <tr>
            <td style="text-align: center; height: 20px; border-right: 1px solid black; border-bottom: 1px solid black; padding: 2px 0px 0px 0px;">
                11.
            </td>
            <td style="height: 18px; padding: 2px 0px 0px 5px; border-bottom: 1px solid black;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: center; height: 20px; border: 1px solid black; padding: 0px 0px 0px 0px;">
                <?= Html::img("@web/img/square.png", ['width' => '22px', 'height' => '22px']) ?>
            </td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; height: 20px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center; height: 18px; border-right: 1px solid black; padding: 2px 0px 0px 0px; letter-spacing: 2px; font-weight: bold">JUMLAH</td>
            <td style="height: 18px; padding: 2px 4px 0px 5px; text-align: right;"><?php
                $jumlahHarga = 0;
                for ($n = 1; $n <= 11; $n++) {
                    if (isset($harga[$n])) {
                        $jumlahHarga = $jumlahHarga + $harga[$n];
                    }
                }
                echo number_format($jumlahHarga, '0', '', '.');
                ?>            
            </td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="text-align: left; background-color: #ccccb3; height: 18px; border: 1px solid black; padding: 0px 0px 4px 0px;"></td>
            <td style="height: 18px; padding: 2px 4px 0px 5px; text-align: right;"><?php
                $jumlahPph = 0;
                for ($n = 1; $n <= 11; $n++) {
                    if (isset($pph[$n])) {
                        $jumlahPph = $jumlahPph + $pph[$n];
                    }
                }
                echo number_format($jumlahPph, '0', '', '.');
                ?>        
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left; height: 20px; border-right: 1px solid black; border-top: 1px solid black; padding: 0px 0px 0px 4px;">
                Terbilang: Rupiah    
            </td>
        </tr>
    </table>

    <div style="overflow: hidden; margin-top: 2px; padding: 0px 0px 2px 0px">
        <div class="floatLeft" style="width: 36.5%; float: left;">
            <table width="100%" style="font-size: 10px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left">
            </table>
        </div>
        <div class="floatRight" style="width: 63.5%; float: right;">
            <table width="89.4%" style="font-size: 10px; padding: 8px 4px 2px 4px; float: left; text-align: center">
                <tr>
                    <td style="font-weight: bold">KOTA, <?php
                        $fixDate = date("d F Y", strtotime($tanggal));
                        $month = preg_replace("/[^A-Za-z?!]/", '', $fixDate);
                        $transMonth = \Yii::t('app', $month);
                        $fixDate = str_replace($month, $transMonth, $fixDate);
                        echo $fixDate;
                        ?></td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-weight: bold; padding-top: 10px">Pemungut Pajak,</td>
                </tr>
            </table>
        </div>
    </div>
    <table style="margin-left: 230px; border-collapse: collapse; width: 40%">
        <tr>
            <td style = "letter-spacing: 3px; height: 12px"><b><font align="left" size="2">NPWP</font></b></td>
            <td width="17px" style="height: 12px; padding-right: 10px">:</td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[0])) {
                    echo $npwpPemungutPajak[0];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[1])) {
                    echo $npwpPemungutPajak[1];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[2])) {
                    echo $npwpPemungutPajak[2];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[3])) {
                    echo $npwpPemungutPajak[3];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[4])) {
                    echo $npwpPemungutPajak[4];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[5])) {
                    echo $npwpPemungutPajak[5];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[6])) {
                    echo $npwpPemungutPajak[6];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[7])) {
                    echo $npwpPemungutPajak[7];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[8])) {
                    echo $npwpPemungutPajak[8];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[9])) {
                    echo $npwpPemungutPajak[9];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[10])) {
                    echo $npwpPemungutPajak[10];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[11])) {
                    echo $npwpPemungutPajak[11];
                }
                ?>
            </td>
            <td style="height: 12px; width: 15px"></td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[12])) {
                    echo $npwpPemungutPajak[12];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[13])) {
                    echo $npwpPemungutPajak[13];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($npwpPemungutPajak[14])) {
                    echo $npwpPemungutPajak[14];
                }
                ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style = "height: 12px;"><b><font align="left" size="2">Nama</font></b></td>
            <td width="17px" style="height: 12px; padding-right: 10px">:</td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[0])) {
                    echo $namaPemungutPajak[0];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[1])) {
                    echo $namaPemungutPajak[1];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[2])) {
                    echo $namaPemungutPajak[2];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[3])) {
                    echo $namaPemungutPajak[3];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[4])) {
                    echo $namaPemungutPajak[4];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[5])) {
                    echo $namaPemungutPajak[5];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[6])) {
                    echo $namaPemungutPajak[6];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[7])) {
                    echo $namaPemungutPajak[7];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[8])) {
                    echo $namaPemungutPajak[8];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[9])) {
                    echo $namaPemungutPajak[9];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[10])) {
                    echo $namaPemungutPajak[10];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[11])) {
                    echo $namaPemungutPajak[11];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[12])) {
                    echo $namaPemungutPajak[12];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[13])) {
                    echo $namaPemungutPajak[13];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[14])) {
                    echo $namaPemungutPajak[14];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[15])) {
                    echo $namaPemungutPajak[15];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[16])) {
                    echo $namaPemungutPajak[16];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px"><?php
                if (isset($namaPemungutPajak[17])) {
                    echo $namaPemungutPajak[17];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                <?php
                if (isset($namaPemungutPajak[18])) {
                    echo $namaPemungutPajak[18];
                }
                ?>
            </td>
            <td style="height: 12px; border: 1px solid black; width: 17px; text-align: center; font-size: 10px">
                <?php
                if (isset($namaPemungutPajak[19])) {
                    echo $namaPemungutPajak[19];
                }
                ?>
            </td>
        </tr>
    </table>
    <!--<p style="text-align: right; font-size: 10px; font-weight: bold; padding: 15px 130px 0px 0px">
        Tanda tangan, nama, dan cap
    </p>
    <p style="text-align: right; font-size: 10px; font-weight: bold; padding: px 130px 0px 0px">
        Tanda tangan, nama, dan cap
    </p>-->
    <div style="overflow: hidden; margin-top: 2px">
        <div class="floatLeft" style="width: 34.2%; float: left; margin-left: 15px">
            <table width="100%" style="font-size: 11px; border: 1px solid black; padding: 2px 4px 2px 4px; float: left">
                <tr>
                    <td></td>
                    <td style="padding-left: -10px">Perhatian:</td>
                </tr>
                <tr>
                    <td style="width: 4px; padding: -3px 0px 0px 4px">1. </td>
                    <td rowspan="6" style="padding: 0px 0px 0px 6px">Jumlah PPh Pasal 22 yang dipungut di atas merupakan pembayaran di muka atas PPh yang terutang untuk tahun pajak yang bersangkutan. Simpanlah Bukti Pemungutan ini baik-baik untuk diperhitungkan sebagai kredit pajak dalam Surat Pemberitahuan (SPT) Tahunan PPh</td>
                </tr>
                <tr>
                    <td style="width: 4px;"> </td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px;"></td>
                </tr>
                <tr>
                    <td style="width: 4px; padding-top: -10px">2. </td>
                    <td>Bukti Pemungutan ini dianggap sah apabila diisi dengan lengkap dan benar</td>
                </tr>

            </table>
        </div>
        <div class="floatRight" style="width: 63.5%; float: right;"> 
            <table width="89.4%" style="font-size: 10px; padding: 8px 4px 2px 4px; float: left; text-align: center;">
                <tr>
                    <td style="font-weight: bold">Tanda tangan, nama dan cap</td>
                </tr>
                <tr>
                    <td style="padding-top: 98px"><?php
                        echo $namaPenanggungJawab;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td><?php
                        echo $jabatan;
                        ?></td>
                </tr>
            </table>
        </div>
    </div>
<?php } ?>
</body>
</html>

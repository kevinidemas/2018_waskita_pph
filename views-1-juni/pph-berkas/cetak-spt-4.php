<?php

//use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formatter = Yii::$app->formatter;
?>

<?php
//header('Content-Type: application/pdf');
//header("Pragma: no-cache");
//header("Expires: 0");
//header("Content-Type: application/force-download");
//header('Content-Disposition: attachment; filename="July Report.pdf"');
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];

function kekata($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = kekata($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = kekata($x / 10) . " puluh" . kekata($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . kekata($x - 100);
    } else if ($x < 1000) {
        $temp = kekata($x / 100) . " ratus" . kekata($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . kekata($x - 1000);
    } else if ($x < 1000000) {
        $temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
    } else if ($x < 1000000000) {
        $temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
    }
    return $temp;
}

function terbilang($x, $style = 4) {
    if ($x < 0) {
        $hasil = "minus " . trim(kekata($x));
    } else {
        $hasil = trim(kekata($x));
    }
    switch ($style) {
        case 1:
            $hasil = strtoupper($hasil);
            break;
        case 2:
            $hasil = strtolower($hasil);
            break;
        case 3:
            $hasil = ucwords($hasil);
            break;
        default:
            $hasil = ucfirst($hasil);
            break;
    }
    return $hasil;
}
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];
$date = date('dmY');
$toDate = str_split($date, 1);

?>

<style>
    .header-f{
        width:6.1%;
        height:17px;
        background:#000;
        position:fixed;
        top:0;
        left: -14;
        margin: -10px 0px 0px -12px;
    }
    .header-s{
        width:4.1%;
        height:17px;
        background:#000;
        position:fixed;
        top:0;
        left:600;
        margin: -10px 0px 0px 0px;
    }
    
    .header-f-child{
        width:6.1%;
        height:30px;
        background:red;
        position:fixed;
        top:1100;
        left: -14;
        margin: 0px 0px 0px -12px;
    }
    .header-s-child{
        width:4.1%;
        height:17px;
        background:red;
        position:fixed;
        top:500;
        left:1400;
        margin: 0px 0px 0px 0px;
    }
    
    .f-header-table-child{
        margin-left: -26px;
        border: 1px solid black;
        width: 27%;
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
    }

    .f-header-table-child th {
        height: 22px;
    }

    .f-header-table-child td {
        border: 1px solid black;
        height: 15px;
    }

    .s-header-table-child{    
        margin: -48.9px 0px 0px 163.5px;
        border: 1px solid black;
        width: 68%;
        border-collapse: collapse;
    }

    .s-header-table-child th {
        border: 1px solid black;
        border-left-width: 0px;
        height: 29.6px;
        font-size: 11.5px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
        border-bottom-width: 0px;
    }

    .s-header-table-child td {
        border-left-width: 0px;
        border-collapse: collapse;
        border-style:solid;
        border-width: 1px;
        border-top-width: 0px;
        border-left-width: 0px;
        height: 17.6px;
        padding: 0px 10px 0px 10px;        
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;              
    }

    .f-header-table{
        margin-left: -26px;
        border: 1px solid black;
        width: 26.5%;
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
    }

    .f-header-table th {
        height: 37.5px;
    }

    .f-header-table td {
        border: 1px solid black;
        height: 15px;
    }

    .table-bagian-header-s{  
        border-collapse: collapse;
        margin-top: 120px;
        margin-left: -26px;
        margin-bottom: 5px;
        font-size: 11px;
        font-family: "Arial", Arial, sans-serif; 
        width: 92.2%;
    }
    
    .s-header-table{    
        margin: -76px 0px 0px 160.1px;
        border: 1px solid black;
        width: 74%;
        border-collapse: collapse;
    }

    .s-header-table th {
        border: 1px solid black;
        border-left-width: 0px;
        height: 45.5px;
        font-size: 11.5px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
        padding: 0px 10px 0px 10px;

    }

    .s-header-table td {
        border-left-width: 0px;
        border-collapse: collapse;
        border-style:solid;
        border-width: 1px;
        height: 13px;
        padding: 0px 10px 0px 10px;
        font-size: 7px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;              
    }

    .f-header-table{
        margin-left: -26px;
        border: 1px solid black;
        width: 26.5%;
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
    }

    .f-header-table th {
        height: 37.5px;
    }

    .f-header-table td {
        border: 1px solid black;
        height: 15px;
    }

    .s-header-table{    
        margin: -76px 0px 0px 160.1px;
        border: 1px solid black;
        width: 74%;
        border-collapse: collapse;
    }

    .s-header-table th {
        border: 1px solid black;
        border-left-width: 0px;
        height: 45.5px;
        font-size: 11.5px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
        padding: 0px 10px 0px 10px;

    }

    .s-header-table td {
        border-left-width: 0px;
        border-collapse: collapse;
        border-style:solid;
        border-width: 1px;
        height: 13px;
        padding: 0px 10px 0px 10px;
        font-size: 7px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;              
    }

    .checkboxs{
        /*background-color: red;*/
    }

    .sub-a{
        font-family: "Arial", Arial, sans-serif;
        font-size: 11px;
        font-weight: bold;
        padding: 12px 0px 0px 0px;
        margin-left: -22px;
    }

    .table-bagian-d-child{
        margin: 10px 0px 0px -26px;
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }

    .table-bagian-d-child td{
        border-style: solid;
        border-collapse: collapse;
        border-width: 1px;
        padding: 2px 0px 2px 2px;
    }

    .table-bagian-d{
        margin-left: -26px;
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }

    .table-bagian-d td{
        border-style: solid;
        border-collapse: collapse;
        border-width: 1px;
        /*height: 17px;*/
        padding: 6px 0px 9px 5px;
    }

    .table-bagian-c{
        margin-left: -26px;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }

    .table-bagian-c td{
        padding: 4px 0px 0px 6px;
        height: 14px;
    }

    .table-bagian-b-child{
        margin: 10px 0px 0px -26px;
        border-collapse: collapse;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }

    .table-bagian-b-child th {
        background-color: #ccccb3;
        border: 1px solid black;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
    }

    .table-bagian-b-child td{
        border-style: solid;
        border-width: 1px;
        font-size: 10px;
        border-collapse: collapse;
        padding: 2px 0px 2px 2px;
        border-top-width: 0px;
        border-bottom-width: 0px;
    }

    .table-bagian-b{
        margin-left: -26px;
        border-collapse: collapse;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }

    .table-bagian-b th {
        background-color: #ccccb3;
        border: 1px solid black;
        height: 25px;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
        padding: 0px 10px 0px 10px;
    }

    .table-bagian-b td{
        border-style: solid;
        border-width: 1px;
        border-collapse: collapse;
        padding: 6px 0px 0px 4px;
        height: 14px;
    }

    .table-bagian-a{
        margin-left: -26px;
        font-size: 11px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }

    .table-bagian-a td{
        height: 25px;
        padding: 4px 0px 4px 10px;
    }

    .table-inside td{
        border-style: solid;
        border-width: 1px;
        height: 15.2px;
        width: 15.2px;
        border-collapse: collapse;
        padding: 0px 0px 0px 0px;
        text-align: center;
    }

    .catatan-bintang{
        font-size: 7px;
        font-style: italic;
        margin: 6px 0px 0px 2px;
        font-family: "Arial", Arial, sans-serif;  
    }

    .footer-m{
        font-weight: bold; 
        font-size: 9.7px;
        padding-bottom: 0px;
        position:fixed;
        bottom:-2;
        left:-12;
        margin: 0px -50px 0px -14px;
    }

    .footer-f{
        width:7.8%;
        height:10px;
        background:#000;
        position:fixed;
        bottom:39;
        left: 0;
        margin: -52px 0px -52px -26px;
    }
    .footer-s{
        width:4.1%;
        height:10.5px;
        background:#000;
        position:fixed;
        bottom:39;
        left:598;
        margin: -52px 0px -52px 0px;
    }
</style>

<?php
$masaSession = $session['masaPajak'];
$masaTahun = preg_replace('/[^0-9]/', '', $masaSession);
$masaBulan = $session['masaId'];
if (strlen($masaBulan) == 1) {
    $masaBulan = '0' . $masaBulan;
}
$arrayMasa = [];
$arrayMasa[0] = $masaBulan;
$arrayMasa[1] = $masaTahun;
?>

<html>
    <body>
        <div class="header-f"></div>
            <div class="header-s"></div>
        <table class="f-header-table">
            <tr>
                <th style="width: 67px; border-right-style: solid; border-width: 1px;" rowspan="2">                       
                    <?= Html::img("@web/img/djp.png", ['width' => '50px', 'height' => '50px', 'margin-top' => '-10px']) ?>
                </th>                                
                <th style="width: 67px; border-right-width: 0px;">    
                    DEPARTEMEN KEUANGAN R.I
                </th>                                
            </tr>    
            <tr>
                <th style="width: 108px">DIREKTORAT JENDERAL PAJAK</th>     
            </tr>
        </table>
        <table class="s-header-table">
            <tr>
                <th style="width: 315.6px">SURAT PEMBERITAHUAN (SPT) MASA PAJAK PENGHASILAN PASAL 22</th>
                <th style="width: 81.2px">
            <div class="checkboxs"><?php echo Html::img("@web/img/checkbox-spt2.png", ['width' => '130px', 'height' => '13px', 'margin-top' => '10px']) ?></div>
            <?php echo Html::img("@web/img/checkbox-spt-pembetulan2.png", ['width' => '130px', 'height' => '13px', 'margin-top' => '10px']) ?>                        
        </th>
    </tr>
    <tr>
        <td style="border-bottom-width: 0px"><center>Formulir ini digunakan untuk melaporkan Pemungutan</center></td>
<td style="font-size: 8.5px;border-bottom-width: 0px"><center><b>Masa Pajak</b></center></td>
</tr>         
<tr>
    <td style="border-top-width: 0px;"><center>Pajak Penghasilan Pasal 22</center></td>
<td style="border-top-width: 0px">
    <table class="table-inside" style="border-collapse: collapse;padding: -36px 0px 0px 0px">
        <tr>                    
            <td style="font-size: 10px"><?php echo $arrayMasa[0][0]; ?></td>
                    <td style="font-size: 10px"><?php echo $arrayMasa[0][1]; ?></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="font-size: 10px"><?php echo $arrayMasa[1][0]; ?></td>
                    <td style="font-size: 10px"><?php echo $arrayMasa[1][1]; ?></td>
                    <td style="font-size: 10px"><?php echo $arrayMasa[1][2]; ?></td>
                    <td style="font-size: 10px"><?php echo $arrayMasa[1][3]; ?></td>                    
        </tr>
    </table>
</td>
</tr>
</table>

<div class="sub-a">BAGIAN A. IDENTITAS PEMUNGUT PAJAK/WAJIB PAJAK</div>
<table class="table-bagian-a">
    <tr>
        <td style="width: 73px; padding-left: 3px">1. NPWP</td>
        <td style="width: 1px;">:</td>
        <td>
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[0])) {
                            echo $npwpPemungutPajak[0];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[1])) {
                            echo $npwpPemungutPajak[1];
                        }
                        ?>
                    </td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[2])) {
                            echo $npwpPemungutPajak[2];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[3])) {
                            echo $npwpPemungutPajak[3];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[4])) {
                            echo $npwpPemungutPajak[4];
                        }
                        ?>
                    </td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[5])) {
                            echo $npwpPemungutPajak[5];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[6])) {
                            echo $npwpPemungutPajak[6];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[7])) {
                            echo $npwpPemungutPajak[7];
                        }
                        ?>
                    </td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[8])) {
                            echo $npwpPemungutPajak[8];
                        }
                        ?>
                    </td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[9])) {
                            echo $npwpPemungutPajak[9];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[10])) {
                            echo $npwpPemungutPajak[10];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[11])) {
                            echo $npwpPemungutPajak[11];
                        }
                        ?>
                    </td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[12])) {
                            echo $npwpPemungutPajak[12];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[13])) {
                            echo $npwpPemungutPajak[13];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($npwpPemungutPajak[14])) {
                            echo $npwpPemungutPajak[14];
                        }
                        ?>
                    </td>
                <tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 3px">2. Nama</td>
        <td>:</td>
        <td>
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[0])) {
                            echo $namaPemungutPajak[0];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[1])) {
                            echo $namaPemungutPajak[1];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[2])) {
                            echo $namaPemungutPajak[2];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[3])) {
                            echo $namaPemungutPajak[3];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[4])) {
                            echo $namaPemungutPajak[4];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[5])) {
                            echo $namaPemungutPajak[5];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[6])) {
                            echo $namaPemungutPajak[6];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[7])) {
                            echo $namaPemungutPajak[7];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[8])) {
                            echo $namaPemungutPajak[8];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[9])) {
                            echo $namaPemungutPajak[9];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[10])) {
                            echo $namaPemungutPajak[10];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[11])) {
                            echo $namaPemungutPajak[11];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[12])) {
                            echo $namaPemungutPajak[12];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[13])) {
                            echo $namaPemungutPajak[13];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[14])) {
                            echo $namaPemungutPajak[14];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[15])) {
                            echo $namaPemungutPajak[15];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[16])) {
                            echo $namaPemungutPajak[16];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[17])) {
                            echo $namaPemungutPajak[17];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[18])) {
                            echo $namaPemungutPajak[18];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPemungutPajak[19])) {
                            echo $namaPemungutPajak[19];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[20])) {
                            echo $namaPemungutPajak[20];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[21])) {
                            echo $namaPemungutPajak[21];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[22])) {
                            echo $namaPemungutPajak[22];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[23])) {
                            echo $namaPemungutPajak[23];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[24])) {
                            echo $namaPemungutPajak[24];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[25])) {
                            echo $namaPemungutPajak[25];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[26])) {
                            echo $namaPemungutPajak[26];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[27])) {
                            echo $namaPemungutPajak[27];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[28])) {
                            echo $namaPemungutPajak[28];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[29])) {
                            echo $namaPemungutPajak[29];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[30])) {
                            echo $namaPemungutPajak[30];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[31])) {
                            echo $namaPemungutPajak[31];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPemungutPajak[32])) {
                            echo $namaPemungutPajak[32];
                        }
                        ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-left: 3px">3. Alamat</td>
        <td>:</td>
        <td>
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                <tr>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[0])) {
                            echo $alamatPemungutPajak[0];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[1])) {
                            echo $alamatPemungutPajak[1];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[2])) {
                            echo $alamatPemungutPajak[2];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[3])) {
                            echo $alamatPemungutPajak[3];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[4])) {
                            echo $alamatPemungutPajak[4];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[5])) {
                            echo $alamatPemungutPajak[5];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[6])) {
                            echo $alamatPemungutPajak[6];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[7])) {
                            echo $alamatPemungutPajak[7];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[8])) {
                            echo $alamatPemungutPajak[8];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[9])) {
                            echo $alamatPemungutPajak[9];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[10])) {
                            echo $alamatPemungutPajak[10];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[11])) {
                            echo $alamatPemungutPajak[11];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[12])) {
                            echo $alamatPemungutPajak[12];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[13])) {
                            echo $alamatPemungutPajak[13];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[14])) {
                            echo $alamatPemungutPajak[14];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[15])) {
                            echo $alamatPemungutPajak[15];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[16])) {
                            echo $alamatPemungutPajak[16];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[17])) {
                            echo $alamatPemungutPajak[17];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[18])) {
                            echo $alamatPemungutPajak[18];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($alamatPemungutPajak[19])) {
                            echo $alamatPemungutPajak[19];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[20])) {
                            echo $alamatPemungutPajak[20];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[21])) {
                            echo $alamatPemungutPajak[21];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[22])) {
                            echo $alamatPemungutPajak[22];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[23])) {
                            echo $alamatPemungutPajak[23];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[24])) {
                            echo $alamatPemungutPajak[24];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[25])) {
                            echo $alamatPemungutPajak[25];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[26])) {
                            echo $alamatPemungutPajak[26];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[27])) {
                            echo $alamatPemungutPajak[27];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[28])) {
                            echo $alamatPemungutPajak[28];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[29])) {
                            echo $alamatPemungutPajak[29];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[30])) {
                            echo $alamatPemungutPajak[30];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[31])) {
                            echo $alamatPemungutPajak[31];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($alamatPemungutPajak[32])) {
                            echo $alamatPemungutPajak[32];
                        }
                        ?>
                    </td>
                </tr>
                <tr>
            </table>
        </td>
    </tr>
</table> 
<div class="sub-a">BAGIAN B. OBJEK PAJAK</div>
<table class="table-bagian-b">
    <tr>
        <th style="width: 270px" colspan="2">Uraian</th>
        <th style="width: 80px">KAP/KJS</th>
        <th style="width: 140px">Nilai Objek Pajak(Rp)</th>
        <th style="width: 120px">PPh yang dipungut(Rp)</th>
    </tr>
    <tr>
        <td colspan="2"><b><center>(1)</center></b></td>
        <td><b><center>(2)</center></b></td>
        <td><b><center>(3)</center></b></td>
        <td><b><center>(4)</center></b></td>
        <td><b><center>(5)</center></b></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">1. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Bunga Deposito/Tabungan, Diskonto SBI dan Jasa Giro</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">a. Bunga Deposito/Tabungan</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">b. Diskonto Sertifikat Bank Indonesia</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">c. Jasa Giro</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">2. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Transaksi Penjualan Saham</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">a. Saham Pendiri</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">b. Bukan Saham Pendiri</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">3. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Bunga/Diskonto Obligasi dan Surat Berharga Negara</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td style="border-top-width: 1px; text-align: right;">
            <?php
            if (isset($jumBruto)) {
                echo number_format($jumBruto, '0', '', '.');
            }
            ?>
        </td>
        <td style="border-top-width: 1px; text-align: right;">
            <?php
            if (isset($jumPph)) {
                echo number_format($jumPph, '0', '', '.');
            }
            ?>
        </td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">4. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Hadiah Undian</td>
        <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="background-color: #ccccb3;"></td>
        <td style="background-color: #ccccb3;"></td>
    </tr>    
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">5. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Persewaan Tanah dan/atau Bangunan</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">6. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Jasa Konstruksi</td>
        <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="background-color: #ccccb3;"></td>
        <td style="background-color: #ccccb3;"></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">a. SPBU/Agen/Penyalur(Final)</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">b. Pihak Lain(Tidak Final)</td>
        <td style="border-top-width: 0px; border-bottom-width: 0px"><center><b>411122/100</b></center></td>
        <td></td>
        <td></td>
    </tr>
    <tr>            
        <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">7. </td>
        <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Wajib Pajak yang Melakukan Pengalihan Hak Atas Tanah/Bangunan</td>
        <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
        <td></td>
        <td></td>
    </tr>    
    <tr>
        <td colspan="2" style="letter-spacing: 3px"><center>JUMLAH</center></td>
        <td style="background-color: #ccccb3;"></td>
        <td style="border-top-width: 1px; text-align: right;">
            <?php
            if (isset($jumBruto)) {
                echo number_format($jumBruto, '0', '', '.');
            }
            ?>
        </td>
        <td style="border-top-width: 1px; text-align: right;">
            <?php
            if (isset($jumPph)) {
                echo number_format($jumPph, '0', '', '.');
            }
            ?>
        </td>
    </tr>
    <tr>
        <td colspan="5">Terbilang :  ==  <?php echo terbilang($jumPph, $style = 4) ?> Rupiah ==</td>
    </tr>
</table>
<div class="catatan-bintang">
    *) Coret Yang Tidak Perlu
</div>

<div class="sub-a">BAGIAN C. LAMPIRAN</div>
<table class="table-bagian-c">
    <tr>
        <td style="width: 1px">1. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Daftar Surat Setoran pajak PPh pasal 22 (Khusus untuk Bank Devisa, Bendaharawan/Badan Tertentu Yang Ditunjuk dan Pertamina/Badan Usaha Selain Pertamina)</td>
    </tr>
    <tr>
        <td style="width: 1px">2. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Surat Setoran Pajak (SSP) yang disetor oleh Importit atau pembeli Barang sebanyak 0 lembar Khusus untuk Bank Devisa, Bendaharawan/Badan Tertentu Yang Ditunjuk dan Pertamina/Badan Usaha Selain Pertamina)
        </td>
    </tr>
    <tr>
        <td style="width: 1px">3. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>SSP yang disetor oleh Pemungut pajak sebanyak: 10 lembar (Khusus untuk Badan Usaha Industri/Eksportir Tertentu, Ditjen Bea dan Cukai)
        </td>
    </tr>
    <tr>
        <td style="width: 1px">4. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Daftar Bukti pemungutan PPh Pasal 22 (Khusus untuk Badan Usaha Industri/Eksportit Tertentu dan Ditjen Bea dan Cukai.</td>
    </tr>
    <tr>
        <td style="width: 1px">5. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Bukti Pemungutan PPh Pasal 22, (Khusus untuk Badan Usaha Industri/Eksportit Tertentu dan Ditjen Bea dan Cukai</td>
    </tr>
    <tr>
        <td style="width: 1px">6. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Daftar rincian penjualan (dalam hal ada penjualan retur).</td>
    </tr>
    <tr>
        <td style="width: 1px">7. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Risalah lelang (dalam hal pelaksanaan lelang).</td>
    </tr>
    <tr>
        <td style="width: 1px">8. </td>
        <td style="width: 3px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
        <td>Surat Kuasa Khusus</td>
    </tr>
</table>

<div class="sub-a">BAGIAN D. PERNYATAAN DAN TANDA TANGAN</div>
<table class="table-bagian-d">
    <tr>
        <td colspan="3" rowspan="2" style="font-size: 9px; width: 80px; height: 40px">Dengan menyadari sepenuhnya akan segala akibatnya termasuk sanksi-sanksi sesuai dengan ketentuan perundang-undangan yang berlaku, saya menyatakan bahwa apa yang telah saya beritahukan di atas beserta lampiran-lampirannya adalah benar, lengkap dan jelas.</td>
        <td style="width: 159px; height: 10px; background-color: #ccccb3;"><b><center>Diisi Oleh Petugas</center></b></td>
    </tr>
    <tr>                        
        <td rowspan="2" style="width: 155px; padding: -4px 0px 0px 4px">
            SPT Masa Diterima:
            <table class="table-inside" style="border-collapse: collapse;padding: 0px 0px 0px 0px">                              
                <tr>                    
                    <td style="border: 1px solid black; heigh: 14px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px; width: 130px; text-align: left">Langsung dar WP</td>
                </tr>                 
                <tr>
                    <td style="border: 1px solid black; heigh: 14px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px; width: 100px; text-align: left">Melalui Pos</td>
                </tr>
            </table> 
        </td>
    </tr>
    <tr>
        <td style="border-left-width: 1px; border-right-width: 0px; border-bottom-width: 0px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?> PEMUNGUT PAJAK</td>
        <td colspan="2" style="width: 120px; border-width: 0px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?> KUASA WAJIB PAJAK</td>
    </tr>
    <tr style="width: 60px">
        <td colspan="3" style="border-left-width: 1px; border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px">
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td style="border-width: 0px">Nama</td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[0])) {
                            echo $namaPenanggungJawab[0];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[1])) {
                            echo $namaPenanggungJawab[1];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[2])) {
                            echo $namaPenanggungJawab[2];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[3])) {
                            echo $namaPenanggungJawab[3];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[4])) {
                            echo $namaPenanggungJawab[4];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[5])) {
                            echo $namaPenanggungJawab[5];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[6])) {
                            echo $namaPenanggungJawab[6];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[7])) {
                            echo $namaPenanggungJawab[7];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[8])) {
                            echo $namaPenanggungJawab[8];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[9])) {
                            echo $namaPenanggungJawab[9];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[10])) {
                            echo $namaPenanggungJawab[10];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[11])) {
                            echo $namaPenanggungJawab[11];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[12])) {
                            echo $namaPenanggungJawab[12];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[13])) {
                            echo $namaPenanggungJawab[13];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[14])) {
                            echo $namaPenanggungJawab[14];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[15])) {
                            echo $namaPenanggungJawab[15];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[16])) {
                            echo $namaPenanggungJawab[16];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[17])) {
                            echo $namaPenanggungJawab[17];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[18])) {
                            echo $namaPenanggungJawab[18];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[19])) {
                            echo $namaPenanggungJawab[19];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[20])) {
                            echo $namaPenanggungJawab[20];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[21])) {
                            echo $namaPenanggungJawab[21];
                        }
                        ?>
                    </td>                    
                <tr>
            </table>
        </td>
        <td rowspan="2" style="width: 100px;">
            <table class="table-inside" style="border-collapse: collapse;padding: -32px 0px 0px 0px">
                <tr>
                    <td style="border-width: 0px;padding: -4px 0px 0px 0px" colspan="8">Tanggal</td>
                </tr>
                <tr>                    
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr style="width: 60px">
        <td colspan="3" style="border-left-width: 1px; border-right-width: 0px; border-bottom-width: 0px; border-top-width: 0px">
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td style="border-width: 0px">NPWP</td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>                    
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 60px; width: 198px; padding: -32px 0px 0px 4px">Tanda Tangan & Cap</td>
        <td colspan="2" style="width: 210px; height: 55px; padding: -32px 0px 0px 4px">
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td style="border-width: 0px;padding: 2px 0px 0px 4px">Tanggal</td>
                    <td style="border-width: 0px"></td>
                    <td><?php echo $toDate[0]?></td>
                    <td><?php echo $toDate[1]?></td>
                    <td><?php echo $toDate[2]?></td>
                    <td><?php echo $toDate[3]?></td>
                    <td><?php echo $toDate[4]?></td>
                    <td><?php echo $toDate[5]?></td>
                    <td><?php echo $toDate[6]?></td>
                    <td><?php echo $toDate[7]?></td>
                <tr>
            </table>
        </td>
        <td style="width: 155px; height: 55px; padding: -32px 0px 0px 4px">Tanda Tangan</td>
    </tr>
</table>

<div class="footer-m">
    F.1.1.32.02
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    Lampiran III.1 Peraturan Direktur Jenderal Pajak Nomor PER-53/PJ2009
</div>
<div class="footer-f"></div>
<div class="footer-s"></div>


<table class="table-bagian-header-s">
    <tr>        
        <td style="border: 1px solid black; height: 8.5px; background-color: #000"><div class="header-f-child"></div></td>
        <td style="width: 91.4%; border-width: 0px"></td>
        <td style="border: 1px solid black; height: 8.5px; background-color: #000"><div class="header-s-child"></div></td>
    </tr>
</table>
<table class="f-header-table-child">
    <tr>
        <th style="width: 67px; border-right-style: solid; border-width: 1px;" rowspan="2">                       
            <?= Html::img("@web/img/djp.png", ['width' => '40px', 'height' => '40px', 'margin-top' => '-10px']) ?>
        </th>                                
        <th style="width: 67px; border-right-width: 0px;">    
            DEPARTEMEN KEUANGAN R.I
        </th>                          
    </tr>
    <tr>
        <th style="width: 108px">DIREKTORAT JENDERAL PAJAK</th>     
    </tr>
</table>
<table class="s-header-table-child">
    <tr>
        <th style="width: 300.6px; padding: 0px 40px -10px 40px;">DAFTAR BUKTI PEMUNGUTAN</th>
        <th style="width: 81.2px; padding: 0px 10px 0px 10px; font-size: 11.5px;">Masa Pajak</th>
    </tr>
    <tr>
        <td style="font-size: 11px; padding: -10px 0px 0px 0px;"><b><center>PPh PASAL 22</center></b></td>
        <td style="width: 81.2px; padding: 0px 10px 0px 10px;">
            <table class="table-inside" style="border-collapse: collapse;padding: -36px 0px 0px 0px">                            
                <tr>                    
                    <td style="font-size: 9px"><?php echo $arrayMasa[0][0]; ?></td>
                    <td style="font-size: 9px"><?php echo $arrayMasa[0][1]; ?></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px">/</td>
                    <td style="border-width: 0px"></td>
                    <td style="font-size: 9px"><?php echo $arrayMasa[1][0]; ?></td>
                    <td style="font-size: 9px"><?php echo $arrayMasa[1][1]; ?></td>
                    <td style="font-size: 9px"><?php echo $arrayMasa[1][2]; ?></td>
                    <td style="font-size: 9px"><?php echo $arrayMasa[1][3]; ?></td>                    
                </tr>                 
</table> 
</td>
</tr>
</table>

<table class="table-bagian-b-child">
    <tr>
        <th rowspan="2" style="width: 25px">No</th>
        <th rowspan="2" style="width: 107px">NPWP</th>
        <th rowspan="2" style="width: 140px">Nama</th>
        <th colspan="2">Bukti Pemungutan</th>
        <th style="width: 100px">Nilai Objek Pajak</th>
        <th style="width: 99px">PPh yang</th>
    </tr>
    <tr>
        <th>Nomor</th>
        <th>Tanggal</th>
        <th>(Rp)</th>
        <th>Dipungut(Rp)</th>
    </tr>
    <tr>
        <th>(1)</th>
        <th>(2)</th>
        <th>(3)</th>
        <th>(4)</th>
        <th>(5)</th>
        <th>(6)</th>
        <th>(7)</th>
    </tr>
    <?php
    $jumBruto = 0;
    $jumPph = 0;
    for ($n = 0; $n < $countPdf; $n++) {
        ?>
        <tr>
            <td style="<?php $nz = $n + 1;
    if ($nz != $countPdf) {
        echo 'border-top-width: 0px; border-bottom-width: 0px;';
    } ?>">
        <center><?php echo $nomorUrut = $n + 1; ?></center>
    </td>
    <td style="font-size: 9px">
    <center>
        <?php
        if (isset($npwp[$n])) {
            echo substr($npwp[$n], 0, 2) . '.' . substr($npwp[$n], 2, 3) . '.' . substr($npwp[$n], 5, 3) . '.' . substr($npwp[$n], 8, 1) . '-' . substr($npwp[$n], 9, 3) . '.' . substr($npwp[$n], 12, 3);
        }
        ?>
    </center>
    </td>
    <td style="font-size: 9px">
        <?php
        if (isset($nama[$n])) {
            echo $nama[$n];
        }
        ?>
    </td>
    <td style="font-size: 9px; padding: 0px 2px 0px 2px">
        <?php
        if (isset($nomorBuktiPotong[$n])) {
            echo $nomorBuktiPotong[$n];
        }
        ?>
    </td>
    <td style="font-size: 9px; padding: 0px 2px 0px 2px">
        <?php
        if (isset($tanggal[$n])) {
            echo $tanggal[$n];
        }
        ?>
    </td>
    <td style="text-align: right;">
        <?php
        if (isset($jumlahBruto[$n])) {
            $harga = $jumlahBruto[$n];
            echo number_format($harga, '0', '', '.');

            $jumBruto = $jumBruto + $jumlahBruto[$n];
        }
        ?>                
    </td>
    <td style="text-align: right;">
        <?php
        if (isset($pphDipungut[$n])) {
            $harga = $pphDipungut[$n];
            echo number_format($harga, '0', '', '.');

            $jumPph = $jumPph + $pphDipungut[$n];
        }
        ?>
    </td>
    </tr>      
    <?php } ?>
<tr>
    <td colspan="5" style="border-top-width: 1px; letter-spacing: 3px"><center>JUMLAH</center></td>
<td style="border-top-width: 1px; text-align: right;">
<?php
echo number_format($jumBruto, '0', '', '.');
?>
</td>
<td style="border-top-width: 1px; text-align: right;">
<?php
echo number_format($jumPph, '0', '', '.');
?>
</td>
</tr>
</table>   

<table class="table-bagian-d-child">
    <tr>
        <td style="border-left-width: 1px; border-right-width: 0px; border-bottom-width: 0px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?> PEMUNGUT PAJAK</td>
        <td colspan="2" style="width: 120px; border-width: 0px; border-top-width: 1px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?> KUASA WAJIB PAJAK</td>
        <td rowspan="2" style="border: 1px solid black">
            <table class="table-inside" style="border-collapse: collapse;padding: -32px 0px 0px 0px">
                <tr>
                    <td style="border-width: 0px;padding: -4px 0px 0px 0px" colspan="8">Tanggal</td>
                </tr>
                <tr>                    
                    <td><?php echo $toDate[0]?></td>
                    <td><?php echo $toDate[1]?></td>
                    <td><?php echo $toDate[2]?></td>
                    <td><?php echo $toDate[3]?></td>
                    <td><?php echo $toDate[4]?></td>
                    <td><?php echo $toDate[5]?></td>
                    <td><?php echo $toDate[6]?></td>
                    <td><?php echo $toDate[7]?></td>                  
                </tr>
            </table>
        </td>
    </tr>
    <tr style="width: 60px">
        <td colspan="3" style="border-left-width: 1px; border-right-width: 0px; border-top-width: 0px; border-bottom-width: 0px">
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td style="border-width: 0px">Nama</td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[0])) {
                            echo $namaPenanggungJawab[0];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[1])) {
                            echo $namaPenanggungJawab[1];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[2])) {
                            echo $namaPenanggungJawab[2];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[3])) {
                            echo $namaPenanggungJawab[3];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[4])) {
                            echo $namaPenanggungJawab[4];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[5])) {
                            echo $namaPenanggungJawab[5];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[6])) {
                            echo $namaPenanggungJawab[6];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[7])) {
                            echo $namaPenanggungJawab[7];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[8])) {
                            echo $namaPenanggungJawab[8];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[9])) {
                            echo $namaPenanggungJawab[9];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[10])) {
                            echo $namaPenanggungJawab[10];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[11])) {
                            echo $namaPenanggungJawab[11];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[12])) {
                            echo $namaPenanggungJawab[12];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[13])) {
                            echo $namaPenanggungJawab[13];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[14])) {
                            echo $namaPenanggungJawab[14];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[15])) {
                            echo $namaPenanggungJawab[15];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[16])) {
                            echo $namaPenanggungJawab[16];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[17])) {
                            echo $namaPenanggungJawab[17];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[18])) {
                            echo $namaPenanggungJawab[18];
                        }
                        ?>
                    </td><td>
                        <?php
                        if (isset($namaPenanggungJawab[19])) {
                            echo $namaPenanggungJawab[19];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[20])) {
                            echo $namaPenanggungJawab[20];
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (isset($namaPenanggungJawab[21])) {
                            echo $namaPenanggungJawab[21];
                        }
                        ?>
                    </td>                       
                <tr>
            </table>
        </td>
    </tr>
    <tr style="width: 60px;">
        <td colspan="3" style="border-left-width: 1px; border-right-width: 0px; border-top-width: 0px; height: 60px">
            <table class="table-inside" style="border-collapse: collapse;">
                <tr>
                    <td style="border-width: 0px">NPWP</td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-width: 0px"></td>
                    <td></td>                    
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </td>
        <td style="height: 60px; padding-top: -36px">Tanda Tangan & Cap</td>
    </tr>
</table>
</body>
</html>
<?php

//use kartik\grid\GridView;
use yii\web\Session;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$formatter = Yii::$app->formatter;
?>

<?php
$session = Yii::$app->session;
$countPdf = $session['pdfCount'];
?>

<style>
    .f-header-table{
        border: 1px solid black;
        width: 26%;
        border-collapse: collapse;
        font-size: 8px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
    }

    .f-header-table th {
        /*border: 1px solid black;*/
        height: 30.5px;
    }

    .f-header-table td {
        border: 1px solid black;
        height: 12px;
    }

    .s-header-table{
        margin: -62px 0px 0px 176px;
        border: 1px solid black;
        width: 74%;
        border-collapse: collapse;
    }

    .s-header-table th {
        border: 1px solid black;
        height: 39px;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
        padding: 0px 10px 0px 10px;

    }

    .s-header-table td {
        border-collapse: collapse;
        border-style:solid;
        border-width: 1px;
        height: 11px;
        padding: 0px 10px 0px 10px;
        font-size: 7px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;              
    }

    .checkboxs{
        /*background-color: red;*/
    }
    
     .sub-a{
        font-family: "Arial", Arial, sans-serif;
        font-size: 10px;
        font-weight: bold;
        padding: 12px 0px 0px 4px;
    }
    
    .table-bagian-d{
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 82.2%;
    }
    
    .table-bagian-d td{
        border-style: solid;
        border-collapse: collapse;
        border-width: 1px;
        /*height: 17px;*/
        padding-left: 4px;
    }
    
    .table-bagian-c{
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }
    
    .table-bagian-c td{
        padding: 0px 0px 0px 4px;
        height: 14px;
    }
    
    .table-bagian-b{
        border-collapse: collapse;
        font-size: 9px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }
    
    .table-bagian-b th {
        background-color: #ccccb3;
        border: 1px solid black;
        height: 22px;
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif;  
        letter-spacing: 0.5px;
        padding: 0px 10px 0px 10px;
    }
    
    .table-bagian-b td{
        border-style: solid;
        border-width: 1px;
        border-collapse: collapse;
        padding: 0px 0px 0px 4px;
        height: 14px;
    }
    
    .table-bagian-a{
        font-size: 10px;
        font-family: "Arial", Arial, sans-serif; 
        border: 1px solid black;
        width: 92.2%;
    }
    
    .table-bagian-a td{
        height: 17px;
        padding-left: 10px;
    }
    
    .table-inside td{
        border-style: solid;
        border-width: 1px;
        height: 14px;
        width: 14px;
        border-collapse: collapse;
        padding: 0px 0px 0px 6px;
    }
    
    .catatan-bintang{
        font-size: 5px;
        font-style: italic;
        margin: 6px 0px 0px 14px;
        font-family: "Arial", Arial, sans-serif;  
    }
</style>

<html>
    <body>
        <?php for ($n = 0; $n < $countPdf; $n++) { ?>
            <table class="f-header-table">
                <tr>
                    <th style="width: 67px; border-right-style: solid; border-width: 1px;" rowspan="2">                       
                        <?= Html::img("@web/img/djp.png", ['width' => '47px', 'height' => '47px', 'margin-top' => '-10px']) ?>
                    </th>                                
                    <th style="width: 67px; border-right-width: 0px;">    
                        DEPARTEMEN KEUANGAN R.I
                    </th>                                
                </tr>    
                <tr>
                    <th style="width: 108px">DIREKTORAT JENDERAL PAJAK</th>     
                </tr>
            </table>
            <table class="s-header-table">
                <tr>
                    <th style="width: 300px">SURAT PEMBERITAHUAN (SPT) MASA PAJAK PENGHASILAN PASAL 22</th>
                    <th style="width: 70px">
                <div class="checkboxs"><?php echo Html::img("@web/img/checkbox-spt2.png", ['width' => '130px', 'height' => '13px', 'margin-top' => '10px']) ?></div>
                <?php echo Html::img("@web/img/checkbox-spt-pembetulan2.png", ['width' => '130px', 'height' => '13px', 'margin-top' => '10px']) ?>                        
            </th>
        </tr>
        <tr>
            <td style="border-bottom-width: 0px"><center>Formulir ini digunakan untuk melaporkan Pemungutan</center></td>
    <td style="font-size: 7.5px;border-bottom-width: 0px"><center><b>Masa Pajak</b></center></td>
    </tr>         
    <tr>
        <td style="border-top-width: 0px;"><center>Pajak Penghasilan Pasal 22</center></td>
    <td style="border-top-width: 0px">B</td>
    </tr>
    </table>

    <div class="sub-a">BAGIAN A. IDENTITAS PEMUNGUT PAJAK/WAJIB PAJAK</div>
    <table class="table-bagian-a">
        <tr>
            <td style="width: 73px; padding-left: 3px">1. NPWP</td>
            <td style="width: 1px;">:</td>
            <td>
                <table class="table-inside" style="border-collapse: collapse;">
                    <tr>
                    <td></td>
                    <td></td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td></td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="border-top-width: 0px; border-bottom-width: 0px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 3px">2. Nama</td>
            <td>:</td>
            <td>
                <table class="table-inside" style="border-collapse: collapse;">
                    <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 3px">3. Alamat</td>
            <td>:</td>
            <td>
                <table class="table-inside" style="border-collapse: collapse;">
                    <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <tr>
                </table>
            </td>
        </tr>
    </table> 
    <div class="sub-a">BAGIAN B. OBJEK PAJAK</div>
    <table class="table-bagian-b">
        <tr>
            <th style="width: 270px" colspan="2">Uraian</th>
            <th style="width: 80px">KAP/KJS</th>
            <th style="width: 140px">Nilai Objek Pajak(Rp)</th>
            <th style="width: 120px">PPh yang dipungut(Rp)</th>
        </tr>
        <tr>
            <td colspan="2"><b><center>(1)</center></b></td>
            <td><b><center>(2)</center></b></td>
            <td><b><center>(3)</center></b></td>
            <td><b><center>(4)</center></b></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">1. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Badan Usaha Industri/Eksportir</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">2. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Penjualan Barang yang tergolong Sangat Mewah</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px; padding-top: -12px">3. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Pembelian Barang oleh Bendaharawan/Badan Tertentu Yang Ditunjuk</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">4. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Nilai Impor Bank Devisa/Ditjen Bea dan Cukai *)</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="background-color: #ccccb3;"></td>
            <td style="background-color: #ccccb3;"></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">a. API</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">b. Non API</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">5. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Hasil Lelang (Ditjen Bea dan Cukai)</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">6. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">Penjualan Migas Oleh Pertamina / Badan Usaha Selain Pertamina</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="background-color: #ccccb3;"></td>
            <td style="background-color: #ccccb3;"></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">a. SPBU/Agen/Penyalur(Final)</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">b. Pihak Lain(Tidak Final)</td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td style="border-right-width: 0px; border-top-width: 0px;  border-bottom-width: 0px">7. </td>
            <td style="border-left-width: 0px; border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td style="border-top-width: 0px;  border-bottom-width: 0px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="2" style="letter-spacing: 3px"><center>JUMLAH</center></td>
            <td style="background-color: #ccccb3;"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="5">Terbilang: </td>
        </tr>
    </table>
    <div class="catatan-bintang">
        *) Coret Yang Tidak Perlu
    </div>
    
    <div class="sub-a">BAGIAN C. LAMPIRAN</div>
    <table class="table-bagian-c">
        <tr>
            <td style="width: 1px">1. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Daftar Surat Setoran pajak PPh pasal 22 (Khusus untuk Bank Devisa, Bendaharawan/Badan Tertentu Yang Ditunjuk dan Pertamina/Badan Usaha Selain Pertamina)</td>
        </tr>
        <tr>
            <td style="width: 1px">2. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Surat Setoran Pajak (SSP) yang disetor oleh Importit atau pembeli Barang sebanyak 0 lembar Khusus untuk Bank Devisa, Bendaharawan/Badan Tertentu Yang Ditunjuk dan Pertamina/Badan Usaha Selain Pertamina)
            </td>
        </tr>
        <tr>
            <td style="width: 1px">3. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>SSP yang disetor oleh Pemungut pajak sebanyak: 10 lembar (Khusus untuk Badan Usaha Industri/Eksportir Tertentu, Ditjen Bea dan Cukai)
            </td>
        </tr>
        <tr>
            <td style="width: 1px">4. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Daftar Bukti pemungutan PPh Pasal 22 (Khusus untuk Badan Usaha Industri/Eksportit Tertentu dan Ditjen Bea dan Cukai.</td>
        </tr>
        <tr>
            <td style="width: 1px">5. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Bukti Pemungutan PPh Pasal 22, (Khusus untuk Badan Usaha Industri/Eksportit Tertentu dan Ditjen Bea dan Cukai</td>
        </tr>
        <tr>
            <td style="width: 1px">6. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/checkbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Daftar rincian penjualan (dalam hal ada penjualan retur).</td>
        </tr>
        <tr>
            <td style="width: 1px">7. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Risalah lelang (dalam hal pelaksanaan lelang).</td>
        </tr>
        <tr>
            <td style="width: 1px">8. </td>
            <td style="width: 3px"><?php echo Html::img("@web/img/uncheckbox-spt.png", ['width' => '10px', 'height' => '10px']) ?></td>
            <td>Surat Kuasa Khusus</td>
        </tr>
    </table>
    
    <div class="sub-a">BAGIAN D. PERNYATAAN DAN TANDA TANGAN</div>
    <table class="table-bagian-d">
        <tr>
            <td colspan="2" rowspan="2" style="font-size: 9px; width: 570px; height: 40px">Dengan menyadari sepenuhnya akan segala akibatnya termasuk sanksi-sanksi sesuai dengan ketentuan perundang-undangan yang berlaku, saya menyatakan bahwa apa yang telah saya beritahukan di atas beserta lampiran-lampirannya adalah benar, lengkap dan jelas.</td>
            <td style="width: 150px"></td>
        </tr>
        <tr>
            <td rowspan="2" style="width: 150px">SPT Masa Diterima:</td>
        </tr>
        <tr>
            <td style="width: 100px">9A</td>
            <td style="width: 100px">9B</td>
        </tr>
        <tr>
            <td colspan="2" rowspan="2" style="width: 100px">11</td>
        </tr>
        <tr>
            <td style="width: 150px">10</td>
        </tr>
        <tr>
            <td style="height: 45px">Tanda Tangan & Cap</td>
            <td>Tanggal</td>
            <td style="width: 150px">Tanda Tangan</td>
        </tr>
    </table>
<?php } ?>
</body>
</html>
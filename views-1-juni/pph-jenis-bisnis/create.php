<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PphJenisBisnis */

$this->title = 'Create Pph Jenis Bisnis';
$this->params['breadcrumbs'][] = ['label' => 'Pph Jenis Bisnis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jenis-bisnis-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\search\PphJenisBisnisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pph Jenis Bisnis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pph-jenis-bisnis-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pph Jenis Bisnis', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'jenisBisnisId',
            'jenisBisnis',
            'userId',
            'parentId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
